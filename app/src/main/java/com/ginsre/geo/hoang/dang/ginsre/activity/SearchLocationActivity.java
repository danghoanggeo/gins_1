package com.ginsre.geo.hoang.dang.ginsre.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.SearchResultAdapter;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.model.core.geometry;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.utils.AnimationUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingLeftInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchLocationActivity extends AppCompatActivity implements View.OnClickListener{
    final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    @BindView(R.id.search_options_priority)
            LinearLayout search_options_priority;
    @BindView(R.id.search_activity_search_x)
    com.ginsre.geo.hoang.dang.ginsre.font.MaterialDesignIconsTextView search_activity_search_x;
    @BindView(R.id.search_activity_search_microfon)
    com.ginsre.geo.hoang.dang.ginsre.font.MaterialDesignIconsTextView search_activity_search_microfon;
    @BindView(R.id.search_activity_check_google)
    com.ginsre.geo.hoang.dang.ginsre.font.MaterialDesignIconsTextView search_activity_check_google;
    @BindView(R.id.search_activity_check_favorites)
    com.ginsre.geo.hoang.dang.ginsre.font.MaterialDesignIconsTextView search_activity_check_favorites;
    @BindView(R.id.search_activity_check_friends)
    com.ginsre.geo.hoang.dang.ginsre.font.MaterialDesignIconsTextView search_activity_check_friends;
    @BindView(R.id.search_activity_check_gins)
    com.ginsre.geo.hoang.dang.ginsre.font.MaterialDesignIconsTextView search_activity_check_gins;
    @BindView(R.id.search_activity_search_field)
            EditText search_activity_search_field;
    @BindView(R.id.listview_searchResult)
            DynamicListView listview_searchResult;
    @BindView(R.id.search_activity_back)
            ImageView search_activity_back;
    @BindView(R.id.search_activity_result_home)
            RelativeLayout search_activity_result_home;
    @BindView(R.id.search_activity_result_work)
            RelativeLayout search_activity_result_work;
    ArrayList<Friends> listMyFriend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);
        ButterKnife.bind(this);
        init();
    }

    private void addControl() {
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View view = inflater.inflate(R.layout.activity_search_location,null,false);

        /*search_options_priority =(LinearLayout)findViewById(R.id.search_options_priority);
        search_activity_search_x = (MaterialDesignIconsTextView)findViewById(R.id.search_activity_search_x);
        search_activity_search_microfon = (MaterialDesignIconsTextView)findViewById(R.id.search_activity_search_microfon);
        search_activity_search_field = (EditText)findViewById(R.id.search_activity_search_field);
        listview_searchResult = (DynamicListView)findViewById(R.id.listview_searchResult);
        search_activity_back = (ImageView)findViewById(R.id.search_activity_back);*/
    }

    private void init(){
        search_activity_back.setOnClickListener(this);
        search_activity_result_home.setOnClickListener(this);
        search_activity_result_work.setOnClickListener(this);
        search_activity_check_friends.setOnClickListener(this);
        search_activity_check_google.setOnClickListener(this);
        search_activity_check_favorites.setOnClickListener(this);
        search_activity_check_gins.setOnClickListener(this);
        search_activity_search_microfon.setOnClickListener(this);

        search_activity_search_x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(search_activity_search_x.getText().equals(R.string.fontello_x_mark_masked))
                {
                    listview_searchResult.setAdapter(null);
                    search_activity_search_x.setText(R.string.fontello_x_mark);
                    showOrHideOptionSearchWith(1f,0f, View.VISIBLE);
                }
            }
        });
        search_activity_search_field.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @SuppressLint("DefaultLocale")
            @Override
            public void afterTextChanged(Editable editable) {
                String searchText = editable.toString().trim();
                ArrayList<String> searchedArray = new ArrayList<String>();
                /*for (DummyModel dm : searchableArrayList) {
                    if (dm.getText().toLowerCase()
                            .contains(searchText.toLowerCase())) {
                        searchedArray.add(dm.getText());
                    }
                }*/
                if (searchText.isEmpty()) {
                    listview_searchResult.setAdapter(null);
                    search_activity_search_x.setText(R.string.fontello_x_mark);
                    showOrHideOptionSearchWith(1f,0f, View.VISIBLE);
                } else {
                    /*mListView.setAdapter(new SearchAdapter(
                            SearchBarShopActivity.this, searchedArray));*/
                    if(search_activity_check_friends.getText() == getString(R.string.material_icon_check_full)) {
                        listMyFriend = new ArrayList<>();
                        showOrHideOptionSearchWith(0f, 1f, View.GONE);
                        showFriendResult(searchText);
                        search_activity_search_x.setText(R.string.fontello_x_mark_masked);
                    }
                }
            }
        });
    }

    private void showOrHideOptionSearchWith(Float from, float to,final int action) {
        Animation animation = AnimationUtils.buildAlphaAnimation(from,to,200);
        search_options_priority.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                search_options_priority.setVisibility(action);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private void showFriendResult(String searchText)
    {
        com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB friendDB = com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB.getInstance(getApplicationContext());
        listMyFriend = (ArrayList<Friends>) friendDB.searchFriendsByName(searchText);
        //listMyFriend.addAll( listMyFriends.values());
        SearchResultAdapter searchResultAdapter = new SearchResultAdapter(listMyFriend,this);
        //friendsNet_MyFriendsRCView.setAdapter(myFriendAdapter);
        appearanceAnimate(new Random().nextInt(5),searchResultAdapter);
    }


    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (view.getId())
        {
            case R.id.search_activity_back:
            {
                finish();
                break;
            }
            case R.id.search_activity_check_google:
            {
                if (search_activity_check_google.getText() == getString(R.string.material_icon_check_empty)) {
                    setOptionSearchWithEmpty();
                    //showOrHideOptionSearchWith(0f, 1f, View.GONE);
                    findPlaceWithGoogle();
                    search_activity_check_google.setText(getString(R.string.material_icon_check_full));
                } else {
                    search_activity_check_google.setText(getString(R.string.material_icon_check_empty));
                }
                break;
            }
            case R.id.search_activity_check_favorites:
            {
                if (search_activity_check_favorites.getText() == getString(R.string.material_icon_check_empty)) {
                    setOptionSearchWithEmpty();
                    search_activity_check_favorites.setText(getString(R.string.material_icon_check_full));
                } else {
                    search_activity_check_favorites.setText(getString(R.string.material_icon_check_empty));
                }
                break;
            }
            case R.id.search_activity_check_friends:
            {
                if (search_activity_check_friends.getText() == getString(R.string.material_icon_check_empty)) {
                    setOptionSearchWithEmpty();
                    search_activity_check_friends.setText(getString(R.string.material_icon_check_full));
                } else {
                    search_activity_check_friends.setText(getString(R.string.material_icon_check_empty));
                }
                break;
            }
            case R.id.search_activity_check_gins:
            {
                if (search_activity_check_gins.getText() == getString(R.string.material_icon_check_empty)) {
                    setOptionSearchWithEmpty();
                    search_activity_check_gins.setText(getString(R.string.material_icon_check_full));
                } else {
                    search_activity_check_gins.setText(getString(R.string.material_icon_check_empty));
                }
                break;
            }
            case R.id.search_activity_result_home:
            {
                getDirectionToUserPlace("Home",ConstantUtils.USER_REF_USER_PLACES);
                break;
            }
            case R.id.search_activity_result_work:
            {
                getDirectionToUserPlace("Work",ConstantUtils.USER_REF_USER_PLACES_WORK);
                break;
            }
            case R.id.search_activity_search_microfon:
            {
                controlByVoice();
                break;
            }
        }
    }

    private void controlByVoice(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getResources().getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, ConstantUtils.REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Receiving speech input
     * */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ConstantUtils.REQ_CODE_SPEECH_INPUT: {

                if (resultCode == Activity.RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    //header_txtSearch.setText(result.get(0));
                    if(search_activity_check_google.getText() == getString(R.string.material_icon_check_full)){
                        //findPlaceWithGoogle();
                    }else if(search_activity_check_favorites.getText() == getString(R.string.material_icon_check_full)){
                            //TODO:
                    }else if(search_activity_check_friends.getText() == getString(R.string.material_icon_check_full)){
                        showFriendResult(result.get(0));
                        search_activity_search_field.setText(result.get(0));
                    }else if(search_activity_check_gins.getText() == getString(R.string.material_icon_check_full)){

                    }else{
                        if(result.get(0).equalsIgnoreCase("Go home")|| result.get(0).equals("Come back home"))
                        {
                            //TODO: get location home here;
                            getDirectionToUserPlace(result.get(0),ConstantUtils.USER_REF_USER_PLACES);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.Cannot_Recognize_voice),Toast.LENGTH_LONG).show();
                        }
                    }
                }

                break;
            }
            case  PLACE_AUTOCOMPLETE_REQUEST_CODE: {
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    HashMap<String,Object> hashMap = new HashMap<>();
                    hashMap.put(ConstantUtils.USER_REF_USER_PLACES_HOME,place.getLatLng());
                    DataChangeNotification.getInstance().notifyChange(IssueKey.DIRECTTION_TO_ADDRESS, hashMap);
                    finish();
                    //Toast.makeText(getApplicationContext(), place.getName(),Toast.LENGTH_LONG).show();
                    //Log.i(TAG, "Place: " + place.getName());
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    // TODO: Handle the error.
                    //Log.i(TAG, status.getStatusMessage());

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }
            }
        }

    }

    public void findPlaceWithGoogle() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    private void getDirectionToUserPlace(final String result,final String ref) {
        try {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ref).child(mAuth.getUid())
                    .child(ConstantUtils.USER_REF_USER_PLACES_HOME).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot!=null) {
                        for(DataSnapshot data : dataSnapshot.getChildren()){
                            try {
                                if((Boolean) data.child("stay").getValue()){
                                    ArrayList<Double> llon = (ArrayList<Double>)data.child("l").getValue();
                                    LatLng latlong = new LatLng( llon.get(0)
                                            , llon.get(1));
                                    search_activity_search_field.setText(result);
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.Comeback_To_Home), Toast.LENGTH_LONG).show();
                                    HashMap<String,Object> hashMap = new HashMap<>();
                                    hashMap.put(ConstantUtils.USER_REF_USER_PLACES_HOME,latlong);
                                    DataChangeNotification.getInstance().notifyChange(IssueKey.DIRECTTION_TO_ADDRESS, hashMap);
                                    finish();
                                    break;
                                }
                            }catch (Exception e) {
                                FirebaseCrash.log(e.getMessage());
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.yourHomeAddressIsNotUpdate), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.Canoot_find_your_home),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }
    private void setOptionSearchWithEmpty()
    {
        search_activity_check_google.setText(getString(R.string.material_icon_check_empty));
        search_activity_check_favorites.setText(getString(R.string.material_icon_check_empty));
        search_activity_check_friends.setText(getString(R.string.material_icon_check_empty));
        search_activity_check_gins.setText(getString(R.string.material_icon_check_empty));
    }
    private void appearanceAnimate(int key, SearchResultAdapter myFriendAdapter) {

        AnimationAdapter animAdapter;
        switch (key) {
            default:
            case 0:
                animAdapter = new AlphaInAnimationAdapter(myFriendAdapter);
                break;
            case 1:
                animAdapter = new ScaleInAnimationAdapter(myFriendAdapter);
                break;
            case 2:
                animAdapter = new SwingBottomInAnimationAdapter(myFriendAdapter);
                break;
            case 3:
                animAdapter = new SwingLeftInAnimationAdapter(myFriendAdapter);
                break;
            case 4:
                animAdapter = new SwingRightInAnimationAdapter(myFriendAdapter);
                break;
        }
        animAdapter.setAbsListView(listview_searchResult);
        listview_searchResult.setAdapter(animAdapter);
    }
}
