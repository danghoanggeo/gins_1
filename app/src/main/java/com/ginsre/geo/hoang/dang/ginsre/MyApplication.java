package com.ginsre.geo.hoang.dang.ginsre;

import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by hoang on 11/23/2017.
 */

public class MyApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
