package com.ginsre.geo.hoang.dang.ginsre.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.fragment.LocationFragment;
import com.ginsre.geo.hoang.dang.ginsre.fragment.LosingFragment;
import com.ginsre.geo.hoang.dang.ginsre.fragment.PostDetailFragment;
import com.ginsre.geo.hoang.dang.ginsre.fragment.SocialFragment;
import com.ginsre.geo.hoang.dang.ginsre.fragment.WeatherFragment;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoLocation;
import com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHashQuery;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostDetail;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.view.PagerSlidingTabStrip;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ViewPostDetailActivity extends AppCompatActivity implements OnDataChangeObserver {

    private ViewPostDetailActivity.MyPagerAdapter adapter;
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private ImageView imv_header_back;

    com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.properties properties;
    List<PostDetail> lstPosDetail;
    android.support.v7.widget.RecyclerView fragment_posted_card_recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post_detail);
        addControl();
        addEvent();
    }



    private void addControl() {

        DataChangeNotification.getInstance().addObserver(IssueKey.CLOSE_POST_DETAIL, this);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.activity_view_post_tabs);
        pager = (ViewPager) findViewById(R.id.activity_view_post_pager);
        imv_header_back = (ImageView)findViewById(R.id.imv_header_back);
        fragment_posted_card_recyclerView = (android.support.v7.widget.RecyclerView)findViewById(R.id.fragment_posted_card_recyclerView);
        initFirstData();
    }


    private void initFirstData() {
        try {
            MobileAds.initialize(this, "ca-app-pub-7976758780471871~3195084766");
            AdView mAdView= findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice(
                    AdRequest.DEVICE_ID_EMULATOR).build();
            mAdView.loadAd(adRequest);
            Intent intent = getIntent();
            String dateTime = intent.getStringExtra(ConstantUtils.GINS_VIEW_POSTDETAIL_DATE);
            GeoLocation geoLocation = new GeoLocation(intent.getDoubleExtra(ConstantUtils.GINS_VIEW_POSTDETAIL_LAT,0),intent.getDoubleExtra(ConstantUtils.GINS_VIEW_POSTDETAIL_LONG,0));
            intiTabPage(geoLocation,intent.getStringExtra(ConstantUtils.GINS_VIEW_POSTDETAIL_TYPE),dateTime);
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void intiTabPage(GeoLocation geoLocation,String ginsType,String date){
        adapter = new ViewPostDetailActivity.MyPagerAdapter(getSupportFragmentManager(),geoLocation,date);
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
        final int pageMargin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 3, getResources()
                        .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[0])){
            pager.setCurrentItem(0);
        }else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[1])){
            pager.setCurrentItem(1);
        }else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[2])){
            pager.setCurrentItem(2);
        }else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[4])){
            pager.setCurrentItem(3);
        }else{
            pager.setCurrentItem(4);
        }


        tabs.setOnTabReselectedListener(new PagerSlidingTabStrip.OnTabReselectedListener() {
            @Override
            public void onTabReselected(int position) {
                // Toast.makeText(LoactionPageActivity.this, "Tab reselected: " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addEvent() {
        imv_header_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataChangeNotification.getInstance().notifyChange(IssueKey.CLOSE_POST_COMMENT,"closeComment");
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        DataChangeNotification.getInstance().notifyChange(IssueKey.CLOSE_POST_COMMENT,"closeComment");
    }

    @Override
    public void onDataChanged(IssueKey key, Object object) {
        if(key.equals(IssueKey.CLOSE_POST_DETAIL)){
            finish();
        }
    }

    /**
     *
     */
    public class MyPagerAdapter extends FragmentPagerAdapter {
        GeoLocation geoLocation;
        String dateTime;
        private final ArrayList<String> tabNames = new ArrayList<String>() {
            {
                add(getResources().getString(R.string.app_name));
                add(getResources().getString(R.string.traffic));
                add(getResources().getString(R.string.location));
            }
        };

        public MyPagerAdapter(FragmentManager fm,GeoLocation mgeoLocation,String mdateTime) {
            super(fm);
            geoLocation = mgeoLocation;
            dateTime = mdateTime;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabNames.get(position);
        }

        @Override
        public int getCount() {
            return tabNames.size();
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return PostDetailFragment.newInstance(getApplicationContext()
                        ,geoLocation,ConstantUtils.GINS_SOCIAL_LOCATION.replace("/",""),dateTime);
            } else if(position == 1) {
                return PostDetailFragment.newInstance(getApplicationContext(),geoLocation,ConstantUtils.GINS_TRAFFIC_LOCATION.replace("/",""),dateTime);
            }else {
                return LocationFragment.newInstance(getApplicationContext(),geoLocation,ConstantUtils.GINS_PLACES_LOCATION.replace("/",""));
            }
        }
    }
}
