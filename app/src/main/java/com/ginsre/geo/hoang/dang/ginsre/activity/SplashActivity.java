package com.ginsre.geo.hoang.dang.ginsre.activity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.common.SharedPrefenceKey;
import com.ginsre.geo.hoang.dang.ginsre.utils.PreferencesUtils;
import com.ginsre.geo.hoang.dang.ginsre.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {

    //private com.ginsre.geo.hoang.dang.ginsre.view.kbv.KenBurnsView mKenBurns;
    private ImageView mLogo,ken_burns_images;
    private TextView welcomeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar
//        getSupportActionBar().hide();

        setContentView(R.layout.activity_splash);
        PreferencesUtils.init(getApplicationContext());

    }


    @Override
    protected void onResume() {
        super.onResume();
        //testlogin();
        //gotToNext();
        if(!PreferencesUtils.getBoolean(SharedPrefenceKey.FIRST_TIME_LOAD_APP.name(), false)){
            gotToNext();
        }else {
            //TODO: den dung view cuoi cung ma user thoat
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            //finish();
        }
    }

    /**
     *
     */
    public void gotToNext(){
        //TODO: delay 2s to welcome user in the first time.
        //initDataTrafficType();
        //printKeyHash(this);
        PreferencesUtils
                .edit()
                .putBoolean(SharedPrefenceKey.FIRST_TIME_LOAD_APP.name(), true)
                .commit();
        ken_burns_images = (ImageView) findViewById(R.id.ken_burns_images);
        mLogo = (ImageView) findViewById(R.id.logo);
        welcomeText = (TextView) findViewById(R.id.welcome_text);
        setAnimation();
    }

    /**
     *
     * @param context
     * @return
     */
    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (android.content.pm.Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    /**
     *
     */
    private void testlogin() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signInWithEmailAndPassword("dang.hoang.geo@gmail.com", "")
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        } else {
                            // If sign in fails, display a message to the user.
                        }
                    }
                });
    }

    /** Animation depends on category.
     * */
    private void setAnimation() {
           // mKenBurns.setImageResource(R.drawable.gins_welcome);
            animation1();
            animation2();
            animation3();
    }

    private void animation1() {
        Animation animation = com.ginsre.geo.hoang.dang.ginsre.utils.AnimationUtils.buildScaleAnimation(0.0f,1.0f,0.0f,1.0f,2000);
        ken_burns_images.setAnimation(animation);
    }

    private void animation2() {
        mLogo.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.translate_top_to_center);
        mLogo.startAnimation(anim);
    }

    private void animation3() {
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(welcomeText, "alpha", 0.0F, 1.0F);
        alphaAnimation.setStartDelay(1000);
        alphaAnimation.setDuration(2000);
        alphaAnimation.start();
        alphaAnimation.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startActivity(new Intent(SplashActivity.this,LoginActivity.class));
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }


}
