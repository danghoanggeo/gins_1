package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.activity.LoactionPageActivity;
import com.ginsre.geo.hoang.dang.ginsre.activity.MainActivity;
import com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.LocationPlaces;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 12/21/2017.
 */

public class LocationViewAdapter extends RecyclerView.Adapter<LocationViewAdapter.LocationViewHolder> {

    private List<LocationPlaces> listPlaceDetail;
    private Context context;
    public LocationViewAdapter(List<LocationPlaces> listPlaceDetail, Context mcontext)
    {
        this.listPlaceDetail = listPlaceDetail;
        context = mcontext;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_places_cardview,parent,false);
        LocationViewAdapter.LocationViewHolder locationViewHolder = new LocationViewAdapter.LocationViewHolder(view);
        return locationViewHolder;
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {

        try {
            holder.item_places_cardview_name.setText(listPlaceDetail.get(position).getLocation_name());
            holder.item_places_cardview_name.setTag(listPlaceDetail.get(position).getLocation_id());
            holder.item_places_cardview_description.setText(listPlaceDetail.get(position).getLoaction_description());
            if(listPlaceDetail.get(position).getBackGroundUrl() != "") {
                holder.item_places_cardview_image_cover.setVisibility(View.VISIBLE);
                ImageUtil.displayImage(holder.item_places_cardview_image_cover, listPlaceDetail.get(position).getBackGroundUrl(), null);
            }
        }catch (Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return listPlaceDetail.size();
    }

    public class LocationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.item_places_cardview_name)
        TextView item_places_cardview_name;
        @BindView(R.id.item_places_cardview_rating)
        RatingBar item_places_cardview_rating;
        @BindView(R.id.item_places_cardview_num_review)
        TextView item_places_cardview_num_review;
        @BindView(R.id.item_places_cardview_description)
        TextView item_places_cardview_description;
        @BindView(R.id.item_places_cardview_image_cover)
        ImageView item_places_cardview_image_cover;

        @BindView(R.id.item_places_cardview_num_like)
        FancyButton item_places_cardview_num_like;
        @BindView(R.id.item_places_cardview_num_love)
        FancyButton item_places_cardview_num_love;
        @BindView(R.id.item_places_cardview_fbtn_review)
        FancyButton item_places_cardview_fbtn_review;

        @BindView(R.id.item_places_cardview_fbtn_like)
        FancyButton item_places_cardview_fbtn_like;
        @BindView(R.id.item_places_cardview_fbtn_love)
        FancyButton item_places_cardview_fbtn_love;
        @BindView(R.id.item_places_cardview_fbtn_share)
        FancyButton item_places_cardview_fbtn_share;

        LocationViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);
            setEvent();
        }

        public void setEvent(){
            item_places_cardview_name.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            int viewId = view.getId();
            try {
                switch (viewId)
                {
                    case R.id.item_places_cardview_name:
                    {
                        Intent intent = new Intent(context,LoactionPageActivity.class);
                        intent.putExtra(ConstantUtils.LOCATION_ID,item_places_cardview_name.getTag().toString());
                        context.startActivity(intent);
                        break;
                    }
                }
            }catch (Exception e){

            }
        }
    }
}

