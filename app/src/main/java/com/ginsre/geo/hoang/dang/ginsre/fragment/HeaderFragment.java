package com.ginsre.geo.hoang.dang.ginsre.fragment;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by hoang on 10/4/2017.
 */

public class HeaderFragment extends Activity implements View.OnClickListener {
    private ImageButton header_imvMenu;
    //private ImageButton ic_voice_search;
    private TextView header_txtSearch;
    private Activity context;

    public HeaderFragment(Activity context)
    {
        this.context = context;
        addControl();
    }

    private void addControl() {
        header_imvMenu = (ImageButton)context.findViewById(R.id.header_imvMenu);
        //ic_voice_search = (ImageButton)context.findViewById(R.id.ic_voice_search);
        header_txtSearch = (TextView) context.findViewById(R.id.header_txtSearch);
    }

    public void addEvent() {
        //ic_voice_search.setOnClickListener(this);
        header_imvMenu.setOnClickListener(this);
        header_txtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                DataChangeNotification.getInstance().notifyChange(IssueKey.HEADER_SEARCH_EDIT,v.getText().toString());
                return false;
            }
        });
    }

    private void controlByVoice(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                context.getString(R.string.speech_prompt));
        try {
            context.startActivityForResult(intent, ConstantUtils.REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(context,
                    context.getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Receiving speech input
     * */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ConstantUtils.REQ_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    header_txtSearch.setText(result.get(0));
                    actionForVoiceControl(result.get(0));
                }

                break;
            }

        }

    }
    private void actionForVoiceControl(String result) {
        DataChangeNotification.getInstance().notifyChange(IssueKey.HEADER_VOICE_CLICK,result);
        Toast.makeText(context,result,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        int controlId = v.getId();
        if(controlId == R.id.header_imvMenu)
        {
            DataChangeNotification.getInstance().notifyChange(IssueKey.HEADER_MENU_CLICK,null);
        }
        else if(controlId == R.id.header_txtSearch)
        {

        }
    }
}
