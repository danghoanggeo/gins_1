package com.ginsre.geo.hoang.dang.ginsre.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.FriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.IFriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.core.geometry;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 10/7/2017.
 */

public class MyFriendsMoreOptionFragment extends Fragment implements View.OnClickListener {
    @BindView(R.id.fragment_myfriend_fbtnDirectionTo)
    FancyButton fragment_myfriend_fbtnDirectionTo;
    @BindView(R.id.fragment_myfriend_fbtnCanGetMyAddress)
    FancyButton fragment_myfriend_fbtnCanGetMyAddress;
    @BindView(R.id.fragment_myfriend_fbtnCanSeeMyCurrentPosition)
    FancyButton fragment_myfriend_fbtnCanSeeMyCurrentPosition;

    private static Activity mcontext;
    private static  FriendShip mfriendShip;
    private static String mUserId;

    public static MyFriendsMoreOptionFragment newInstance(Activity context, FriendShip friendShip, String userID)
    {
        MyFriendsMoreOptionFragment myFriendsMoreOptionFragment = new MyFriendsMoreOptionFragment();
        mcontext = context;
        mfriendShip = friendShip;
        mUserId = userID;
        return myFriendsMoreOptionFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myfriend_setting,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initView();
        fragment_myfriend_fbtnDirectionTo.setOnClickListener(this);
        fragment_myfriend_fbtnCanGetMyAddress.setOnClickListener(this);
        fragment_myfriend_fbtnCanSeeMyCurrentPosition.setOnClickListener(this);
        super.onViewCreated(view, savedInstanceState);
    }

    private void initView() {
        canGetAddressSetYesNoToControl();
        canSeeLocationSetYesNoToControl();
    }

    private void canGetAddressSetYesNoToControl()
    {

    }
    private void canSeeLocationSetYesNoToControl()
    {
        if(mfriendShip.getSeePosi()) {
            fragment_myfriend_fbtnCanSeeMyCurrentPosition.setText(mcontext.getResources().getString(R.string.yes));
            fragment_myfriend_fbtnCanSeeMyCurrentPosition.setTextColor(mcontext.getResources().getColor(R.color.button_blue));
        }
        else {
            fragment_myfriend_fbtnCanSeeMyCurrentPosition.setText(mcontext.getResources().getString(R.string.no));
            fragment_myfriend_fbtnCanSeeMyCurrentPosition.setTextColor(mcontext.getResources().getColor(R.color.button_red));
        }
    }

    @Override
    public void onClick(View v) {
        int view = v.getId();
        if(view == R.id.fragment_myfriend_fbtnDirectionTo)
        {
            //getUserPermisionAndDirectionToFriend();
            directionToFriendAddress();
        }
        else if(view == R.id.fragment_myfriend_fbtnCanSeeMyCurrentPosition)
        {
            setUserPermissionCanSeeMyRealTimePostion();
        }
    }

    private void getUserPermisionAndDirectionToFriend() {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(mfriendShip.getId()).child(mUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    FriendShip friendShip = dataSnapshot.getValue(FriendShip.class);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(mcontext,mcontext.getResources().getString(R.string.access_to_friend_address_is_not_allow),Toast.LENGTH_LONG).show();
                }
            });
        }
        catch (Exception e)
        {
            //TODO: log
        }
    }

    private void directionToFriendAddress() {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_PLACES).child(mfriendShip.getId())
                    .child(ConstantUtils.USER_REF_USER_PLACES_HOME).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot != null)
                    {
                        for(DataSnapshot data : dataSnapshot.getChildren()){
                            try {
                                if((Boolean) data.child("properties").child("isStay").getValue()){
                                    geometry geometry = data.child("geometry").getValue(com.ginsre.geo.hoang.dang.ginsre.model.core.geometry.class);
                                    LatLng latlong = new LatLng( (geometry.getCoor()).get(1)
                                            , (geometry.getCoor()).get(0));
                                    Toast.makeText(mcontext, mcontext.getResources().getString(R.string.letGoToYourFriendHome), Toast.LENGTH_LONG).show();
                                    DataChangeNotification.getInstance().notifyChange(IssueKey.DIRECTTION_TO_ADDRESS, latlong);
                                    mcontext.finish();
                                    break;
                                }
                            }catch (Exception e) {
                                FirebaseCrash.log(e.getMessage());
                                Toast.makeText(mcontext, mcontext.getResources().getString(R.string.friend_is_not_update_home_address), Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                    else
                    {
                        Toast.makeText(mcontext,mcontext.getResources().getString(R.string.friend_is_not_update_home_address),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    FirebaseCrash.log(databaseError.getMessage());
                    DataChangeNotification.getInstance().notifyChange(IssueKey.SHOW_DIALOG_ERROR_FRIENDS, mcontext.getResources().getString(R.string.access_to_friend_address_is_not_allow));

                    //Toast.makeText(mcontext, mcontext.getResources().getString(R.string.access_to_friend_address_is_not_allow), Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
        }

    }

    private void setUserPermissionCanSeeMyRealTimePostion() {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mfriendShip.setSeePosi(!mfriendShip.getSeePosi());
            canSeeLocationSetYesNoToControl();
            mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(mUserId).child(mfriendShip.getId()).child(IFriendShip.seeRealTimePosition).setValue(mfriendShip.getSeePosi());
        }
        catch (Exception e)
        {
            //TODO: log
        }
    }
}
