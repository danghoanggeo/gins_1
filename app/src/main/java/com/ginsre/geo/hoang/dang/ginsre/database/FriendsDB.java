package com.ginsre.geo.hoang.dang.ginsre.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.IFriends;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hoang on 11/27/2017.
 */

public class FriendsDB extends SQLiteOpenHelper {

    private static final String DB_NAME = "gins.db";
    private static final int VERSION = 1;
    private static FriendsDB sInstance = null;
    private static IFriends iFriends;
    private final Context mContext;
    SQLiteDatabase db;

    public FriendsDB(Context context) {
        super(context, DB_NAME, null, VERSION);
        mContext = context;
    }

    public synchronized final static FriendsDB getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new FriendsDB(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + IFriends.NAME + " ( ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + IFriends.friendsId + " TEXT NOT NULL,"
                + IFriends.friendsName + " TEXT NOT NULL,"
                + IFriends.friendsUriAvatar + " TEXT,"
                + IFriends.friendsStatus + " TEXT,"
                + IFriends.friendsAvatar + " BLOB,"
                + IFriends.friendsNumPriority + " INTEGER NOT NULL"
                + ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + IFriends.NAME);
    }


    /**
     *
     * @param friend
     * @param status
     */
    public void insert(com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends friend,String status) {

        if(getFriendId(friend.getFriendsId()) == null){
            db = getWritableDatabase();
            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(IFriends.friendsId, friend.getFriendsId());
            values.put(IFriends.friendsName, friend.getFriendsName());
            values.put(IFriends.friendsUriAvatar,friend.getFriendsUriAvatar());
            values.put(IFriends.friendsStatus,status);
            values.put(IFriends.friendsAvatar, friend.getPictureData());
            values.put(IFriends.friendsNumPriority,friend.getFriendsNumPriority());
            db.insert(IFriends.NAME, null, values);
            closeTransaction();
        }
    }

    public void update(String friendId,String status) {
        db = getWritableDatabase();
        ContentValues values = new ContentValues();
        Friends friend = getFriendId(friendId);
        values.put(IFriends.friendsId, friend.getFriendsId());
        values.put(IFriends.friendsName, friend.getFriendsName());
        values.put(IFriends.friendsUriAvatar,friend.getFriendsUriAvatar());
        values.put(IFriends.friendsStatus,status);
        values.put(IFriends.friendsAvatar, friend.getPictureData());
        values.put(IFriends.friendsNumPriority,friend.getFriendsNumPriority());
        //String strSQL = "UPDATE "+IFriends.NAME+" "+IFriends.friendsStatus+" = "+status+" WHERE "+IFriends.friendsId+ " == "+ friendId;
        //db.execSQL(strSQL);
        db.update(IFriends.NAME, values,IFriends.friendsId+"==?",  new String[]{friendId});
        //closeTransaction();
    }

    public boolean updateFriendPriority(String friendId) {

        db = getReadableDatabase();
        db.beginTransaction();
        ContentValues values = new ContentValues();
        Friends friend = getFriendId(friendId);
        int priority = friend.getFriendsNumPriority()+1;
        values.put(IFriends.friendsNumPriority,priority);
        db.update(IFriends.NAME, values,IFriends.friendsId+"==?",  new String[]{friendId});
        closeTransaction();
        return true;
    }


    public int getNumOfFriends() {
        db = getReadableDatabase();
        Map<String,com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends> friendList = new HashMap<>();
        //db.beginTransaction();
        Cursor cursor = db.query(IFriends.NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        //closeTransaction();
        return cursor.getCount();
    }

    public Map<String,com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends> getAllFriends() {

        db = getReadableDatabase();
        Map<String,com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends> friendList = new HashMap<>();
        //db.beginTransaction();
        Cursor cursor = db.query(IFriends.NAME, null, IFriends.friendsStatus + "==?", new String[]{""+ConstantUtils.FRIENDS_MYFRIENDS}, null, null,IFriends.friendsNumPriority+" DESC");
        //cursor.moveToFirst();
        while (cursor.moveToNext()) {
            com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends friend = new com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends(
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getBlob(5),
                    cursor.getInt(6)
            );
            friendList.put(cursor.getString(1),friend);
        }
        //closeTransaction();
        return friendList;
    }

    public Map<String,com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends> getListRequest(String status) {

        db = getReadableDatabase();
        Map<String,com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends> friendList = new HashMap<>();
        //db.beginTransaction();
        Cursor cursor = db.query(IFriends.NAME, null, IFriends.friendsStatus + "==?", new String[]{""+status}, null, null, null);
        //cursor.moveToFirst();
        while (cursor.moveToNext()) {
            com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends friend = new com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends(
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getBlob(5),
                    cursor.getInt(6)
            );
            friendList.put(cursor.getString(1),friend);
        }
        //closeTransaction();
        return friendList;
    }

    public List<com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends> searchFriendsByName(String name) {

        db = getReadableDatabase();
        List<com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends> friendList = new ArrayList<>();
        //db.beginTransaction();
        Cursor cursor = db.query(IFriends.NAME, null, IFriends.friendsName + " like? AND "+IFriends.friendsStatus+" =?", new String[]{"%"+name+"%",ConstantUtils.FRIENDS_MYFRIENDS}, null, null, IFriends.friendsNumPriority+" DESC");
        //cursor.moveToFirst();
        while (cursor.moveToNext()) {
            com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends friend = new com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends(
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getBlob(5),
                    cursor.getInt(6)
            );
            friendList.add(friend);
        }
        //closeTransaction();
        return friendList;
    }

    public com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends getFriendId(String id) {

        db = getReadableDatabase();
        //db.beginTransaction();
        Cursor cursor = db.query(IFriends.NAME, null, IFriends.friendsId + " ==?", new String[]{id + ""}, null, null, null);
        //cursor.moveToFirst();
        com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends friend = null;
        while (cursor.moveToNext()) {
            friend = new com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends(
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getBlob(5),
                    cursor.getInt(6)
            );
        }
        //closeTransaction();
        return friend;
    }

    public com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends unFriends(String id) {

        db = getReadableDatabase();
        //db.beginTransaction();
        //while (cursor.moveToNext()){
        Cursor cursor = db.query(IFriends.NAME, null, IFriends.friendsId + " ==?", new String[]{id + ""}, null, null, IFriends.friendsNumPriority+" DESC");
        cursor.moveToFirst();
        com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends friend = null;
        friend = new com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends(
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getBlob(5),
                cursor.getInt(6)
        );
        //closeTransaction();
        return friend;
    }

    public boolean deleteFriend(String id) {

        db = getReadableDatabase();
        //db.beginTransaction();
        db.delete(IFriends.NAME,IFriends.friendsId + " ==?", new String[]{id + ""});
       // closeTransaction();
        return true;

    }


    private void closeTransaction()
    {
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }
}

