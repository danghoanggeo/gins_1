package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.Application;

/**
 * Created by trach on 7/11/2017.
 * Modify by Dang on 09/26/2017
 */

public class ResourceUtils {

    @SuppressWarnings("deprecation")
    public static Drawable getDrawableByName(Context context, String identify){
        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(identify, "drawable", context.getPackageName());
        return resources.getDrawable(resourceId);
    }

    public static Drawable getDrawableForMapMarkerByName(Context context, String identifier){
        identifier = identifier.replace(identifier.substring(identifier.length()-3),"");
        return getDrawableByName(context, identifier);
    }


    public static void setFont(View view){
        Typeface font = Typeface.createFromAsset(
                Application.getContext().getAssets(),
                "flower.ttf");
        if(view instanceof TextView){
            ((TextView)view).setTypeface(font);
        }else if(view instanceof Button){
            ((Button)view).setTypeface(font);
        }

    }

}
