package com.ginsre.geo.hoang.dang.ginsre.utils;


/**
 * Created by Dang Hoang 09/07/2017.
 */

public class ConstantUtils {
    public static int TIME_INTERVAL = 100;
    public static final int  REQ_CODE_SPEECH_INPUT = 999;
    // FIREBASE
    public static String GINS_DATA_STATIC = "stakhongdoitic/";
    public static String GINS_DATA_DYNAMIC = "dythaydoinamic/";
    public static String GINS_TRAFFIC_LOCATION = "tragiaothongffic/";
    public static String GINS_SOCIAL_LOCATION = "soxahoicial/";
    public static String GINS_PLACES_LOCATION = "pladiadiemce/";
    public static String GINS_MAPCHAT_LOCATION = "maptrochuyenchat/";
    public static String GINS_WEATHER_LOCATION = "weather/";
    public static String GINS_MEMORIES_LOCATION = "memkyniemmories/";
    public static String GINS_LOSING_LOCATION = "thatlosinglac/";

    public static String GEO_FIRE_REF_LOCATION = "location/geofire";
    public static String REF_LOCATION_PAGE ="locaPage";
    public static String REF_LOCATION_POST = "locaPost";
    public static String REF_LOCATION_REVIEW = "locaReview";
    public static String REF_LOCATION_IMAGES ="locaPage/Images";

    public static String GEO_FIRE_REF_TRAFFIC = "location/traffic/geofire";
    public static String GEO_FIRE_REF_USER_LOCAL = "location/userLocal/geofire";
    public static String GEO_FIRE_REF_USER_ADDRESS = "location/userAddress/geofire";
    public static String USER_TEMP_LOCATION = "usersTempLocation/";
    public static String REF_POST_MORE_INFO = "postMoreInfor/";
    public static String REF_POST_CONTENT = "postContent";
    public static String REF_POST_THANKS = "postThank";
    public static String REF_POST_LIKE = "postLike";
    public static String REF_POST_SPAM = "postSpam";
    public static String REF_POST_COMMENT = "postComment";
    public static String USER_REF_NOTIFICATION = "notification";
    public static String USER_REF_MESSAGES = "messages";
    public static String USER_REF_USER = "users";
    public static String POST_PROPERTIES = "properties";
    public static String POST_GEOMERTRY = "geometry";

    public static String USER_REF_USER_GENERAL = "user_general";
    public static String USER_REF_USER_SEARCH = "user_search";
    public static String USER_REF_USER_TOKENS = "user_tokens";
    public static String LIST_DEVICE_TOKENS = "uToken";
    public static String PREFIX_TOKENS = "-lg-";
    public static String USER_REF_USER_PLACES = "user_places";
    public static String USER_REF_USER_PLACES_WORK = "work";
    public static String USER_REF_USER_PLACES_HOME = "home";
    public static String USER_REF_USER_FRIENDS = "users/Friends";
    public static String USER_REF_USER_FRIENDS_REQUEST = "users/FriendsRequest";
    public static String USER_REF_USER_PLACES_FAVORITES = "Favorites";

    //Weather
    public static String WEATHER = "weather";
    public static String STORM = "storm_tembin";

    //Traffic now

    public static String TRAFFIC_NOW = "traffic_now";

    //LOcationID
    public static String LOCATION_ID ="Loaction_id";
    public static String POST_ID ="Post_id";
    //FIREBASE STORE
    public static String STORE_TRAFFIC_IMAGES = "userPost/traffic";
    // Permission
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 10;
    //Geofire
    public static int GEOFIRE_RADIUS_QUERY = 5;
    public static int GEOFIRE_RADIUS_NOTIFI_TRAFFIC_QUERY = 2;
    public static int QUERY_BACK_DAY = 10;
    public static int LENG_FOR_GEO_HASH = 5;
    // Map
    public static int INITIAL_ZOOM_LEVEL = 1;
    public static int ZOOM_LEVEL = 14;
    public static String CURRENT_LOCATION = "Current location";
    public static String DESTINATION = "Destination";
    public static String MAPCHAT_LOCATION = "mapchatlocation";
    public static String MEMORIES_LOCATION = "memorieslocation";
    public static String LATITUTE = "Latitute";
    public static String LONGTITUTE = "Longtitute";
    public static String DIRECT_TO_FRIENDS = "directToFriend";
    public static String DIRECT_TO_HOME = "directToHome";
    public static String DIRECT_TO_PLACES = "directToPlace";

    // TYPE OF GINS
    public static final String[] GINS_MAIN_TYPE = {"SC0","TF0","LC0","WE0","FR0"};

    // Prefix for each ginsre type.
    public static String SOCIAL_PREFIX = GINS_MAIN_TYPE[0];
    public static String TRAFFIC_PREFIX = GINS_MAIN_TYPE[1];
    public static String LOCATION_PREFIX = GINS_MAIN_TYPE[2];
    public static String WEATHER_PREFIX = GINS_MAIN_TYPE[3];
    public static String FRIEND_PREFIX = GINS_MAIN_TYPE[4];


    // Noti Friend REQUEST;
    public static String NOTIFI_FREQUEST = "FR0";
    public static String NOTIFI_FREACCEPT = "FR1";

    public static String NOTIFI_COMMENT ="CMN";
    // View display

    // View POST DETAIL
    public static String GINS_VIEW_POSTDETAIL_TYPE = "ViewDetail_type";
    public static String GINS_VIEW_POSTDETAIL_LAT = "ViewDetail_lat";
    public static String GINS_VIEW_POSTDETAIL_LONG = "ViewDetail_lon";
    public static String GINS_VIEW_POSTDETAIL_DATE = "ViewDetail_date";
    public static int LIMIT_COMMENT_GET = 20;
    public static int MAX_COMMENT_LENGTH = 800;
    public static final int MENU_CONTAINER_WIDTH_DP = 240; // dp
    // Notification
    public static final String VIEW_POST_LOCATION = "ViewPostLocation";
    public static final String NOTIFICATION_TRAFFIC_ID = "Traffic Id";

    //Friend type;

    public static final String FRIENDS_ADDNEW = "-1";
    public static final String FRIENDS_REQUEST = "0";
    public static final String FRIENDS_WAITING = "1";
    public static final String FRIENDS_MYFRIENDS = "2";
    public static final String FRIENDS_DENY = "3";

    public static final String DIALOG_SETTING = "setting";
    public static final String DIALOG_CONFIRM = "confirm";


}
