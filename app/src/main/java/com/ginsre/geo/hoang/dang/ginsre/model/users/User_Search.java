package com.ginsre.geo.hoang.dang.ginsre.model.users;

/**
 * Created by hoang on 1/1/2018.
 */

public class User_Search {
    private String uName;
    private String uAvUrl;
    private String uPhone;

    public User_Search(){}

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuAvUrl() {
        return uAvUrl;
    }

    public void setuAvUrl(String uAvUrl) {
        this.uAvUrl = uAvUrl;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }
}
