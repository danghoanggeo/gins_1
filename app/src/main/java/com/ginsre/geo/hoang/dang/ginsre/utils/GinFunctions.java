package com.ginsre.geo.hoang.dang.ginsre.utils;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.common.SharedPrefenceKey;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoFire;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoLocation;
import com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHash;
import com.ginsre.geo.hoang.dang.ginsre.model.users.User_Search;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Users;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by hoang on 12/1/2017.
 */

public class GinFunctions {
    private final String LOG_TAG = "Gins functions: ";
    private static final String BASE32_CHARS = "0-1-2-3-4-5-6-7-8-9-b-c-d-e-f-g-h-j-k-m-n-p-q-r-s-t-u-v-w-x-y-z";
    /**
     *
     * @param latLng Coordinate of point
     * @param dateTime Date time
     * @return String
     */
    public static String getPrefixReferencesToData(com.google.android.gms.maps.model.LatLng latLng, String dateTime)
    {
        String result = "data/";
        result += MapUtils.getZonesByLocation(latLng)+"/"+dateTime+"/";
        return result;
    }

    /**
     *
     * @param context
     * @return
     */
    public static int[] getDeviceWidthHeight(Activity context){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int result[] = new int[2];
        result[0] =  displayMetrics.widthPixels;
        result[1] = displayMetrics.heightPixels;
        return result;
    }

    public static String getCurrentUserId(){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        return mAuth.getUid();
    }

    public static com.ginsre.geo.hoang.dang.ginsre.model.users.Users getCurrentUserFromAuthentication(){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        try {
            com.ginsre.geo.hoang.dang.ginsre.model.users.Users users = new com.ginsre.geo.hoang.dang.ginsre.model.users.Users();

            if(mAuth.getCurrentUser().getDisplayName()!=null)
                users.setuName(mAuth.getCurrentUser().getDisplayName());
            if(mAuth.getCurrentUser().getPhotoUrl()!=null)
                users.setuAvUrl(mAuth.getCurrentUser().getPhotoUrl().toString());
            if(mAuth.getCurrentUser().getEmail()!=null)
                users.setuEmail(mAuth.getCurrentUser().getEmail());
            if(mAuth.getCurrentUser().getPhoneNumber()!=null)
                users.setuPhone(mAuth.getCurrentUser().getPhoneNumber());
            return users;
        }catch (Exception e){
            return null;
        }
    }

    public static Locale getCurrentLocale(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return context.getResources().getConfiguration().getLocales().get(0);
        } else{
            //noinspection deprecation
            return context.getResources().getConfiguration().locale;
        }
    }

    /**
     *
     * @param context Activity context
     * @param notificationKey have 4 characters for example TF01
     * @return context of notification
     */

    public static String getNotificationContextBaseOnPrefix(android.content.Context context, String notificationKey){
        try {
            if(notificationKey.split("-").length >2) {
                String prefix = notificationKey.split("-")[1];
                String ginsType = prefix.substring(0, 3);
                int index = Integer.parseInt(prefix.substring(3, 4));

                List<String> postName = getListSubPostName(context, ginsType);

                if (ginsType.equals(ConstantUtils.SOCIAL_PREFIX)) {

                } else if (ginsType.equals(ConstantUtils.TRAFFIC_PREFIX)) {
                    String distace = notificationKey.split(">")[1];
                    if (distace.length() > 4) {
                        distace = distace.replace("<", ".").substring(0, 4);   // "0<234"
                    }
                    return context.getResources().getString(R.string.traffic_noti_prefix).replace("xx", postName.get(index))
                            + "\n" + context.getResources().getString(R.string.traffic_noti_distance).replace("xx", distace);
                } else if (ginsType.equals(ConstantUtils.LOCATION_PREFIX)) {

                } else if (ginsType.equals(ConstantUtils.FRIEND_PREFIX)) {

                } else if (ginsType.equals(ConstantUtils.WEATHER_PREFIX)) {

                }
            }
            else{
                if(notificationKey.split("-")[1].equalsIgnoreCase(ConstantUtils.NOTIFI_FREQUEST)){
                    return context.getResources().getString(R.string.friend_request);
                }else if(notificationKey.split("-")[1].equalsIgnoreCase(ConstantUtils.NOTIFI_FREACCEPT)){
                    return context.getResources().getString(R.string.friend_accept);
                }
            }
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
        return "";
    }

    /**
     *
     * @param ginsType
     * @return
     */
    public static String getDatabaseRefForGinsType(String ginsType){
        //String result = "";
        if(ginsType.length()==4){
            ginsType = ginsType.substring(0,3);
        }
        if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[0]))
        {
            return ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.GINS_SOCIAL_LOCATION;
        }
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[1]))
        {
            return ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.GINS_TRAFFIC_LOCATION;
        }
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[2]))
        {
            return ConstantUtils.GINS_DATA_STATIC+ConstantUtils.GINS_PLACES_LOCATION;
        }
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[3]))
        {
            return ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.GINS_WEATHER_LOCATION;
        }
        else //if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[4]))
        {
            return ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.USER_TEMP_LOCATION;
        }
        //return result;
    }

    /**
     *
     * @param ginsType
     * @return
     */
    public static String getDatabaseRefForPostDetail(String ginsType){
        String result = "";
        if(ginsType.length()==4){
            ginsType = ginsType.substring(0,3);
        }
        if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[0]))
            result = ConstantUtils.GINS_DATA_DYNAMIC + ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_SOCIAL_LOCATION;
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[1]))
            result = ConstantUtils.GINS_DATA_DYNAMIC + ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_TRAFFIC_LOCATION;
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[2]))
            result = ConstantUtils.GINS_DATA_DYNAMIC + ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_PLACES_LOCATION;
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[3]))
            result = ConstantUtils.GINS_DATA_DYNAMIC + ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_WEATHER_LOCATION;
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[4]))
            result = ConstantUtils.GINS_DATA_DYNAMIC + ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_LOSING_LOCATION;

            return result;
    }

    /**
     *
     * @param ginsType
     * @return
     */
    public static String getFireStoreDBRefForPostDetail(String ginsType){
        String result = "";
        if(ginsType.length()==4){
            ginsType = ginsType.substring(0,3);
        }
        if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[0]))
            result =ConstantUtils.GINS_SOCIAL_LOCATION;
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[1]))
            result = ConstantUtils.GINS_TRAFFIC_LOCATION;
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[2]))
            result = ConstantUtils.GINS_PLACES_LOCATION;
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[3]))
            result = ConstantUtils.GINS_WEATHER_LOCATION;
        else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[4]))
            result = ConstantUtils.GINS_LOSING_LOCATION;

        return result.replace("/","");
    }

    /**
     *
     * @param subType
     * @return
     */
    public static List<String> getListPostIconMarkerName(String subType)
    {
        if(subType.length()==4){
            subType = subType.substring(0,2);
        }
        List<String> lstImageIndentifers = new ArrayList<>();
        if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[0]))// social
        {
            lstImageIndentifers.add("festival_72");
            //lstImageIndentifers.add("event_72");
            lstImageIndentifers.add("education_72");
            //lstImageIndentifers.add("social_72");
            lstImageIndentifers.add("health_72");
            lstImageIndentifers.add("theft_72");
            //lstImageIndentifers.add("entertainment_72");
            lstImageIndentifers.add("fires_72");
            lstImageIndentifers.add("items_72");
            lstImageIndentifers.add("otherlos_72");
            if(DateUtils.getDateForVNStorm())
            {
                lstImageIndentifers.add("vn_48");
            }
        }
        else if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[1])) // traffic
        {
            lstImageIndentifers.add("traffic_72");
            lstImageIndentifers.add("speed_limit_72");
            lstImageIndentifers.add("traffic_attentions_72");
            lstImageIndentifers.add("car_accident_72");
            lstImageIndentifers.add("road_worker_72");
            lstImageIndentifers.add("flood_72");
            if(DateUtils.getDateForVNStorm())
            {
                lstImageIndentifers.add("vn_48");
            }

        }
        else if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[2]))// location
        {
            lstImageIndentifers.add("church_72");
            lstImageIndentifers.add("temple_72");
            lstImageIndentifers.add("hospital_72");
            lstImageIndentifers.add("travel_place_72");
            lstImageIndentifers.add("restaurant_72");
            lstImageIndentifers.add("coffe_shop_72");
            lstImageIndentifers.add("shopping_72");
            lstImageIndentifers.add("car_service_72");
            lstImageIndentifers.add("bike_service_72");
        }
        else if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[3])) // weather
        {
            //lstImageIndentifers.add("group_chat_72");
            lstImageIndentifers.add("storm_48");
            lstImageIndentifers.add("wether_flood_48");
            //lstImageIndentifers.add("boy_chat_72");
            //lstImageIndentifers.add("girl_chat_72");
        }
        else if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[4]))// mymemories
        {
            //lstImageIndentifers.add("my_love_place_72");
            //lstImageIndentifers.add("my_memories_72");
            lstImageIndentifers.add("pet_72");
            lstImageIndentifers.add("items_72");
            lstImageIndentifers.add("idcard_72");
            lstImageIndentifers.add("otherlos_72");
        }
        return lstImageIndentifers;
    }

    public static String getGeoHashForPostQuery(int lenght, GeoLocation geoLocation)
    {
       return new GeoHash(geoLocation).getGeoHashString().substring(0,lenght);
    }

    /**
     *
     * @param context
     * @param subType have length = 3;
     * @return
     */

    public static List<String> getListSubPostName(Context context, String subType){
        List<String> lstImageIndentifers = new ArrayList<>();
        if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[0]))// social
        {
            return Arrays.asList(context.getResources().getStringArray(R.array.lstSocialType));
        }
        else if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[1])) // traffic
        {
            return Arrays.asList(context.getResources().getStringArray(R.array.lstTrafficType));
        }
        else if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[2]))// location
        {
            return Arrays.asList(context.getResources().getStringArray(R.array.lstLocation));
        }
        else if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[3])) // map chat
        {
            return Arrays.asList(context.getResources().getStringArray(R.array.lstWeather));
        }
        else if(subType.equals(ConstantUtils.GINS_MAIN_TYPE[4]))// losing
        {
            return Arrays.asList(context.getResources().getStringArray(R.array.lstMyLosing));
        }
        else{
            return lstImageIndentifers;
        }
    }

    /**
     *
     * @param bitmapResource
     * @param number
     * @return
     */
    public static Bitmap drawNewImageWithCycleNumber(Bitmap bitmapResource,int number) {
        if(number==0){
            return bitmapResource;
        }else{
            float Cycelwidth = (float)bitmapResource.getWidth()/4;
            float Cycelheight = (float)bitmapResource.getHeight()/4;
            Bitmap alteredBitmap = Bitmap.createBitmap(bitmapResource.getWidth()+(int)(2*Cycelwidth), bitmapResource
                    .getHeight()+(int)Cycelheight, bitmapResource.getConfig());
            Canvas canvas = new Canvas(alteredBitmap);
            Paint paint = new Paint();
            canvas.drawBitmap(bitmapResource,0.0f,Cycelheight, paint);
            paint.setColor(Color.RED);
            canvas.drawCircle(4*Cycelwidth, Cycelheight+Cycelheight/4 , Cycelwidth+Cycelwidth/4, paint);
            paint.setColor(Color.WHITE);
            paint.setTextSize(38f);
            paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
            if(number>10){
                canvas.drawText(String.valueOf(number),3*Cycelwidth+Cycelwidth/4, Cycelheight+Cycelheight,paint);
            }else{
                canvas.drawText(String.valueOf(number),3*Cycelwidth+Cycelwidth/3, Cycelheight+Cycelheight,paint);
            }

            return alteredBitmap;
        }
    }

    public static Drawable drawUserName(Context context,Bitmap bitmapRe, String userName,int dimen){
        //Bitmap bitmapRe = BitmapFactory.decodeResource(context.getResources(), R.drawable.user_location);
        Bitmap alteredBitmap = Bitmap.createBitmap(dimen,dimen,bitmapRe.getConfig());
        float height = (float)bitmapRe.getHeight();
        Canvas canvas = new Canvas(alteredBitmap);
        Paint paint = new Paint();
        canvas.drawBitmap(bitmapRe,0.0f,0.0f, paint);
        paint.setColor(context.getResources().getColor(R.color.material_blue_500));
        paint.setTextSize(48f);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        canvas.drawText(userName,0.2f, height+height/4,paint);
        return new BitmapDrawable(context.getResources(), alteredBitmap);
    }

    public static Drawable drawUserName(Context context,Drawable drawable, String userName,int dimen) {
        Bitmap bitmap = null;
        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Bitmap alteredBitmap = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),bitmap.getConfig());
        Canvas canvas = new Canvas(alteredBitmap);
        Paint paint = new Paint();
        canvas.drawBitmap(bitmap,0.0f,0.0f, paint);
        float height = (float)bitmap.getHeight();
        paint.setColor(context.getResources().getColor(R.color.material_blue_500));
        paint.setTextSize(32f);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        canvas.drawText(userName,height/4, height+height/4,paint);
        return new BitmapDrawable(context.getResources(), alteredBitmap);
    }

    // To save image to database
    public static byte[] drawableToByteArray(Drawable d) {

        if (d != null) {
            Bitmap imageBitmap = ((BitmapDrawable) d).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteData = baos.toByteArray();

            return byteData;
        } else
            return null;

    }

    // To save image to database
    public static byte[] bitmapToByteArray(Bitmap bitmap, int quality) {
        if(quality>60){
            quality = 60;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // Compress the bitmap to jpeg format and quality% image quality
        bitmap.compress(Bitmap.CompressFormat.JPEG,quality, stream);
        byte[] byteArray = stream.toByteArray();
        return stream.toByteArray();
    }


    // EnCode number to String

    /**
     *
     * @param number
     * @return string of number
     */
    public static String endCodeNumberToString(int number){
        if(number>0&&number<=32)
        {
            String listString[] = BASE32_CHARS.split("-");
            return listString[number-1];
        }else{
            return null;
        }
    }

    /**
     *
     * @param dateTime in format yy/MM/dd/HH
     * @return
     */
    public static String getDateTimeCode(String dateTime){
        String result = "";
        for(int i = 0;i<dateTime.split("/").length;i++){
            result += endCodeNumberToString(Integer.parseInt(dateTime.split("/")[i]));
        }
        return result;
    }

    public static String deCodeStringToNumber(String str){
        return null;
    }


    public static void setUserThankLikeSpamToFireStore(String postId,String subCollec, Map<String,Object> value )
    {
        String collection = getFireStoreDBRefForPostDetail(postId.split("-")[1]);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(collection).document(postId).collection(subCollec).document(getUserId()).update(value);
    }


    public static String getUserId(){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String userId = "";
        if(mAuth!=null)
            if(mAuth.getCurrentUser()!=null)
                userId = mAuth.getCurrentUser().getUid();
        return userId;
    }





    public static void addNewUserToFirebase(Context context,com.ginsre.geo.hoang.dang.ginsre.model.users.Users users) {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            User_Search user_search = new User_Search();
            user_search.setuName(users.getuName());
            user_search.setuAvUrl(users.getuAvUrl());
            user_search.setuPhone(users.getuPhone());
            updateNewTokensToUser(context);
            users.setnNoTi(0);
            users.setnMess(0);
            users.setStatus("1");
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(getCurrentUserId()).setValue(users);
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_SEARCH).child(getCurrentUserId()).setValue(user_search);
            //mDatabase.child(ConstantUtils.USER_REF_USER).child(user.getUid()).child("uName").setValue(ginser.getuName());

        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log("BaseActivity_507: "+e.getMessage());
        }
    }

    /**
     *
     * @param currentLocation
     */

    public static void writeUserLocationToFirebase(LatLng currentLocation,Context context) {
        try {
            DatabaseReference mref = FirebaseDatabase.getInstance().getReference(ConstantUtils.GINS_DATA_DYNAMIC + ConstantUtils.USER_TEMP_LOCATION);
            GeoFire geoFire = new GeoFire(mref);
            String userToken = PreferencesUtils.getString(SharedPrefenceKey.USER_TOKEN_ID.name(),"");
            String country = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentLocale(context).getCountry();
            if(userToken.equals("")){ // if user token have not been saved
                userToken = FirebaseInstanceId.getInstance().getToken();
                PreferencesUtils.edit().putString(SharedPrefenceKey.USER_TOKEN_ID.name(),userToken);
                updateNewTokensToUser(context);
            }
            userToken = FirebaseInstanceId.getInstance().getToken();
            if(com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId() != null)
            {
                String uId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId();
                geoFire.setLocationForUserTemp(uId,country+ConstantUtils.PREFIX_TOKENS+userToken, new GeoLocation(currentLocation.latitude, currentLocation.longitude));
            }
        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    public static void updateNewTokensToUser(Context context) {
        Map<String, String> mtokens = new HashMap<>();
        String token = FirebaseInstanceId.getInstance().getToken();
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String country = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentLocale(context).getCountry();
        mtokens.put(android_id, country+ConstantUtils.PREFIX_TOKENS+token);
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_TOKENS).child(getUserId()).setValue(mtokens);
    }


    public static String getStormLevel(int velocity){
        if(velocity<=29)
            return "<4";
        else if(velocity>29 && velocity<=38)
            return "5";
        else if(velocity>38 && velocity<=49)
            return "6";
        else if(velocity>49 && velocity<=61)
            return "7";
        else if(velocity>61 && velocity<=74)
            return "8";
        else if(velocity>74 && velocity<=88)
            return "9";
        else if(velocity>88 && velocity<=102)
            return "10";
        else if(velocity>102 && velocity<=117)
            return "11";
        else if(velocity>117 && velocity<=133)
            return "12";
        else if(velocity>134 && velocity<=149)
            return "13";
        else if(velocity>149 && velocity<=166)
            return "14";
        else if(velocity>166 && velocity<=183)
            return "15";
        else if(velocity>183 && velocity<=201)
            return "16";
        else if(velocity>201 && velocity<220)
            return "17";
        else
            return "18+";

    }
}
