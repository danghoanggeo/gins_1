package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.activity.FriendsNetActivity;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.FriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.IFriendShip;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 1/4/2018.
 */

public class DialogFriendSetting {
    private Activity activity;
    private Dialog mDialog;
    private FriendShip friendShip;
    private String typeOfDialog;
    private TextView mDialog_check_see_position,dialog_title;
    private FancyButton dialog_ok,dialog_cancel;
    private ImageView dialog_image;

    public void showDialog(Activity mactivity, FriendShip mfriendShip,String mtypeOfDialog) {
        activity = mactivity;
        friendShip = mfriendShip;
        typeOfDialog = mtypeOfDialog;
        if (mDialog == null) {
            mDialog = new Dialog(activity, R.style.CustomDialogTheme);
        }
        mDialog.setContentView(R.layout.dialog_custom);
        mDialog.setCancelable(true);
        mDialog.show();
        dialog_title = (TextView)mDialog.findViewById(R.id.dialog_title);
        dialog_image = (ImageView)mDialog.findViewById(R.id.dialog_image);
        mDialog_check_see_position = (TextView) mDialog.findViewById(R.id.dialog_check_see_position);
        dialog_ok = (FancyButton) mDialog.findViewById(R.id.dialog_ok);
        dialog_cancel = (FancyButton) mDialog.findViewById(R.id.dialog_cancel);
        if(mtypeOfDialog.equalsIgnoreCase(ConstantUtils.DIALOG_SETTING)){
            dialog_cancel.setVisibility(View.GONE);
            dialog_ok.setText(activity.getResources().getString(R.string.dialog_ok_button));
        }
        if(friendShip.getSeePosi())
            mDialog_check_see_position.setText(activity.getResources().getString(R.string.material_icon_check_full));
        else
            mDialog_check_see_position.setText(activity.getResources().getString(R.string.material_icon_check_empty));
        initDialogButtons();
    }

    private void initDialogButtons(){
        dialog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(typeOfDialog.equalsIgnoreCase(ConstantUtils.DIALOG_SETTING))
                    upDateSettingToDatatbase();
                else
                    friendAcceptToDatabase();

                mDialog.dismiss();
            }
        });

        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                friendNotAccept();
                mDialog.dismiss();
            }
        });

        mDialog_check_see_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDialog_check_see_position.getText() == activity.getResources().getString(R.string.material_icon_check_empty)) {
                    mDialog_check_see_position.setText(activity.getResources().getString(R.string.material_icon_check_full));
                } else {
                    mDialog_check_see_position.setText(activity.getResources().getString(R.string.material_icon_check_empty));
                }
            }
        });
    }

    private void friendNotAccept() {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        FriendShip friendShip1 = new FriendShip();// Save to list friend of user 1
        friendShip1.setCreated(DateUtils.getDateTimeForGeoHashQuery());
        friendShip1.setSeePosi(false);
        friendShip1.setStatus(ConstantUtils.FRIENDS_DENY);
        mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS_REQUEST).child(GinFunctions.getUserId())
                .child(friendShip.getId()).setValue(friendShip1);
    }

    private void friendAcceptToDatabase() {
        com.ginsre.geo.hoang.dang.ginsre.utils.SaveFriendToDB.saveFriendAccept(friendShip.getId(),mDialog_check_see_position.getText()==activity.getResources().getString(R.string.material_icon_check_full));
    }

    private void upDateSettingToDatatbase() {
        if((mDialog_check_see_position.getText()==activity.getResources().getString(R.string.material_icon_check_full)) && !friendShip.getSeePosi()) // Allowed
        {
            setUserPermissionCanSeeMyRealTimePostion(true);
        }else if((mDialog_check_see_position.getText()==activity.getResources().getString(R.string.material_icon_check_empty)) && friendShip.getSeePosi()) // Disallowed
        {
            setUserPermissionCanSeeMyRealTimePostion(false);
        }
    }

    private void setUserPermissionCanSeeMyRealTimePostion(Boolean seeMyRealTimePostion) {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(GinFunctions.getUserId()).child(friendShip.getId()).child(IFriendShip.seeRealTimePosition).setValue(seeMyRealTimePostion);
        }
        catch (Exception e)
        {
            //TODO: log
        }
    }
}
