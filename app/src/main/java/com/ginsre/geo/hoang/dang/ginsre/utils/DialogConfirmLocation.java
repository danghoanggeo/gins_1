package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.activity.FriendsNetActivity;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.FriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.IFriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.users.UserPlaces;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 1/4/2018.
 */

public class DialogConfirmLocation {
    private Activity activity;
    private Dialog mDialog;
    private String typeOfDialog;
    private TextView mDialog_check_see_position,dialog_title,dialog_text_content;
    private FancyButton dialog_ok,dialog_cancel;
    private ImageView dialog_image;
    private UserPlaces userPlaces;

    public void showDialog(Activity mactivity,String mtypeOfDialog,UserPlaces mUserPlaces) {
        activity = mactivity;
        typeOfDialog = mtypeOfDialog;
        userPlaces = mUserPlaces;
        if (mDialog == null) {
            mDialog = new Dialog(activity, R.style.CustomDialogTheme);
        }
        mDialog.setContentView(R.layout.dialog_custom);
        mDialog.setCancelable(true);
        mDialog.show();
        dialog_title = (TextView)mDialog.findViewById(R.id.dialog_title);
        dialog_text_content = (TextView)mDialog.findViewById(R.id.dialog_text_content);
        dialog_text_content.setText(activity.getResources().getString(R.string.menu_home_staying));
        dialog_image = (ImageView)mDialog.findViewById(R.id.dialog_image);
        dialog_image.setImageDrawable(activity.getResources().getDrawable(R.drawable.home));
        mDialog_check_see_position = (TextView) mDialog.findViewById(R.id.dialog_check_see_position);
        dialog_ok = (FancyButton) mDialog.findViewById(R.id.dialog_ok);
        dialog_cancel = (FancyButton) mDialog.findViewById(R.id.dialog_cancel);
        if(mtypeOfDialog.equalsIgnoreCase(ConstantUtils.DIALOG_SETTING)){
            dialog_cancel.setVisibility(View.GONE);
            dialog_ok.setText(activity.getResources().getString(R.string.dialog_ok_button));
        }
        if(userPlaces.getStay())
            mDialog_check_see_position.setText(activity.getResources().getString(R.string.material_icon_check_full));
        else
            mDialog_check_see_position.setText(activity.getResources().getString(R.string.material_icon_check_empty));
        initDialogButtons();
    }

    private void initDialogButtons(){
        dialog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(typeOfDialog.equalsIgnoreCase(ConstantUtils.DIALOG_SETTING))
                    saveToFirebase();
                mDialog.dismiss();
            }
        });

        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog_check_see_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDialog_check_see_position.getText() == activity.getResources().getString(R.string.material_icon_check_empty)) {
                    mDialog_check_see_position.setText(activity.getResources().getString(R.string.material_icon_check_full));
                } else {
                    mDialog_check_see_position.setText(activity.getResources().getString(R.string.material_icon_check_empty));
                }
            }
        });
    }

    private void saveToFirebase() {
        try {
            boolean flag = (mDialog_check_see_position.getText() == activity.getResources().getString(R.string.material_icon_check_full));
            if((!userPlaces.getStay() && flag) ||(userPlaces.getStay() && !flag)){
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId();
                mDatabase.child(ConstantUtils.USER_REF_USER)
                        .child(ConstantUtils.USER_REF_USER_PLACES).child(userId).child(ConstantUtils.USER_REF_USER_PLACES_HOME)
                        .child(userPlaces.getG()).child("stay").setValue(flag);
            }
        }catch (Exception e){

        }

    }


}
