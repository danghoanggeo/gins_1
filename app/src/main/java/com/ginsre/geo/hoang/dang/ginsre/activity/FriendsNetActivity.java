package com.ginsre.geo.hoang.dang.ginsre.activity;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.FriendsAdapter;
import com.ginsre.geo.hoang.dang.ginsre.adapter.FindNewFriendsAdapter;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.font.MaterialDesignIconsTextView;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.FriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.IFriends;
import com.ginsre.geo.hoang.dang.ginsre.model.users.IUser;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Users;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingLeftInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class FriendsNetActivity extends AppCompatActivity implements OnDataChangeObserver {

    public ImageView friendsNet_ibtnHome;
    public EditText friendsNet_txtSearch;
    public MaterialDesignIconsTextView friendsNet_search, friendsNet_voice_search;
    public RecyclerView friendsNet_AddNewRCView;
    //public RecyclerView friendsNet_MyFriendsRCView;
    public DynamicListView dynamic_listview;
    public RecyclerView friendsNet_RequestRCView;
    public RecyclerView friendsNet_MyWaitingFriends;
    private RelativeLayout friendsNet_myFriendMoreOption;
    private TabHost friendsNet_tabHot;
    int numOfFriendShip = 0;
    private Map<String,Friends> listMyFriends;
    private Map<String,Friends> listMyWaitingFriends;
    private Map<String,Friends> listMyRequestFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_net);

        addControl();
        addEvent();
        initData();
    }



    private void addControl() {
        // Data notification
        DataChangeNotification.getInstance().addObserver(IssueKey.SHOW_DIALOG_ERROR_FRIENDS, this);
        //
        friendsNet_ibtnHome = (ImageView) findViewById(R.id.friendsNet_ibtnHome);
        friendsNet_tabHot = (TabHost)findViewById(R.id.friendsNet_tabHot);
        friendsNet_txtSearch = (EditText)findViewById(R.id.friendsNet_txtSearch);
        friendsNet_search = (MaterialDesignIconsTextView)findViewById(R.id.friendsNet_search);
        friendsNet_voice_search = (MaterialDesignIconsTextView) findViewById(R.id.friendsNet_voice_search);
        friendsNet_AddNewRCView = (RecyclerView) findViewById(R.id.friendsNet_AddNewRCView);
        dynamic_listview = (DynamicListView)findViewById(R.id.dynamic_listview);
        //friendsNet_MyFriendsRCView = (RecyclerView)findViewById(R.id.friendsNet_MyFriendsRCView);
        friendsNet_RequestRCView = (RecyclerView)findViewById(R.id.friendsNet_RequestRCView);
        friendsNet_MyWaitingFriends = (RecyclerView)findViewById(R.id.friendsNet_MyWaitingFriends);
        friendsNet_myFriendMoreOption = (RelativeLayout)findViewById(R.id.friendsNet_myFriendMoreOption);
        setTabIndexToTabhost();
        // [START declare_auth]


    }
    private void initData() {
        String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getUserId();
        listMyFriends = new HashMap<>();
        listMyWaitingFriends = new HashMap<>();
        listMyRequestFriends = new HashMap<>();
        try {
            com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB friendDB = com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB.getInstance(getApplicationContext());
            try {
                listMyFriends = friendDB.getAllFriends();
                if(listMyFriends != null) {
                    ArrayList<Friends> listFriend = new ArrayList<>();
                    listFriend.addAll(listMyFriends.values());
                    FriendsAdapter myFriendAdapter = new FriendsAdapter(this, listFriend, userId, ConstantUtils.FRIENDS_MYFRIENDS);
                    //friendsNet_MyFriendsRCView.setAdapter(myFriendAdapter);
                    appearanceAnimate(new Random().nextInt(5),myFriendAdapter);
                }
            }catch (Exception e){

            }

            try {
                listMyRequestFriends = friendDB.getListRequest(ConstantUtils.FRIENDS_REQUEST);
                if(listMyRequestFriends.size() == 0)
                    friendsNet_RequestRCView.setVisibility(View.GONE);
                else
                {
                    LinearLayoutManager manager3 = new LinearLayoutManager(
                            getApplicationContext(), LinearLayoutManager.VERTICAL, false
                    );
                    friendsNet_RequestRCView.setLayoutManager(manager3);
                    ArrayList<Friends> listRequestFriends = new ArrayList<>();
                    listRequestFriends.addAll(listMyRequestFriends.values());
                    friendsNet_RequestRCView.setVisibility(View.VISIBLE);
                    FindNewFriendsAdapter requestFriendAdapter = new FindNewFriendsAdapter(this,listRequestFriends,userId,ConstantUtils.FRIENDS_REQUEST);
                    friendsNet_RequestRCView.setAdapter(requestFriendAdapter);
                }
            }catch (Exception e){

            }
            DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS_REQUEST).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    numOfFriendShip = (int)dataSnapshot.getChildrenCount();
                    for(DataSnapshot data :dataSnapshot.getChildren())
                    {
                        FriendShip friendship = data.getValue(FriendShip.class);
                        if(friendship.getId() == null){
                            friendship.setId(data.getKey());
                        }
                        getFriendInformation(friendship);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    com.google.firebase.crash.FirebaseCrash.log(databaseError.getMessage());
                }
            });
        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }



    private void getFriendInformation(final FriendShip friendship) {
        DatabaseReference mDatabase= FirebaseDatabase.getInstance().getReference();
        mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(friendship.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Users user = dataSnapshot.getValue(Users.class);
                    Friends friend = new Friends();
                    friend.setFriendsName(user.getuName());
                    friend.setFriendsId(friendship.getId());
                    friend.setFriendsUriAvatar(user.getuAvUrl());
                    friend.setCanSeeRealTimePosition(friendship.getSeePosi());
                    if(friendship.getStatus().equals(ConstantUtils.FRIENDS_WAITING))// 1 is waiting friends
                    {
                        listMyWaitingFriends.put(friendship.getId(),friend);
                    }
                    if(listMyWaitingFriends.size() == numOfFriendShip)
                    {
                        setDataToRecyleView();
                    }
                }catch (Exception e)
                {
                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                com.google.firebase.crash.FirebaseCrash.log(databaseError.getMessage());
            }
        });

    }

    private void setDataToRecyleView() {

        if(listMyWaitingFriends.size()>0)
        {
            LinearLayoutManager manager4 = new LinearLayoutManager(
                    getApplicationContext(), LinearLayoutManager.VERTICAL, false
            );
            String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getUserId();
            friendsNet_MyWaitingFriends.setLayoutManager(manager4);
            ArrayList<Friends> listWaitingFriends = new ArrayList<>();
            listWaitingFriends.addAll(listMyWaitingFriends.values());
            FindNewFriendsAdapter waitingFriendAdapter = new FindNewFriendsAdapter(this,listWaitingFriends,userId,ConstantUtils.FRIENDS_WAITING);
            friendsNet_MyWaitingFriends.setAdapter(waitingFriendAdapter);
        }
    }

    private void addEvent() {
        friendsNet_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(friendsNet_tabHot.getCurrentTab() == 0)
                    searchingNewFriend();
                else
                    searchCurrentFriendList();
            }
        });
        friendsNet_voice_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        friendsNet_myFriendMoreOption.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(friendsNet_myFriendMoreOption.getVisibility() == View.VISIBLE)
                {
                    friendsNet_myFriendMoreOption.setVisibility(View.GONE);
                }
                return false;
            }
        });

        // Back to main activity
        friendsNet_ibtnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void searchCurrentFriendList() {
        String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getUserId();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_FRIENDS).child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void searchingNewFriend() {
        String searchString = friendsNet_txtSearch.getText().toString();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_SEARCH).orderByChild(IUser.uName).startAt(searchString).endAt(searchString+"\uf8ff").limitToLast(20).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    ArrayList<Friends> listFriends = new ArrayList<Friends>();
                    for(DataSnapshot data : dataSnapshot.getChildren())
                    {
                        String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getUserId();
                        if(!data.getKey().equals(userId)) {

                            if (!((listMyFriends == null?false:listMyFriends.containsKey(data.getKey())) ||
                                    (listMyWaitingFriends==null?false:listMyWaitingFriends.containsKey(data.getKey())) ||
                                    (listMyWaitingFriends==null?false:listMyRequestFriends.containsKey(data.getKey())))) {
                                Friends friend = new Friends(
                                        data.getKey(), (String) data.child(IUser.uName).getValue(), (String) data.child(IUser.uAvatarUri).getValue()
                                );
                                listFriends.add(friend);
                            }
                        }
                    }
                    if(listFriends.size()==0)
                    {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.cannot_find_friends),Toast.LENGTH_LONG).show();
                    }else {
                        LinearLayoutManager manager1 = new LinearLayoutManager(
                                getApplicationContext(), LinearLayoutManager.VERTICAL, false
                        );
                        friendsNet_AddNewRCView.setLayoutManager(manager1);
                        String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getUserId();
                        FindNewFriendsAdapter findNewFriendAdapter = new FindNewFriendsAdapter(FriendsNetActivity.this, listFriends, userId, ConstantUtils.FRIENDS_ADDNEW);
                        friendsNet_AddNewRCView.setAdapter(findNewFriendAdapter);
                    }
                }catch (Exception e){
                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void appearanceAnimate(int key, FriendsAdapter myFriendAdapter) {

        AnimationAdapter animAdapter;
        switch (key) {
            default:
            case 0:
                animAdapter = new AlphaInAnimationAdapter(myFriendAdapter);
                break;
            case 1:
                animAdapter = new ScaleInAnimationAdapter(myFriendAdapter);
                break;
            case 2:
                animAdapter = new SwingBottomInAnimationAdapter(myFriendAdapter);
                break;
            case 3:
                animAdapter = new SwingLeftInAnimationAdapter(myFriendAdapter);
                break;
            case 4:
                animAdapter = new SwingRightInAnimationAdapter(myFriendAdapter);
                break;
        }
        animAdapter.setAbsListView(dynamic_listview);
        dynamic_listview.setAdapter(animAdapter);
    }

    private void setTabIndexToTabhost() {
        friendsNet_tabHot.setup();
        TabHost.TabSpec spec = friendsNet_tabHot.newTabSpec("tab0");
        spec.setIndicator("",getResources().getDrawable(R.drawable.add_friends));
        spec.setContent(R.id.tab1);
        friendsNet_tabHot.addTab(spec);
        spec = friendsNet_tabHot.newTabSpec("tab1");
        spec.setIndicator("",getResources().getDrawable(R.drawable.friends));
        spec.setContent(R.id.tab2);
        friendsNet_tabHot.addTab(spec);
        spec = friendsNet_tabHot.newTabSpec("tab2");
        spec.setIndicator(getResources().getString(R.string.friend_status_2));
        spec.setContent(R.id.tab3);
        friendsNet_tabHot.addTab(spec);
        friendsNet_tabHot.setCurrentTab(1);
    }


    @Override
    public void onDataChanged(IssueKey key, Object object) {
        if(key.equals(IssueKey.SHOW_DIALOG_ERROR_FRIENDS)){
            showDialogError((String)object,getApplicationContext().getResources().getString(R.string.dialog_error_title));
            friendsNet_myFriendMoreOption.setVisibility(View.GONE);
        }
    }

    private void showDialogError(String context,String title){
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(context)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
