package com.ginsre.geo.hoang.dang.ginsre.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.FriendsAdapter;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.ScaleInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingLeftInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import butterknife.ButterKnife;

/**
 * Created by hoang on 1/4/2018.
 */

public class FriendsFragment extends Fragment {
    private static Activity context;
    private static final String ARG_POSITION = "position";
    private Map<String,Friends> listMyFriends;
    public DynamicListView dynamic_listview;
    int position;

    public static FriendsFragment newInstance(Activity mcontext,int position) {
        FriendsFragment f = new FriendsFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        context = mcontext;
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_friends,
                container, false);
        dynamic_listview = (DynamicListView)rootView.findViewById(R.id.dynamic_listview);
        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initFirstData();
    }

    private void initFirstData() {
        try {
            com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB friendDB = com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB.getInstance(context);
            String pageType = "";

            if(position == 1){
                listMyFriends = friendDB.getAllFriends();
                pageType =ConstantUtils.FRIENDS_MYFRIENDS;
            }
            else if(position == 2){
                listMyFriends = friendDB.getListRequest(ConstantUtils.FRIENDS_REQUEST);
                pageType =ConstantUtils.FRIENDS_REQUEST;
            }
            ArrayList<Friends> listFriend = new ArrayList<>();
            listFriend.addAll(listMyFriends.values());
            FriendsAdapter myFriendAdapter = new FriendsAdapter(context, listFriend,com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getUserId(),pageType);
            //friendsNet_MyFriendsRCView.setAdapter(myFriendAdapter);
            appearanceAnimate(new Random().nextInt(5),myFriendAdapter);
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }

    }


    private void appearanceAnimate(int key, FriendsAdapter myFriendAdapter) {

        AnimationAdapter animAdapter;
        switch (key) {
            default:
            case 0:
                animAdapter = new AlphaInAnimationAdapter(myFriendAdapter);
                break;
            case 1:
                animAdapter = new ScaleInAnimationAdapter(myFriendAdapter);
                break;
            case 2:
                animAdapter = new SwingBottomInAnimationAdapter(myFriendAdapter);
                break;
            case 3:
                animAdapter = new SwingLeftInAnimationAdapter(myFriendAdapter);
                break;
            case 4:
                animAdapter = new SwingRightInAnimationAdapter(myFriendAdapter);
                break;
        }
        animAdapter.setAbsListView(dynamic_listview);
        dynamic_listview.setAdapter(animAdapter);
    }
}
