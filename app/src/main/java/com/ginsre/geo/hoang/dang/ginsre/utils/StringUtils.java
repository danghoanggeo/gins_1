package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.text.Html;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class StringUtils {
    private static final int CACHE_SIZE = 4096;

    /**
     *
     * @param delimiter
     * @param segments
     * @return chuỗi da kết nối
     * @see StringUtils#join(String, Object[])
     */
    public static String join(String delimiter, Collection<?> segments) {
        StringBuilder stringBuilder = new StringBuilder();
        if (segments != null) {
            appendCollectionObjectToStringBuilder(stringBuilder, delimiter, segments);
        }
        String outString = stringBuilder.toString();
        if (outString.endsWith(delimiter)) {
            return outString.substring(0, outString.length() - delimiter.length());
        }
        return outString;
    }

    public static String join(String delimiter, Object... segments) {
        StringBuilder stringBuilder = new StringBuilder();
        if (segments != null) {
            int size = segments.length;
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    appendObjectToStringBuilder(stringBuilder, delimiter, segments[i]);
                }
            }
        }
        String outString = stringBuilder.toString();
        if (outString.endsWith(delimiter)) {
            return outString.substring(0, outString.length() - delimiter.length());
        }
        return outString;
    }

    public static String html2String(String str) {
        try {
            return StringUtils.isEmpty(str) ? "" : Html.fromHtml(str).toString();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.gc();
        }
        return StringUtils.isEmpty(str) ? "" : str;
    }

    private static void appendArrayObjectToStringBuilder(StringBuilder stringBuilder, String delimiter, Object array) {
        int length = Array.getLength(array);
        for (int i = 0; i < length; i++) {
            appendObjectToStringBuilder(stringBuilder, delimiter, Array.get(array, i));
        }
    }

    private static void appendCollectionObjectToStringBuilder(StringBuilder stringBuilder, String delimiter, Collection<?> collection) {
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            appendObjectToStringBuilder(stringBuilder, delimiter, iterator.next());
        }
    }

    private static void appendObjectToStringBuilder(StringBuilder stringBuilder, String delimiter, Object object) {
        if (object == null) {
            return;
        }
        if (object.getClass().isArray()) {
            appendArrayObjectToStringBuilder(stringBuilder, delimiter, object);
        } else if (object instanceof Collection) {
            appendCollectionObjectToStringBuilder(stringBuilder, delimiter, (Collection)object);
        } else {
            String objectString = object.toString();
            stringBuilder.append(objectString);
            if (!isEmpty(objectString) && !objectString.endsWith(delimiter)) {
                stringBuilder.append(delimiter);
            }
        }
    }
    public static boolean isEmpty(String string) {
        return string == null || string.trim().length() == 0;
    }

    public static boolean equal(String a, String b) {
        return a == b || (a != null && b != null && a.length() == b.length() && a.equals(b));
    }


    public static long[] splitToLongArray(String string, String delimiter) {
        List<String> stringList = splitToStringList(string, delimiter);

        long[] longArray = new long[stringList.size()];
        int i = 0;
        for (String str : stringList) {
            longArray[i++] = Long.parseLong(str);
        }
        return longArray;
    }


    public static List<Long> splitToLongList(String string, String delimiter) {
        List<String> stringList = splitToStringList(string, delimiter);

        List<Long> longList = new ArrayList<Long>(stringList.size());
        for (String str : stringList) {
            longList.add(Long.parseLong(str));
        }
        return longList;
    }


    public static String[] splitToStringArray(String string, String delimiter) {
        List<String> stringList = splitToStringList(string, delimiter);
        return stringList.toArray(new String[stringList.size()]);
    }


    public static List<String> splitToStringList(String string, String delimiter) {
        List<String> stringList = new ArrayList<String>();
        if (!isEmpty(string)) {
            int length = string.length();
            int pos = 0;

            do {
                int end = string.indexOf(delimiter, pos);
                if (end == -1) {
                    end = length;
                }
                stringList.add(string.substring(pos, end));
                pos = end + 1; // skip the delimiter
            } while (pos < length);
        }
        return stringList;
    }


    public static String stringFromInputStream(InputStream inputStream) {
        try {
            byte[] readBuffer = new byte[CACHE_SIZE];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int readLen = inputStream.read(readBuffer, 0, CACHE_SIZE);
                if (readLen <= 0) {
                    break;
                }

                byteArrayOutputStream.write(readBuffer, 0, readLen);
            }

            return byteArrayOutputStream.toString();
        } catch (OutOfMemoryError e) {
            System.gc();
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static boolean isEmailFormat(String string) {
        if (StringUtils.isEmpty(string)) {
            return false;
        }
        String emailFormat = "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
        return Pattern.matches(emailFormat, string);
    }


    public static boolean isPhoneNumFormat(String phoneNum) {
        if (StringUtils.isEmpty(phoneNum)) {
            return false;
        }
        final int phoneNumLength = 11;
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(phoneNum).matches() && phoneNum.length() == phoneNumLength;
    }

    public static boolean isPhoneNumForViFormat(String phoneNum) {
        if (StringUtils.isEmpty(phoneNum)) {
            return false;
        }
        final int phoneNumLength = 11;
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(phoneNum).matches() && (phoneNum.length() == phoneNumLength ||
                phoneNum.length() == 10);
    }
    //TODO: need format of japan number phone
    public static boolean isPhoneNumForJPFormat(String phoneNum) {
        return false;
    }
    //TODO: need format of japan card id
    public static boolean isIDCardFormat(String IDCard) {
        if (StringUtils.isEmpty(IDCard)) {
            return false;
        }
        final int idCardLength = 18;
        return IDCard.length() == idCardLength;
    }


    public static boolean isLoginEmailFormat(String string) {
        if (StringUtils.isEmpty(string)) {
            return false;
        }
        String emailFormat = "^\\s*\\w+(?:\\.?[\\w-]+\\.?)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
        return Pattern.matches(emailFormat, string);
    }


	public static boolean isUserNameFormat(String string) {
		if (StringUtils.isEmpty(string)) {
			return false;
		}
		String emailFormat = "^[a-zA-Z0-9_-]{0,}$";
		return Pattern.matches(emailFormat, string);
	}

    public static boolean lengthInRange(String string, int begin, int end) {
        return string != null && string.length() >= begin && string.length() <= end;
    }

    public static String validateFilePath(String srcStr) {
        return StringUtils.isEmpty(srcStr) ? srcStr : srcStr.replaceAll("[\\\\/:\"*?<>|]+", "_");
    }

    public static String upercaseString(String string)
    {
        return string.toUpperCase();
    }

    public static String getUidByUidLoca(String string)
    {
        int len = Integer.parseInt(string.substring(0,3));
        return string.substring(3,len+3);
    }
    public static String getUTokenByUidLoca(String string)
    {
        int len = Integer.parseInt(string.substring(0,3));
        return string.substring(len+3,string.length());
    }

}


