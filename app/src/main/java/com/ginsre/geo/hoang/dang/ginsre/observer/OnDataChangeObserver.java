package com.ginsre.geo.hoang.dang.ginsre.observer;

import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;

/**
 * Created by hoang on 9/5/2017.
 */

public interface OnDataChangeObserver {
    /**
     *
     * @param key
     * @param object
     */
    void onDataChanged(IssueKey key, Object object);
}
