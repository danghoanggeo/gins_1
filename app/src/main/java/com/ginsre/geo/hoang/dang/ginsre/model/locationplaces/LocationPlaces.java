package com.ginsre.geo.hoang.dang.ginsre.model.locationplaces;

/**
 * Created by hoang on 12/9/2017.
 */

public class LocationPlaces {
    private String location_id;
    private String location_name;
    private String loaction_description;
    private String userId;
    private boolean userPostOwn;
    private String backGroundUrl;
    private String openHourStart;
    private String openHourClose;
    private boolean onPromotion;

    public LocationPlaces(String location_name, String loaction_description, String userId, boolean userPostOwn, String backGroundUrl, String openHourStart, String openHourClose, boolean onPromotion) {
        this.location_name = location_name;
        this.loaction_description = loaction_description;
        this.userId = userId;
        this.userPostOwn = userPostOwn;
        this.backGroundUrl = backGroundUrl;
        this.openHourStart = openHourStart;
        this.openHourClose = openHourClose;
        this.onPromotion = onPromotion;
    }

    public LocationPlaces(){}

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getLoaction_description() {
        return loaction_description;
    }

    public void setLoaction_description(String loaction_description) {
        this.loaction_description = loaction_description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isUserPostOwn() {
        return userPostOwn;
    }

    public void setUserPostOwn(boolean userPostOwn) {
        this.userPostOwn = userPostOwn;
    }

    public String getBackGroundUrl() {
        return backGroundUrl;
    }

    public void setBackGroundUrl(String backGroundUrl) {
        this.backGroundUrl = backGroundUrl;
    }

    public String getOpenHourStart() {
        return openHourStart;
    }

    public void setOpenHourStart(String openHourStart) {
        this.openHourStart = openHourStart;
    }

    public String getOpenHourClose() {
        return openHourClose;
    }

    public void setOpenHourClose(String openHourClose) {
        this.openHourClose = openHourClose;
    }

    public boolean isOnPromotion() {
        return onPromotion;
    }

    public void setOnPromotion(boolean onPromotion) {
        this.onPromotion = onPromotion;
    }
}
