package com.ginsre.geo.hoang.dang.ginsre.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.LocationReviewAdapter;
import com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.LocationReview;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 12/15/2017.
 */

public class LoactionReviewPageFragment  extends Fragment implements View.OnClickListener{
    private static final String ARG_OWNER = "owner";
    private static final String ARG_LOCATIONID = "locationID";
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2;
    private boolean isTheManager;
    private String locationID;
    private static Activity context;
    private ArrayList<LocationReview> listReview;

    private ArrayList<Bitmap> listBitMapPost;
    LocationReviewAdapter listReviewAdapter;

    @BindView(R.id.fragment_location_tab_review_containter)
    LinearLayout fragment_location_tab_review_containter;
    @BindView(R.id.fragment_location_tab_review_linerContainNewPost)
    LinearLayout fragment_location_tab_review_linerContainNewPost;
    @BindView(R.id.fragment_location_tab_review_recycelelstview)
    RecyclerView fragment_location_tab_review_recycelelstview;
    @BindView(R.id.fragment_location_tab_review_textAlert)
    TextView fragment_location_tab_review_textAlert;
    @BindView(R.id.fragment_location_tab_review_ibtn_new)
    FancyButton fragment_location_tab_review_ibtn_new;
    @BindView(R.id.fragment_location_tab_review_txt_new)
    EditText fragment_location_tab_review_txt_new;
    // Add new
    @BindView(R.id.fragment_trafficPostAddNew_container)
    LinearLayout fragment_trafficPostAddNew_container;
    @BindView(R.id.fragment_PostAddNew_txtTitle)
    EditText fragment_PostAddNew_txtTitle;
    @BindView(R.id.fragment_trafficPostAddNew_txtcontent)
    EditText fragment_trafficPostAddNew_txtcontent;
    @BindView(R.id.fragment_trafficPostAddNew_ibtnpicture)
    ImageButton fragment_trafficPostAddNew_ibtnpicture;
    @BindView(R.id.fragment_trafficPostAddNew_imvImagePost)
    ImageView fragment_trafficPostAddNew_imvImagePost;
    @BindView(R.id.fragment_trafficPostAddNew_rating_assess)
    RatingBar fragment_trafficPostAddNew_rating_assess;
    @BindView(R.id.fragment_trafficPostAddNew_ftbnCancel)
    FancyButton fragment_trafficPostAddNew_ftbnCancel;
    @BindView(R.id.fragment_trafficPostAddNew_fbtnOk)
    FancyButton fragment_trafficPostAddNew_fbtnOk;
    @BindView(R.id.fragment_PostAddNew_Rating_container)
    LinearLayout fragment_PostAddNew_Rating_container;

    public static LoactionReviewPageFragment newInstance(Activity mcontext, String locationID, boolean isTheManager) {
        LoactionReviewPageFragment f = new LoactionReviewPageFragment();
        Bundle b = new Bundle();
        b.putBoolean(ARG_OWNER,isTheManager);
        b.putString(ARG_LOCATIONID,locationID);
        context = mcontext;
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTheManager = getArguments().getBoolean(ARG_OWNER);
        locationID = getArguments().getString(ARG_LOCATIONID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_loaction_review,
                container, false);
        ButterKnife.bind(this,rootView);

        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getLocationReview(1,2);
        addEvent();
    }

    private void addEvent() {
        fragment_location_tab_review_ibtn_new.setOnClickListener(this);
        fragment_trafficPostAddNew_ibtnpicture.setOnClickListener(this);
        fragment_trafficPostAddNew_fbtnOk.setOnClickListener(this);
        fragment_trafficPostAddNew_ftbnCancel.setOnClickListener(this);
    }

    private void getLocationReview(int i, int i1) {
        try {
            com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
            db.collection(ConstantUtils.REF_LOCATION_PAGE).document(locationID)
                    .collection(ConstantUtils.REF_LOCATION_REVIEW).orderBy("dateTime", Query.Direction.ASCENDING).limit(20).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    try {
                        if (task.isSuccessful()) {
                            listReview = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {
                                LocationReview locationReview = document.toObject(LocationReview.class);
                                locationReview.setUserId(document.getId());
                                listReview.add(locationReview);
                            }
                            setLocationReviewPresent();
                        } else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }catch (Exception e){
                        com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                    }
                }
            });
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void setLocationReviewPresent() {
        LinearLayoutManager myLayoutManager = new LinearLayoutManager(getActivity());
        myLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        if (listReview.size() > 0 & fragment_location_tab_review_recycelelstview != null) {
            listReviewAdapter = new LocationReviewAdapter(context,listReview);
            fragment_location_tab_review_recycelelstview.setAdapter(listReviewAdapter);
        }
        fragment_location_tab_review_recycelelstview.setLayoutManager(myLayoutManager);
        addEvent(myLayoutManager);
    }

    int lastScroll;
    private void addEvent(final LinearLayoutManager myLayoutManager) {
        if(isTheManager) {
            lastScroll = myLayoutManager.findFirstVisibleItemPosition();
            fragment_location_tab_review_recycelelstview.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if(listReview.size()>2){
                        if (lastScroll < myLayoutManager.findFirstVisibleItemPosition()) {
                            fragment_location_tab_review_linerContainNewPost.setVisibility(View.GONE);
                        } else {
                            if (myLayoutManager.findFirstVisibleItemPosition() == 0) {
                                fragment_location_tab_review_linerContainNewPost.setVisibility(View.VISIBLE);
                            }
                        }
                        lastScroll = myLayoutManager.findFirstVisibleItemPosition();
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId)
        {
            case R.id.fragment_location_tab_review_ibtn_new:
            {
                fragment_location_tab_review_containter.setVisibility(View.GONE);
                fragment_trafficPostAddNew_container.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.fragment_trafficPostAddNew_ibtnpicture:
            {
                if(listBitMapPost == null)
                    listBitMapPost = new ArrayList<>();
                if(listBitMapPost.size()<5)
                    userTakePictureToPost();
                else
                    Toast.makeText(context,context.getResources().getString(R.string.cannot_upload_more_than_4),Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.fragment_trafficPostAddNew_fbtnOk:
            {
                testUserPostNewToFireStore();
                break;
            }
            case R.id.fragment_trafficPostAddNew_ftbnCancel:
            {
                closePostFragment();
                break;
            }
        }
    }
    private void userTakePictureToPost() {
        try {


            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
            else
            {
                Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT, null);
                galleryintent.setType("image/*");
                //galleryintent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                //galleryintent.putExtra("GALLERY","gallery");
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //cameraIntent.putExtra("CAMERA","camera");
                Intent chooser = new Intent(Intent.ACTION_CHOOSER);
                chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
                chooser.putExtra(Intent.EXTRA_TITLE, context.getResources().getString(R.string.get_picture_or_take));

                Intent[] intentArray =  {cameraIntent};
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooser,CAMERA_REQUEST_CODE);
            }
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }


    private void testUserPostNewToFireStore() {

        if(fragment_trafficPostAddNew_txtcontent.getText().length()<3){
            Toast.makeText(context,context.getResources().getString(R.string.location_name_is_invalid),Toast.LENGTH_LONG).show();
            fragment_trafficPostAddNew_txtcontent.setFocusable(true);
        }else{
            fragment_trafficPostAddNew_txtcontent.clearFocus();
            if(listBitMapPost == null)
                pushPostDetailToFireStore(null);
            else
                uploadImagesToFirebase();

            closePostFragment();
        }
    }


    private void closePostFragment(){
        fragment_PostAddNew_txtTitle.setVisibility(View.GONE);
        fragment_location_tab_review_containter.setVisibility(View.VISIBLE);
        fragment_trafficPostAddNew_container.setVisibility(View.GONE);

    }

    private void uploadImagesToFirebase(){
        final ArrayList<String> listImageUrl = new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.user_post_uploading));
        for(int i = 0;i<listBitMapPost.size();i++){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap imagePost = listBitMapPost.get(i);
            imagePost.compress(Bitmap.CompressFormat.JPEG,90, baos);
            byte[] data = baos.toByteArray();
            final String mpostDataRef = ConstantUtils.REF_LOCATION_POST;
            if(data != null)
            {
                if(i==0)
                    progressDialog.show();
                final String nlocationDetailId = locationID+DateUtils.formatCurrentDate(DateUtils.FORMATMINISECOND);
                StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
                //StorageReference childPath = mStorageRef.child(ConstantUtils.STORE_TRAFFIC_IMAGES).child(nlocationDetailId);
                mStorageRef.child(ConstantUtils.REF_LOCATION_IMAGES).child(nlocationDetailId).putBytes(data)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Uri downloadUri = taskSnapshot.getDownloadUrl();
                                listImageUrl.add(downloadUri.toString());
                                if(listImageUrl.size() == listBitMapPost.size()){
                                    progressDialog.dismiss();
                                    pushPostDetailToFireStore(listImageUrl);
                                    listBitMapPost = new ArrayList<>();
                                }
                            }
                        });
            }
        }

    }

    private void pushPostDetailToFireStore(ArrayList<String> listImageUrl) {
        try {
            LocationReview locationReview = new LocationReview(fragment_trafficPostAddNew_txtcontent.getText().toString(),listImageUrl,0);
            locationReview.setDateTime(DateUtils.getTimeLongForPostFromRef());
            com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
            db.collection(ConstantUtils.REF_LOCATION_PAGE).document(locationID).collection(ConstantUtils.REF_LOCATION_REVIEW).document(com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId()).set(locationReview);
            locationReview.setUserId(com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId());
            listReview.add(locationReview);
            if(listReviewAdapter == null){
                setLocationReviewPresent();
            }else {
                listReviewAdapter.notifyDataSetChanged();
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST_CODE && resultCode == context.RESULT_OK)
        {
            if (data == null) {
                return;
            }
            try {

                /*if(data.getStringExtra("CAMERA").equals("camera")){
                    photo = (Bitmap)data.getExtras().get("data");

                }else {*/
                //String type = data.getStringExtra("CAMERA");
                InputStream inputStream = null;
                try {
                    inputStream = context.getApplicationContext().getContentResolver().openInputStream(data.getData());
                }catch (Exception e){

                }
                if(inputStream == null){
                    listBitMapPost.add ((Bitmap)data.getExtras().get("data"));
                }else{
                    listBitMapPost.add (BitmapFactory.decodeStream(inputStream));
                }

                //ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //photo.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                fragment_trafficPostAddNew_imvImagePost.setVisibility(View.VISIBLE);

                fragment_trafficPostAddNew_imvImagePost.setImageBitmap(listBitMapPost.get(0));

            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,CAMERA_REQUEST_CODE);
                }
            }
        }
    }



}
