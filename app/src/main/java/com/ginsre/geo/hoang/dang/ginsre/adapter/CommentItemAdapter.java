package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostComment;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Users;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by hoang on 9/19/2017.
 */

public class CommentItemAdapter extends RecyclerView.Adapter<CommentItemAdapter.CommentViewHolder> {
    private static final String TAG = CommentItemAdapter.class.getSimpleName();
    private ArrayList<PostComment> lstPostComment;
    private Context context;

    public CommentItemAdapter(ArrayList<PostComment> lstPostComment){
        this.lstPostComment = lstPostComment;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.item_post_comment_layout;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        CommentViewHolder viewHolder = new CommentViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder,final int position) {
        final String userId = lstPostComment.get(position).getComment_userId();
        final CommentViewHolder mHolder = holder;
        if(userId != null)
        {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Users ginser = dataSnapshot.getValue(Users.class);
                    if(ginser != null) {
                        if(position>=1)
                        {
                            if(lstPostComment.get(position-1).getComment_userId().equals(userId)){
                                mHolder.bind("-1","-1"
                                        ,lstPostComment.get(position).getComment_content()
                                        ,lstPostComment.get(position).getComment_dateTime());
                            }else{
                                mHolder.bind(ginser.getuAvUrl(),ginser.getuName()
                                        ,lstPostComment.get(position).getComment_content()
                                        ,lstPostComment.get(position).getComment_dateTime());
                            }
                        }else{
                            mHolder.bind(ginser.getuAvUrl(),ginser.getuName()
                                    ,lstPostComment.get(position).getComment_content()
                                    ,lstPostComment.get(position).getComment_dateTime());
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return lstPostComment.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        ImageView item_post_comment_imvUserAvatar;
        TextView item_post_comment_txvUserName,item_post_comment_txvUserCommnet,item_post_comment_txvTimeComment;

        public CommentViewHolder(View itemView) {
            super(itemView);
            item_post_comment_imvUserAvatar = (ImageView) itemView.findViewById(R.id.item_post_comment_imvUserAvatar);
            item_post_comment_txvUserName = (TextView)itemView.findViewById(R.id.item_post_comment_txvUserName);
            item_post_comment_txvUserCommnet = (TextView)itemView.findViewById(R.id.item_post_comment_txvUserCommnet);
            item_post_comment_txvTimeComment = (TextView)itemView.findViewById(R.id.item_post_comment_txvTimeComment);
        }

        public void bind(String userAvaRe,String uName,String uComment,String timeComment) {
            if(userAvaRe != null)
            {
                if(!userAvaRe.equals("-1")) {
                    //TODO:(1) set user avatar
                    // item_post_comment_imvUserAvatar.setImageURI();
                    //Load user avatar to this imageView
                    ImageUtil.displayRoundImage(item_post_comment_imvUserAvatar,userAvaRe,null);
                    /*Picasso.with(context).load(userAvaRe)
                            .resize(DisplayUtils.dp2px(24),DisplayUtils.dp2px(24))
                            .centerCrop()
                            .into(item_post_comment_imvUserAvatar);*/
                }
                else
                    item_post_comment_imvUserAvatar.setVisibility(View.GONE);
            }
            if(uName!=null) {
                if(uName.equals("-1"))
                    item_post_comment_txvUserName.setText("");
                else
                    item_post_comment_txvUserName.setText(uName);
            }
            item_post_comment_txvUserCommnet.setText(uComment);
            item_post_comment_txvTimeComment.setText(timeComment);
        }
    }
}
