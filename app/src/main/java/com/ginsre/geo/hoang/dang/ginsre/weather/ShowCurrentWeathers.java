package com.ginsre.geo.hoang.dang.ginsre.weather;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.clustering.ui.IconGenerator;
import com.ginsre.geo.hoang.dang.ginsre.model.core.WeatherDay;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class ShowCurrentWeathers extends AsyncTask<String, Void, String[]>{
	private static final String API_KEY = "eb8ae93f4b1c2dc0221acae4c5d5be61";
	private static final String API_ENDPOINT = "http://api.openweathermap.org/data/2.5/";
	private Context context;
	private GoogleMap mMap;
	private LatLng latLng;

	public ShowCurrentWeathers(Context context, GoogleMap map, LatLng latLng){
		this.context = context;
		mMap = map;
		this.latLng = latLng;
	}

	protected String getASCIIContentFromEntity(HttpEntity entity) throws IllegalStateException, IOException {
			InputStream in = entity.getContent();
			StringBuffer out = new StringBuffer();
			int n = 1;
			while (n>0) {
				byte[] b = new byte[4096];
				n = in.read(b);
				if (n>0) out.append(new String(b, 0, n));
			}
			return out.toString();
		}
	@Override
	protected String[] doInBackground(String... params) {
		// TODO Auto-generated method stub
		HttpClient httpClient = new DefaultHttpClient();
		String lanParam = "&lang=en&appid=";
		if(com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentLocale(context).getCountry().equalsIgnoreCase("vn")){
			lanParam = "&lang=vi&appid=";
		}
		HttpGet request = new HttpGet(API_ENDPOINT+params[0]+lanParam+API_KEY);
		String[] text = new String[2];
		try {
			HttpResponse response = httpClient.execute(request);
			HttpEntity entity = response.getEntity();
			text[0] = getASCIIContentFromEntity(entity);
        	text[1] = params[1];
		} catch (Exception e) {
			return null;
		}
		return text;
	}
	@Override
	protected void onPostExecute(String[] data){
		if(data == null){
			Toast.makeText(context, "Error!", Toast.LENGTH_LONG).show();
		}else{
			if(data[1].equals("showListWeather")){
				try {
					JSONObject jsonOject = new JSONObject(data[0]);
		        	// Create list weather for every day;
		        	JSONArray jsonArray = new JSONArray(jsonOject.getString("list"));
		        	List<WeatherDay> lstWeather = new ArrayList<WeatherDay>();
		        	for(int i = 0;i<jsonArray.length();i++){
		        		JsonToWeather Jweather = new JsonToWeather();
		        		WeatherDay dForecast = Jweather.getWeatherDay(jsonArray.getString(i),data[1]);
		        		//add to list
		        		lstWeather.add(dForecast);
		        		
		        	}
		        	WeatherAdapter weathers = new WeatherAdapter(context, lstWeather);
					//ListView lvsweathers = (ListView)activity.findViewById(R.id.lvsweathers);
			    	//lvsweathers.setAdapter(weathers);
				} catch (Exception e) {
					Toast.makeText(context,e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
				}
				
			}else if(data[1].equals("showCurrentWeather")){
				try {
					JsonToWeather Jweather = new JsonToWeather();
					WeatherDay dForecast = Jweather.getWeatherDay(data[0],data[1]);
					/*
					TextView txtDateTime = (TextView)activity.findViewById(R.id.txtDateTime);
					TextView txtStatus = (TextView)activity.findViewById(R.id.txtStatus);
					TextView txtTempt = (TextView)activity.findViewById(R.id.txtTempt);
					TextView txtPress= (TextView)activity.findViewById(R.id.txtPress);
					txtDateTime.setText(dForecast.getDateTime());
					txtStatus.setText(dForecast.weCondition.getDescription());
					txtTempt.setText(String.valueOf(dForecast.tempt.getDay()));
					txtPress.setText(String.valueOf(dForecast.getPressure()));
					*/

					/*new AlertDialog.Builder((Activity)context)
							.setTitle(context.getResources().getString(R.string.dialog_weather_title))
							.setMessage(context.getResources().getString(R.string.weather_main)+": "+dForecast.getWeCondition().getDescription()
									+"\n"+context.getResources().getString(R.string.tempt) +": "+dForecast.getTempt().getGetTempt(context)+
							"\n"+context.getResources().getString(R.string.wind_speed) +": "+dForecast.getWind().getWindDirection(context))
							.setPositiveButton(android.R.string.ok, null)
							.setIcon(android.R.drawable.ic_dialog_info)
							.show();*/
					String content = context.getResources().getString(R.string.weather_main).replace("xx",dForecast.getWeCondition().getDescription())
							+"\n"+context.getResources().getString(R.string.tempt).replace("xx",dForecast.getTempt().getGetTempt(context))
							+ "\n"+context.getResources().getString(R.string.wind_speed).replace("xx",dForecast.getWind().getWindDirection(context));
					addIcon(content,latLng);
					//Toast.makeText(context,,Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					//Toast.makeText(context,e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
				}
				
			}else{
				
			}
			
		}
	}

	private void addIcon(CharSequence text, LatLng position) {
		IconGenerator iconFactory = new IconGenerator(context);
		MarkerOptions markerOptions = new MarkerOptions().
				icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(text))).
				position(position).
				anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

		mMap.addMarker(markerOptions);
	}
}
