package com.ginsre.geo.hoang.dang.ginsre.model.users;

/**
 * Created by hoang on 12/8/2017.
 */

public interface IUser {
    public static final String uName = "uName";
    public static final String  uPass = "uPass";
    public static final String uEmail = "uEmail";
    public static final String  uPhone  = "uPhone";
    public static final String  lstDeviceTokens = "";
    public static final String  uAvatarUri = "uAvUrl";
    public static final String  status= "status";
    public static final String  birthday= "birthday";
    public static final String  grade= "grade";
    public static final String  mPosition= "mPosition";
    public static final String numOfNotis = "nNoTi";
    public static final String numOfMessage = "nMess";
}
