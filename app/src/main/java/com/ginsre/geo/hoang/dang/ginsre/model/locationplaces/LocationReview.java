package com.ginsre.geo.hoang.dang.ginsre.model.locationplaces;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hoang on 12/15/2017.
 */

public class LocationReview {
    private String userId;
    private String content;
    private String dateTime;
    private String status;
    private ArrayList<String> listImagesUrl;
    private int numOfComment;
    private int numOfLikes;
    private int numOfSpam;
    private HashMap<String,String> lastComment;
    private float rating;
    private boolean like;
    private boolean spam;

    public LocationReview(){}

    public LocationReview(String content, ArrayList<String> listImagesUrl, float rating) {
        this.content = content;
        this.listImagesUrl = listImagesUrl;
        this.rating = rating;
        numOfComment = 0;
        numOfLikes = 0;
        numOfSpam = 0;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public int getNumOfSpam() {
        return numOfSpam;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public boolean isSpam() {
        return spam;
    }

    public void setSpam(boolean spam) {
        this.spam = spam;
    }

    public void setNumOfSpam(int numOfSpam) {
        this.numOfSpam = numOfSpam;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getListImagesUrl() {
        return listImagesUrl;
    }

    public void setListImagesUrl(ArrayList<String> listImagesUrl) {
        this.listImagesUrl = listImagesUrl;
    }

    public int getNumOfComment() {
        return numOfComment;
    }

    public void setNumOfComment(int numOfComment) {
        this.numOfComment = numOfComment;
    }

    public int getNumOfLikes() {
        return numOfLikes;
    }

    public void setNumOfLikes(int numOfLikes) {
        this.numOfLikes = numOfLikes;
    }

    public HashMap<String, String> getLastComment() {
        return lastComment;
    }

    public void setLastComment(HashMap<String, String> lastComment) {
        this.lastComment = lastComment;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
