package com.ginsre.geo.hoang.dang.ginsre.fragment;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.PostTypeItemAdapter;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.model.core.properties;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostDetail;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostType;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.FirebaseUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.ginsre.geo.hoang.dang.ginsre.utils.StringUtils;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoFire;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoLocation;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoQuery;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
/**
 * Created by hoang on 9/26/2017.
 */

public class PostAddNewFragment extends Fragment implements View.OnClickListener, OnDataChangeObserver {
    private static final String TAG = "AddNewPostFragment";
    RelativeLayout addNewTrafficsPost_layout; // from main activity.xml file.

    @BindView(R.id.fragment_trafficPostAddNew_grvSelect)
    GridView fragment_trafficPostAddNew_grvSelect;

    @BindView(R.id.fragment_trafficPostAddNew_container)
    LinearLayout fragment_trafficPostAddNew_container;
    @BindView(R.id.fragment_trafficPostAddNew_imvicon)
    ImageView fragment_trafficPostAddNew_imvicon;
    @BindView(R.id.fragment_trafficPostAddNew_txvtitle)
    TextView fragment_trafficPostAddNew_txvtitle;
    @BindView(R.id.fragment_trafficPostAddNew_txtcontent)
    EditText fragment_trafficPostAddNew_txtcontent;
    @BindView(R.id.fragment_trafficPostAddNew_ibtnpicture)
    ImageButton fragment_trafficPostAddNew_ibtnpicture;
    @BindView(R.id.fragment_trafficPostAddNew_imvImagePost)
    ImageView fragment_trafficPostAddNew_imvImagePost;
    @BindView(R.id.fragment_trafficPostAddNew_fbtnOk)
    FancyButton fragment_trafficPostAddNew_fbtnOk;
    @BindView(R.id.fragment_trafficPostAddNew_ftbnCancel)
    FancyButton fragment_trafficPostAddNew_ftbnCancel;
    @BindView(R.id.fragment_trafficPostAddNew_rating_assess)
    RatingBar fragment_trafficPostAddNew_rating_assess;
    @BindView(R.id.fragment_PostAddNew_Rating_container)
    LinearLayout fragment_PostAddNew_Rating_container;

    private static Activity context;
    private static LatLng mlocation;
    Bitmap imagePost;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2;
    private static String postTypeId = "";
    private static String postSubType;

    public static PostAddNewFragment newInstance(Activity mcontext, LatLng location, String userToken, String mpostSubType)
    {
        PostAddNewFragment fragment = new PostAddNewFragment();
        context = mcontext;
        mlocation = location;
        postSubType = mpostSubType;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_traffic_post_add_new,container,false);
        ButterKnife.bind(this,rootView);
        addNewTrafficsPost_layout = (RelativeLayout) context.findViewById(R.id.addNewTrafficsPost_layout);

        return rootView;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        //GridView
        try {
            fragment_trafficPostAddNew_grvSelect.setAdapter(new PostTypeItemAdapter(context,postSubType));
            // add or cancel post
            fragment_trafficPostAddNew_fbtnOk.setOnClickListener(this);
            fragment_trafficPostAddNew_ftbnCancel.setOnClickListener(this);
            fragment_trafficPostAddNew_ibtnpicture.setOnClickListener(this);
            if(fragment_trafficPostAddNew_imvImagePost.getVisibility()==View.VISIBLE)
                fragment_trafficPostAddNew_imvImagePost.setVisibility(View.GONE);
            //
        }catch (Exception e)
        {
            Log.d("New Item fragment 140: ",e.getMessage());
        }

        // Data change notification
        DataChangeNotification.getInstance().addObserver(IssueKey.ON_ADD_NEW_TRAFFIC_POST_SELECT_TYPE, this);
    }


    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        if(viewId == R.id.fragment_trafficPostAddNew_ibtnpicture)
        {
            userTakePictureToPost();
        }
        else if(viewId == R.id.fragment_trafficPostAddNew_fbtnOk)
        {
            pushTrafficPostToFirebase();
        }
        else if(viewId == R.id.fragment_trafficPostAddNew_ftbnCancel)
        {
            closeTrafficPost();
        }
    }

    @Override
    public void onDataChanged(IssueKey key, Object object) {
        if(IssueKey.ON_ADD_NEW_TRAFFIC_POST_SELECT_TYPE == key)
        {
            PostType postType = (PostType)object;
            postTypeId = postType.getPostTypeId();
            addNewPosts(postType);
        }
    }

    private void addNewPosts(PostType postType) {
        fragment_trafficPostAddNew_grvSelect.setVisibility(View.GONE);
        fragment_trafficPostAddNew_imvicon.setImageDrawable(postType.getPostTypeImage(context));
        fragment_trafficPostAddNew_container.setVisibility(View.VISIBLE);
        /*if(postSubType.equals(ConstantUtils.GINS_MAIN_TYPE[3])){// Map Chat
            settingViewForMapChat(true);
        }else{
            settingViewForMapChat(false);
        }*/
        settingViewForMapChat(false);
        fragment_trafficPostAddNew_txvtitle.setText(StringUtils.upercaseString(postType.getPostTypeName()));
    }

    private void settingViewForMapChat(boolean setMap) {
        if(setMap){
            fragment_trafficPostAddNew_ibtnpicture.setVisibility(View.GONE);
            fragment_trafficPostAddNew_imvImagePost.setVisibility(View.GONE);
            fragment_trafficPostAddNew_txtcontent.setHint(context.getResources().getString(R.string.chatroomtitle));
        }else{
            if(postSubType.equals(ConstantUtils.GINS_MAIN_TYPE[1]))
                fragment_PostAddNew_Rating_container.setVisibility(View.VISIBLE);
            fragment_trafficPostAddNew_ibtnpicture.setVisibility(View.VISIBLE);
            fragment_trafficPostAddNew_txtcontent.setHint(context.getResources().getString(R.string.hint_write_your_thinking));
        }
    }

    public void closeTrafficPost(){
        addNewTrafficsPost_layout.setVisibility(View.GONE);
        fragment_trafficPostAddNew_grvSelect.setVisibility(View.GONE);
        fragment_trafficPostAddNew_container.setVisibility(View.GONE);
        FancyButton main_fbtn_addNewPost =(FancyButton)context.findViewById(R.id.main_fbtn_addNewPost);
        main_fbtn_addNewPost.setIconResource(R.drawable.addnew);
        main_fbtn_addNewPost.setTag("0");
    }

    private void pushTrafficPostToFirebase() {
        //get location reference
        /*String trafficGeoReferences = ConstantUtils.getPrefixReferencesToData(mlocation,
                DateUtils.formatCurrentDate(DateUtils.FORMAT_TO_DAY)
        )+ConstantUtils.GEO_FIRE_REF_TRAFFIC;*/
        try {
            String postGeoReferences = GinFunctions.getDatabaseRefForGinsType(postSubType);
            String postDataRef = GinFunctions.getDatabaseRefForPostDetail(postSubType);
            /*
            if(postSubType.equals(ConstantUtils.GINS_MAIN_TYPE[0])){// Social
                postGeoReferences = ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.GINS_SOCIAL_LOCATION;
                postDataRef = ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_SOCIAL_LOCATION;
            }else if(postSubType.equals(ConstantUtils.GINS_MAIN_TYPE[1])){// Traffic
                postGeoReferences = ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.GINS_TRAFFIC_LOCATION;
                postDataRef = ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_TRAFFIC_LOCATION;
            }
            else if(postSubType.equals(ConstantUtils.GINS_MAIN_TYPE[2])){// PLACES
                postGeoReferences = ConstantUtils.GINS_DATA_STATIC+ConstantUtils.GINS_PLACES_LOCATION;
                postDataRef = ConstantUtils.GINS_DATA_STATIC+ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_PLACES_LOCATION;
            }
            else if(postSubType.equals(ConstantUtils.GINS_MAIN_TYPE[3])){// MapChat
                postGeoReferences = ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.GINS_MAPCHAT_LOCATION;
                postDataRef = ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_MAPCHAT_LOCATION;
            }else if(postSubType.equals(ConstantUtils.GINS_MAIN_TYPE[4])){// Memories
                postGeoReferences = ConstantUtils.GINS_DATA_STATIC+ConstantUtils.GINS_MEMORIES_LOCATION;
                postDataRef = ConstantUtils.GINS_DATA_STATIC+ConstantUtils.REF_POST_MORE_INFO+ConstantUtils.GINS_MEMORIES_LOCATION;
            }
            */
            // PostID and its locationId is the same; contain type and date time format to minisecond;
            com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHash geoHash = new com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHash(
                            mlocation.latitude,mlocation.longitude);
            final String locationDetailId = DateUtils.getTimeLongForPostFromRef()+"-"+postTypeId+"-"+geoHash.getGeoHashString();
                    //+mDatabase.child(postGeoReferences).push().getKey();
            // save Image to firebase store first
            pushImagePostToFirebase(locationDetailId,postGeoReferences,postDataRef);
        }
        catch (Exception e)
        {
            //TODO: LOG
        }
    }

    private void pushImagePostToFirebase(String mlocationDetailId,String mtrafficGeoReferences,String postDataRef) {
        //fragment_trafficPostAddNew_imvImagePost.setDrawingCacheEnabled(true);
        //fragment_trafficPostAddNew_imvImagePost.buildDrawingCache();
        //Bitmap bitmap = fragment_trafficPostAddNew_imvImagePost.getDrawingCache();
        try {
            if(imagePost == null)
            {
                if(fragment_trafficPostAddNew_txtcontent.getText().toString().equals(""))
                {
                    Toast.makeText(context,context.getResources().getString(R.string.user_post_blank),Toast.LENGTH_LONG).show();
                }else{
                    pushTrafficLocationAndContainToFirebase(mlocationDetailId,mtrafficGeoReferences,null,postDataRef);
                }
            }else{
                //TODO: notice that image quality here
                byte[] data = GinFunctions.bitmapToByteArray(imagePost,80);
                final String mpostDataRef = postDataRef;
                if(data != null)
                {
                    final ProgressDialog progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage(context.getResources().getString(R.string.user_post_uploading));
                    progressDialog.show();
                    final String nlocationDetailId = mlocationDetailId;
                    final String ntrafficGeoReferences = mtrafficGeoReferences;
                    StorageReference mStorageRef = com.google.firebase.storage.FirebaseStorage.getInstance().getReference();
                    //StorageReference childPath = mStorageRef.child(ConstantUtils.STORE_TRAFFIC_IMAGES).child(nlocationDetailId);
                    mStorageRef.child(ConstantUtils.STORE_TRAFFIC_IMAGES).child(nlocationDetailId).putBytes(data)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    Uri downloadUri = taskSnapshot.getDownloadUrl();
                                    progressDialog.dismiss();
                                    pushTrafficLocationAndContainToFirebase(nlocationDetailId,ntrafficGeoReferences,downloadUri.toString(),mpostDataRef);
                                }
                            });
                }

            }
            closeTrafficPost();
        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void pushTrafficLocationAndContainToFirebase(String locationDetailId,String postGeoReferences,String imagesPathInFirebaseStore,String postDataRef)
    {
        //Save location to firebase
        //TODO: delete locationID, type;
        try {
            String mUserId = GinFunctions.getUserId();
            properties mproperties =
                    new properties(mUserId
                            ,fragment_trafficPostAddNew_txtcontent.getText().toString()
                            ,(int)fragment_trafficPostAddNew_rating_assess.getRating(),imagesPathInFirebaseStore,true);
            writeGeoFire(locationDetailId,mlocation,postGeoReferences);

            writeNewPostToFireStore(locationDetailId,mproperties,GinFunctions.getFireStoreDBRefForPostDetail(postSubType));
            Toast.makeText(context, R.string.post_success,Toast.LENGTH_SHORT).show();
            imagePost = null;
        }catch (Exception e){

        }
    }
    private void writeNewPostToFirebase(String postId,String ref){
        DatabaseReference mref = FirebaseDatabase.getInstance().getReference(ref);
        mref.child(postId).setValue(ServerValue.TIMESTAMP);
    }
    private void writeNewPostToFireStore(String postId, properties postDetail, String collection)
    {
       //mDatabase.child(references).child(trafficPostId).child(ConstantUtils.REF_POST_CONTENT).setValue(postDetail);
        com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHash geoHash = new com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHash(new GeoLocation(mlocation.latitude,mlocation.longitude));
        postDetail.setG(geoHash.getGeoHashString().substring(0,ConstantUtils.LENG_FOR_GEO_HASH));
        postDetail.setTime(DateUtils.getDateTimeForGeoHashQuery());
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(collection).document(postId).set(postDetail);
    }

    private void writeGeoFire(String locationDetailId,LatLng mlocation,String geoReference)
    {
        DatabaseReference mref = FirebaseDatabase.getInstance().getReference(geoReference);
        GeoFire geoFire  = new GeoFire(mref);
        geoFire.setLocation(locationDetailId,new GeoLocation(mlocation.latitude,mlocation.longitude));
    }

    private void userTakePictureToPost() {
        try {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) getContext(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
            else
            {
                Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT, null);
                galleryintent.setType("image/*");
                //galleryintent.putExtra("GALLERY","gallery");
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        //cameraIntent.putExtra("CAMERA","camera");
                Intent chooser = new Intent(Intent.ACTION_CHOOSER);
                chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
                chooser.putExtra(Intent.EXTRA_TITLE, "Take or select a photo");

                Intent[] intentArray =  {cameraIntent};
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooser,CAMERA_REQUEST_CODE);
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST_CODE && resultCode == context.RESULT_OK)
        {
            if (data == null) {
                return;
            }
            try {
                //String type = data.getStringExtra("CAMERA");
                InputStream inputStream = null;
                try {
                    inputStream = context.getApplicationContext().getContentResolver().openInputStream(data.getData());
                }catch (Exception e){

                }
                 if(inputStream == null){
                     imagePost = (Bitmap)data.getExtras().get("data");
                 }else{
                     imagePost = BitmapFactory.decodeStream(inputStream);
                 }

                fragment_trafficPostAddNew_imvImagePost.setVisibility(View.VISIBLE);

                fragment_trafficPostAddNew_imvImagePost.setImageBitmap(imagePost);

            }catch (Exception e){

            }

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,CAMERA_REQUEST_CODE);
                }
            }
        }
    }
}
