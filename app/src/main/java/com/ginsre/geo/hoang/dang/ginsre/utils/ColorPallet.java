package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.graphics.Color;

import java.util.Random;

/**
 * Created by trach on 6/12/2017.
 */

public class ColorPallet {
    /** BLUE **/
    public static final int BLUE = 0xFF5EADD6;
    /** DARK_PURPLE **/
    public static final int DARK_PURPLE = 0xFF8781C0;
    /** GREEN **/
    public static final int GREEN = 0xFF4AAC94;
    /** LIGHT_PURPLE **/
    public static final int LIGHT_PURPLE = 0xFFBD63A0;
    /** ORANGE **/
    public static final int ORANGE = 0xFFF58B41;
    /** RED **/
    public static final int RED = 0xFFE35E59;

    /** COLORS **/
    public static final int[] COLORS = new int[]{BLUE, DARK_PURPLE, GREEN, LIGHT_PURPLE, ORANGE, RED};

    /**
     * @return color
     */
    public static int randomColor() {
        return COLORS[new Random().nextInt(COLORS.length)];
    }

    /**
     * @param index index
     * @return color
     */
    public static int color(int index) {
        if (index >= 0 && index < COLORS.length) {
            return COLORS[index];
        }
        return Color.TRANSPARENT;
    }
}
