package com.ginsre.geo.hoang.dang.ginsre.geofire;

interface EventRaiser {
    void raiseEvent(Runnable r);
}
