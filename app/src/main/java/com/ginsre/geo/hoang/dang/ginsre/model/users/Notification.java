package com.ginsre.geo.hoang.dang.ginsre.model.users;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by hoang on 11/30/2017.
 */

public class Notification {
    private int iconId;
    private String context;
    private String dateTime;
    private String key;
    private boolean readed;
    private LatLng latLng;
    private String userId;

    public Notification(){}

    public Notification(String key,boolean readed){
        this.key = key;
        this.readed = readed;
    }

    public Notification(int iconId, String context, String dateTime) {
        this.iconId = iconId;
        this.context = context;
        this.dateTime = dateTime;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getContext(Context context) {
        return com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getNotificationContextBaseOnPrefix(context,key);
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getDateTime(Context context) {
        return com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils.getTimeFromCurrentTime(key.split("-")[0],context);
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }

    public LatLng getLatLng() {
        try {
            String string[] = key.split("-");
            double result[] = com.ginsre.geo.hoang.dang.ginsre.maps.GeoHash.decodeGeohash(string[2]);
            return new LatLng(result[0],result[1]);
        }catch (Exception e){
            return null;
        }
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getContext() {
        return context;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
