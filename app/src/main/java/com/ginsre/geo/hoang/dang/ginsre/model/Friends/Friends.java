package com.ginsre.geo.hoang.dang.ginsre.model.Friends;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.clustering.ClusterItem;
import com.ginsre.geo.hoang.dang.ginsre.clustering.ui.IconGenerator;
import com.ginsre.geo.hoang.dang.ginsre.utils.BitmapUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DisplayUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by hoang on 10/6/2017.
 */

public class Friends implements ClusterItem {
    String friendsId;
    String friendsName;
    String friendsEmail;
    String friendsPhoneNum;
    LatLng friendsAdress;
    boolean isCanSeeRealTimePosition;
    boolean isCanGetAddress;
    String friendsIsActive;
    String friendsUriAvatar;
    int friendsNumPriority;
    Drawable pictureDataDrawable;
    private LatLng latLng;

    public Friends(){}
    public Friends(String friendsId,String friendsName,String friendsUriAvatar,byte[] pictureDataDrawable,int friendsNumPriority)
    {
        this.friendsId = friendsId;
        this.friendsName = friendsName;
        this.friendsUriAvatar = friendsUriAvatar;
        this.pictureDataDrawable = BitmapUtils.byteToDrawable(pictureDataDrawable);
        this.friendsNumPriority = friendsNumPriority;
    }
    public Friends(String friendsId,String friendsName,String friendsUriAvatar)
    {
        this.friendsId = friendsId;
        this.friendsName = friendsName;
        this.friendsUriAvatar = friendsUriAvatar;
    }
    public String getFriendsId() {
        return friendsId;
    }

    public void setFriendsId(String friendsId) {
        this.friendsId = friendsId;
    }

    public String getFriendsName() {
        return friendsName;
    }

    public void setFriendsName(String friendsName) {
        this.friendsName = friendsName;
    }

    public String getFriendsEmail() {
        return friendsEmail;
    }

    public void setFriendsEmail(String friendsEmail) {
        this.friendsEmail = friendsEmail;
    }

    public String getFriendsPhoneNum() {
        return friendsPhoneNum;
    }

    public void setFriendsPhoneNum(String friendsPhoneNum) {
        this.friendsPhoneNum = friendsPhoneNum;
    }

    public LatLng getFriendsAdress() {
        return friendsAdress;
    }

    public void setFriendsAdress(LatLng friendsAdress) {
        this.friendsAdress = friendsAdress;
    }

    public boolean isCanSeeRealTimePosition() {
        return isCanSeeRealTimePosition;
    }

    public void setCanSeeRealTimePosition(boolean canSeeRealTimePosition) {
        isCanSeeRealTimePosition = canSeeRealTimePosition;
    }

    public boolean isCanGetAddress() {
        return isCanGetAddress;
    }

    public void setCanGetAddress(boolean canGetAddress) {
        isCanGetAddress = canGetAddress;
    }

    public String getFriendsIsActive() {
        return friendsIsActive;
    }

    public void setFriendsIsActive(String friendsIsActive) {
        this.friendsIsActive = friendsIsActive;
    }

    public String getFriendsUriAvatar() {
        return friendsUriAvatar;
    }

    public void setFriendsUriAvatar(String friendsUriAvatar) {
        this.friendsUriAvatar = friendsUriAvatar;
    }

    public int getFriendsNumPriority() {
        return friendsNumPriority;
    }

    public void setFriendsNumPriority(int friendsNumPriority) {
        this.friendsNumPriority = friendsNumPriority;
    }

    public void setPictureData(byte[] pictureData) {
        pictureDataDrawable = BitmapUtils.byteToDrawable(pictureData);
    }
    public Drawable getPictureDataDrawable(){return pictureDataDrawable;}
    public byte[] getPictureData(){
        return GinFunctions.drawableToByteArray(pictureDataDrawable);
    }

    public void setPictureDataDrawable(Drawable pictureDataDrawable) {
        this.pictureDataDrawable = pictureDataDrawable;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public Bitmap getUserMarker(Context context){
        ImageView mImageView = new ImageView(context);
        int mDimension = (int) context.getResources().getDimension(R.dimen.friend_image);
        mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
        int padding = (int) context.getResources().getDimension(R.dimen.post_image_padding);
        mImageView.setPadding(padding, padding, padding, padding);
        IconGenerator mIconGenerator = new IconGenerator(context);
        mIconGenerator.setContentView(mImageView);
        if(pictureDataDrawable!=null){
            mImageView.setImageDrawable(pictureDataDrawable);
        }else{
            mImageView.setImageDrawable(GinFunctions.drawUserName(context
                    ,BitmapFactory.decodeResource(context.getResources()
                            ,R.drawable.user_location)
                    ,getFriendsName()
                    , DisplayUtils.dp2px(96)));
        }
        return mIconGenerator.makeIcon(friendsName);
    }

    @Override
    public LatLng getPosition() {
        return friendsAdress;
    }

    @Override
    public String getTitle() {
        return friendsName;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
