package com.ginsre.geo.hoang.dang.ginsre.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.PostTypeItemAdapter;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostType;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.FirebaseUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 11/10/2017.
 */

public class LocationAddNewFragment extends Fragment implements View.OnClickListener, OnDataChangeObserver {

    private static Activity context;
    private  static LatLng locationLatlon;
    private static String postSubType;
    Bitmap imagePost;
    RelativeLayout addNewLocation_layout;
    private static String postTypeId = "";

    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2;

    @BindView(R.id.fragment_location_new_grvSelect)
    GridView fragment_location_new_grvSelect;
    @BindView(R.id.fragment_location_new_container)
    ScrollView fragment_location_new_container;
    @BindView(R.id.fragment_location_new_imvicon)
    ImageView fragment_location_new_imvicon;
    @BindView(R.id.fragment_location_new_txvtitle)
    EditText fragment_location_new_txvtitle;
    @BindView(R.id.fragment_location_new_txtDiscription)
    EditText fragment_location_new_txtDiscription;
    @BindView(R.id.fragment_location_new_ibtnBackgound)
    ImageButton fragment_location_new_ibtnBackgound;
    @BindView(R.id.fragment_location_new_imvImagePost)
    ImageView fragment_location_new_imvImagePost;
    @BindView(R.id.fragment_location_new_txv_OpenHourFrom)
    EditText fragment_location_new_txv_OpenHourFrom;
    @BindView(R.id.fragment_location_new_txv_OpenHourTo)
    EditText fragment_location_new_txv_OpenHourTo;
    @BindView(R.id.fragment_location_new_ftbnCancel)
    FancyButton fragment_location_new_ftbnCancel;
    @BindView(R.id.fragment_location_new_fbtnOk)
    FancyButton fragment_location_new_fbtnOk;

    public static LocationAddNewFragment newInstance(Activity mcontext, LatLng location, String mpostSubType){
        LocationAddNewFragment locationAddNewFragment = new LocationAddNewFragment();
        context = mcontext;
        locationLatlon = location;
        postSubType= mpostSubType;
        return locationAddNewFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_location_new,container,false);
        ButterKnife.bind(this,rootView);
        addNewLocation_layout = (RelativeLayout) context.findViewById(R.id.addNewTrafficsPost_layout);

        return rootView;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        //GridView
        try {
            fragment_location_new_grvSelect.setAdapter(new PostTypeItemAdapter(context,postSubType));
            // add or cancel post
            fragment_location_new_fbtnOk.setOnClickListener(this);
            fragment_location_new_ftbnCancel.setOnClickListener(this);
            fragment_location_new_ibtnBackgound.setOnClickListener(this);
        }catch (Exception e)
        {
            Log.d("New Item fragment 140: ",e.getMessage());
        }
        // Data change notification
        DataChangeNotification.getInstance().addObserver(IssueKey.ON_ADD_NEW_LOCATION_POST_SELECT_TYPE, this);
    }



    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        if(viewId == R.id.fragment_location_new_ibtnBackgound)
        {
            userTakePictureToPost();
        }else if(viewId == R.id.fragment_location_new_fbtnOk){
            registerNewLocation();
        }else if(viewId == R.id.fragment_location_new_ftbnCancel){
            closeTrafficPost();
        }
    }


    /**
     *
     */

    private void registerNewLocation() {
        try {
            String postGeoReferences = GinFunctions.getDatabaseRefForGinsType(postSubType);
            //String postDataRef = GinFunctions.getDatabaseRefForPostDetail(postSubType);
            com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHash geoHash = new com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHash(
                    locationLatlon.latitude,locationLatlon.longitude);
            final String locationDetailId = DateUtils.getTimeLongForPostFromRef()+"-"+postTypeId+"-"+geoHash.getGeoHashString();
            pushImagePostToFirebase(locationDetailId,postGeoReferences);
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    /**
     *
     * @param mlocationDetailId
     * @param mLocationPostReferences
     */
    private void pushImagePostToFirebase(final String mlocationDetailId,final String mLocationPostReferences) {
        //fragment_trafficPostAddNew_imvImagePost.setDrawingCacheEnabled(true);
        //fragment_trafficPostAddNew_imvImagePost.buildDrawingCache();
        //Bitmap bitmap = fragment_trafficPostAddNew_imvImagePost.getDrawingCache();
        try {
            if(fragment_location_new_txvtitle.getText().toString().length()<3){
                Toast.makeText(context,context.getResources().getString(R.string.location_name_is_invalid),Toast.LENGTH_LONG).show();
            }else{
                if(fragment_location_new_txtDiscription.getText().toString().length()<5){
                    Toast.makeText(context,context.getResources().getString(R.string.location_description_is_invalid),Toast.LENGTH_LONG).show();
                }else{
                    if(!(imagePost == null))
                    {
                        byte[] data = GinFunctions.bitmapToByteArray(imagePost,80);
                        if(data != null)
                        {
                            final ProgressDialog progressDialog = new ProgressDialog(context);
                            progressDialog.setMessage(context.getResources().getString(R.string.user_register_location));
                            progressDialog.show();
                            StorageReference mStorageRef = (new FirebaseUtils()).getmStorageRef();
                            //StorageReference childPath = mStorageRef.child(ConstantUtils.STORE_TRAFFIC_IMAGES).child(nlocationDetailId);
                            mStorageRef.child(ConstantUtils.STORE_TRAFFIC_IMAGES).child(mlocationDetailId).putBytes(data)
                                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            Uri downloadUri = taskSnapshot.getDownloadUrl();
                                            progressDialog.dismiss();
                                            showDialogConfirm(mlocationDetailId,mLocationPostReferences,downloadUri.toString());
                                            //pushLocationAndContainToFirebase(mlocationDetailId,mLocationPostReferences,downloadUri.toString());
                                        }
                                    });
                        }
                    }else{
                        showDialogConfirm(mlocationDetailId,mLocationPostReferences,"");
                        //pushLocationAndContainToFirebase(mlocationDetailId,mLocationPostReferences,null);
                    }
                }
            }
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }


    /**
     *
     * @param mlocationDetailId
     * @param mLocationPostReferences
     * @param downloadUrl
     */
    private void showDialogConfirm(final String mlocationDetailId,final String mLocationPostReferences,final String downloadUrl){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.Confirm_dialog_box));
        builder.setMessage(context.getResources().getString(R.string.location_confirm_owner));
        builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                pushLocationAndContainToFirebase(mlocationDetailId,mLocationPostReferences,downloadUrl,true);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                pushLocationAndContainToFirebase(mlocationDetailId,mLocationPostReferences,downloadUrl,false);
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     *
     * @param mlocationDetailId
     * @param mLocationPostReferences
     * @param downloadUrl
     * @param owner
     */
    private void pushLocationAndContainToFirebase(String mlocationDetailId, String mLocationPostReferences, String downloadUrl,boolean owner) {
        try{
            com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.properties properties = new com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.properties();
            properties.setLocation_name(fragment_location_new_txvtitle.getText().toString());
            properties.setLoaction_description(fragment_location_new_txtDiscription.getText().toString());
            //locationPage.setLocation_id(mlocationDetailId);
            if(fragment_location_new_txv_OpenHourFrom.getText().toString().length()>0){
                properties.setOpenHourStart(fragment_location_new_txv_OpenHourFrom.getText().toString());
            }
            if(fragment_location_new_txv_OpenHourTo.getText().toString().length()>0){
                properties.setOpenHourClose(fragment_location_new_txv_OpenHourTo.getText().toString());
            }
            properties.setBackGroundUrl(downloadUrl);
            properties.setUserPostOwn(owner);
            properties.setUserId(com.google.firebase.auth.FirebaseAuth.getInstance().getUid());
            com.google.firebase.database.DatabaseReference mref = com.google.firebase.database.FirebaseDatabase.getInstance().getReference(mLocationPostReferences);
            com.ginsre.geo.hoang.dang.ginsre.geofire.GeoFire geoFire  = new com.ginsre.geo.hoang.dang.ginsre.geofire.GeoFire (mref);
            geoFire.setPlaceLocationHaveProperties(mlocationDetailId,new com.ginsre.geo.hoang.dang.ginsre.geofire.GeoLocation(locationLatlon.latitude,locationLatlon.longitude),properties);
            //
            pushLocationAndContainToFirestore(mlocationDetailId,properties);

            Toast.makeText(context,context.getResources().getString(R.string.location_have_been_register),Toast.LENGTH_LONG).show();
            closeTrafficPost();
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }


    /**
     *
     * @param mlocationDetailId
     * @param properties
     */
    private void pushLocationAndContainToFirestore(String mlocationDetailId, com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.properties properties) {
        try{

            com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
            db.collection(ConstantUtils.REF_LOCATION_PAGE).document(mlocationDetailId).set(properties);
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    /**
     *
     */
    public void closeTrafficPost(){
        addNewLocation_layout.setVisibility(View.GONE);
        fragment_location_new_grvSelect.setVisibility(View.GONE);
        fragment_location_new_container.setVisibility(View.GONE);
        FancyButton main_fbtn_addNewPost =(FancyButton)context.findViewById(R.id.main_fbtn_addNewPost);
        main_fbtn_addNewPost.setIconResource(R.drawable.addnew);
        main_fbtn_addNewPost.setTag("0");
    }

    @Override
    public void onDataChanged(IssueKey key, Object object) {
        if(IssueKey.ON_ADD_NEW_LOCATION_POST_SELECT_TYPE == key)
        {
            PostType postType = (PostType)object;
            postTypeId = postType.getPostTypeId();
            addNewLoaction(postType);
        }
    }

    /**
     *
     * @param postType
     */
    private void addNewLoaction(PostType postType) {
        fragment_location_new_grvSelect.setVisibility(View.GONE);
        fragment_location_new_imvicon.setImageDrawable(postType.getPostTypeImage(context));
        fragment_location_new_container.setVisibility(View.VISIBLE);
        //fragment_location_new_txvtitle.setText(StringUtils.upercaseString(postType.getPostTypeName()));
    }

    /**
     *
     */
    private void userTakePictureToPost() {
        try {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) getContext(),
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
            else
            {
                Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT, null);
                galleryintent.setType("image/*");
                //galleryintent.putExtra("GALLERY","gallery");
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //cameraIntent.putExtra("CAMERA","camera");
                Intent chooser = new Intent(Intent.ACTION_CHOOSER);
                chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
                chooser.putExtra(Intent.EXTRA_TITLE, "Take or select a photo");

                Intent[] intentArray =  {cameraIntent};
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooser,CAMERA_REQUEST_CODE);
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST_CODE && resultCode == context.RESULT_OK)
        {
            if (data == null) {
                return;
            }
            try {

                /*if(data.getStringExtra("CAMERA").equals("camera")){
                    photo = (Bitmap)data.getExtras().get("data");

                }else {*/
                //String type = data.getStringExtra("CAMERA");
                InputStream inputStream = null;
                try {
                    inputStream = context.getApplicationContext().getContentResolver().openInputStream(data.getData());
                }catch (Exception e){

                }
                if(inputStream == null){
                    imagePost = (Bitmap)data.getExtras().get("data");
                }else{
                    imagePost = BitmapFactory.decodeStream(inputStream);
                }

                //ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //photo.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                fragment_location_new_imvImagePost.setVisibility(View.VISIBLE);

                fragment_location_new_imvImagePost.setImageBitmap(imagePost);

            }catch (Exception e){

            }

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,CAMERA_REQUEST_CODE);
                }
            }
        }
    }
}
