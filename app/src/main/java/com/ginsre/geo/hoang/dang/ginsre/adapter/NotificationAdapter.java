package com.ginsre.geo.hoang.dang.ginsre.adapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.activity.MainActivity;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.FriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.users.INotification;
import com.ginsre.geo.hoang.dang.ginsre.model.users.IUser;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Notification;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DialogFriendSetting;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.UndoAdapter;
import com.nhaarman.listviewanimations.util.Swappable;

import java.util.ArrayList;
import java.util.Collections;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 11/30/2017.
 */

public class NotificationAdapter extends BaseAdapter implements
        Swappable, UndoAdapter, OnDismissCallback, View.OnClickListener {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Notification> mNotificationModelList;

    public NotificationAdapter(Context context,
                               ArrayList<Notification> notificationModelList) {
        mContext = context;
        mInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mNotificationModelList = notificationModelList;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getCount() {
        return mNotificationModelList.size();
    }

    @Override
    public Object getItem(int i) {
        return mNotificationModelList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void swapItems(int positionOne, int positionTwo) {
        Collections.swap(mNotificationModelList, positionOne, positionTwo);
    }

    @Override
    public void onDismiss(@NonNull ViewGroup viewGroup, @NonNull int[] reverseSortedPositions) {
        for (int position : reverseSortedPositions) {
            remove(position);
        }
    }

    public void remove(int position) {
        mNotificationModelList.remove(position);
        removeANotificationInFirebase(mNotificationModelList.get(position));
    }



    @NonNull
    @Override
    public View getUndoView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(
                    R.layout.list_item_undo_view, parent, false);
        }
        return view;
    }

    @NonNull
    @Override
    public View getUndoClickView(@NonNull View convertView) {
        return convertView.findViewById(R.id.undo_button_notification);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.item_notification_dimiss, viewGroup, false);
            viewHolder = new ViewHolder(convertView);
            viewHolder.onBindView(mNotificationModelList.get(position));
            convertView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.list_item_notification_context.setTag(position);
        viewHolder.list_item_notification_icon.setTag(mNotificationModelList.get(position).getKey());
        return convertView;
    }

    private class ViewHolder{
        ImageView list_item_notification_icon;
        TextView list_item_notification_context,list_item_notification_time;
        LinearLayout list_item_notification_click_detail;
        FancyButton list_item_notification_viewLocation;
        LinearLayout list_item_notification_background;

        ViewHolder(View view){
            addControl(view);
            addEvent();
        }

        /**
         *
         * @param view
         */
        public void addControl(View view){
            list_item_notification_icon = (ImageView) view.findViewById(R.id.list_item_notification_icon);
            list_item_notification_context = (TextView)view.findViewById(R.id.list_item_notification_context);
            list_item_notification_time = (TextView)view.findViewById(R.id.list_item_notification_time);
            list_item_notification_viewLocation = (FancyButton)view.findViewById(R.id.list_item_notification_viewLocation);
            list_item_notification_click_detail = (LinearLayout)view.findViewById(R.id.list_item_notification_click_detail);
            list_item_notification_background = (LinearLayout)view.findViewById(R.id.list_item_notification_background);
        }

        /**
         *
         */
        private void addEvent() {
            list_item_notification_viewLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setReadedNotificationToFirebase((String)list_item_notification_icon.getTag());
                    setIconIsReaded();
                    // Back to main activity and view post location;
                    Intent intent = new Intent(mContext, MainActivity.class);
                    String key  = ((String)list_item_notification_icon.getTag()).split(">")[0];
                    intent.putExtra(ConstantUtils.VIEW_POST_LOCATION,key);
                    mContext.startActivity(intent);
                }
            });
            list_item_notification_click_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                try {
                    String key = (String)list_item_notification_icon.getTag();
                    if(key.split("-").length == 2){
                        //TODO: detail notification; post related or friends
                        String typeNoti = key.split("-")[1];
                        if(typeNoti.equalsIgnoreCase(ConstantUtils.NOTIFI_FREQUEST)){
                            if(!mNotificationModelList.get((int)list_item_notification_context.getTag()).isReaded()){
                                DialogFriendSetting dialogFriendSetting = new DialogFriendSetting();
                                FriendShip friendShip = new FriendShip();
                                friendShip.setId(mNotificationModelList.get((int)list_item_notification_context.getTag()).getUserId());
                                friendShip.setSeePosi(false);
                                dialogFriendSetting.showDialog((Activity)mContext,friendShip,ConstantUtils.DIALOG_CONFIRM);
                            }
                        }
                    }else{
                        String postID  = key.split(">")[0];
                        DataChangeNotification.getInstance().notifyChange(IssueKey.VIEW_POST_DETAIL,postID);
                    }
                    setReadedNotificationToFirebase((String)list_item_notification_icon.getTag());
                    setIconIsReaded();
                }catch (Exception e){

                }
                }
            });
        }

        /**
         *
         * @param notification
         */
        public void onBindView(com.ginsre.geo.hoang.dang.ginsre.model.users.Notification notification){
            if(notification.isReaded()) {
                setIconIsReaded();
            }
            if(notification.getKey().split("-").length == 2)
                list_item_notification_viewLocation.setVisibility(View.GONE);
            String context = "Ginser "+notification.getContext(mContext);
            list_item_notification_context.setText(context);
            if(notification.getUserId() != null){
                setIconForUserPost(notification,list_item_notification_icon,list_item_notification_context);
            }
            list_item_notification_time.setText(notification.getDateTime(mContext));
        }

        private void setIconForUserPost(final com.ginsre.geo.hoang.dang.ginsre.model.users.Notification notification, final ImageView mItem_find_new_friend_imvAvatar, final TextView list_item_notification_context) {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(notification.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.child(IUser.uAvatarUri).getValue() != null){
                        ImageUtil.displayRoundImage(mItem_find_new_friend_imvAvatar,(String)dataSnapshot.child(IUser.uAvatarUri).getValue(),null);
                    }
                    if(dataSnapshot.child(IUser.uName).getValue() != null) {
                        String context = dataSnapshot.child(IUser.uName).getValue()+" "+notification.getContext(mContext);
                        list_item_notification_context.setText(context);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

        public void setIconIsReaded(){
            list_item_notification_background.setBackgroundColor(Color.WHITE);
        }
    }

    /**
     * Set readed Notification to firebase;
     * @param postKey
     */
    private void setReadedNotificationToFirebase(final String postKey){
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            String userId = mAuth.getCurrentUser().getUid();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_NOTIFICATION).child(userId)
                    .child(postKey).child(INotification.readed).setValue(true);
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    /**
     *
     * @param notification
     */
    private void removeANotificationInFirebase(Notification notification)
    {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            String userId = mAuth.getCurrentUser().getUid();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_NOTIFICATION).child(userId).child(notification.getKey()).removeValue();
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }
}
