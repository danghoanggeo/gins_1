package com.ginsre.geo.hoang.dang.ginsre.base;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.activity.FriendsActivity;
import com.ginsre.geo.hoang.dang.ginsre.activity.LoginActivity;
import com.ginsre.geo.hoang.dang.ginsre.activity.FriendsNetActivity;
import com.ginsre.geo.hoang.dang.ginsre.activity.MainActivity;
import com.ginsre.geo.hoang.dang.ginsre.activity.NotificationActivity;
import com.ginsre.geo.hoang.dang.ginsre.activity.RegisterActivity;
import com.ginsre.geo.hoang.dang.ginsre.activity.SearchLocationActivity;
import com.ginsre.geo.hoang.dang.ginsre.adapter.LeftMenuAdapter;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.common.SharedPrefenceKey;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoLocation;
import com.ginsre.geo.hoang.dang.ginsre.model.core.geometry;
import com.ginsre.geo.hoang.dang.ginsre.model.users.IUser;
import com.ginsre.geo.hoang.dang.ginsre.model.users.UserPlaces;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Users;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.utils.AnimationUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DisplayUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.ginsre.geo.hoang.dang.ginsre.utils.PreferencesUtils;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 9/10/2017.
 */

public class BaseActivity  extends AppCompatActivity implements View.OnClickListener, OnDataChangeObserver {

    private static int mStackActivityCount = 0;
    protected GoogleMap mMap;
    protected LatLng cameraLocation,mLastKnownLocation;//,currentLocation;
    protected LinearLayout footer_layout;
    protected RelativeLayout header_layout;
    // header
    protected LinearLayout header_custom_menu;
    private ImageButton header_imvMenu;
    private ImageView header_message,header_notification,header_search;
    private TextView header_txtSearch;
    protected boolean flag_control_is_visible  = true;
    protected FancyButton main_fbtn_mylocation,main_fbtn_addNewPost,main_fbtn_trafficLayers,main_fbtn_direction,main_txvDateTime,main_fbtnDateTimeStart;
    protected ImageView main_imvViewAllPostDetail;
    // Footer control
    private FancyButton footer_ibtn_social,footer_ibtn_traffic,footer_ibtn_location
            ,footer_ibtn_friends,footer_ibtn_gint;
    // Menu
    protected TextView menu_txvUserName,menu_txvNumOfFriendNoti,menu_txvNumOfGroupNoti;
    protected ImageView menu_imvUserAvatar,menu_addHomeLocation;
    protected LinearLayout menu_myPlaces,menu_myPlacesExtend,
            menu_myPlaceHome,menu_myPlaceWork,menu_myPlaceFavorites,menu_setting,menu_aboutApp,menu_fbtn_signout;
    protected RelativeLayout menu_myFriends,menu_myGroups;
    // fag of activity is present;
    protected String flag_activity_choose_present ; // is main activity


    protected DrawerLayout mDrawerLayout;

    // [START declare_auth]
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Face book login
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        ActivityManager.getInstance().onCreate(this);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        PreferencesUtils.init(this);
    }
    protected void extractHeaderFooterToActivity()
    {
        addControl();
        addEvent();
        init();
    }

    private void init() {
        //setImagesActiveOrUnactive();
        //User
        setNumOfMessAndNotiToMenu();
        flag_activity_choose_present = PreferencesUtils.getString(SharedPrefenceKey.LAST_GINS_TYPE_SELECT.name(),"");
        if(flag_activity_choose_present.equals(""))
            flag_activity_choose_present = ConstantUtils.GINS_MAIN_TYPE[1];
        setFooterIbtnActive(flag_activity_choose_present);
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(BaseActivity.this));
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(GinFunctions.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        Users users = dataSnapshot.getValue(Users.class);
                        if(users == null){
                            Intent intent = new Intent(BaseActivity.this,RegisterActivity.class);
                            startActivity(intent);
                            /*
                            if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                                GinFunctions.addNewUserToFirebase(getApplicationContext());
                            }
                            menu_txvUserName.setText(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                            ImageUtil.displayRoundImage(menu_imvUserAvatar,FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString(),null);*/
                        }else {
                            menu_txvUserName.setText(users.getuName());
                            ImageUtil.displayRoundImage(menu_imvUserAvatar,users.getuAvUrl(),null);
                        }
                    }catch (Exception e){

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
           if(!PreferencesUtils.getBoolean(SharedPrefenceKey.REMIND_UPDATE_HOME_LOCATION.name(),false))
           {
               mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_PLACES).child(GinFunctions.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(DataSnapshot dataSnapshot) {
                       if(dataSnapshot == null){
                           showAlertDialogBoxToGetUserAction(
                                   getResources().getString(R.string.setYour_home_location),
                                   ConstantUtils.USER_REF_USER_PLACES_HOME,
                                   SharedPrefenceKey.USER_HOME_LOCATION_LATITURE.name(),
                                   SharedPrefenceKey.USER_HOME_LOCATION_LONGTITUTE.name()
                           );
                           PreferencesUtils.edit().putBoolean(SharedPrefenceKey.REMIND_UPDATE_HOME_LOCATION.name(),true);
                       }
                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               });
           }

                //menu_txvUserName.setText(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                //ImageUtil.displayRoundImage(menu_imvUserAvatar,FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString(),null);
            /*Picasso.with(getApplicationContext())
                    .load(currentUser.getPhotoUrl())
                    .fit()
                    .centerCrop()
                    .into(menu_imvUserAvatar);*/
        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }

    }

    protected void addControl() {
        DisplayUtils.init(this);
        main_fbtn_mylocation = (FancyButton) findViewById(R.id.main_fbtn_mylocation);
        main_fbtn_addNewPost = (FancyButton) findViewById(R.id.main_fbtn_addNewPost);
        main_fbtn_trafficLayers = (FancyButton)findViewById(R.id.main_fbtn_trafficLayers);
        main_fbtn_direction = (FancyButton)findViewById(R.id.main_fbtn_direction);
        main_txvDateTime = (FancyButton)findViewById(R.id.main_fbtnDateTime);
        main_fbtnDateTimeStart  = (FancyButton)findViewById(R.id.main_fbtnDateTimeStart);
        main_imvViewAllPostDetail = (ImageView)findViewById(R.id.main_imvViewAllPostDetail);
        //header
        footer_layout = (LinearLayout)findViewById(R.id.layout_footer);
        header_layout = (RelativeLayout)findViewById(R.id.layout_info);
        header_search = (ImageView) findViewById(R.id.header_search);
        header_message = (ImageView) findViewById(R.id.header_message);
        header_message.setOnClickListener(this);
        header_notification = (ImageView)findViewById(R.id.header_notification);
        header_notification.setOnClickListener(this);
        header_search.setOnClickListener(this);
        header_txtSearch = (TextView) findViewById(R.id.header_txtSearch);
        header_imvMenu = (ImageButton)findViewById(R.id.header_imvMenu);
        header_imvMenu.setOnClickListener(this);
        header_custom_menu =(LinearLayout)findViewById(R.id.header_custom_menu);

        //get width and height of the screen;
        int width = GinFunctions.getDeviceWidthHeight(this)[0];

        //footer_margin_left;
        main_imvViewAllPostDetail.setY((GinFunctions.getDeviceWidthHeight(this)[1]/2)-DisplayUtils.dp2px(36));
        //footer control
        footer_ibtn_social = (FancyButton)findViewById(R.id.footer_ibtn_social);
        footer_ibtn_traffic = (FancyButton)findViewById(R.id.footer_ibtn_traffic);
        footer_ibtn_location = (FancyButton)findViewById(R.id.footer_ibtn_location);
        footer_ibtn_friends = (FancyButton)findViewById(R.id.footer_ibtn_friends);
        footer_ibtn_gint = (FancyButton)findViewById(R.id.footer_ibtn_gint);

        // Menu Control;

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);
        //mDrawerLayout.openDrawer(mDrawerList);

        menu_txvUserName = (TextView)findViewById(R.id.menu_txvUserName);
        menu_txvNumOfFriendNoti = (TextView)findViewById(R.id.menu_txvNumOfFriendNoti);
        menu_txvNumOfGroupNoti = (TextView)findViewById(R.id.menu_txvNumOfGroupNoti);
        menu_imvUserAvatar = (ImageView) findViewById(R.id.menu_imvUserAvatar);
        menu_myPlaces = (LinearLayout)findViewById(R.id.menu_myPlaces);
        menu_myPlaces.setOnClickListener(this);
        menu_addHomeLocation = (ImageView)findViewById(R.id.menu_addHomeLocation);
        menu_addHomeLocation.setOnClickListener(this);
        menu_myPlacesExtend = (LinearLayout)findViewById(R.id.menu_myPlacesExtend);
        menu_myPlaceHome= (LinearLayout)findViewById(R.id.menu_myPlaceHome);
        menu_myPlaceHome.setOnClickListener(this);
        menu_myPlaceWork = (LinearLayout)findViewById(R.id.menu_myPlaceWork);
        menu_myPlaceWork.setOnClickListener(this);
        menu_myPlaceFavorites = (LinearLayout)findViewById(R.id.menu_myPlaceFavorites);
        menu_myPlaceFavorites.setOnClickListener(this);
        menu_myFriends= (RelativeLayout) findViewById(R.id.menu_myFriends);
        menu_myFriends.setOnClickListener(this);
        menu_myGroups = (RelativeLayout)findViewById(R.id.menu_myGroups);
        menu_myGroups.setOnClickListener(this);
        menu_setting = (LinearLayout)findViewById(R.id.menu_setting);
        menu_setting.setOnClickListener(this);
        menu_aboutApp = (LinearLayout)findViewById(R.id.menu_aboutApp);
        menu_aboutApp.setOnClickListener(this);
        menu_fbtn_signout = (LinearLayout) findViewById(R.id.menu_fbtn_signout);
        menu_fbtn_signout.setOnClickListener(this);
    }

    protected void addEvent() {

        footer_ibtn_social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            try {
                if(footer_ibtn_social.getTag() == null || footer_ibtn_social.getTag().toString().equals("0"))
                {
                    setOtherActiveUnActive();
                    setFooterIbtnActive(ConstantUtils.GINS_MAIN_TYPE[0]);
                    changeGeoQueryNotification(ConstantUtils.GINS_MAIN_TYPE[0]);
                }
            }
            catch (Exception e)
            {
                com.google.firebase.crash.FirebaseCrash.log("BaseActivity- Line: 227: "+e.getMessage());
            }
            }
        });
        footer_ibtn_traffic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            try {
                if(footer_ibtn_traffic.getTag() == null || footer_ibtn_traffic.getTag().toString().equals("0"))
                {
                    setOtherActiveUnActive();
                    setFooterIbtnActive(ConstantUtils.GINS_MAIN_TYPE[1]);
                    changeGeoQueryNotification(ConstantUtils.GINS_MAIN_TYPE[1]);
                }
            }
            catch (Exception e)
            {
                //TODO: LOG
            }
            }
        });
        footer_ibtn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            try {
                if(footer_ibtn_location.getTag() == null || footer_ibtn_location.getTag().toString().equals("0"))
                {
                    setOtherActiveUnActive();
                    setFooterIbtnActive(ConstantUtils.GINS_MAIN_TYPE[2]);
                    changeGeoQueryNotification(ConstantUtils.GINS_MAIN_TYPE[2]);
                }
            }
            catch (Exception e)
            {
              // TODO: LOG
            }
            }
        });

        footer_ibtn_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            try {
                if(footer_ibtn_friends.getTag() == null || footer_ibtn_friends.getTag().toString().equals("0"))
                {
                    setOtherActiveUnActive();
                    setFooterIbtnActive(ConstantUtils.GINS_MAIN_TYPE[3]);
                    changeGeoQueryNotification(ConstantUtils.GINS_MAIN_TYPE[3]);
                }
            }
            catch (Exception e)
            {
                // TODO: LOG
            }
            }
        });
        footer_ibtn_gint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(footer_ibtn_gint.getTag() == null || footer_ibtn_gint.getTag().toString().equals("0"))
                    {
                        setOtherActiveUnActive();
                        setFooterIbtnActive(ConstantUtils.GINS_MAIN_TYPE[4]);
                        changeGeoQueryNotification(ConstantUtils.GINS_MAIN_TYPE[4]);
                    }
                }
                catch (Exception e)
                {
                    // TODO: LOG
                }
            }
        });
    }

    protected void setFooterIbtnActive(String ginsType) {
        if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[0]))
        {
            footer_ibtn_social.setIconResource(R.drawable.social_active);
            footer_ibtn_social.setTextColor(getResources().getColor(R.color.acvite));
            flag_activity_choose_present = ConstantUtils.GINS_MAIN_TYPE[0];
            footer_ibtn_social.setTag("1");
        }else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[1])){
            footer_ibtn_traffic.setIconResource(R.drawable.traffic_active);
            footer_ibtn_traffic.setTextColor(getResources().getColor(R.color.acvite));
            flag_activity_choose_present = ConstantUtils.GINS_MAIN_TYPE[1];
            footer_ibtn_traffic.setTag("1");
        }else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[2])){
            footer_ibtn_location.setIconResource(R.drawable.location_active);
            footer_ibtn_location.setTextColor(getResources().getColor(R.color.acvite));
            flag_activity_choose_present = ConstantUtils.GINS_MAIN_TYPE[2];
            footer_ibtn_location.setTag("1");
        }else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[3])){
            footer_ibtn_friends.setIconResource(R.drawable.weather_active);
            footer_ibtn_friends.setTextColor(getResources().getColor(R.color.acvite));
            flag_activity_choose_present = ConstantUtils.GINS_MAIN_TYPE[3];
            footer_ibtn_friends.setTag("1");
        }else if(ginsType.equals(ConstantUtils.GINS_MAIN_TYPE[4])){
            footer_ibtn_gint.setIconResource(R.drawable.friends_active);
            footer_ibtn_gint.setTextColor(getResources().getColor(R.color.acvite));
            flag_activity_choose_present = ConstantUtils.GINS_MAIN_TYPE[4];
            footer_ibtn_gint.setTag("1");
        }

    }

    private void changeGeoQueryNotification(String s) {
        DataChangeNotification.getInstance().notifyChange(IssueKey.CHANGE_GEO_QUERY_NOTI,s);
    }

    private void setOtherActiveUnActive() {
        try {
            if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[0])){
                footer_ibtn_social.setIconResource(R.drawable.social_unactive);
                footer_ibtn_social.setTextColor(getResources().getColor(R.color.unactive));
                footer_ibtn_social.setTag("0");
            }else if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[1])){
                footer_ibtn_traffic.setIconResource(R.drawable.traffic_unactive);
                footer_ibtn_traffic.setTextColor(getResources().getColor(R.color.unactive));
                footer_ibtn_traffic.setTag("0");
            }else if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[2])){
                footer_ibtn_location.setIconResource(R.drawable.location_unactive);
                footer_ibtn_location.setTextColor(getResources().getColor(R.color.unactive));
                footer_ibtn_location.setTag("0");
            }else if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[3])){
                footer_ibtn_friends.setIconResource(R.drawable.weather_unactive);
                footer_ibtn_friends.setTextColor(getResources().getColor(R.color.unactive));
                footer_ibtn_friends.setTag("0");
            }else if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[4])){
                footer_ibtn_gint.setIconResource(R.drawable.friends_unactive);
                footer_ibtn_gint.setTextColor(getResources().getColor(R.color.unactive));
                footer_ibtn_gint.setTag("0");
            }
        }
        catch (Exception e)
        {

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getLastKnowLocation();
        mStackActivityCount++;
        if(mStackActivityCount == 1){
        }
        //mLastKnownLocation.latitude = PreferencesUtils.getFloat(SharedPrefenceKey.LAST_KNOW_LOCATION)
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLastKnowLocation();
        ActivityManager.getInstance().onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        PreferencesUtils.edit().putString(SharedPrefenceKey.LAST_GINS_TYPE_SELECT.name(),flag_activity_choose_present);
        saveLastKnowLocation(SharedPrefenceKey.LAST_KNOW_LOCATION_LATITUTE.name(),SharedPrefenceKey.LAST_KNOW_LOCATION_LONGTITUTE.name());
        ActivityManager.getInstance().onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mStackActivityCount--;
        PreferencesUtils.edit().putString(SharedPrefenceKey.LAST_GINS_TYPE_SELECT.name(),flag_activity_choose_present);
        saveLastKnowLocation(SharedPrefenceKey.LAST_KNOW_LOCATION_LATITUTE.name(),SharedPrefenceKey.LAST_KNOW_LOCATION_LONGTITUTE.name());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferencesUtils.edit().putString(SharedPrefenceKey.LAST_GINS_TYPE_SELECT.name(),flag_activity_choose_present);
        saveLastKnowLocation(SharedPrefenceKey.LAST_KNOW_LOCATION_LATITUTE.name(),SharedPrefenceKey.LAST_KNOW_LOCATION_LONGTITUTE.name());
        PreferencesUtils.edit().putBoolean(SharedPrefenceKey.REMIND_UPDATE_HOME_LOCATION.name(),false);
        ActivityManager.getInstance().onDestroy(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case (R.id.header_imvMenu): {
                //setNumOfMessAndNotiToMenu();
                if(mDrawerLayout.isDrawerOpen(header_custom_menu))
                    mDrawerLayout.closeDrawer(header_custom_menu);
                else
                    mDrawerLayout.openDrawer(header_custom_menu);
                //setHeaderMenuInOut(false);
                break;
            }
            case (R.id.header_search):
            {
                startActivity(new Intent(BaseActivity.this,SearchLocationActivity.class));
                break;
            }
            case R.id.header_message:{
                setNumOfNotifiToZero(IUser.numOfMessage);
                break;
            }
            case R.id.header_notification:{
                setNumOfNotifiToZero(IUser.numOfNotis);
                startNewActivity(new Intent(BaseActivity.this, NotificationActivity.class));
                break;
            }
            case R.id.menu_addHomeLocation:
            {
                showAlertDialogBoxToGetUserAction(
                        getResources().getString(R.string.Are_you_sure_to_set_home_location),
                        ConstantUtils.USER_REF_USER_PLACES_HOME,
                        SharedPrefenceKey.USER_HOME_LOCATION_LATITURE.name(),
                        SharedPrefenceKey.USER_HOME_LOCATION_LONGTITUTE.name()
                );
                mDrawerLayout.closeDrawer(header_custom_menu);
                break;
            }
            case (R.id.menu_myPlaceHome):
            {
                showAllUserPlace(ConstantUtils.USER_REF_USER_PLACES_HOME);
                mDrawerLayout.closeDrawer(header_custom_menu);
                break;
            }
            case (R.id.menu_myPlaceWork):
            {
                showAlertDialogBoxToGetUserAction(
                        getResources().getString(R.string.Are_you_sure_to_set_work_location),
                        ConstantUtils.USER_REF_USER_PLACES_WORK,
                        SharedPrefenceKey.USER_WORK_LOCATION_LAT.name(),
                        SharedPrefenceKey.USER_WORK_LOCATION_LON.name()
                );
                mDrawerLayout.closeDrawer(header_custom_menu);
                break;
            }
            case (R.id.menu_myPlaceFavorites):
            {
                mDrawerLayout.closeDrawer(header_custom_menu);
                //TODO (3) call to the My_place_activity and sent request my favorite places;

                break;
            }
            case (R.id.menu_timeLineSharing):
            {
                mDrawerLayout.closeDrawer(header_custom_menu);
                //setHeaderMenuInOut(true);
                //TODO (4) Time line sharing activity.

                break;
            }

            case (R.id.menu_myFriends):
            {
                mDrawerLayout.closeDrawer(header_custom_menu);
                Intent intent = new Intent(getApplicationContext(), FriendsNetActivity.class);
                startActivity(intent);
                break;
            }
            case (R.id.menu_myGroups):
            {
                //TODO (6) Call to mynetworks activity and request friends;
                mDrawerLayout.closeDrawer(header_custom_menu);
                //setHeaderMenuInOut(true);
                Intent intent = new Intent(getApplicationContext(), FriendsActivity.class);
                startActivity(intent);
                break;
            }
            case (R.id.menu_setting):
            {
                mDrawerLayout.closeDrawer(header_custom_menu);
                //TODO (10) SettingActivity
                break;
            }
            case (R.id.menu_aboutApp):
            {
                mDrawerLayout.closeDrawer(header_custom_menu);
                //TODO (11) Aboutus Activity

                break;
            }
            case (R.id.menu_fbtn_signout):
            {
                mDrawerLayout.closeDrawer(header_custom_menu);
                signOut();
            }
                break;
            }

        }

    private void showAllUserPlace(String userRefUserPlace) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId();
        mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_PLACES).child(userId).child(userRefUserPlace).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot data:dataSnapshot.getChildren()) {
                    UserPlaces userPlaces = data.getValue(UserPlaces.class);
                    userPlaces.setG(data.getKey());
                    addPlaceToMap(userPlaces);
                }
                addMapMarkerClickListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    private void addPlaceToMap(UserPlaces uPlaces) {
        LatLng location = new LatLng(uPlaces.getL().get(0),uPlaces.getL().get(1));
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(location)
                .title(getResources().getString(uPlaces.getStay()?R.string.menu_home_staying:R.string.menu_home_extra))
                .icon(BitmapDescriptorFactory.fromResource(uPlaces.getStay()?R.drawable.home_address_no:R.drawable.home_local)));
        marker.setTag(ConstantUtils.USER_REF_USER_PLACES+"-"+uPlaces.getG()+"-"+uPlaces.getStay());
    }

    private void addMapMarkerClickListener() {
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                if(marker.getTag()!= null){
                    String tag = marker.getTag().toString();
                    String type = tag.split("-")[0];
                    if(type.equalsIgnoreCase(ConstantUtils.USER_REF_USER_PLACES))
                    {
                        com.ginsre.geo.hoang.dang.ginsre.utils.DialogConfirmLocation dialogConfirmLocation = new com.ginsre.geo.hoang.dang.ginsre.utils.DialogConfirmLocation();
                        UserPlaces userPlaces = new UserPlaces();
                        userPlaces.setG(tag.split("-")[1]);
                        userPlaces.setStay(Boolean.parseBoolean(tag.split("-")[2]));
                        dialogConfirmLocation.showDialog(BaseActivity.this,ConstantUtils.DIALOG_SETTING,userPlaces);
                    }
                }
                return false;
            }
        });
    }

    private void setNumOfNotifiToZero(String notifiOrMessage) {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(userId).child(notifiOrMessage).setValue(0);
        }catch (Exception e) {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void setNumOfMessAndNotiToMenu() {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(userId).child(IUser.numOfNotis).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        int num = dataSnapshot.getValue(Integer.class);
                        //menu_txvNumOfNotifi.setText(num);
                        if(num>0) {
                            Bitmap bitmapRe = BitmapFactory.decodeResource(getResources(), R.drawable.notification);
                            Bitmap bitmap = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.drawNewImageWithCycleNumber(bitmapRe,num);
                            if(bitmap != bitmapRe) {
                                header_notification.setPadding(DisplayUtils.dp2px(8),0,DisplayUtils.dp2px(8),0);
                                header_notification.setImageBitmap(bitmap);
                            }
                        }else{
                            header_notification.setImageDrawable(getResources().getDrawable(R.drawable.notification));
                        }
                    }catch (Exception e){

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(userId).child(IUser.numOfMessage).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        int num = dataSnapshot.getValue(Integer.class);
                        //menu_txvNumOfNotifi.setText(num);
                        if(num>0) {
                            Bitmap bitmapRe = BitmapFactory.decodeResource(getResources(), R.drawable.messages);
                            Bitmap bitmap = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.drawNewImageWithCycleNumber(bitmapRe,num);
                            if(bitmap != bitmapRe) {
                                header_message.setPadding(DisplayUtils.dp2px(8),0,DisplayUtils.dp2px(8),0);
                                header_message.setImageBitmap(bitmap);
                            }
                        }else{
                            header_message.setImageDrawable(getResources().getDrawable(R.drawable.messages));
                        }
                    }catch (Exception e){

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }catch (Exception e) {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void showAlertDialogBoxToGetUserAction(String alertContent,String firebaseRef,String preRefLat,String preRefLon) {
            try {
                //TODO: test if user have added location to home or work place;

                final String placeRef = firebaseRef;
                final String mpreRefLat = preRefLat;
                final String mpreRefLon = preRefLon;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.Confirm_dialog_box));
                builder.setMessage(alertContent);
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        writeUserPlacesToDatabase(placeRef,mLastKnownLocation);
                        saveLastKnowLocation(mpreRefLat, mpreRefLon);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            } catch (Exception e) {
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
    }
    // placeRef is workplace, homeplace, or favorite places
    private void writeUserPlacesToDatabase(String placeRef,LatLng latlon) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        String userId = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId();
        UserPlaces userPlaces = new UserPlaces();
        GeoLocation geoLocation = new GeoLocation(latlon.latitude,latlon.longitude);
        String g = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getGeoHashForPostQuery(10,geoLocation);
        List<Double> arr = new ArrayList<>();
        arr.add(latlon.latitude);arr.add(latlon.longitude);
        userPlaces.setL(arr);
        userPlaces.setStay(true);
        mDatabase.child(ConstantUtils.USER_REF_USER)
                .child(ConstantUtils.USER_REF_USER_PLACES).child(userId).child(placeRef)
                .child(g).setValue(userPlaces);
    }

    private void signOut() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        Intent intent = new Intent(getApplication(), LoginActivity.class);
        startNewActivity(intent);
    }


    private void startNewActivity(Intent intent) {
        if(cameraLocation == null)
        {
            intent.putExtra(ConstantUtils.LATITUTE,mLastKnownLocation.latitude);
            intent.putExtra(ConstantUtils.LONGTITUTE,mLastKnownLocation.longitude);
        }
        else
        {
            intent.putExtra(ConstantUtils.LATITUTE,cameraLocation.latitude);
            intent.putExtra(ConstantUtils.LONGTITUTE,cameraLocation.longitude);
        }
        startActivity(intent);
    }

    protected void getLastKnowLocation() {
        try {
            if(PreferencesUtils.contains(SharedPrefenceKey.LAST_KNOW_LOCATION_LATITUTE.name())){
                mLastKnownLocation = new LatLng(Double.parseDouble(
                        PreferencesUtils.getString(SharedPrefenceKey.LAST_KNOW_LOCATION_LATITUTE.name(),"1.0")
                ),Double.parseDouble(
                        PreferencesUtils.getString(SharedPrefenceKey.LAST_KNOW_LOCATION_LONGTITUTE.name(),"1.0")
                ));
            }else{
                mLastKnownLocation = new LatLng(10,10);
            }
        } catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }

    }

    private void saveLastKnowLocation(String latRef,String lonRef) {
        if(mLastKnownLocation!=null) {
            PreferencesUtils.edit().putString(latRef
                    ,
                    String.valueOf(mLastKnownLocation.latitude)).commit();
            PreferencesUtils.edit().putString(
                    lonRef,
                    String.valueOf(mLastKnownLocation.longitude)).commit();
        }
    }

    @Override
    public void onDataChanged(IssueKey key, Object object) {

    }

    private void controlByVoice(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getResources().getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, ConstantUtils.REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Receiving speech input
     * */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ConstantUtils.REQ_CODE_SPEECH_INPUT: {
                if (resultCode == Activity.RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    //header_txtSearch.setText(result.get(0));
                    if(result.get(0).equalsIgnoreCase("Go home")|| result.get(0).equals("Come back home"))
                    {
                        //TODO: get location home here;
                        getDirectionToAPlace(result.get(0),ConstantUtils.USER_REF_USER_PLACES_HOME);
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.Cannot_Recognize_voice),Toast.LENGTH_LONG).show();
                    }
                }

                break;
            }

        }

    }

    private void getDirectionToAPlace(String s, String userRefUserPlaceName) {
        try {
            final String result1 = s;
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserId()).child(ConstantUtils.USER_REF_USER_PLACES)
                    .child(userRefUserPlaceName).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot!=null) {
                        LatLng latlong = new LatLng((Double) dataSnapshot.child("latitude").getValue(), (Double) dataSnapshot.child("longitude").getValue());

                            header_txtSearch.setText(result1);
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.Comeback_To_Home), Toast.LENGTH_LONG).show();
                            DataChangeNotification.getInstance().notifyChange(IssueKey.DIRECTTION_TO_ADDRESS, latlong);
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.Canoot_find_your_home),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            Toast.makeText(BaseActivity.this,
                    "You selected position: " + position, Toast.LENGTH_SHORT)
                    .show();
            mDrawerLayout.closeDrawer(header_custom_menu);
        }
    }
}
