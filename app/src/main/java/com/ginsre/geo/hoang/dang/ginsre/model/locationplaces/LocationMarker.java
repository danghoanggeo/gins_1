package com.ginsre.geo.hoang.dang.ginsre.model.locationplaces;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.ginsre.geo.hoang.dang.ginsre.clustering.ClusterItem;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostType;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ResourceUtils;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoang on 12/14/2017.
 */

public class LocationMarker implements ClusterItem {

    private String  postTypeId;
    private String postTypeName;
    private String postTypeDrawable;
    private String postTypeIsActive;
    private String subPostType;
    private String dateTime;
    private LatLng latLng;
    public LocationMarker(){}

    public String getPostTypeId() {
        return postTypeId;
    }

    public void setPostTypeId(String postTypeId) {
        this.postTypeId = postTypeId;
    }

    public String getPostTypeName() {
        return postTypeName;
    }
    public String getPostTypeName(int typeId,Context context)
    {
        List<String> listTypeName = getListPostTypeName(context,this.subPostType);
        return listTypeName.get(typeId);
    }
    public void setPostTypeName(String postTypeName) {
        this.postTypeName = postTypeName;
    }

    public String getPostTypeDrawable() {
        return postTypeDrawable;
    }
    public String getPostTypeDrawable(int typeId)
    {
        List<String> listDraw = getListPostTypeImageName(subPostType);
        return listDraw.get(typeId);
    }
    public void setPostTypeDrawable(String postTypeDrawable) {
        this.postTypeDrawable = postTypeDrawable;
    }

    public String getPostTypeIsActive() {
        return postTypeIsActive;
    }

    public void setPostTypeIsActive(String postTypeIsActive) {
        this.postTypeIsActive = postTypeIsActive;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getSubPostType() {
        return subPostType;
    }

    public void setSubPostType(String subPostType) {
        this.subPostType = subPostType;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    @Override
    public LatLng getPosition() {
        return latLng;
    }

    @Override
    public String getTitle() {
        return postTypeName;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    public Drawable getPostTypeImage(Context context)
    {
        return ResourceUtils.getDrawableByName(context,getPostTypeDrawable());
    }
    public Drawable getPostTypeMarker(Context context)
    {
        return ResourceUtils.getDrawableForMapMarkerByName(context,getPostTypeDrawable());
    }

    private List<String> getListPostTypeName(Context context,String subType)
    {
        return com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getListSubPostName(context,subType);
    }

    private List<String> getListPostTypeImageName(String subType)
    {
        return com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getListPostIconMarkerName(subType);
    }


    public List<PostType> initDataTrafficType(Context context, String posSubType) {
        final List<PostType> lst = new ArrayList<>();
        List<String> lstTrafficTypeName = getListPostTypeName(context,posSubType);
        List<String> lstImageIndentifers = getListPostTypeImageName(posSubType);
        int i = 0;
        // Police
        //if(lstTrafficTypeName.size()<lstImageIndentifers.size())
        // lstImageIndentifers.remove(5);
        //
        for (String typename:lstTrafficTypeName) {
            PostType postType = new PostType();
            if(posSubType.equals(ConstantUtils.GINS_MAIN_TYPE[0])) {
                postType.setPostTypeId(ConstantUtils.SOCIAL_PREFIX + i);
                postType.setSubPostType(ConstantUtils.GINS_MAIN_TYPE[0]);
            }
            else if(posSubType.equals(ConstantUtils.GINS_MAIN_TYPE[1])) {
                postType.setPostTypeId(ConstantUtils.TRAFFIC_PREFIX + i);
                postType.setSubPostType(ConstantUtils.GINS_MAIN_TYPE[1]);
            }
            else if(posSubType.equals(ConstantUtils.GINS_MAIN_TYPE[2])) {
                postType.setPostTypeId(ConstantUtils.LOCATION_PREFIX + i);
                postType.setSubPostType(ConstantUtils.GINS_MAIN_TYPE[2]);
            }
            else if(posSubType.equals(ConstantUtils.GINS_MAIN_TYPE[3])) {
                postType.setPostTypeId(ConstantUtils.FRIEND_PREFIX + i);
                postType.setSubPostType(ConstantUtils.GINS_MAIN_TYPE[3]);
            }else if(posSubType.equals(ConstantUtils.GINS_MAIN_TYPE[4])) {
                postType.setPostTypeId(ConstantUtils.FRIEND_PREFIX + i);
                postType.setSubPostType(ConstantUtils.GINS_MAIN_TYPE[4]);
            }
            postType.setPostTypeName(typename);
            postType.setPostTypeDrawable(lstImageIndentifers.get(i));
            postType.setPostTypeIsActive("1");
            lst.add(postType);
            i++;
        }
        return lst;
    }
}
