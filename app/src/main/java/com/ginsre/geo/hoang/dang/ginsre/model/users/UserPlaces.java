package com.ginsre.geo.hoang.dang.ginsre.model.users;

import java.util.List;

/**
 * Created by hoang on 1/4/2018.
 */

public class UserPlaces {
    private List<Double> l;
    private String g;
    private Boolean isStay;

    public UserPlaces(){}
    public UserPlaces(List<Double> l, String g) {
        this.l = l;
        this.g = g;
        isStay = true;
    }

    public List<Double> getL() {
        return l;
    }

    public void setL(List<Double> l) {
        this.l = l;
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public Boolean getStay() {
        return isStay;
    }

    public void setStay(Boolean stay) {
        isStay = stay;
    }
}
