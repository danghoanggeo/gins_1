package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.content.Context;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.google.firebase.database.ServerValue;
import com.google.firebase.firestore.ServerTimestamp;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hoang dang on 11/02/2017.
 */

public class DateUtils {
    /** 20371105 00:00:00*/
    public static final long LAST_DATE_TIME_REF = 2140992000000L;
    /** 20130225 */
    public static final int FORMAT_TO_MINUTE_EN = 0;
    /** 2013022511 */
    public static final int FORMAT_TO_MINUTE_VN = 1;
    /** 201302251134 */
    public static final int FORMAT_TO_DAY_YEAR = 2;
    /** 20130225113420 */
    public static final int FORMAT_TO_DAY_NO_YEAR = 3;
    public static final int FORMAT_TO_DAY = 4;
    public static final int FORMAT_TO_HH_MM = 5;
    public static final int FORMAT_TO_YYYY_MM_DD = 6;
    /** 20130225113420888 */
    public static final int FORMAT_TO_SECOND = 7;
    public static final int FORMAT_TO_MINISECOND = 8;
    public static final int FORMATMINISECOND = 9;
    /** 04-18 13:00 */
    //public static final int FORMAT_TO_CUSTOM1 = 5;


    private static final String[] SIMPLE_TIME_FORMAT = {"yyyy-MM-dd-HH-mm","dd/MM/yyyy HH:mm","EEE, d MMM yyyy HH:mm",
            "EEE, d MMM HH:mm","yyyy-MM-dd","HH:mm","yy/MM/dd","yyyy-MM-dd-HH-mm-ss","yyyy-MM-dd-HH-mm-ss-SSS","ssSSS"
    };

    public static String getTimeLongForPostFromRef()
    {
        long currentDateTime = new Date().getTime();
        return String.valueOf(LAST_DATE_TIME_REF - currentDateTime);
    }

    public static String timeLong2DatetimeFromRef(String dateTimeFromRef,int formatNum){
        long dateTimeFromRefLong = LAST_DATE_TIME_REF-Long.parseLong(dateTimeFromRef);
        String formatString = SIMPLE_TIME_FORMAT[formatNum];
        return new SimpleDateFormat(formatString).format(new Date(dateTimeFromRefLong));
    }

    public static String timeLong2DateTimeFromRefForDatatbaseQuery(String dateTimeFromRef,int numOfBackDay){
        long dateTimeFromRefLong = LAST_DATE_TIME_REF-Long.parseLong(dateTimeFromRef)-numOfBackDay*24*60*60*1000; // Lui lai 1 ngay;
        String formatString = new SimpleDateFormat("yy/MM/dd").format(new Date(dateTimeFromRefLong));
        return GinFunctions.getDateTimeCode(formatString);
    }

    public static String getDateTimeBack(int numOfBackDay){
        long dateTimeFromRefLong = (new Date().getTime())-numOfBackDay*24*60*60*1000; // Lui lai 1 ngay;
        String formatString = new SimpleDateFormat("yy/MM/dd").format(new Date(dateTimeFromRefLong));
        return GinFunctions.getDateTimeCode(formatString);
    }
    public static String getDateTime(int numOfBackDay){
        long dateTimeFromRefLong = (new Date().getTime())-numOfBackDay*24*60*60*1000; // Lui lai 1 ngay;
        String formatString = new SimpleDateFormat("yy/MM/dd").format(new Date(dateTimeFromRefLong));
        return formatString;
    }

    public static boolean getDateForVNStorm(){
        long dateTimeFromRefLong = (new Date().getTime())/1000; // Lui lai 1 ngay;
        if((dateTimeFromRefLong-1517033067)<48*60*60)
            return true;
        else
            return false;
    }

    public static boolean isNight()
    {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("HH");
        int hour = Integer.parseInt(format.format(date));
        if(hour>=18 || hour <=6)
        {
            return true;
        }
        return false;
    }

    public static String getDateTimeForGeoHashQuery(){
        String formatString = new SimpleDateFormat("yy/MM/dd").format(new Date());
        return GinFunctions.getDateTimeCode(formatString);
    }

    public static String getDateTimeForUserTempGeo(int numHour){
        long dateTimeFromRefLong = (new Date().getTime() - numHour*60*60*1000);
        String zone = new SimpleDateFormat("Z").format(new Date());
        int timeZon = Integer.parseInt(zone)/100;
        dateTimeFromRefLong -= timeZon*60*60*1000;
        String formatString = new SimpleDateFormat("yy/MM/dd/HH").format(new Date(dateTimeFromRefLong));
        return GinFunctions.getDateTimeCode(formatString);
    }
    /**
     *
     * @param format
     * @return
     */
    public static String formatCurrentDate(int format){
        return formatDate(new Date(), format, "");
    }

    /**
     *
     * @param date
     * @param format
     * @param delimiter
     * @return
     */
    private static String formatDate(Date date, int format, String delimiter) {
        return formatDate(format, delimiter).format(date);
    }

    /**
     *
     * @param format
     * @param delimiter
     * @return
     */
    private static SimpleDateFormat formatDate(int format, String delimiter) {
        String formatString = SIMPLE_TIME_FORMAT[format];
        if (delimiter != null) {
            formatString = formatString.replace("-", delimiter);
        }
        return new SimpleDateFormat(formatString);
    }
    public static String getTimeFromCurrentTime(String postedTime, Context context)
    {
        return getPostTimeFromCurrent(String.valueOf(LAST_DATE_TIME_REF-Long.parseLong(postedTime)),context);
    }
    public static String getPostTimeFromCurrent(String postedTime, Context context){
        long timePost = Long.parseLong(postedTime);
        long currentTimeLong = new Date().getTime();
        long timePeriod = (currentTimeLong - timePost)/1000;
        String result = "";
        if(timePeriod < 3600) // minute ago
        {
            if(timePeriod/60 == 1){
                result =  String.valueOf(timePeriod / 60) + " " + context.getResources().getString(R.string.minute_ago);
            }else {
                result =  String.valueOf(timePeriod / 60) + " " + context.getResources().getString(R.string.minutes_ago);
            }
        }else
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String ddPosted = sdf.format(new Date(timePost));
            String currentdd = sdf.format(new Date());
            if(ddPosted.equals(currentdd)){
                if((int)timePeriod/3600 < 3) {
                    if((int)timePeriod/3600 == 1) {
                        result = String.valueOf((int) timePeriod / 3600) + " " + context.getResources().getString(R.string.hour_ago);
                    }else
                    {
                        result =  String.valueOf((int) timePeriod / 3600) + " " + context.getResources().getString(R.string.hours_ago);
                    }
                }else{
                    sdf = new SimpleDateFormat("++ HH:mm");
                    result =  sdf.format(new Date(timePost));
                }
            }else{
                sdf = new SimpleDateFormat("yyyy");
                ddPosted = sdf.format(new Date(timePost));
                currentdd = sdf.format(new Date());
                if(ddPosted.equals(currentdd)){
                    sdf = new SimpleDateFormat("MMdd");
                    ddPosted = sdf.format(new Date(timePost+(24*60*60*1000)));
                    currentdd = sdf.format(new Date());
                    if(ddPosted.equals(currentdd)){
                        sdf = new SimpleDateFormat(" ++ HH:mm");
                        result =  context.getResources().getString(R.string.post_yesterday)+sdf.format(new Date(timePost));
                    }else{
                        sdf = new SimpleDateFormat("dd/MM ++ HH:mm");
                        result =  sdf.format(new Date(timePost));
                    }
                }else
                {
                    sdf = new SimpleDateFormat("yyyy/dd/MM ++ HH:mm");
                    result =  sdf.format(new Date(timePost));
                }
            }
        }
        return result.replace("++",context.getResources().getString(R.string.time_at));
    }
    public static String getTimePosted(String mDateTime, Context context) {
        final String datetime = mDateTime;
        String currentdatetime = formatCurrentDate(DateUtils.FORMAT_TO_MINUTE_EN);
        String postedhour = datetime.substring(8, 10);
        String postedminute = datetime.substring(10);
        String posteddate = datetime.substring(6, 8);
        String postedmonth = datetime.substring(4, 6);
        String postedyear =  datetime.substring(0, 4);
        String result = "";
        if (currentdatetime.substring(0, 8).equals(datetime.substring(0, 8))) {
            String currenthour = currentdatetime.substring(8, 10);
            String currentminute = currentdatetime.substring(10,12);
            if (currenthour.equals(postedhour)) {
                result =  String.valueOf(Integer.parseInt(currentminute) - Integer.parseInt(postedminute)) +" " +context.getResources().getString(R.string.minute_ago);
            } else {
                result =  postedhour + " : " + postedminute;
            }
        }
        else if (currentdatetime.substring(0, 4).equals(postedyear))
        {
            result =  posteddate +"/"+postedmonth+" "+ postedhour + " : " + postedminute;
        }
        else
            result =  postedyear+ "/"+posteddate +"/"+postedmonth+" "+ postedhour + " : " + postedminute;

        return result;

    }
}
