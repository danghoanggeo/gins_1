package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.content.Context;
import android.support.compat.BuildConfig;
import android.text.TextUtils;

/**
 * Created by trach on 6/4/2017.
 */

public class EnvironmentUtils {

    private static final String TAG = "EnvironmentUtils";

    private static String mPackageName;
    private static String mSDCardPath;
    private static String mSecondSDCardPath;
    private static String mSecondSDCardValidFolder;
    private static String mAppVersion = "";
    private static String mVersionName = "";
    private static boolean mTestMode = false;
    private static boolean mLogEnable = true;

    public static void init(Context context) {
        mPackageName = context.getPackageName();
        mVersionName = BuildConfig.VERSION_NAME;
    }

    public static boolean isLogEnable() {
        return mLogEnable;
    }

    public static String getVersionName() {
        return mVersionName;
    }

    public static String getAppVersion() {
        return mAppVersion;
    }

    public static String getPackageName() {
        return mPackageName;
    }


    public static String getProcessAppVersion() {
        if (!TextUtils.isEmpty(mAppVersion)) {
            final int dateTimeLen = 11;
            String appVersion = getAppVersion();
            int length = appVersion.length() - dateTimeLen;
            return length > 0 ? appVersion.substring(0, length) : "0.0.0";
        }
        return "0.0.0";
    }

    public static boolean isTestMode() {
        return mTestMode;
    }

}
