package com.ginsre.geo.hoang.dang.ginsre.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;

import java.util.Calendar;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 9/20/2017.
 */

public class DateTimePickerDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),getTheme(),this,year,month,day);
        datePickerDialog.setTitle(getResources().getString(R.string.date_picker_inform));
        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String text = ""+(year-2000);
        int mMonth = month+1;
        FancyButton txvDateTime = (FancyButton)getActivity().findViewById(R.id.main_fbtnDateTime);
        if(mMonth<10)
            text +="/0"+mMonth;
        else
            text += "/"+mMonth;
        if(dayOfMonth<10)
            text += "/0"+dayOfMonth;
        else
            text += "/"+dayOfMonth;
        //txvDateTime.setText(text);
        DataChangeNotification.getInstance().notifyChange(IssueKey.CHANGE_GEO_QUERY_DATE,text);
    }
}
