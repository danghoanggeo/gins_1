package com.ginsre.geo.hoang.dang.ginsre.model.core;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by hoang on 11/1/2017.
 */
// Post properties
public class properties {
    private String content;
    private int assess;
    private String userId;
    private String imagesUrl;
    private int numOfComment;
    private int numOfThanks;
    private int numOfLikes;
    private int numOfSpam;
    private boolean post_public;
    private String g;
    private String time;

    public properties(){}

    public properties(String content, int assess)
    {
        this.content = content;
        this.assess = assess;
        try {
            userId = FirebaseAuth.getInstance().getUid();
        }catch (Exception e){
            Log.d("properties class - 27: ",e.getMessage());
        }
    }

    public properties(String userId, String content, int assess, String imagesUrl, boolean post_public)
    {
        this.userId = userId;
        this.content = content;
        this.assess = assess;
        this.imagesUrl = imagesUrl;
        this.numOfComment = 0;
        this.numOfThanks = 1;
        this.numOfLikes = 0;
        this.numOfSpam = 0;
        this.post_public = post_public;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getAssess() {
        return assess;
    }

    public void setAssess(int assess) {
        this.assess = assess;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(String imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public int getNumOfComment() {
        return numOfComment;
    }

    public void setNumOfComment(int numOfComment) {
        this.numOfComment = numOfComment;
    }

    public int getNumOfThanks() {
        return numOfThanks;
    }

    public void setNumOfThanks(int numOfThanks) {
        this.numOfThanks = numOfThanks;
    }

    public int getNumOfLikes() {
        return numOfLikes;
    }

    public void setNumOfLikes(int numOfLikes) {
        this.numOfLikes = numOfLikes;
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getNumOfSpam() {
        return numOfSpam;
    }

    public void setNumOfSpam(int numOfSpam) {
        this.numOfSpam = numOfSpam;
    }

    public boolean isPost_public() {
        return post_public;
    }

    public void setPost_public(boolean post_public) {
        this.post_public = post_public;
    }
}
