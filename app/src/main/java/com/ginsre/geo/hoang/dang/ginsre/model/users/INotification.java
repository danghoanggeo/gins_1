package com.ginsre.geo.hoang.dang.ginsre.model.users;

/**
 * Created by hoang on 1/6/2018.
 */

public interface INotification {
    public static final String iconId = "iconId";
    public static final String  context = "context";
    public static final String dateTime = "dateTime";
    public static final String  key  = "key";
    public static final String readed = "readed";
    public static final String  latLng  = "latLng";
}
