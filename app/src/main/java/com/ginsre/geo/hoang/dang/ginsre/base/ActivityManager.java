package com.ginsre.geo.hoang.dang.ginsre.base;

import android.app.Activity;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by hoang on 8/31/2017.
 */

public class ActivityManager {
    //list of activities
    private Set<Activity> mActivitySet = new HashSet<>();
    //current activity
    private Activity mCurrentActivity = null;
    //top activity
    private Activity mTopActivity = null;
    //last activity
    private Activity mLastActivity = null;

    private static ActivityManager sInstance;

    public static ActivityManager getInstance(){
        if(sInstance == null){
            sInstance = new ActivityManager();
        }
        return sInstance;
    }

    private ActivityManager(){ }

    /**
     * lastest activity added to set is top activity
     * @param activity
     */
    public void onCreate(Activity activity){
        mActivitySet.add(activity);
        mTopActivity = activity;
    }

    /**
     *
     * @return mTopActivity
     */
    public Activity getTopActivity(){
        return mTopActivity;
    }

    /**
     * onDestroy -> this activity is last activity
     * remove it from activity set
     * @param activity
     */
    public void onDestroy(Activity activity){
        mActivitySet.remove(activity);
        if(mLastActivity == activity){
            mLastActivity = null;
        }
        if (mTopActivity == activity) {
            mTopActivity = null;
        }
        mActivitySet.remove(activity);
    }

    /**
     * finish all activities in list
     */
    public void finishActivities(){
        for (Activity activity: mActivitySet) {
            activity.finish();
        }
        mActivitySet.clear();
    }

    /**
     *
     * @param activity
     */
    public void onResume(Activity activity) {
        mCurrentActivity = activity;
        mTopActivity = activity;
    }

    public void onPause(Activity activity) {
        mCurrentActivity = null;
        mLastActivity = activity;
    }

    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    /**
     * @param activity activity
     * @return current: true
     */
    public boolean isCurrentActivity(Activity activity) {
        return mCurrentActivity == activity;
    }

    /**
     * @return last Activity
     */
    public Activity getLastActivity() {
        return mLastActivity;
    }

    /**
     *  @return Activities
     */
    public Set<Activity> getActivities() {
        return mActivitySet;
    }
}
