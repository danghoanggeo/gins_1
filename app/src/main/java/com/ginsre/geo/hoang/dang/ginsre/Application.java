package com.ginsre.geo.hoang.dang.ginsre;

import android.content.Context;

import com.ginsre.geo.hoang.dang.ginsre.base.BaseAplication;
import com.ginsre.geo.hoang.dang.ginsre.utils.DisplayUtils;

/**
 * Created by trach on 6/4/2017.
 */

public class Application extends BaseAplication {

    public static Context sApplicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sApplicationContext = getApplicationContext();
        DisplayUtils.init(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    public static Context getContext(){
        return sApplicationContext;
    }

    @Override
    public void exitProcess() {
//        if (!Preferences.getBoolean(SharedPreferenceKey.HAS_SHORTCUT, false)
//                && !ShortCutUtils.hasShortCut(sApplication)) {
//            ShortCutUtils.createLaunchShortcut(sApplication, EntryActivity.class);
//            Preferences.edit().putBoolean(SharedPreferenceKey.HAS_SHORTCUT, true).apply();
//        }
//        Intent i = new Intent(sApplication, BaseService.class);
//        sApplication.stopService(i);
//        StorageUtils.close();
//        CommandCenter.instance().exeCommand(new Command(CommandID.DISCONNECT_IM_SOCKET));
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                ImageCacheUtils.getCache().clearDiskCache(ShowConfig.CLEAR_CACHE_INTERVAL);
//                ImageCacheUtils.close();
//                Cache.close();
//                android.os.Process.killProcess(android.os.Process.myPid());
//            }
//        }, KILL_SELF_DELAY);
    }
}
