package com.ginsre.geo.hoang.dang.ginsre.model.users;

import com.google.android.gms.maps.model.LatLng;
import com.ginsre.geo.hoang.dang.ginsre.clustering.ClusterItem;

/**
 * Created by hoang on 9/12/2017.
 */

public class Users implements ClusterItem{
    private String uName;
    private String uPass;
    private String uEmail;
    private String uPhone;
    private String uAvUrl;
    private String status;
    private String birthday;
    private int grade;
    private LatLng mPosition;
    private int nNoTi;
    private int nMess;

    public Users(){}

    public Users( String uName, String uPass, String uEmail, String uPhone, String uAvUrl,String birthday) {
        this.uName = uName;
        this.uPass = uPass;
        this.uEmail = uEmail;
        this.uPhone = uPhone;
        this.nNoTi = 0;
        this.nMess = 0;
        this.uAvUrl = uAvUrl;
        this.status = "1";
        this.birthday = birthday;
        grade = 1;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPass() {
        return uPass;
    }

    public void setuPass(String uPass) {
        this.uPass = uPass;
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }


    public String getuAvUrl() {
        return uAvUrl;
    }

    public void setuAvUrl(String uAvUrl) {
        this.uAvUrl = uAvUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public void setmPosition(LatLng mPosition) {
        this.mPosition = mPosition;
    }

    public LatLng getmPosition() {
        return mPosition;
    }

    public int getnNoTi() {
        return nNoTi;
    }

    public void setnNoTi(int nNoTi) {
        this.nNoTi = nNoTi;
    }

    public int getnMess() {
        return nMess;
    }

    public void setnMess(int nMess) {
        this.nMess = nMess;
    }

    @Override
    public LatLng getPosition() {
        if(mPosition != null)
            return mPosition;
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
