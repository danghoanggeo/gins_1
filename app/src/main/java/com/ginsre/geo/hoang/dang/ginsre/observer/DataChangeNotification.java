package com.ginsre.geo.hoang.dang.ginsre.observer;

import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by trach on 2/18/2017.
 */

public final class DataChangeNotification {

    private static DataChangeNotification mDataChangeNotification = new DataChangeNotification();


    private static Map<IssueKey, Set<OnDataChangeObserver>> mObservers;

    private DataChangeNotification(){
        mObservers = new HashMap<>();
    }

    public static DataChangeNotification getInstance(){
        return mDataChangeNotification;
    }

    public void addObserver(IssueKey issueKey, OnDataChangeObserver observer){
        Set<OnDataChangeObserver> observerSet;
        if(!mObservers.containsKey(issueKey)){
            observerSet = new HashSet<>();
            observerSet.add(observer);
            mObservers.put(issueKey, observerSet);
        }else {
            observerSet = mObservers.get(issueKey);
            if(!observerSet.contains(observer)){
                observerSet.add(observer);
            }
        }
    }

    public void  removeObserver(IssueKey issueKey, OnDataChangeObserver observer){
        if(!mObservers.containsKey(issueKey)){
            return;
        }
        Set<OnDataChangeObserver> observerSet = mObservers.get(issueKey);
        if(observerSet.contains(observer)){
            observerSet.remove(observer);
        }

        if(observerSet.isEmpty()){
            mObservers.remove(issueKey);
        }
    }

    public void notifyChange(IssueKey issueKey, Object object){
        if(mObservers.get(issueKey)==null)
            return;
        for (OnDataChangeObserver observer: mObservers.get(issueKey)){
            observer.onDataChanged(issueKey, object);
        }
    }

    /**
     * add 13/6 for overloading method
     * @param issueKey
     */
    public void notifyChange(IssueKey issueKey){
        if(mObservers.get(issueKey)==null)
            return;
        for (OnDataChangeObserver observer: mObservers.get(issueKey)){
            observer.onDataChanged(issueKey, null);
        }
    }

}
