package com.ginsre.geo.hoang.dang.ginsre.model.weather;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by hoang on 12/25/2017.
 */

public class Storm {
    ArrayList<Forecast> forecast;
    boolean isActive;
    String s_Name;

    public Storm(){}

    public Storm(ArrayList<Forecast> forecast, boolean isActive) {
        this.forecast = forecast;
        this.isActive = isActive;
    }


    public List<Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(ArrayList<Forecast> forecast) {
        this.forecast = forecast;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getS_Name() {
        return s_Name;
    }

    public void setS_Name(String s_Name) {
        this.s_Name = s_Name;
    }
}
