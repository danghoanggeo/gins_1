package com.ginsre.geo.hoang.dang.ginsre.model.Friends;


/**
 * Created by hoang on 10/9/2017.
 */

public interface IFriendShip {
    public static final String friendShipId ="id";
    public static final String friendStatus="status";
    public static final String seeRealTimePosition="seePosi";
    public static final String canGetAddress="canGetAddress";
    public static final String dateCreated="created";
}

