package com.ginsre.geo.hoang.dang.ginsre.model.map;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by hoang on 9/5/2017.
 */
@IgnoreExtraProperties
public class LocationDetail implements Serializable {
    private LatLng location;
    private String postcode;
    private String description;

    public LocationDetail(){}
    public LocationDetail(LatLng location, String postcode, String description) {
        this.setLocation(location);
        this.setPostcode(postcode);
        this.setDescription(description);
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
