package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by hoang on 12/15/2017.
 */

public class ImageAdapter extends BaseAdapter {

    ArrayList<String> imagesUrls;
    Activity context;
    public ImageAdapter(Activity context,ArrayList<String> imagesUrls){
        this.imagesUrls = imagesUrls;
        this.context = context;
    }

    @Override
    public int getCount() {
        return imagesUrls.size();
    }

    @Override
    public Object getItem(int i) {
        return imagesUrls.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(context);
            int width = GinFunctions.getDeviceWidthHeight(context)[0];
            imageView.setLayoutParams(new GridView.LayoutParams(width/2, width/2));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(1, 1, 1, 1);
        }
        else
        {
            imageView = (ImageView) convertView;
        }
        Picasso.with(context).load(imagesUrls.get(i)).into(imageView);
        return imageView;
    }
}
