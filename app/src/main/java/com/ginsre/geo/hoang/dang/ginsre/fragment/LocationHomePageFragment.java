package com.ginsre.geo.hoang.dang.ginsre.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.CommentItemAdapter;
import com.ginsre.geo.hoang.dang.ginsre.adapter.LocationReviewAdapter;
import com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.LocationPost;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostComment;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 12/9/2017.
 */

public class LocationHomePageFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_POSITION = "position";
    private static final String ARG_OWNER = "owner";
    private static final String ARG_LOCATIONID = "locationID";
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2;
    private int position;
    private boolean isTheManager;
    private String locationID;
    private static Activity context;
    private ArrayList<LocationPost> listPostDetail;
    //private ArrayList<LocationReview> listReview;
    private ArrayList<Bitmap> listBitMapPost;
    ListPostAdapter listPostAdapter;



    @BindView(R.id.fragment_location_tab_containter)
    LinearLayout fragment_location_tab_containter;
    @BindView(R.id.fragment_location_tab_home_linerContainNewPost)
    LinearLayout fragment_location_tab_home_linerContainNewPost;
    @BindView(R.id.fragment_location_tab_recycelelstview)
    RecyclerView fragment_location_tab_recycelelstview;
    @BindView(R.id.fragment_location_tab_textAlert)
    TextView fragment_location_tab_textAlert;
    @BindView(R.id.fragment_location_tab_home_ibtn_new)
    FancyButton fragment_location_tab_home_ibtn_new;
    @BindView(R.id.fragment_location_tab_home_txt_new)
    EditText fragment_location_tab_home_txt_new;
    // Add new
    @BindView(R.id.fragment_trafficPostAddNew_container)
    LinearLayout fragment_trafficPostAddNew_container;
    @BindView(R.id.fragment_PostAddNew_txtTitle)
    EditText fragment_PostAddNew_txtTitle;
    @BindView(R.id.fragment_trafficPostAddNew_txtcontent)
    EditText fragment_trafficPostAddNew_txtcontent;
    @BindView(R.id.fragment_trafficPostAddNew_ibtnpicture)
    ImageButton fragment_trafficPostAddNew_ibtnpicture;
    @BindView(R.id.fragment_trafficPostAddNew_imvImagePost)
    ImageView fragment_trafficPostAddNew_imvImagePost;
    @BindView(R.id.fragment_trafficPostAddNew_rating_assess)
    RatingBar fragment_trafficPostAddNew_rating_assess;
    @BindView(R.id.fragment_trafficPostAddNew_ftbnCancel)
    FancyButton fragment_trafficPostAddNew_ftbnCancel;
    @BindView(R.id.fragment_trafficPostAddNew_fbtnOk)
    FancyButton fragment_trafficPostAddNew_fbtnOk;
    @BindView(R.id.fragment_PostAddNew_Rating_container)
    LinearLayout fragment_PostAddNew_Rating_container;


    public static LocationHomePageFragment newInstance(Activity mcontext,int position, String locationID, boolean isTheManager) {
        LocationHomePageFragment f = new LocationHomePageFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        b.putBoolean(ARG_OWNER,isTheManager);
        b.putString(ARG_LOCATIONID,locationID);
        context = mcontext;
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
        isTheManager = getArguments().getBoolean(ARG_OWNER);
        locationID = getArguments().getString(ARG_LOCATIONID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_location_page,
                container, false);
        ButterKnife.bind(this,rootView);

        if(isTheManager && position == 0){
            fragment_location_tab_home_linerContainNewPost.setVisibility(View.VISIBLE);
        }
        return rootView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(position==0) {
            getLocationPost(1, 2);
        }
        addEvent();
    }

    private void addEvent() {
        fragment_location_tab_home_ibtn_new.setOnClickListener(this);
        fragment_trafficPostAddNew_ibtnpicture.setOnClickListener(this);
        fragment_trafficPostAddNew_fbtnOk.setOnClickListener(this);
        fragment_trafficPostAddNew_ftbnCancel.setOnClickListener(this);
    }


    private void getLocationPost(int from, int to){
        try {
            com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
            db.collection(ConstantUtils.REF_LOCATION_PAGE).document(locationID)
                    .collection(ConstantUtils.REF_LOCATION_POST).limit(10).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        listPostDetail = new ArrayList<>();
                        for (DocumentSnapshot document : task.getResult()) {
                            LocationPost locationPost = document.toObject(LocationPost.class);
                            locationPost.setPostId(document.getId());
                            locationPost.setDateTime(DateUtils.getTimeFromCurrentTime((String)document.getId(),getContext()));
                            listPostDetail.add(locationPost);
                        }
                        setLocationPostPresent();
                    } else {
                        //Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                }
            });
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }

    }



    private void setLocationPostPresent(){
        LinearLayoutManager myLayoutManager = new LinearLayoutManager(getActivity());
        myLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        if (listPostDetail.size() > 0 & fragment_location_tab_recycelelstview != null) {
            listPostAdapter = new LocationHomePageFragment.ListPostAdapter(listPostDetail);
            fragment_location_tab_recycelelstview.setAdapter(listPostAdapter);
        }
        fragment_location_tab_recycelelstview.setLayoutManager(myLayoutManager);
        addLocationPostEvent(myLayoutManager);
    }

    int lastPostScroll;
    private void addLocationPostEvent(final LinearLayoutManager myLayoutManager) {
        if(isTheManager) {
            lastPostScroll = myLayoutManager.findFirstVisibleItemPosition();
            fragment_location_tab_recycelelstview.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if(listPostDetail.size()>2){
                        if (lastPostScroll < myLayoutManager.findFirstVisibleItemPosition()) {
                            fragment_location_tab_home_linerContainNewPost.setVisibility(View.GONE);
                        } else {
                            if (myLayoutManager.findFirstVisibleItemPosition() == 0) {
                                fragment_location_tab_home_linerContainNewPost.setVisibility(View.VISIBLE);
                            }
                        }
                        lastPostScroll = myLayoutManager.findFirstVisibleItemPosition();
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId)
        {
            case R.id.fragment_location_tab_home_ibtn_new:
            {
                fragment_PostAddNew_txtTitle.setVisibility(View.VISIBLE);
                fragment_location_tab_containter.setVisibility(View.GONE);
                fragment_trafficPostAddNew_container.setVisibility(View.VISIBLE);
                if(position==0) // admin
                    fragment_PostAddNew_Rating_container.setVisibility(View.GONE);
                break;
            }
            case R.id.fragment_trafficPostAddNew_ibtnpicture:
            {
                if(listBitMapPost == null)
                    listBitMapPost = new ArrayList<>();
                if(listBitMapPost.size()<5)
                    userTakePictureToPost();
                else
                    Toast.makeText(context,context.getResources().getString(R.string.cannot_upload_more_than_4),Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.fragment_trafficPostAddNew_fbtnOk:
            {
                testUserPostNewToFireStore();
                break;
            }
            case R.id.fragment_trafficPostAddNew_ftbnCancel:
            {
                closePostFragment();
                break;
            }
        }
    }

    private void closePostFragment(){
        fragment_PostAddNew_txtTitle.setVisibility(View.GONE);
        fragment_location_tab_containter.setVisibility(View.VISIBLE);
        fragment_trafficPostAddNew_container.setVisibility(View.GONE);

    }

    private void userTakePictureToPost() {
        try {
            if(listBitMapPost == null)
                listBitMapPost = new ArrayList<>();

            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
            else
            {
                Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT, null);
                galleryintent.setType("image/*");
                //galleryintent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                //galleryintent.putExtra("GALLERY","gallery");
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //cameraIntent.putExtra("CAMERA","camera");
                Intent chooser = new Intent(Intent.ACTION_CHOOSER);
                chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
                chooser.putExtra(Intent.EXTRA_TITLE, context.getResources().getString(R.string.get_picture_or_take));

                Intent[] intentArray =  {cameraIntent};
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooser,CAMERA_REQUEST_CODE);
            }
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }


    private void testUserPostNewToFireStore() {
        if(fragment_PostAddNew_txtTitle.getText().length()<=3){
            fragment_PostAddNew_txtTitle.setFocusable(true);
            Toast.makeText(context,context.getResources().getString(R.string.location_name_is_invalid),Toast.LENGTH_LONG).show();
        }else{
            fragment_PostAddNew_txtTitle.clearFocus();
            if(fragment_trafficPostAddNew_txtcontent.getText().length()<3){
                Toast.makeText(context,context.getResources().getString(R.string.location_name_is_invalid),Toast.LENGTH_LONG).show();
                fragment_trafficPostAddNew_txtcontent.setFocusable(true);
            }else{
                fragment_trafficPostAddNew_txtcontent.clearFocus();
                if(listBitMapPost == null)
                    pushPostDetailToFireStore(null);
                else
                    uploadImagesToFirebase();

                closePostFragment();
            }
        }
    }

    private void uploadImagesToFirebase(){
        final ArrayList<String> listImageUrl = new ArrayList<>();
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.user_post_uploading));
        for(int i = 0;i<listBitMapPost.size();i++){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap imagePost = listBitMapPost.get(i);
            imagePost.compress(Bitmap.CompressFormat.JPEG,90, baos);
            byte[] data = baos.toByteArray();
            final String mpostDataRef = ConstantUtils.REF_LOCATION_POST;
            if(data != null)
            {
                if(i==0)
                    progressDialog.show();
                final String nlocationDetailId = locationID+DateUtils.formatCurrentDate(DateUtils.FORMATMINISECOND);
                StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
                //StorageReference childPath = mStorageRef.child(ConstantUtils.STORE_TRAFFIC_IMAGES).child(nlocationDetailId);
                mStorageRef.child(ConstantUtils.REF_LOCATION_IMAGES).child(nlocationDetailId).putBytes(data)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Uri downloadUri = taskSnapshot.getDownloadUrl();
                                listImageUrl.add(downloadUri.toString());
                                if(listImageUrl.size() == listBitMapPost.size()){
                                    progressDialog.dismiss();
                                    pushPostDetailToFireStore(listImageUrl);
                                    listBitMapPost = new ArrayList<>();
                                }
                            }
                        });
            }
        }

    }

    private void pushPostDetailToFireStore(ArrayList<String> listImageUrl) {
        LocationPost locationPost = new LocationPost(fragment_PostAddNew_txtTitle.getText().toString(),fragment_trafficPostAddNew_txtcontent.getText().toString(),listImageUrl);
        com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
        db.collection(ConstantUtils.REF_LOCATION_PAGE).document(locationID).collection(ConstantUtils.REF_LOCATION_POST).document(DateUtils.getTimeLongForPostFromRef()).set(locationPost);
        listPostDetail.add(locationPost);
        listPostAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST_CODE && resultCode == context.RESULT_OK)
        {
            if (data == null) {
                return;
            }
            try {

                /*if(data.getStringExtra("CAMERA").equals("camera")){
                    photo = (Bitmap)data.getExtras().get("data");

                }else {*/
                //String type = data.getStringExtra("CAMERA");
                InputStream inputStream = null;
                try {
                    inputStream = context.getApplicationContext().getContentResolver().openInputStream(data.getData());
                }catch (Exception e){

                }
                if(inputStream == null){
                    listBitMapPost.add ((Bitmap)data.getExtras().get("data"));
                }else{
                    listBitMapPost.add (BitmapFactory.decodeStream(inputStream));
                }

                //ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                //photo.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                fragment_trafficPostAddNew_imvImagePost.setVisibility(View.VISIBLE);

                fragment_trafficPostAddNew_imvImagePost.setImageBitmap(listBitMapPost.get(0));

            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,CAMERA_REQUEST_CODE);
                }
            }
        }
    }

    public class ListPostAdapter extends RecyclerView.Adapter<LocationHomePageFragment.PostViewHolder>{
        private List<LocationPost> listPostDetail;

        public ListPostAdapter(List<LocationPost> listPostDetail)
        {
            this.listPostDetail = listPostDetail;
        }
        @Override
        public LocationHomePageFragment.PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // Create new View
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_post_cardview,parent,false);
            LocationHomePageFragment.PostViewHolder postViewHolder = new LocationHomePageFragment.PostViewHolder(view);
            return postViewHolder;
        }

        @Override
        public void onBindViewHolder(LocationHomePageFragment.PostViewHolder holder, int position) {
            //listPostDetail.get(position);
            //setUsersPost(listPostDetail.get(position).getUserId(),holder.item_post_cardview_txvUserName,holder.item_post_cardview_imvAvatar);

            holder.item_post_cardview_txvTime.setText(listPostDetail.get(position).getDateTime());
            holder.item_post_cardview_txvContent.setText(listPostDetail.get(position).getContent());
            if(listPostDetail.get(position).getListImagesUrl()!=null)
            {
                holder.item_post_cardview_coverImageView.setVisibility(View.VISIBLE);
                holder.item_post_cardview_coverImageView.setTag(position);
                //Load user avatar to this imageView
                ImageUtil.displayImage(holder.item_post_cardview_coverImageView,listPostDetail.get(position).getListImagesUrl().get(0),null);

            }else
                holder.item_post_cardview_coverImageView.setVisibility(View.GONE);

            if(listPostDetail.get(position).getTitle()!=null)
            {
                holder.item_post_cardview_txvUserName.setText(listPostDetail.get(position).getTitle());
            }else
                holder.item_post_cardview_txvUserName.setVisibility(View.GONE);

            //Like
            holder.getLikeOrLikedFromFirebase(position);
            holder.item_post_cardview_fbtnLike.setTag(position);

            //Comment
            holder.item_post_cardview_txtComment.setTag(position);
            if(listPostDetail.get(position).getNumOfComment()>0){
                holder.item_post_cardview_fbtnComment_view.setVisibility(View.VISIBLE);
                //holder.item_post_cardview_lastCommentRecycleView.setVisibility(View.VISIBLE);
                holder.item_post_cardview_fbtnComment_view.setText(String.valueOf(listPostDetail.get(position).getNumOfComment())+" "+context.getResources().getString(R.string.user_post_comments));
                //holder.item_post_comment_imvUserAvatar.setTag(position);
                holder.setLastCommentToView(position);
            }else{
                holder.item_post_cardview_fbtnComment_view.setVisibility(View.GONE);
                holder.item_post_cardview_lastCommentRecycleView.setVisibility(View.GONE);
            }
            //holder.item_post_cardview_txtComment.setText(String.valueOf(listPostDetail.get(position).getNumOfComment()));
        }

        @Override
        public int getItemCount() {
            return listPostDetail.size();
        }

    }
    public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.item_post_cardview_imvAvatar)
        public ImageView item_post_cardview_imvAvatar;
        @BindView(R.id.item_post_cardview_txvUserName)
        public TextView item_post_cardview_txvUserName;
        @BindView(R.id.item_post_cardview_txvTime)
        public TextView item_post_cardview_txvTime;
        @BindView(R.id.item_post_cardview_txvContent)
        public TextView item_post_cardview_txvContent;
        @BindView(R.id.item_post_cardview_coverImageView)
        public ImageView item_post_cardview_coverImageView;
        @BindView(R.id.item_post_cardview_titleTextView)
        public TextView item_post_cardview_titleTextView;
        @BindView(R.id.item_post_cardview_fbtnThank)
        public FancyButton item_post_cardview_fbtnThank;
        @BindView(R.id.item_post_cardview_fbtnLike)
        public FancyButton item_post_cardview_fbtnLike;
        @BindView(R.id.item_post_cardview_fbtnSpam)
        public FancyButton item_post_cardview_fbtnSpam;
        @BindView(R.id.item_post_cardview_fbtnShare)
        public FancyButton item_post_cardview_fbtnShare;
        @BindView(R.id.item_post_cardview_txtComment)
        public TextView item_post_cardview_txtComment;
        @BindView(R.id.item_post_cardview_fbtnThank_view)
        public FancyButton item_post_cardview_fbtnThank_view;
        @BindView(R.id.item_post_cardview_fbtnLike_view)
        public FancyButton item_post_cardview_fbtnLike_view;
        @BindView(R.id.item_post_cardview_fbtnSpam_view)
        public FancyButton item_post_cardview_fbtnSpam_view;
        @BindView((R.id.item_post_cardview_fbtnComment_view))
        public FancyButton item_post_cardview_fbtnComment_view;
        @BindView(R.id.item_post_cardview_lastCommentRecycleView)
        public android.support.v7.widget.RecyclerView item_post_cardview_lastCommentRecycleView;
        //item_post_cardview_fbtnThank_view



        public PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            addEventChild();

        }

        private void addEventChild() {
            item_post_cardview_imvAvatar.setVisibility(View.GONE);
            item_post_cardview_fbtnThank.setVisibility(View.GONE);
            item_post_cardview_fbtnLike.setOnClickListener(this);
            item_post_cardview_fbtnSpam.setVisibility(View.GONE);
            item_post_cardview_fbtnThank_view.setVisibility(View.GONE);
            item_post_cardview_txtComment.setOnClickListener(this);
            item_post_cardview_fbtnShare.setOnClickListener(this);
            item_post_cardview_fbtnComment_view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int controlId = v.getId();
            switch (controlId)
            {
                case R.id.item_post_cardview_fbtnLike:{
                    setLikeOrLikedToFirebase();
                    break;
                 }
                case R.id.item_post_cardview_txtComment:
                {
                    showCommentList();
                    break;
                }
                case R.id.item_post_cardview_fbtnComment_view:
                {
                    showCommentList();
                    break;
                }
                case R.id.item_post_cardview_fbtnShare:
                {
                    item_post_cardview_coverImageView.setDrawingCacheEnabled(true);
                    item_post_cardview_coverImageView.buildDrawingCache();
                    Bitmap bitmap = item_post_cardview_coverImageView.getDrawingCache();
                    if(bitmap != null) {
                        //shareToFaceBook(bitmap,item_post_cardview_coverImageView);
                    }else
                    {
                        //TODO: Push message to user no image to share; or share text;
                    }
                    break;
                }
            }
        }

        private void showCommentList() {

        }

        // getLike or liked from firebase for the first time;
        public void getLikeOrLikedFromFirebase(final int position){
            try {
                String postId = listPostDetail.get(position).getPostId();
                //com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
                com.google.firebase.firestore.FirebaseFirestore.getInstance().collection(ConstantUtils.REF_LOCATION_PAGE).document(locationID).collection(ConstantUtils.REF_LOCATION_POST)
                        .document(listPostDetail.get(position).getPostId()).collection(ConstantUtils.REF_POST_LIKE).document(getUserId()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){
                            if(task.getResult().exists()){
                                Boolean like = task.getResult().getBoolean("like");
                                listPostDetail.get(position).setLike(like);
                            }
                            setLikeOrLikedToControl(position);
                        }
                    }
                });
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
        }

        private void setLikeOrLikedToFirebase() {
            try {
                int position = (int)item_post_cardview_fbtnLike.getTag();
                if(listPostDetail.get(position).isLike()) {
                    if(listPostDetail.get(position).getNumOfLikes()-1 >= 0)
                        listPostDetail.get(position).setNumOfLikes(listPostDetail.get(position).getNumOfLikes()-1);
                    listPostDetail.get(position).setLike(false);
                }
                else {
                    listPostDetail.get(position).setNumOfLikes(listPostDetail.get(position).getNumOfLikes()+1);
                    listPostDetail.get(position).setLike(true);
                }

                //TODO: Hard code exist here
                HashMap<String,Integer> numOfLike = new HashMap<>();
                numOfLike.put("numOfLikes",listPostDetail.get(position).getNumOfLikes());
                com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
                db.collection(ConstantUtils.REF_LOCATION_PAGE).document(locationID).collection(ConstantUtils.REF_LOCATION_POST)
                        .document(listPostDetail.get(position).getPostId()).set(numOfLike);
                HashMap<String,Boolean> userlike = new HashMap<>();
                userlike.put("like",listPostDetail.get(position).isLike());
                db.collection(ConstantUtils.REF_LOCATION_PAGE).document(locationID).collection(ConstantUtils.REF_LOCATION_POST)
                        .document(listPostDetail.get(position).getPostId()).collection(ConstantUtils.REF_POST_LIKE).document(getUserId()).set(userlike);
                //String postId = listPostDetail.get(position).getPostId();
                setLikeOrLikedToControl(position);
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
        }

        // Set last comment;

        public void setLastCommentToView(final int position){
            try {
                int postIndext = (int)item_post_cardview_txtComment.getTag();
                if(listPostDetail.get(position).getListComment()!= null) {
                    if(item_post_cardview_lastCommentRecycleView.getVisibility()==View.GONE){
                        item_post_cardview_lastCommentRecycleView.setVisibility(View.VISIBLE);
                    }
                    HashMap<String, String> hashMap = listPostDetail.get(position).getListComment().get(0);
                    PostComment postCommnet = new PostComment();
                    ArrayList<PostComment> lstPostCommnet = new ArrayList<>();
                    for (String key : hashMap.keySet()) {
                        postCommnet.setComment_dateTime(DateUtils.getPostTimeFromCurrent((key.split("-"))[0], context)); //Key contain dateTime and User key;
                        postCommnet.setComment_userId((key.split("-"))[1]);
                        postCommnet.setComment_content(hashMap.get(key));
                        lstPostCommnet.add(postCommnet);
                    }
                    LinearLayoutManager manager = new LinearLayoutManager(
                            context, LinearLayoutManager.VERTICAL, false
                    );
                    item_post_cardview_lastCommentRecycleView.setLayoutManager(manager);
                    CommentItemAdapter lastCommentAd = new CommentItemAdapter(lstPostCommnet);
                    item_post_cardview_lastCommentRecycleView.setAdapter(lastCommentAd);
                }
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }

        }


        public void getCommentListFromFirebase(final int mpostIndex) {
            //final int mpostIndex = postIndex;
            //final FancyButton mitem_post_cardview_fbtnComment = item_post_cardview_fbtnComment;
            String postId = listPostDetail.get(mpostIndex).getPostId();

        }

        public String getUserId(){
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            String userId = "";
            if(mAuth!=null)
                if(mAuth.getCurrentUser()!=null)
                    userId = mAuth.getCurrentUser().getUid();
            return userId;
        }

        //
        private void setLikeOrLikedToControl(int position) {
            try {
                int numOfLike = listPostDetail.get(position).getNumOfLikes();
                if(numOfLike>0) {
                    item_post_cardview_fbtnLike_view.setVisibility(View.VISIBLE);
                    item_post_cardview_fbtnLike_view.setText(String.valueOf(numOfLike));
                }
                else{
                    item_post_cardview_fbtnLike_view.setText("");
                    item_post_cardview_fbtnLike_view.setVisibility(View.GONE);
                }
                if(listPostDetail.get(position).isLike())
                {
                    item_post_cardview_fbtnLike.setIconResource( R.drawable.liked_24);
                    item_post_cardview_fbtnLike.setText("");
                }else{
                    item_post_cardview_fbtnLike.setIconResource( R.drawable.like_bt);
                    item_post_cardview_fbtnLike.setText(context.getResources().getString(R.string.post_user_like));
                }
            }catch (Exception e){

            }
        }

    }
}
































