package com.ginsre.geo.hoang.dang.ginsre.model.weather;

import java.util.ArrayList;

/**
 * Created by hoang on 1/3/2018.
 */

public class Forecast {
    private int area;
    private int inten;
    private ArrayList<Double> l;
    private String time;

    public Forecast(){}

    public Forecast(int area, int inten, ArrayList<Double> l, String time) {
        this.area = area;
        this.inten = inten;
        this.l = l;
        this.time = time;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getInten() {
        return inten;
    }

    public void setInten(int inten) {
        this.inten = inten;
    }

    public ArrayList<Double> getL() {
        return l;
    }

    public void setL(ArrayList<Double> l) {
        this.l = l;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
