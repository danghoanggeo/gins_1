package com.ginsre.geo.hoang.dang.ginsre.model.locationplaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hoang on 12/14/2017.
 */

public class LocationPost implements Serializable {
    private String postId;
    private String userId;
    private String title;
    private String content;
    private String dateTime;
    private String status;
    private ArrayList<String> listImagesUrl;
    private boolean isLike,isThank,isSpam;
    private int numOfComment;
    private int numOfLikes;
    private int numOfShare;
    private ArrayList<HashMap<String,String>> listComment;

    public LocationPost(){
        // Default constructor required for calls to DataSnapshot.getValue(TrafficPost.class)
    }
    public LocationPost(String title, String content,ArrayList<String> listImagesUrl) {
        this.title = title;
        this.content = content;
        this.listImagesUrl = listImagesUrl;
        this.numOfComment = 0;
        this.numOfLikes = 0;
        this.numOfShare = 0;
        this.status = "0";
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public int getNumOfLikes() {
        return numOfLikes;
    }

    public void setNumOfLikes(int numOfLikes) {
        this.numOfLikes = numOfLikes;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateTime() {
        return this.dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumOfComment() {
        return numOfComment;
    }

    public void setNumOfComment(int numOfComment) {
        this.numOfComment = numOfComment;
    }

    public int getNumOfShare() {
        return numOfShare;
    }

    public void setNumOfShare(int numOfShare) {
        this.numOfShare = numOfShare;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }


    public ArrayList<HashMap<String, String>> getListComment() {
        return listComment;
    }

    public void setListComment(ArrayList<HashMap<String, String>> listComment) {
        this.listComment = listComment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getListImagesUrl() {
        return listImagesUrl;
    }

    public void setListImagesUrl(ArrayList<String> listImagesUrl) {
        this.listImagesUrl = listImagesUrl;
    }
}
