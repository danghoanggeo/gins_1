package com.ginsre.geo.hoang.dang.ginsre.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.fragment.LoactionReviewPageFragment;
import com.ginsre.geo.hoang.dang.ginsre.fragment.LocationHomePageFragment;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.ginsre.geo.hoang.dang.ginsre.view.PagerSlidingTabStrip;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class LoactionPageActivity extends AppCompatActivity {

    private MyPagerAdapter adapter;
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private ImageView image;
    private TextView activity_tab_shop_titletabs,activity_tab_location_have_discount,activity_tab_loaction_off;
    String locationId;

    com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.properties properties;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loaction_page);
        Intent intent = getIntent();
        locationId = intent.getStringExtra(ConstantUtils.LOCATION_ID);
        addControl();
        addEvent();

    }



    private void addControl() {
        tabs = (PagerSlidingTabStrip) findViewById(R.id.activity_tab_shop_tabs);
        pager = (ViewPager) findViewById(R.id.activity_tab_shop_pager);
        image = (ImageView) findViewById(R.id.activity_tab_shop_image);
        activity_tab_shop_titletabs = (TextView)findViewById(R.id.activity_tab_shop_titletabs);

        activity_tab_location_have_discount = (TextView)findViewById(R.id.activity_tab_location_have_discount);
        activity_tab_loaction_off = (TextView)findViewById(R.id.activity_tab_loaction_off);
        initFirstData();
    }


    private void initFirstData() {
        try {
            if(locationId != null){
                /*String databaseRef = GinFunctions.getDatabaseRefForGinsType(locationId.split("-")[1]);
                com.google.firebase.database.DatabaseReference mref = com.google.firebase.database.FirebaseDatabase.getInstance().getReference(databaseRef);
                mref.child(locationId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        properties = dataSnapshot.child(ConstantUtils.POST_PROPERTIES).getValue(com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.properties.class);
                        if(properties.getBackGroundUrl()!=null){
                            ImageUtil.displayImage(image,properties.getBackGroundUrl(),null);
                        }
                        activity_tab_shop_titletabs.setText(properties.getLocation_name());
                        if(properties.isOnPromotion()){
                            activity_tab_location_have_discount.setVisibility(View.VISIBLE);
                            activity_tab_loaction_off.setVisibility(View.VISIBLE);
                        }
                        intiTabPage();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });*/
                com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
                db.collection(ConstantUtils.REF_LOCATION_PAGE).document(locationId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                             properties = task.getResult().toObject(com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.properties.class);
                            if(properties.getBackGroundUrl()!=null){
                                ImageUtil.displayImage(image,properties.getBackGroundUrl(),null);
                            }
                            activity_tab_shop_titletabs.setText(properties.getLocation_name());
                            if(properties.isOnPromotion()){
                                activity_tab_location_have_discount.setVisibility(View.VISIBLE);
                                activity_tab_loaction_off.setVisibility(View.VISIBLE);
                            }
                            intiTabPage();
                        }else{
                            com.google.firebase.crash.FirebaseCrash.log(task.getException().getMessage());
                        }
                    }
                });
            }

        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void intiTabPage(){
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
        final int pageMargin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                        .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        pager.setCurrentItem(0);

        tabs.setOnTabReselectedListener(new PagerSlidingTabStrip.OnTabReselectedListener() {
            @Override
            public void onTabReselected(int position) {
               // Toast.makeText(LoactionPageActivity.this, "Tab reselected: " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addEvent() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     *
     */
    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final ArrayList<String> tabNames = new ArrayList<String>() {
            {
                add(getResources().getString(R.string.location_home_page));
                add(getResources().getString(R.string.location_review_page));
                add(getResources().getString(R.string.location_rating_page));
                if(getCurrentUserId().equals(properties.getUserId())){
                    add(getResources().getString(R.string.location_manager_page));
                }
            }
        };

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabNames.get(position);
        }

        @Override
        public int getCount() {
            return tabNames.size();
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return LocationHomePageFragment.newInstance(LoactionPageActivity.this,position,locationId,properties.getUserId().equals(getCurrentUserId()));
            } else if (position == 1) {
                return LoactionReviewPageFragment.newInstance(LoactionPageActivity.this,locationId,properties.getUserId().equals(getCurrentUserId()));
            } else if(position == 2) {
                return LocationHomePageFragment.newInstance(LoactionPageActivity.this,position,locationId,properties.getUserId().equals(getCurrentUserId()));
            } else {
                return LocationHomePageFragment.newInstance(LoactionPageActivity.this,position,locationId,properties.getUserId().equals(getCurrentUserId()));
            }
        }
    }

    private String getCurrentUserId(){
        return com.google.firebase.auth.FirebaseAuth.getInstance().getUid();
    }
}
