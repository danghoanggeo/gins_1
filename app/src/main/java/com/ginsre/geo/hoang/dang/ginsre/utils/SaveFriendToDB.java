package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.activity.LoginActivity;
import com.ginsre.geo.hoang.dang.ginsre.activity.MainActivity;
import com.ginsre.geo.hoang.dang.ginsre.common.SharedPrefenceKey;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.FriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.http.client.protocol.ClientContextConfigurer;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

/**
 * Created by hoang on 1/5/2018.
 */

public class SaveFriendToDB  {

    static Friends friends;
    static String status;
    static Context context;

    public SaveFriendToDB(){}

    public static void SaveToDB(Context mcontext,Friends mfriends, String mstatus){
        context = mcontext;
        friends = mfriends;
        status = mstatus;
        new SaveDataDB().execute(status);
    }

    public static void initForTheFirstTimeLogin(Context mcontext){
        context = mcontext;
        PreferencesUtils.init(mcontext);
        if(!PreferencesUtils.contains(SharedPrefenceKey.USER_TOKEN_ID.name())) { // for the first time user login with a new device.
            try {
                DatabaseReference mDatabase;
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(GinFunctions.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot data :dataSnapshot.getChildren())
                        {
                            FriendShip friendship = data.getValue(FriendShip.class);
                            if(friendship.getId() == null){
                                friendship.setId(data.getKey());
                            }
                            getFriendInformation(friendship.getId());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
            String token = FirebaseInstanceId.getInstance().getToken();
            PreferencesUtils.edit().putString(SharedPrefenceKey.USER_TOKEN_ID.name(), token).commit();
        }
    }
    public static void saveFriendAccept(String friendId, boolean b) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        try {
            // Save Friend to uer database;
            //Friends friends = listFriends.get(postion);
            status = ConstantUtils.FRIENDS_MYFRIENDS;
            getFriendInformation(friendId);
            //new SaveFriendToDB().execute(String.valueOf(postion));
            // saveFriendToUserDatabase(friends);
            //Save Friend to system database;
            FriendShip friendShip1 = new FriendShip();// Save to list friend of user 1
            friendShip1.setCreated(DateUtils.getDateTimeForGeoHashQuery());
            friendShip1.setSeePosi(b);
            friendShip1.setStatus(ConstantUtils.FRIENDS_MYFRIENDS);
            mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS_REQUEST).child(GinFunctions.getUserId())
                    .child(friendId).setValue(friendShip1);
            Toast.makeText(context,context.getResources().getString(R.string.congra_for_friends),Toast.LENGTH_LONG).show();
        }catch (Exception e)
        {
            //TODO: log here
        }
    }

    public static void getFriendInformation(final String friendId) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_SEARCH).child(friendId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Users user = dataSnapshot.getValue(Users.class);
                    friends = new Friends();
                    friends.setFriendsNumPriority(0);
                    friends.setFriendsName(user.getuName());
                    friends.setFriendsId(friendId);
                    friends.setFriendsUriAvatar(user.getuAvUrl());
                    new SaveDataDB().execute(friendId);
                }
                catch (Exception e)
                {
                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                com.google.firebase.crash.FirebaseCrash.log(databaseError.getMessage());
            }
        });
    }

    static class SaveDataDB extends AsyncTask<String, Void, Friends>{

        public SaveDataDB(){}
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Friends doInBackground(String... URL) {

            String userId = URL[0];
            //int index = Integer.parseInt(URL[0]);

            friends.setFriendsNumPriority(0);
            String imageURL = friends.getFriendsUriAvatar();
            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,100, baos);
                byte[] data = baos.toByteArray();
                friends.setPictureData(data);
            } catch (Exception e) {
                e.printStackTrace();
                return friends;
            }
            return friends;
        }

        @Override
        protected void onPostExecute(Friends result) {
            com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB friendDB = com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB.getInstance(context);
            friendDB.insert(result,status);
        }
    }
}