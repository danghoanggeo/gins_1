package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.model.locationplaces.LocationReview;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostComment;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Users;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 12/15/2017.
 */

public class LocationReviewAdapter extends RecyclerView.Adapter<LocationReviewAdapter.PostViewHolder>{
    private List<LocationReview> listReviewDetail;
    private Activity context;
    public LocationReviewAdapter(Activity context,List<LocationReview> listReviewDetail)
    {
        this.listReviewDetail = listReviewDetail;
        this.context = context;
    }
    @Override
    public LocationReviewAdapter.PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create new View
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_location_review,parent,false);
        LocationReviewAdapter.PostViewHolder postViewHolder = new LocationReviewAdapter.PostViewHolder(view);
        return postViewHolder;
    }

    @Override
    public void onBindViewHolder(LocationReviewAdapter.PostViewHolder holder, int position) {
        try {
            //listPostDetail.get(position);
            setUsersPost(listReviewDetail.get(position).getUserId(),holder.item_location_review_txvUserName,holder.item_location_review_imvAvatar);

            holder.item_location_review_txvTime.setText(DateUtils.getPostTimeFromCurrent(listReviewDetail.get(position).getDateTime(),context));
            holder.item_location_review_txvContent.setText(listReviewDetail.get(position).getContent());

            if(listReviewDetail.get(position).getListImagesUrl()!=null)
            {
                holder.item_location_review_coverImageView.setVisibility(View.VISIBLE);
                holder.item_location_review_coverImageView.setTag(position);
                //Load user avatar to this imageView
                ImageUtil.displayImage(holder.item_location_review_coverImageView,listReviewDetail.get(position).getListImagesUrl().get(0),null);

            }else
                holder.item_location_review_coverImageView.setVisibility(View.GONE);

            if(listReviewDetail.get(position).getUserId()!=null)
            {
                //holder.item_location_review_txvUserName.setText(listReviewDetail.get(position).getTitle());
            }else
                holder.item_location_review_txvUserName.setVisibility(View.GONE);
            //Rating

            if(! (listReviewDetail.get(position).getRating()==0))
                holder.item_location_review_userRating.setRating(listReviewDetail.get(position).getRating());
            if(!listReviewDetail.get(position).getUserId().equals(GinFunctions.getCurrentUserId()))
                holder.item_location_review_userRating.setIsIndicator(true);
            else
                holder.item_location_review_userRating.setIsIndicator(false);
            //Like
            holder.getLikeOrLikedFromFirebase(position);
            holder.item_location_review_fbtnLike.setTag(position);

            //Comment
            holder.item_location_review_txtComment.setTag(position);
            if(listReviewDetail.get(position).getNumOfComment()>0){
                holder.item_location_review_fbtnComment_view.setVisibility(View.VISIBLE);
                //holder.item_location_review_lastCommentRecycleView.setVisibility(View.VISIBLE);
                holder.item_location_review_fbtnComment_view.setText(String.valueOf(listReviewDetail.get(position).getNumOfComment())+" "+context.getResources().getString(R.string.user_post_comments));
                //holder.item_post_comment_imvUserAvatar.setTag(position);
                holder.setLastCommentToView(position);
            }else{
                holder.item_location_review_fbtnComment_view.setVisibility(View.GONE);
                holder.item_location_review_lastCommentRecycleView.setVisibility(View.GONE);
            }
            //holder.item_location_review_txtComment.setText(String.valueOf(listPostDetail.get(position).getNumOfComment()));
        }catch (Exception e){

        }

    }

    @Override
    public int getItemCount() {
        return listReviewDetail.size();
    }

    private void setUsersPost(String userId,final TextView mUserName,final ImageView mUserAvaTar) {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Users ginser = dataSnapshot.getValue(Users.class);
                    if(ginser != null) {
                        mUserName.setText(ginser.getuName());
                        if(ginser.getuAvUrl()!=null)
                        {
                            //Load user avatar to this imageView
                            ImageUtil.displayRoundImage(mUserAvaTar,ginser.getuAvUrl(),null);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    com.google.firebase.crash.FirebaseCrash.log(databaseError.getDetails());
                }
            });
        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.item_location_review_imvAvatar)
        public ImageView item_location_review_imvAvatar;
        @BindView(R.id.item_location_review_txvUserName)
        public TextView item_location_review_txvUserName;
        @BindView(R.id.item_location_review_txvTime)
        public TextView item_location_review_txvTime;
        @BindView(R.id.item_location_review_txvContent)
        public TextView item_location_review_txvContent;
        @BindView(R.id.item_location_review_coverImageView)
        public ImageView item_location_review_coverImageView;
        @BindView(R.id.item_location_review_fbtnThank)
        public FancyButton item_location_review_fbtnThank;
        @BindView(R.id.item_location_review_fbtnLike)
        public FancyButton item_location_review_fbtnLike;
        @BindView(R.id.item_location_review_fbtnSpam)
        public FancyButton item_location_review_fbtnSpam;
        @BindView(R.id.item_location_review_fbtnShare)
        public FancyButton item_location_review_fbtnShare;
        @BindView(R.id.item_location_review_txtComment)
        public TextView item_location_review_txtComment;
        @BindView(R.id.item_location_review_fbtnThank_view)
        public FancyButton item_location_review_fbtnThank_view;
        @BindView(R.id.item_location_review_fbtnLike_view)
        public FancyButton item_location_review_fbtnLike_view;
        @BindView(R.id.item_location_review_fbtnSpam_view)
        public FancyButton item_location_review_fbtnSpam_view;
        @BindView((R.id.item_location_review_fbtnComment_view))
        public FancyButton item_location_review_fbtnComment_view;
        @BindView(R.id.item_location_review_userRating)
        public RatingBar item_location_review_userRating;
        @BindView(R.id.item_location_review_lastCommentRecycleView)
        public android.support.v7.widget.RecyclerView item_location_review_lastCommentRecycleView;




        public PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            addEventChild();

        }

        private void addEventChild() {
            item_location_review_fbtnThank.setVisibility(View.GONE);
            item_location_review_fbtnLike.setOnClickListener(this);
            item_location_review_fbtnSpam.setVisibility(View.GONE);
            item_location_review_txtComment.setOnClickListener(this);
            item_location_review_fbtnShare.setOnClickListener(this);
            item_location_review_fbtnComment_view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int controlId = v.getId();
            if(controlId == R.id.item_location_review_fbtnLike)
            {
                setLikeOrLikedToFirebase();
            }

            else if(controlId == R.id.item_location_review_txtComment){
                showCommentList();
            }
            else if(controlId == R.id.item_location_review_fbtnComment_view){
                showCommentList();
            }
            else if(controlId == R.id.item_location_review_fbtnShare)
            {
                item_location_review_coverImageView.setDrawingCacheEnabled(true);
                item_location_review_coverImageView.buildDrawingCache();
                Bitmap bitmap = item_location_review_coverImageView.getDrawingCache();
                if(bitmap != null) {
                    //shareToFaceBook(bitmap,item_location_review_coverImageView);
                }else
                {
                    //TODO: Push message to user no image to share; or share text;
                }
            }
        }

        private void showCommentList() {

        }


        private void setLikeOrLikedToFirebase() {
            int position = (int)item_location_review_fbtnLike.getTag();
            if(listReviewDetail.get(position).isLike()) {
                if(listReviewDetail.get(position).getNumOfLikes()-1 >= 0)
                    listReviewDetail.get(position).setNumOfLikes(listReviewDetail.get(position).getNumOfLikes()-1);
                listReviewDetail.get(position).setLike(false);
            }
            else {
                listReviewDetail.get(position).setNumOfLikes(listReviewDetail.get(position).getNumOfLikes()+1);
                listReviewDetail.get(position).setLike(true);
            }
            //String postId = listReview.get(position).getPostId();
            setLikeOrLikedToControl(position);
        }

        // Set last comment;

        public void setLastCommentToView(final int position){
            try {
                int postIndext = (int)item_location_review_txtComment.getTag();
                if(listReviewDetail.get(position).getLastComment()!= null) {
                    if(item_location_review_lastCommentRecycleView.getVisibility()==View.GONE){
                        item_location_review_lastCommentRecycleView.setVisibility(View.VISIBLE);
                    }
                    HashMap<String, String> hashMap = listReviewDetail.get(position).getLastComment();
                    PostComment postCommnet = new PostComment();
                    ArrayList<PostComment> lstPostCommnet = new ArrayList<>();
                    for (String key : hashMap.keySet()) {
                        postCommnet.setComment_dateTime(DateUtils.getPostTimeFromCurrent((key.split("-"))[0], context)); //Key contain dateTime and User key;
                        postCommnet.setComment_userId((key.split("-"))[1]);
                        postCommnet.setComment_content(hashMap.get(key));
                        lstPostCommnet.add(postCommnet);
                    }
                    LinearLayoutManager manager = new LinearLayoutManager(
                            context, LinearLayoutManager.VERTICAL, false
                    );
                    item_location_review_lastCommentRecycleView.setLayoutManager(manager);
                    CommentItemAdapter lastCommentAd = new CommentItemAdapter(lstPostCommnet);
                    item_location_review_lastCommentRecycleView.setAdapter(lastCommentAd);
                }
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }

        }


        public void getCommentListFromFirebase(final int mpostIndex) {
            //final int mpostIndex = postIndex;
            //final FancyButton mitem_location_review_fbtnComment = item_location_review_fbtnComment;
            String postId = listReviewDetail.get(mpostIndex).getUserId();

        }

        public String getUserId(){
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            String userId = "";
            if(mAuth!=null)
                if(mAuth.getCurrentUser()!=null)
                    userId = mAuth.getCurrentUser().getUid();
            return userId;
        }

        // getLike or liked from firebase for the first time;
        public void getLikeOrLikedFromFirebase(final int position){
            try {
                /*
                String postId = listPostDetail.get(position).getPostId();
                DatabaseReference mDatabaseRefToFor = getDatabaseRefrence(postId.split("-")[1],"detail");

                mDatabaseRefToFor.child(postId).child(ConstantUtils.REF_POST_LIKE).child(getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue() != null){
                            if((boolean)dataSnapshot.getValue()){
                                listPostDetail.get(position).setLike(true);
                            }
                        }
                        setLikeOrLikedToControl(position);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });*/
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
        }

        //
        private void setLikeOrLikedToControl(int position) {
            try {
                int numOfLike = listReviewDetail.get(position).getNumOfLikes();
                if(numOfLike>0) {
                    item_location_review_fbtnLike_view.setVisibility(View.VISIBLE);
                    //item_location_review_fbtnLike_view.setText(numToStringShort(numOfLike));
                }
                else{
                    item_location_review_fbtnLike_view.setText("");
                    item_location_review_fbtnLike_view.setVisibility(View.GONE);
                }
                if(listReviewDetail.get(position).isLike())
                {
                    item_location_review_fbtnLike.setIconResource( R.drawable.liked_24);
                    item_location_review_fbtnLike.setText("");
                }else{
                    item_location_review_fbtnLike.setIconResource( R.drawable.like_bt);
                    item_location_review_fbtnLike.setText(context.getResources().getString(R.string.post_user_like));
                }
            }catch (Exception e){

            }
        }

    }

}
