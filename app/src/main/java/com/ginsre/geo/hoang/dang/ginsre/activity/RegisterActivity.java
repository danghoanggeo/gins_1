package com.ginsre.geo.hoang.dang.ginsre.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.InputStream;


public class RegisterActivity extends AppCompatActivity {
    private ImageView register_imvAvatar;
    private EditText register_name,register_phone,register_email;
    private mehdi.sakout.fancybuttons.FancyButton register_btnRegister;
    Bitmap imagePost;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2;
    com.ginsre.geo.hoang.dang.ginsre.model.users.Users users;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        addControl();
        addEvent();
    }

    private void addControl() {
        try {
            users = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getCurrentUserFromAuthentication();
            register_imvAvatar = (ImageView)findViewById(R.id.register_imvAvatar);
            register_name = (EditText)findViewById(R.id.register_name);
            register_name.setText(users.getuName());
            register_phone = (EditText)findViewById(R.id.register_phone);
            register_phone.setText(users.getuPhone());
            register_email = (EditText)findViewById(R.id.register_email);
            register_email.setText(users.getuEmail());
            register_btnRegister = (mehdi.sakout.fancybuttons.FancyButton)findViewById(R.id.register_btnRegister);
        }catch (Exception e){

        }
    }

    private void addEvent() {
        register_imvAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userTakePictureToPost();
            }
        });
        register_btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkUserInfor();
            }
        });
    }

    private void checkUserInfor() {
        try {
            if(register_name.getText().toString() == "" || register_name.getText().length() > 50){
                users.setuName(register_name.getText().toString());
            }else{
                if(imagePost == null)
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.select_your_avatar),Toast.LENGTH_LONG).show();
                    userTakePictureToPost();
                }else{
                    saveUserAvatarToFirebase();
                }
            }
        }catch (Exception e){

        }
    }

    private void saveUserAvatarToFirebase() {
        byte[] data = GinFunctions.bitmapToByteArray(imagePost,80);
        if(data != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getResources().getString(R.string.user_registering));
            progressDialog.show();
            StorageReference mStorageRef = com.google.firebase.storage.FirebaseStorage.getInstance().getReference();
            //StorageReference childPath = mStorageRef.child(ConstantUtils.STORE_TRAFFIC_IMAGES).child(nlocationDetailId);
            mStorageRef.child(ConstantUtils.USER_REF_USER).child(GinFunctions.getUserId()).child(com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils.getTimeLongForPostFromRef()).putBytes(data)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Uri downloadUri = taskSnapshot.getDownloadUrl();
                            progressDialog.dismiss();
                            users.setuAvUrl(downloadUri.toString());
                            updateUserInfor();
                        }
                    });
        }
    }

    private void updateUserInfor() {
        com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.addNewUserToFirebase(getApplicationContext(),users);
        startActivity(new Intent(RegisterActivity.this,MainActivity.class));
    }
    private void userTakePictureToPost() {
        try {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
            else
            {
                Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT, null);
                galleryintent.setType("image/*");
                //galleryintent.putExtra("GALLERY","gallery");
                //Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //cameraIntent.putExtra("CAMERA","camera");
                Intent chooser = new Intent(Intent.ACTION_CHOOSER);
                chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
                chooser.putExtra(Intent.EXTRA_TITLE, "Take or select a photo");

                //Intent[] intentArray =  {cameraIntent};
                //chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooser,CAMERA_REQUEST_CODE);
            }
        }catch (Exception e){

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK)
        {
            if (data == null) {
                return;
            }
            try {
                //String type = data.getStringExtra("CAMERA");
                InputStream inputStream = null;
                try {
                    inputStream = getApplicationContext().getContentResolver().openInputStream(data.getData());
                }catch (Exception e){

                }
                if(inputStream == null){
                    imagePost = (Bitmap)data.getExtras().get("data");
                }else{
                    imagePost = BitmapFactory.decodeStream(inputStream);
                }
                register_imvAvatar.setImageBitmap(imagePost);

            }catch (Exception e){

            }

        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent,CAMERA_REQUEST_CODE);
                }
            }
        }
    }
}
