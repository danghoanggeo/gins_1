package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostComment;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostDetail;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Users;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 12/21/2017.
 */

public class PostViewAdapter extends RecyclerView.Adapter<PostViewAdapter.PostViewHolder>{
    private List<PostDetail> listPostDetail;
    private Context context;
    public PostViewAdapter(List<PostDetail> listPostDetail, Context mcontext)
    {
        this.listPostDetail = listPostDetail;
        context = mcontext;
    }
    @Override
    public PostViewAdapter.PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create new View
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_post_cardview,parent,false);
        PostViewAdapter.PostViewHolder postViewHolder = new PostViewAdapter.PostViewHolder(view);
        return postViewHolder;
    }

    @Override
    public void onBindViewHolder(PostViewAdapter.PostViewHolder holder, int position) {
        //listPostDetail.get(position);
        setUsersPost(listPostDetail.get(position).getUserId(),holder.item_post_cardview_txvUserName,holder.item_post_cardview_imvAvatar);

        holder.item_post_cardview_txvTime.setText(listPostDetail.get(position).getDateTime());
        holder.item_post_cardview_txvContent.setText(listPostDetail.get(position).getContent());
        if(listPostDetail.get(position).getImagesUrl()!=null)
        {
            holder.item_post_cardview_coverImageView.setVisibility(View.VISIBLE);
            holder.item_post_cardview_coverImageView.setTag(position);
            //Load user avatar to this imageView
            ImageUtil.displayImage(holder.item_post_cardview_coverImageView,listPostDetail.get(position).getImagesUrl(),null);
        }else
            holder.item_post_cardview_coverImageView.setVisibility(View.GONE);

        if(listPostDetail.get(position).getType()!=null)
        {
            holder.item_post_cardview_titleTextView.setVisibility(View.VISIBLE);
            holder.item_post_cardview_titleTextView.setText(listPostDetail.get(position).getType());
        }else
            holder.item_post_cardview_titleTextView.setVisibility(View.GONE);
        //Thank
        holder.getThankOrThankedFromFirebase(position);
        holder.item_post_cardview_fbtnThank.setTag(position);
        //Like
        //TODO:
        holder.getLikeOrLikedFromFirebase(position);
        holder.item_post_cardview_fbtnLike.setTag(position);
        //Spam

        holder.getSpamOrSpamedFromFirebase(position);
        holder.item_post_cardview_fbtnSpam.setTag(position);
        //Comment
        holder.item_post_cardview_txtComment.setTag(listPostDetail.get(position).getPostId());
        if(listPostDetail.get(position).getNumOfComment()>0){
            holder.item_post_cardview_fbtnComment_view.setVisibility(View.VISIBLE);
            //holder.item_post_cardview_lastCommentRecycleView.setVisibility(View.VISIBLE);
            holder.item_post_cardview_fbtnComment_view.setText(String.valueOf(listPostDetail.get(position).getNumOfComment())+" "+context.getResources().getString(R.string.user_post_comments));
            //holder.item_post_comment_imvUserAvatar.setTag(position);
            holder.setLastCommentToView(position);
        }else{
            holder.item_post_cardview_fbtnComment_view.setVisibility(View.GONE);
            holder.item_post_cardview_lastCommentRecycleView.setVisibility(View.GONE);
        }
        //holder.item_post_cardview_txtComment.setText(String.valueOf(listPostDetail.get(position).getNumOfComment()));
    }

    @Override
    public int getItemCount() {
        return listPostDetail.size();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.item_post_cardview_imvAvatar)
         ImageView item_post_cardview_imvAvatar;
        @BindView(R.id.item_post_cardview_txvUserName)
         TextView item_post_cardview_txvUserName;
        @BindView(R.id.item_post_cardview_txvTime)
         TextView item_post_cardview_txvTime;
        @BindView(R.id.item_post_cardview_txvContent)
         TextView item_post_cardview_txvContent;
        @BindView(R.id.item_post_cardview_coverImageView)
         ImageView item_post_cardview_coverImageView;
        @BindView(R.id.item_post_cardview_titleTextView)
         TextView item_post_cardview_titleTextView;
        @BindView(R.id.item_post_cardview_fbtnThank)
         FancyButton item_post_cardview_fbtnThank;
        @BindView(R.id.item_post_cardview_fbtnLike)
         FancyButton item_post_cardview_fbtnLike;
        @BindView(R.id.item_post_cardview_fbtnSpam)
         FancyButton item_post_cardview_fbtnSpam;
        @BindView(R.id.item_post_cardview_fbtnShare)
         FancyButton item_post_cardview_fbtnShare;
        @BindView(R.id.item_post_cardview_txtComment)
         TextView item_post_cardview_txtComment;
        @BindView(R.id.item_post_cardview_fbtnThank_view)
         FancyButton item_post_cardview_fbtnThank_view;
        @BindView(R.id.item_post_cardview_fbtnLike_view)
         FancyButton item_post_cardview_fbtnLike_view;
        @BindView(R.id.item_post_cardview_fbtnSpam_view)
         FancyButton item_post_cardview_fbtnSpam_view;
        @BindView((R.id.item_post_cardview_fbtnComment_view))
         FancyButton item_post_cardview_fbtnComment_view;
        @BindView(R.id.item_post_cardview_lastCommentRecycleView)
         android.support.v7.widget.RecyclerView item_post_cardview_lastCommentRecycleView;




        PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            addEventChild();

        }

        private void addEventChild() {
            item_post_cardview_fbtnThank.setOnClickListener(this);
            item_post_cardview_fbtnLike.setOnClickListener(this);
            item_post_cardview_fbtnSpam.setOnClickListener(this);
            item_post_cardview_txtComment.setOnClickListener(this);
            item_post_cardview_fbtnShare.setOnClickListener(this);
            item_post_cardview_fbtnComment_view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int controlId = v.getId();
            if(controlId == R.id.item_post_cardview_fbtnThank)
            {

                setThankOrThankedToFirebase();// User have only thank, like or spam a post;
                if(listPostDetail.get((int)item_post_cardview_fbtnThank.getTag()).isSpam())
                    setSpamOrSpamedToFirebase();
            }
            else if(controlId == R.id.item_post_cardview_fbtnLike)
            {
                setLikeOrLikedToFirebase();
                if(listPostDetail.get((int)item_post_cardview_fbtnLike.getTag()).isSpam())
                    setSpamOrSpamedToFirebase();
            }
            else if(controlId == R.id.item_post_cardview_fbtnSpam)
            {
                setSpamOrSpamedToFirebase();
                if(listPostDetail.get((int)item_post_cardview_fbtnSpam.getTag()).isThank())
                    setThankOrThankedToFirebase();
                if(listPostDetail.get((int)item_post_cardview_fbtnSpam.getTag()).isLike())
                    setLikeOrLikedToFirebase();
            }
            else if(controlId == R.id.item_post_cardview_txtComment){
                showCommentList((String)item_post_cardview_txtComment.getTag());
            }
            else if(controlId == R.id.item_post_cardview_fbtnComment_view){
                showCommentList((String)item_post_cardview_txtComment.getTag());
            }
            else if(controlId == R.id.item_post_cardview_fbtnShare)
            {
                item_post_cardview_coverImageView.setDrawingCacheEnabled(true);
                item_post_cardview_coverImageView.buildDrawingCache();
                Bitmap bitmap = item_post_cardview_coverImageView.getDrawingCache();
                if(bitmap != null) {
                    shareToFaceBook(bitmap,item_post_cardview_coverImageView);
                }else
                {
                    //TODO: Push message to user no image to share; or share text;
                }
            }
        }

        private void showCommentList(String postId) {
            DataChangeNotification.getInstance().notifyChange(IssueKey.POST_COMMENT_VIEW,postId);
        }


        private void setThankOrThankedToFirebase() {
            int position = (int)item_post_cardview_fbtnThank.getTag();
            if(!GinFunctions.getUserId().equals(listPostDetail.get(position).getUserId())){
                if(listPostDetail.get(position).isThank()) {
                    if(listPostDetail.get(position).getNumOfThanks()-1 > 0)
                        listPostDetail.get(position).setNumOfThanks(listPostDetail.get(position).getNumOfThanks()-1);
                    listPostDetail.get(position).setThank(false);
                }
                else {
                    listPostDetail.get(position).setNumOfThanks(listPostDetail.get(position).getNumOfThanks()+1);
                    listPostDetail.get(position).setThank(true);
                }
                String postId = listPostDetail.get(position).getPostId();
                Map<String,Boolean> userThank = new HashMap<>();
                userThank.put(GinFunctions.getUserId(),listPostDetail.get(position).isThank());

                DatabaseReference mDatabaseRefToFor = FirebaseDatabase.getInstance().getReference(GinFunctions.getDatabaseRefForPostDetail(postId.split("-")[1]));
                mDatabaseRefToFor.child(postId).child(ConstantUtils.REF_POST_THANKS).child(GinFunctions.getUserId()).setValue(listPostDetail.get(position).isThank());
                setThankOrThanedToControl(position);
            }

        }

        private void setLikeOrLikedToFirebase() {
            int position = (int)item_post_cardview_fbtnLike.getTag();
            if(listPostDetail.get(position).isLike()) {
                if(listPostDetail.get(position).getNumOfLikes()-1 >= 0)
                    listPostDetail.get(position).setNumOfLikes(listPostDetail.get(position).getNumOfLikes()-1);
                listPostDetail.get(position).setLike(false);
            }
            else {
                listPostDetail.get(position).setNumOfLikes(listPostDetail.get(position).getNumOfLikes()+1);
                listPostDetail.get(position).setLike(true);
            }
            String postId = listPostDetail.get(position).getPostId();
            DatabaseReference mDatabaseRefToFor = FirebaseDatabase.getInstance().getReference(GinFunctions.getDatabaseRefForPostDetail(postId.split("-")[1]));
            mDatabaseRefToFor.child(listPostDetail.get(position).getPostId())
                    .child(ConstantUtils.REF_POST_LIKE).child(GinFunctions.getUserId()).setValue(listPostDetail.get(position).isLike());
            setLikeOrLikedToControl(position);
        }

        private void setSpamOrSpamedToFirebase() {
            int position = (int)item_post_cardview_fbtnSpam.getTag();
            if(!GinFunctions.getUserId().equals(listPostDetail.get(position).getUserId())) {
                if (listPostDetail.get(position).isSpam()) {
                    if (listPostDetail.get(position).getNumOfSpam() - 1 >= 0)
                        listPostDetail.get(position).setNumOfSpam(listPostDetail.get(position).getNumOfSpam() - 1);
                    listPostDetail.get(position).setSpam(false);
                } else {
                    listPostDetail.get(position).setNumOfSpam(listPostDetail.get(position).getNumOfSpam() + 1);
                    listPostDetail.get(position).setSpam(true);
                }
                String postId = listPostDetail.get(position).getPostId();
                DatabaseReference mDatabaseRefToFor = FirebaseDatabase.getInstance().getReference(GinFunctions.getDatabaseRefForPostDetail(postId.split("-")[1]));
                mDatabaseRefToFor.child(postId).child(ConstantUtils.REF_POST_SPAM).child(GinFunctions.getUserId()).setValue(listPostDetail.get(position).isSpam());
                setSpamOrSpamedToControl(position);
            }
        }

        // Set last comment;

        public void setLastCommentToView(final int position){
            try {
                //int postIndext = (int)item_post_cardview_txtComment.getTag();
                if(listPostDetail.get(position).getListComment()!= null) {
                    if(item_post_cardview_lastCommentRecycleView.getVisibility()==View.GONE){
                        item_post_cardview_lastCommentRecycleView.setVisibility(View.VISIBLE);
                    }
                    Map<String, String> hashMap = listPostDetail.get(position).getListComment().get(0);
                    PostComment postCommnet = new PostComment();
                    ArrayList<PostComment> lstPostCommnet = new ArrayList<>();
                    for (String key : hashMap.keySet()) {
                        postCommnet.setComment_dateTime(DateUtils.getTimeFromCurrentTime((key.split("-"))[0], context)); //Key contain dateTime and User key;
                        postCommnet.setComment_userId((key.split("-"))[1]);
                        postCommnet.setComment_content(hashMap.get(key));
                        lstPostCommnet.add(postCommnet);
                    }
                    LinearLayoutManager manager = new LinearLayoutManager(
                            context, LinearLayoutManager.VERTICAL, false
                    );
                    item_post_cardview_lastCommentRecycleView.setLayoutManager(manager);
                    CommentItemAdapter lastCommentAd = new CommentItemAdapter(lstPostCommnet);
                    item_post_cardview_lastCommentRecycleView.setAdapter(lastCommentAd);
                }
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }

        }


        // getThank or thanked from firebase for the first time;
        public void getThankOrThankedFromFirebase(final int position){
            try {
                String postId = listPostDetail.get(position).getPostId();
                DatabaseReference mDatabaseRefToFor = FirebaseDatabase.getInstance().getReference(GinFunctions.getDatabaseRefForPostDetail(postId.split("-")[1]));

                mDatabaseRefToFor.child(postId).child(ConstantUtils.REF_POST_THANKS).child(GinFunctions.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue() != null){
                            if((boolean)dataSnapshot.getValue()){
                                listPostDetail.get(position).setThank(true);
                            }
                        }
                        setThankOrThanedToControl(position);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
        }

        //Set thank or thanked
        public void setThankOrThanedToControl(int position)
        {
            try {
                int thankIconId;
                int numOfThanksInit = listPostDetail.get(position).getNumOfThanks();
                if(numOfThanksInit>0){
                    if(numOfThanksInit>100)
                        thankIconId = R.drawable.thank_24_3;
                    else if(numOfThanksInit>50)
                        thankIconId = R.drawable.thank_24_2;
                    else
                        thankIconId = R.drawable.thank_24;

                    item_post_cardview_fbtnThank_view.setText(numToStringShort(numOfThanksInit));
                    item_post_cardview_fbtnThank_view.setIconResource(thankIconId);
                }

                if(listPostDetail.get(position).isThank()) {
                    item_post_cardview_fbtnThank.setIconResource(R.drawable.handshaked);
                    item_post_cardview_fbtnThank.setText("");
                }
                else {
                    item_post_cardview_fbtnThank.setIconResource(R.drawable.handshake);
                    item_post_cardview_fbtnThank.setText(context.getResources().getString(R.string.post_user_thank));
                }
            }catch (Exception e){

            }
        }

        // getLike or liked from firebase for the first time;
        public void getLikeOrLikedFromFirebase(final int position){
            try {
                String postId = listPostDetail.get(position).getPostId();
                DatabaseReference mDatabaseRefToFor = FirebaseDatabase.getInstance().getReference(GinFunctions.getDatabaseRefForPostDetail(postId.split("-")[1]));

                mDatabaseRefToFor.child(postId).child(ConstantUtils.REF_POST_LIKE).child(GinFunctions.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue() != null){
                            if((boolean)dataSnapshot.getValue()){
                                listPostDetail.get(position).setLike(true);
                            }
                        }
                        setLikeOrLikedToControl(position);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
        }

        //
        private void setLikeOrLikedToControl(int position) {
            try {
                int numOfLike = listPostDetail.get(position).getNumOfLikes();
                if(numOfLike>0) {
                    item_post_cardview_fbtnLike_view.setVisibility(View.VISIBLE);
                    item_post_cardview_fbtnLike_view.setText(numToStringShort(numOfLike));
                }
                else{
                    item_post_cardview_fbtnLike_view.setText("");
                    item_post_cardview_fbtnLike_view.setVisibility(View.GONE);
                }
                if(listPostDetail.get(position).isLike())
                {
                    item_post_cardview_fbtnLike.setIconResource( R.drawable.liked_24);
                    item_post_cardview_fbtnLike.setText("");
                }else{
                    item_post_cardview_fbtnLike.setIconResource( R.drawable.like_bt);
                    item_post_cardview_fbtnLike.setText(context.getResources().getString(R.string.post_user_like));
                }
            }catch (Exception e){

            }
        }

        public void getSpamOrSpamedFromFirebase(final int position){
            try {
                String postId = listPostDetail.get(position).getPostId();
                DatabaseReference mDatabaseRefToFor = FirebaseDatabase.getInstance().getReference(GinFunctions.getDatabaseRefForPostDetail(postId.split("-")[1]));

                mDatabaseRefToFor.child(postId).child(ConstantUtils.REF_POST_SPAM).child(GinFunctions.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getValue() != null){
                            if((boolean)dataSnapshot.getValue()){
                                listPostDetail.get(position).setSpam(true);
                            }
                        }
                        setSpamOrSpamedToControl(position);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }catch (Exception e){
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
        }

        public void setSpamOrSpamedToControl(int position) {
            try {
                int numOfSpam = listPostDetail.get(position).getNumOfSpam();
                if (numOfSpam > 0) {
                    item_post_cardview_fbtnSpam_view.setVisibility(View.VISIBLE);
                    item_post_cardview_fbtnSpam_view.setText(numToStringShort(numOfSpam));
                    int spamIconId = R.drawable.spam_22;
                    if (numOfSpam > 20)
                        spamIconId = R.drawable.spam_24_4;
                    else if (numOfSpam > 10)
                        spamIconId = R.drawable.spam_24_3;
                    else if (numOfSpam > 5)
                        spamIconId = R.drawable.spam_24_1;
                    item_post_cardview_fbtnSpam_view.setIconResource(spamIconId);
                } else {
                    item_post_cardview_fbtnSpam_view.setText("");
                    item_post_cardview_fbtnSpam_view.setVisibility(View.GONE);
                }

                if (listPostDetail.get(position).isSpam()) {
                    item_post_cardview_fbtnSpam.setText("");
                    item_post_cardview_fbtnSpam.setIconResource(R.drawable.spamed_24);
                } else {
                    item_post_cardview_fbtnSpam.setIconResource(R.drawable.spam_22);
                    item_post_cardview_fbtnSpam.setText(context.getResources().getString(R.string.post_user_spam));
                }
            } catch (Exception e) {

            }
        }

    }
    /**
     *
     * @param userId
     * @param mUserName
     * @param mUserAvaTar
     */
    private void setUsersPost(String userId,final TextView mUserName,final ImageView mUserAvaTar) {
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_GENERAL).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Users ginser = dataSnapshot.getValue(Users.class);
                    if(ginser != null) {
                        mUserName.setText(ginser.getuName());
                        if(ginser.getuAvUrl()!="")
                        {
                            //Load user avatar to this imageView
                            ImageUtil.displayRoundImage(mUserAvaTar,ginser.getuAvUrl(),null);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    com.google.firebase.crash.FirebaseCrash.log(databaseError.getDetails());
                }
            });
        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private String numToStringShort(int num)
    {
        String numThanks="";
        if(num>1000) {
            numThanks += num / 1000 + "k+";
        }
        else
            numThanks += num;
        return numThanks;
    }

    private void shareToFaceBook(Bitmap bitmap, ImageView item_post_cardview_coverImageView) {
        final int index = (int)item_post_cardview_coverImageView.getTag();
        final PostDetail postDetail = listPostDetail.get(index);
        if(postDetail!=null){
            CallbackManager callbackManager = CallbackManager.Factory.create();
            ShareDialog shareDialog = new ShareDialog((Activity) context);
            if(ShareDialog.canShow(SharePhotoContent.class))
            {
                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(bitmap)
                        .build();

                // Create the content
                final SharePhotoContent content = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .setRef("GINS - "+postDetail.getType())
                        .build();
                shareDialog.show(content);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(context,context.getResources().getString(R.string.fb_sharing_successfull),Toast.LENGTH_SHORT).show();
                        //mDatabaseRefToFor.child(postDetail.getPostId()).child(ConstantUtils.REF_POST_CONTENT).child("numOfComment").setValue(listPostDetail.get(index).getNumOfShare()+1);
                        //listPostDetail.get(index).setNumOfShare(listPostDetail.get(index).getNumOfShare()+1);
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(context,context.getResources().getString(R.string.fb_sharing_error),Toast.LENGTH_SHORT).show();
                    }
                });
            }else
            {
                Toast.makeText(context,context.getResources().getString(R.string.fb_sharing_error),Toast.LENGTH_SHORT).show();
            }
        }
    }

}