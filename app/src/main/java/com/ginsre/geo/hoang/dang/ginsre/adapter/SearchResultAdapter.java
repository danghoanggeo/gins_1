package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.model.core.geometry;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.UndoAdapter;
import com.nhaarman.listviewanimations.util.Swappable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hoang on 11/27/2017.
 */

public class SearchResultAdapter extends BaseAdapter implements Swappable, UndoAdapter, OnDismissCallback {

    ArrayList<Friends> friendsArrayList;
    Activity context;

    public SearchResultAdapter(ArrayList<Friends> lstFriend, Activity context){
        this.friendsArrayList = lstFriend;
        this.context = context;
    }
    @Override
    public int getCount() {
        return friendsArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return friendsArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final SearchResultAdapter.ViewHolder holder;
        if (view == null) {
            LayoutInflater inflagter = LayoutInflater.from(context);
            view = inflagter.inflate(R.layout.item_search_friends, viewGroup, false);
            holder = new SearchResultAdapter.ViewHolder(view);
            //holder.item_search_result_friend_item.setTag(position);
            holder.bind(position);
            view.setTag(holder);
        } else {
            holder = (SearchResultAdapter.ViewHolder) view.getTag();
        }

        return view;
    }

    @Override
    @NonNull
    public View getUndoClickView(@NonNull View view) {
        return view.findViewById(R.id.undo_button_notification);
    }

    @Override
    @NonNull
    public View getUndoView(final int position, final View convertView,
                            @NonNull final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_undo_view,
                    parent, false);
        }
        return view;
    }

    @Override
    public void onDismiss(@NonNull final ViewGroup listView,
                          @NonNull final int[] reverseSortedPositions) {
        for (int position : reverseSortedPositions) {
            remove(position);
        }
    }
    public void remove(int position) {
        friendsArrayList.remove(position);
    }

    @Override
    public void swapItems(int i, int i1) {

    }
    private  class ViewHolder {
        LinearLayout item_search_result_friend_item;
        ImageView item_search_result_friend_imvAvatar,item_search_result_friend_current,item_search_result_friend_home;
        TextView item_search_result_friend_txvUserName;

        public ViewHolder(View itemView) {
            addControl(itemView);
            addEvent();
        }

        private void addEvent() {
            item_search_result_friend_home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = (int)item_search_result_friend_imvAvatar.getTag();
                    directionToFriendAddress(index);
                    //Toast.makeText(context,"Go To: "+friendsArrayList.get((int)item_search_result_friend_item.getTag()),Toast.LENGTH_LONG).show();
                }
            });
            item_search_result_friend_current.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = (int)item_search_result_friend_imvAvatar.getTag();
                    directionToFriendCurrent(index);
                }
            });
        }

        private void addControl(View view) {
            item_search_result_friend_item = (LinearLayout) view.findViewById(R.id.item_search_result_friend_item);
            item_search_result_friend_imvAvatar = (ImageView) view.findViewById(R.id.item_search_result_friend_imvAvatar);
            item_search_result_friend_current = (ImageView) view.findViewById(R.id.item_search_result_friend_current);
            item_search_result_friend_home = (ImageView) view.findViewById(R.id.item_search_result_friend_home);
            item_search_result_friend_txvUserName = (TextView) view.findViewById(R.id.item_search_result_friend_txvUserName);
        }

        public void bind(int position) {
            item_search_result_friend_txvUserName.setText(friendsArrayList.get(position).getFriendsName());
            item_search_result_friend_imvAvatar.setTag(position);
            item_search_result_friend_imvAvatar.setImageDrawable(friendsArrayList.get(position).getPictureDataDrawable());
            //ImageUtil.displayRoundImage(item_search_result_friend_imvAvatar, friendsArrayList.get(position).getFriendsUriAvatar(), null);
        }

        private void directionToFriendCurrent(int index){
            final Friends friends = friendsArrayList.get(index);
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child(ConstantUtils.GINS_DATA_DYNAMIC).child(ConstantUtils.USER_TEMP_LOCATION).child(friends.getFriendsId()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                            ArrayList<Double> llon = (ArrayList<Double>)dataSnapshot.child("l").getValue();
                            LatLng latlong = new LatLng( llon.get(0)
                                    , llon.get(1));
                            Toast.makeText(context, context.getResources().getString(R.string.letGoToYourFriendHome).replace("xx",friends.getFriendsName()), Toast.LENGTH_LONG).show();
                            friends.setLatLng(latlong);
                            HashMap<String,Object> hashMap = new HashMap<>();
                            hashMap.put(ConstantUtils.DIRECT_TO_FRIENDS,friends);
                            DataChangeNotification.getInstance().notifyChange(IssueKey.DIRECTTION_TO_ADDRESS, hashMap);
                            context.finish();
                    }catch (Exception e) {
                        FirebaseCrash.log(e.getMessage());
                        //Toast.makeText(context, context.getResources().getString(R.string.friend_is_not_update_home_address), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Toast.makeText(context, context.getResources().getString(R.string.access_to_friend_current_is_not_allow), Toast.LENGTH_LONG).show();
                }
            });
        }

        private void directionToFriendAddress(int index) {
            try {
                final Friends friends = friendsArrayList.get(index);
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_USER_PLACES).child(friends.getFriendsId())
                        .child(ConstantUtils.USER_REF_USER_PLACES_HOME).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot != null)
                        {
                            for(DataSnapshot data : dataSnapshot.getChildren()){
                                try {
                                    if((Boolean) data.child("stay").getValue()){
                                        ArrayList<Double> llon = (ArrayList<Double>)data.child("l").getValue();
                                        LatLng latlong = new LatLng( llon.get(0)
                                                , llon.get(1));
                                        Toast.makeText(context, context.getResources().getString(R.string.letGoToYourFriendHome).replace("xx",friends.getFriendsName()), Toast.LENGTH_LONG).show();
                                        friends.setLatLng(latlong);
                                        HashMap<String,Object> hashMap = new HashMap<>();
                                        hashMap.put(ConstantUtils.DIRECT_TO_FRIENDS,friends);
                                        DataChangeNotification.getInstance().notifyChange(IssueKey.DIRECTTION_TO_ADDRESS, hashMap);
                                        context.finish();
                                        break;
                                    }
                                }catch (Exception e) {
                                    FirebaseCrash.log(e.getMessage());
                                    Toast.makeText(context, context.getResources().getString(R.string.friend_is_not_update_home_address), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                        else
                        {
                            Toast.makeText(context,context.getResources().getString(R.string.friend_is_not_update_home_address),Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        FirebaseCrash.log(databaseError.getMessage());
                        DataChangeNotification.getInstance().notifyChange(IssueKey.SHOW_DIALOG_ERROR_FRIENDS, context.getResources().getString(R.string.access_to_friend_address_is_not_allow));

                        //Toast.makeText(mcontext, mcontext.getResources().getString(R.string.access_to_friend_address_is_not_allow), Toast.LENGTH_LONG).show();
                    }
                });
            }catch (Exception e){
                FirebaseCrash.log(e.getMessage());
            }

        }
    }

}
