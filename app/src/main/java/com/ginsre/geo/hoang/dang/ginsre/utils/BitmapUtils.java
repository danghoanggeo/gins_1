package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;


public class BitmapUtils {

    /**
     * bitmap with alpha channel
     */
    public static final String LOG_TAG = "BitmapUtils";
    private static final String ALPHA_BITMAP_MIME_TYPE_END = "png";

    private static final int DEAFULT_QUALITY = 100;
    private BitmapFactory.Options mInitBitmapFactoryOptions;
    private BitmapFactory.Options mBitmapFactoryOptions;
    private boolean mPreferUseRgb8888 = SDKVersionUtils.hasHoneycombMR2();
    private boolean mPreDecoded = false;





    //To get drawable and set to imagebox

    public static Drawable byteToDrawable(byte[] data) {

        if (data == null)
            return null;
        else
            return new BitmapDrawable(BitmapFactory.decodeByteArray(data, 0, data.length));
    }
    /**
     *
     * @param bitmapFactoryOptions
     */
    public BitmapUtils(BitmapFactory.Options bitmapFactoryOptions) {
        if (bitmapFactoryOptions != null) {
            mInitBitmapFactoryOptions = bitmapFactoryOptions;
        } else {
            mInitBitmapFactoryOptions = createDefaultOptions();
        }
    }


    public BitmapUtils() {
        mInitBitmapFactoryOptions = createDefaultOptions();
    }

    /**
     * set prefer use rgb 8888
     *
     * @param enable enable
     */
    public void setPreferUseRgb8888(boolean enable) {
        mPreferUseRgb8888 = enable;
    }

    private BitmapFactory.Options createDefaultOptions() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inDither = mPreferUseRgb8888;
        return options;
    }

    /**
     * @param cancelLastDecode
     */
    private void resetBitmapFactoryOptions(boolean cancelLastDecode) {
        if (cancelLastDecode && mBitmapFactoryOptions != null) {
            mBitmapFactoryOptions.requestCancelDecode();
        }
        mBitmapFactoryOptions = newBitmapOptionsWithInitOptions();
        mPreDecoded = false;
    }

    private BitmapFactory.Options newBitmapOptionsWithInitOptions() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDither = mInitBitmapFactoryOptions.inDither;
        options.inJustDecodeBounds = mInitBitmapFactoryOptions.inJustDecodeBounds;
        options.inPreferredConfig = mInitBitmapFactoryOptions.inPreferredConfig;
        options.inDensity = mInitBitmapFactoryOptions.inDensity;
        options.inInputShareable = mInitBitmapFactoryOptions.inInputShareable;
        options.inTempStorage = mInitBitmapFactoryOptions.inTempStorage;
        options.inTargetDensity = mInitBitmapFactoryOptions.inTargetDensity;
        options.inScreenDensity = mInitBitmapFactoryOptions.inScreenDensity;
        options.inSampleSize = mInitBitmapFactoryOptions.inSampleSize;
        options.inPurgeable = mInitBitmapFactoryOptions.inPurgeable;
        return options;
    }

    /**
     * get bitmap factory options
     *
     * @return bitmap factory options
     */
    public BitmapFactory.Options getBitmapFactoryOptions() {
        return mInitBitmapFactoryOptions;
    }

    /**
     *
     *
     * @param filePath
     * @return
     */
    public Bitmap decodeBitmap(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return null;
        }
        resetBitmapFactoryOptions(false);
        if (needResetAlphaChannel()) {
            mBitmapFactoryOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, mBitmapFactoryOptions);
            initAlphaChannel(mBitmapFactoryOptions);
            mBitmapFactoryOptions.inJustDecodeBounds = false;
        }
        return processBitmapAlpha(BitmapFactory.decodeFile(filePath, mBitmapFactoryOptions));
    }

    /**
     *
     * @param filePath
     * @param maxWidth
     * @param maxHeight
     * @return
     */
    public Bitmap decodeBitmap(String filePath, int maxWidth, int maxHeight) {
        return decodeBitmap(filePath, maxWidth, maxHeight, false);
    }

    /**
     *
     * @param filePath
     * @param maxWidth
     * @param maxHeight
     * @param cancelLastDecode
     * @return 图片
     */
    public Bitmap decodeBitmap(String filePath, int maxWidth, int maxHeight, boolean cancelLastDecode) {
        try {
            if (!TextUtils.isEmpty(filePath) && maxWidth > 0 && maxHeight > 0) {
                resetBitmapFactoryOptions(cancelLastDecode);
                mBitmapFactoryOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(filePath, mBitmapFactoryOptions);
                if (initBitmapFactoryOptions(mBitmapFactoryOptions, maxWidth, maxHeight)) {
                    if (needResetAlphaChannel()) {
                        initAlphaChannel(mBitmapFactoryOptions);
                    }
                    LogUtils.d(LOG_TAG, "decodeBitmap, filePath: " + filePath);
                    return processBitmapAlpha(BitmapFactory.decodeFile(filePath, mBitmapFactoryOptions));
                }
            }
        } catch (OutOfMemoryError error) {
            System.gc();
            LogUtils.d(LOG_TAG, "decodeBitmap OutOfMemoryError filePath=" + filePath);
        }

        return null;
    }

    private Bitmap processBitmapAlpha(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            bitmap.setHasAlpha(!mayNotHasAlphaChannel(mBitmapFactoryOptions));
        }
        return bitmap;
    }

    private boolean needResetAlphaChannel() {
        return mBitmapFactoryOptions.inPreferredConfig == Bitmap.Config.ARGB_8888 && !mPreferUseRgb8888;
    }

    /**
     * @param bitmapOptions bitmap options
     * @param maxWidth      max width
     * @param maxHeight     max height
     * @return true if success
     */
    public static boolean initBitmapFactoryOptions(BitmapFactory.Options bitmapOptions, int maxWidth, int maxHeight) {
        if (bitmapOptions.inJustDecodeBounds && bitmapOptions.outHeight > 0 && bitmapOptions.outWidth > 0) {
            if (bitmapOptions.outWidth > (maxWidth << 1) || bitmapOptions.outHeight > (maxHeight << 1)) {
                bitmapOptions.inSampleSize = Math.max(
                        bitmapOptions.outWidth / maxWidth
                        , bitmapOptions.outHeight / maxHeight);
            }
            bitmapOptions.inJustDecodeBounds = false;
            return true;
        }
        return false;
    }

    /**
     * set bitmap options preferred config
     *
     * @param bitmapOptions bitmap options
     * @return true if options is valid
     */
    public static boolean initAlphaChannel(BitmapFactory.Options bitmapOptions) {
        if (!TextUtils.isEmpty(bitmapOptions.outMimeType)) {
            if (mayNotHasAlphaChannel(bitmapOptions)) {
                bitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;
                bitmapOptions.inDither = false;
            } else {
                bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            }
            return true;
        }
        return false;
    }

    /**
     * FavoriteIdLoader if bitmap may not contain alpha channel
     *
     * @param bitmapFactoryOptions bitmap factory option, which has decoded
     * @return true if yes
     */
    public static boolean mayNotHasAlphaChannel(BitmapFactory.Options bitmapFactoryOptions) {
        String outMimeType = bitmapFactoryOptions.outMimeType;
        return !TextUtils.isEmpty(outMimeType)
                && !outMimeType.toLowerCase(Locale.US).endsWith(ALPHA_BITMAP_MIME_TYPE_END);
    }

    /**
     *
     * @param res
     * @param resId
     * @return bitmap
     */
    public Bitmap decodeBitmap(Resources res, int resId) {
        if (resId != 0) {
            resetBitmapFactoryOptions(false);
            if (needResetAlphaChannel()) {
                mBitmapFactoryOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(res, resId, mBitmapFactoryOptions);
                initAlphaChannel(mBitmapFactoryOptions);
                mBitmapFactoryOptions.inJustDecodeBounds = false;
            }
            return processBitmapAlpha(BitmapFactory.decodeResource(res, resId, mBitmapFactoryOptions));
        }
        return null;
    }

    /**
     *
     * @param res
     * @param resId
     * @param maxWidth
     * @param maxHeight
     * @return bitmap
     */
    public Bitmap decodeBitmap(Resources res, int resId, int maxWidth, int maxHeight) {
        if (resId != 0 && maxHeight > 0 && maxWidth > 0) {
            resetBitmapFactoryOptions(false);
            mBitmapFactoryOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(res, resId, mBitmapFactoryOptions);
            if (initBitmapFactoryOptions(mBitmapFactoryOptions, maxWidth, maxHeight)) {
                if (needResetAlphaChannel()) {
                    initAlphaChannel(mBitmapFactoryOptions);
                }
                return processBitmapAlpha(BitmapFactory.decodeResource(res, resId, mBitmapFactoryOptions));
            }
        }
        return null;
    }

    /**
     *
     * @param bytes
     * @param offset
     * @param length
     * @return bitmap
     */
    public Bitmap decodeBitmap(byte[] bytes, int offset, int length) {
        if (bytes != null) {
            resetBitmapFactoryOptions(false);
            if (needResetAlphaChannel()) {
                mBitmapFactoryOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(bytes, offset, length, mBitmapFactoryOptions);
                initAlphaChannel(mBitmapFactoryOptions);
                mBitmapFactoryOptions.inJustDecodeBounds = false;
            }
            return processBitmapAlpha(BitmapFactory.decodeByteArray(bytes, offset, length, mBitmapFactoryOptions));
        }
        return null;
    }

    /**
     *
     * @param bytes
     * @param offset
     * @param length
     * @param maxWidth
     * @param maxHeight
     * @return
     */
    public Bitmap decodeBitmap(byte[] bytes, int offset, int length, int maxWidth, int maxHeight) {
        if (bytes != null && maxHeight > 0 && maxWidth > 0) {
            resetBitmapFactoryOptions(false);
            mBitmapFactoryOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bytes, offset, length, mBitmapFactoryOptions);
            if (initBitmapFactoryOptions(mBitmapFactoryOptions, maxWidth, maxHeight)) {
                if (needResetAlphaChannel()) {
                    initAlphaChannel(mBitmapFactoryOptions);
                }
                return processBitmapAlpha(BitmapFactory.decodeByteArray(bytes, offset, length, mBitmapFactoryOptions));
            }
        }
        return null;
    }

    /**
     *
     *
     * @param inputStream
     * @return
     */
    public Bitmap decodeBitmap(InputStream inputStream) {
        if (inputStream != null) {
            if (!mPreDecoded) {
                resetBitmapFactoryOptions(false);
                if (needResetAlphaChannel()) {
                    mBitmapFactoryOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(inputStream, null, mBitmapFactoryOptions);
                    initAlphaChannel(mBitmapFactoryOptions);
                    mBitmapFactoryOptions.inJustDecodeBounds = false;
                }
            }
            return processBitmapAlpha(BitmapFactory.decodeStream(inputStream, null, mBitmapFactoryOptions));
        }
        return null;
    }

    /**
     *
     *
     * @param inputStream
     * @param maxWidth
     * @param maxHeight
     */
    public void preDecodeBitmap(InputStream inputStream, int maxWidth, int maxHeight) {
        if (inputStream != null && maxHeight > 0 && maxWidth > 0) {
            resetBitmapFactoryOptions(false);
            mBitmapFactoryOptions.inJustDecodeBounds = true;
            try {
                inputStream.mark(inputStream.available());
            } catch (IOException e) {
                e.printStackTrace();
            }
            BitmapFactory.decodeStream(inputStream, null, mBitmapFactoryOptions);
            if (initBitmapFactoryOptions(mBitmapFactoryOptions, maxWidth, maxHeight) && needResetAlphaChannel()) {
                initAlphaChannel(mBitmapFactoryOptions);
            }
            mPreDecoded = true;
        }
    }

    /**
     * .
     *
     * @param filePath
     * @param squareLength
     * @param cancelLastDecode ，
     * @return
     */
    public Bitmap decodeFileToSquareBitmap(String filePath, int squareLength, boolean cancelLastDecode) {
//		TTLog.e(LOG_TAG, String.format("decodeFileToSquareBitmap ThreadId=%d %s squareLength=%d"
//				, Thread.currentThread().getId(), filePath, squareLength));

        Bitmap decodedBitmap = decodeBitmap(filePath, squareLength, squareLength, cancelLastDecode);
        if (decodedBitmap != null) {
            Bitmap bitmap = cropBitmapToSquare(decodedBitmap, squareLength);
            if (decodedBitmap != bitmap) {
                decodedBitmap.recycle();
            } else {
                LogUtils.e(LOG_TAG, "decodeFileToSquareBitmap bitmap == decodeBitmap");
            }
            return bitmap;
        }
        return null;
    }

    /**
     *
     * @param bitmap
     * @param squareLength
     * @return
     */
    public static Bitmap cropBitmapToSquare(Bitmap bitmap, int squareLength) {
        if (bitmap != null) {
            int imageSquareLength = Math.min(bitmap.getWidth(), bitmap.getHeight());
            int croppedLength = Math.min(imageSquareLength, squareLength);
            float scale = (float) croppedLength / imageSquareLength;
            Matrix matrix = new Matrix();
            matrix.setScale(scale, scale);
            LogUtils.d(LOG_TAG, String.format("cropBitmapToSquare bitmapW=%d H=%d squareLen=%d scale=%f"
                    , bitmap.getWidth(), bitmap.getHeight(), squareLength, scale));
            try {
                Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0, imageSquareLength, imageSquareLength, matrix, true);
                if (SDKVersionUtils.hasHoneycombMR1()) {
                    newBitmap.setHasAlpha(bitmap.hasAlpha());
                }
                return newBitmap;
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     *
     * @param bitmapPath
     * @param squareLength
     * @return
     */
    public static Bitmap cropBitmapToSquare(String bitmapPath, int squareLength) {
        Bitmap bitmap = decodeSampledBitmapFromFile(bitmapPath, squareLength, squareLength);
        return cropBitmapToSquare(bitmap, squareLength);
    }


    /**
     *
     * @param res       Resources
     * @param resId     resId
     * @param reqWidth
     * @param reqHeight
     * @return The decoded bitmap, or null if the image data could not be
     * decoded
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     *
     * @param filePath
     * @param reqWidth
     * @param reqHeight
     * @return The decoded bitmap, or null if the image data could not be
     * decoded
     */
    public static Bitmap decodeSampledBitmapFromFile(String filePath, int reqWidth, int reqHeight) {
        if (TextUtils.isEmpty(filePath)) {
            return null;
        }

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        try {
            return BitmapFactory.decodeFile(filePath, options);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Cỡ mẫu theo kích thước hình ảnh yêu cầu, và hình ảnh của bức tranh nhỏ ban đầu được tính
     *
     * @param options   option
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if ((reqWidth > 0 && reqHeight > 0) && (height > reqHeight || width > reqWidth)) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return 0 == inSampleSize ? 1 : inSampleSize;
    }

    /**
     *
     *
     * @param bitmap
     * @return
     */
    public static int getBitmapSize(Bitmap bitmap) {
        if (bitmap == null) {
            throw new IllegalArgumentException("bitmap must be not null!");
        }

        return SDKVersionUtils.hasHoneycombMR1() ? bitmap.getByteCount() : (bitmap.getRowBytes() * bitmap.getHeight());
    }

    /**
     *
     * @param bitmap
     * @param savePath
     */

    public static void saveBitmap(final Bitmap bitmap, final String savePath) {

        FileUtils.delete(savePath);

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(savePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, DEAFULT_QUALITY, fileOutputStream);
            fileOutputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param imageView image view
     */
    public static void amendMatrixForCenterCrop(ImageView imageView) {
        if (imageView == null) {
            return;
        }

        Drawable drawable = imageView.getDrawable();
        int drawableHeight = drawable != null ? drawable.getIntrinsicHeight() : 0;
        int drawableWidth = drawable != null ? drawable.getIntrinsicWidth() : 0;
        int viewWidth = imageView.getWidth();
        int viewHeight = imageView.getHeight();
        if (drawableHeight <= 0 || drawableWidth <= 0 || viewWidth <= 0 || viewHeight <= 0) {
            return;
        }
        LogUtils.d(LOG_TAG, String.format("amendMatrixForCenterCrop tag=%s view=%d,%d drawable=%d,%d"
                , imageView.getTag(), viewWidth, viewHeight, drawableWidth, drawableHeight));
        float horizontalScaleRatio = 1.0f * viewWidth / drawableWidth;
        float verticalScaleRatio = 1.0f * viewHeight / drawableHeight;
        if (verticalScaleRatio >= horizontalScaleRatio) {
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            LogUtils.d(LOG_TAG, String.format("use system center_crop %f %f", horizontalScaleRatio, verticalScaleRatio));
        } else {
            imageView.setScaleType(ImageView.ScaleType.MATRIX);
            float scaleRatio = Math.max(horizontalScaleRatio, verticalScaleRatio);
            Matrix matrix = new Matrix();
            matrix.postScale(scaleRatio, scaleRatio);
            imageView.setImageMatrix(matrix);
            LogUtils.d(LOG_TAG, String.format("use my matrix %f %f scale=%f", horizontalScaleRatio, verticalScaleRatio, scaleRatio));
        }
    }

    /**
     * FitCenter
     *
     * @param srcBitmap
     * @param dstWidth
     * @param dstHeight
     * @param tryRecycleSource
     * @return
     */
    public static Bitmap createFitXYBitmap(Bitmap srcBitmap, int dstWidth, int dstHeight, boolean tryRecycleSource) {
        if (srcBitmap == null || dstWidth <= 0 || dstHeight <= 0) {
            return srcBitmap;
        }

        Bitmap dstBitmap = srcBitmap;
        try {
            dstBitmap = Bitmap.createScaledBitmap(srcBitmap, dstWidth, dstHeight, true);
            if (dstBitmap != srcBitmap && tryRecycleSource) {
                srcBitmap.recycle();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return dstBitmap;
    }

    /**
     * FitCenter
     *
     * @param srcBitmap
     * @param dstWidth
     * @param dstHeight
     * @param tryRecycleSource
     * @return
     */
    public static Bitmap createFitCenterBitmap(Bitmap srcBitmap, int dstWidth, int dstHeight, boolean tryRecycleSource) {
        if (srcBitmap == null || dstWidth <= 0 || dstHeight <= 0) {
            return srcBitmap;
        }

        int srcWidth = srcBitmap.getWidth();
        int srcHeight = srcBitmap.getHeight();
        int newWidth = srcWidth;
        int newHeight = srcHeight;

        if (newWidth > dstWidth) {
            newHeight = dstWidth * newHeight / newWidth;
            newWidth = dstWidth;
        }

        if (newHeight > dstHeight) {
            newWidth = dstHeight * newWidth / newHeight;
            newHeight = dstHeight;
        }

        return createFitXYBitmap(srcBitmap, newWidth, newHeight, tryRecycleSource);
    }

    /**
     * CenterCrop
     *
     * @param srcBitmap
     * @param dstWidth
     * @param dstHeight
     * @param tryRecycleSource
     * @return
     */
    public static Bitmap createCenterCropBitmap(Bitmap srcBitmap, int dstWidth, int dstHeight, boolean tryRecycleSource) {
        if (srcBitmap == null || dstWidth == 0 || dstHeight == 0) {
            return srcBitmap;
        }
        int srcWidth = srcBitmap.getWidth();
        int srcHeight = srcBitmap.getHeight();
        Bitmap dstBitmap = srcBitmap;
        try {
            if ((dstHeight / dstWidth) - (srcHeight / srcWidth) > 0) {
                int newWidth = (srcHeight * dstWidth) / dstHeight;
                dstBitmap = Bitmap.createBitmap(srcBitmap, (srcWidth - newWidth) / 2, 0, newWidth, srcHeight);
            } else {
                int newHeight = (dstHeight * srcWidth) / dstWidth;
                dstBitmap = Bitmap.createBitmap(srcBitmap, 0, (srcHeight - newHeight) / 2, srcWidth, newHeight);
            }
            if (dstBitmap != srcBitmap && tryRecycleSource) {
                srcBitmap.recycle();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return dstBitmap;
    }

    public enum Direction {VERTICAL, HORIZONTAL};

    /**
     *
     * Creates a new bitmap by flipping the specified bitmap
     * vertically or horizontally.
     *
     * @param src  Bitmap to flip
     * @param type Flip direction (horizontal or vertical)
     * @return New bitmap created by flipping the given one
     * vertically or horizontally as specified by
     * the <code>type</code> parameter or
     * the original bitmap if an unknown type
     * is specified.
     */
    public static Bitmap flip(Bitmap src, Direction type) {
        Matrix matrix = new Matrix();
        if (type == Direction.VERTICAL) {
            matrix.preScale(1.0f, -1.0f);
        } else if (type == Direction.HORIZONTAL) {
            matrix.preScale(-1.0f, 1.0f);
        } else {
            return src;
        }

        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }
}
