package com.ginsre.geo.hoang.dang.ginsre.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.PostViewAdapter;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoLocation;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostDetail;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hoang on 12/29/2017.
 */

public class WeatherFragment extends Fragment implements View.OnClickListener,OnDataChangeObserver {
    private ArrayList<PostDetail> listPostDetail;
    private static final String TAG = "Show Post Detail Fragment";
    private static final String ARG_COLLECTION = "collection";
    private static final String ARG_POSTID = "postid";
    private static final String ARG_LAT= "latitue";
    private static final String ARG_LON = "longtitue";
    RelativeLayout postedFragmentContainer; // from mainActivity.xml file;
    //static String  mUserToken;
    PostViewAdapter listPostAdapter;
    static Context context;
    String collection;
    GeoLocation geoLocation;
    ArrayList<String> lstpostID;
    // Comment event listener
    //ChildEventListener listener;

    //Firebase
    //private static DatabaseReference mDatabaseRefToFor;
    // Bind control of the fragment to view
    @BindView(R.id.fragment_posted_card_load_more)
    RelativeLayout fragment_posted_card_load_more;
    @BindView(R.id.fragment_posted_card_recyclerView)
    RecyclerView fragment_posted_card_recyclerView;
    @BindView(R.id.fragment_posted_card_layoutComment)
    RelativeLayout fragment_posted_card_layoutComment;
    @BindView(R.id.item_post_detail_lsvListComment)
    RecyclerView item_post_detail_lsvListComment;
    @BindView(R.id.item_post_detail_txtComment)
    EditText item_post_detail_txtComment;
    @BindView(R.id.item_post_detail_ibtnCommentEnter)
    ImageButton item_post_detail_ibtnCommentEnter;
    @BindView(R.id.fragment_posted_card_btn_load_more)
    ImageView fragment_posted_card_btn_load_more;
    @BindView(R.id.fragment_posted_card_btn_init_data)
    ImageView fragment_posted_card_btn_init_data;

    public static WeatherFragment newInstance(Context mcontext, GeoLocation mgeoLocation,String collection) {
        WeatherFragment weatherFragment = new WeatherFragment();
        Bundle b = new Bundle();
        b.putString(ARG_COLLECTION,collection);
        b.putDouble(ARG_LAT,mgeoLocation.latitude);
        b.putDouble(ARG_LON,mgeoLocation.longitude);
        weatherFragment.setArguments(b);
        context = mcontext;
        return weatherFragment;
    }

    public static WeatherFragment newInstance(Context mcontext, ArrayList<String> mpostID,String collection) {
        WeatherFragment weatherFragment = new WeatherFragment();
        Bundle b = new Bundle();
        b.putString(ARG_COLLECTION,collection);
        b.putStringArrayList(ARG_POSTID,mpostID);
        weatherFragment.setArguments(b);
        context = mcontext;
        return weatherFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if(getArguments().getStringArrayList(ARG_POSTID)==null){
                geoLocation = new GeoLocation( getArguments().getDouble(ARG_LAT), getArguments().getDouble(ARG_LON));
            }else{
                lstpostID = getArguments().getStringArrayList(ARG_POSTID);
            }
            collection =  getArguments().getString(ARG_COLLECTION);
        }catch (Exception e){

        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_posted_card_view, container, false);
        ButterKnife.bind(this, rootView);
        View mainView = inflater.inflate(R.layout.activity_main,container,false);
        postedFragmentContainer = (RelativeLayout) mainView.findViewById(R.id.postedFragmentContainer);
        return rootView;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DataChangeNotification.getInstance().addObserver(IssueKey.CLOSE_POST_COMMENT, this);
    }

    private void addEvent(final LinearLayoutManager myLayoutManager) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onDataChanged(IssueKey key, Object object) {
        if(key.equals(IssueKey.POST_COMMENT_VIEW)){

        }
    }

}
