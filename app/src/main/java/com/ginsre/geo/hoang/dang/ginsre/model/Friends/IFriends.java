package com.ginsre.geo.hoang.dang.ginsre.model.Friends;

/**
 * Created by hoang on 10/9/2017.
 */

public interface IFriends {
    public static final String NAME = "Friends";
    public static final String friendsId = "friendsId";
    public static final String friendsName = "friendsName";
    public static final String friendsEmail = "friendsEmail";
    public static final String friendsPhoneNum = "friendsPhoneNum";
    public static final String friendsAdress = "friendsAdress";
    public static final String isCanSeeRealTimePosition = "isCanSeeRealTimePosition";
    public static final String isCanGetAddress = "isCanGetAddress";
    public static final String friendsIsActive = "friendsIsActive";
    public static final String friendsStatus = "friendsStatus";
    public static final String friendsUriAvatar = "friendsUriAvatar";
    public static final String friendsNumPriority = "friendsNumPriority";
    public static final String friendsAvatar = "friendsAvatar";
}
