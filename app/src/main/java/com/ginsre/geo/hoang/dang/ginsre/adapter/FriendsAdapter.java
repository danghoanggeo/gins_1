package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.fragment.MyFriendsMoreOptionFragment;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.FriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.IFriendShip;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DialogFriendSetting;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.UndoAdapter;
import com.nhaarman.listviewanimations.util.Swappable;

import java.util.ArrayList;
import java.util.Collections;

import mehdi.sakout.fancybuttons.FancyButton;

public class FriendsAdapter extends BaseAdapter implements Swappable, UndoAdapter, OnDismissCallback {

	private ArrayList<Friends> listFriends;
	private Activity context;
	private String userId;
	private String typeOfAdapter;
	private RelativeLayout friendsNet_myFriendMoreOption;
	public FriendsAdapter(Activity mcontext, ArrayList<Friends> listUsers, String userId, String typeOfAdapter) {
		context = mcontext;
		this.userId = userId;
		this.listFriends = listUsers;
		this.typeOfAdapter = typeOfAdapter;
		friendsNet_myFriendMoreOption = (RelativeLayout)context.findViewById(R.id.friendsNet_myFriendMoreOption);
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public int getCount() {
		return listFriends.size();
	}

	@Override
	public Object getItem(int position) {
		return listFriends.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public void swapItems(int positionOne, int positionTwo) {
		Collections.swap(listFriends, positionOne, positionTwo);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflagter = LayoutInflater.from(context);
			convertView = inflagter.inflate(R.layout.item_find_new_friends, parent, false);
			holder = new ViewHolder(convertView);
			holder.item_find_new_friend_ibtnDelete.setTag(typeOfAdapter);
			holder.item_find_new_friend_ibtnAdd.setTag(position);
			holder.item_find_new_friend_setting.setTag(position);
			holder.bind(position);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		return convertView;
	}
	
	
	private  class ViewHolder {
		ImageView item_find_new_friend_imvAvatar;
		TextView item_find_new_friend_txvUserName;
		TextView item_find_new_friend_txvNumOfCommonFriend;
		FancyButton item_find_new_friend_ibtnDelete;
		FancyButton item_find_new_friend_ibtnAdd;
		FancyButton item_find_new_friend_setting;
		public ViewHolder(View itemView) {
			addControl(itemView);
			addEvent();
		}

		private void addControl(View view) {
			item_find_new_friend_imvAvatar = (ImageView) view.findViewById(R.id.item_find_new_friend_imvAvatar);
			item_find_new_friend_txvUserName = (TextView)view.findViewById(R.id.item_find_new_friend_txvUserName);
			item_find_new_friend_txvNumOfCommonFriend = (TextView)view.findViewById(R.id.item_find_new_friend_txvNumOfCommonFriend);
			item_find_new_friend_ibtnDelete = (FancyButton) view.findViewById(R.id.item_find_new_friend_ibtnDelete);
			item_find_new_friend_ibtnAdd = (FancyButton) view.findViewById(R.id.item_find_new_friend_ibtnAdd);
			item_find_new_friend_setting = (FancyButton)view.findViewById(R.id.item_find_new_friend_setting);
		}

		public void bind(int position)
		{
			item_find_new_friend_txvUserName.setText(listFriends.get(position).getFriendsName());
			ImageUtil.displayRoundImage(item_find_new_friend_imvAvatar,listFriends.get(position).getFriendsUriAvatar(),null);
			if(typeOfAdapter == null)
				typeOfAdapter = (String)item_find_new_friend_ibtnDelete.getTag();
			settingForTypeOfAdapter(typeOfAdapter);
		}

		private void settingForTypeOfAdapter(String typeOfAdapter) {
			switch (typeOfAdapter)
			{
				case ConstantUtils.FRIENDS_ADDNEW:
				{
					if(item_find_new_friend_setting.getVisibility() == View.VISIBLE)
						item_find_new_friend_setting.setVisibility(View.GONE);
					item_find_new_friend_ibtnAdd.setText(context.getResources().getString(R.string.Add));
					break;
				}
				case ConstantUtils.FRIENDS_REQUEST:
				{
					if(item_find_new_friend_setting.getVisibility() == View.VISIBLE)
						item_find_new_friend_setting.setVisibility(View.GONE);
					item_find_new_friend_ibtnAdd.setText(context.getResources().getString(R.string.friend_status_2));
					break;
				}
				case ConstantUtils.FRIENDS_WAITING:
				{
					if(item_find_new_friend_setting.getVisibility() == View.VISIBLE)
						item_find_new_friend_setting.setVisibility(View.GONE);
					item_find_new_friend_ibtnAdd.setText(context.getResources().getString(R.string.request_friend_accept));
					break;
				}
				case ConstantUtils.FRIENDS_MYFRIENDS:
				{
					if(item_find_new_friend_setting.getVisibility() == View.GONE)
						item_find_new_friend_setting.setVisibility(View.VISIBLE);
					item_find_new_friend_ibtnAdd.setText(context.getResources().getString(R.string.friend_status_1)); //Friend
					item_find_new_friend_ibtnDelete.setText(context.getResources().getString(R.string.friend_status_3)); // Unfriend
					break;
				}
			}
		}

		private void addEvent() {
			item_find_new_friend_imvAvatar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

				}
			});
			item_find_new_friend_ibtnAdd.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
				int postion = (int) item_find_new_friend_ibtnAdd.getTag();
				if(typeOfAdapter == null)
					typeOfAdapter = (String)item_find_new_friend_ibtnDelete.getTag();
				switch (typeOfAdapter)
				{
					case ConstantUtils.FRIENDS_ADDNEW:
					{
						makeNewRequestFriend(postion);
						break;
					}
					case ConstantUtils.FRIENDS_WAITING:
					{
						acceptRequestFriend(postion);
						break;
					}
					case ConstantUtils.FRIENDS_REQUEST:
					{
						removeRequestFriend(postion);
						break;
					}

				}

				}
			});
			item_find_new_friend_ibtnDelete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(typeOfAdapter == null)
						typeOfAdapter = (String)item_find_new_friend_ibtnDelete.getTag();
					if(typeOfAdapter.equalsIgnoreCase(ConstantUtils.FRIENDS_MYFRIENDS))
					{
						com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB friendDB = com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB.getInstance(context);
						friendDB.deleteFriend(listFriends.get((int)item_find_new_friend_setting.getTag()).getFriendsId());
					}
				}
			});
			item_find_new_friend_setting.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					getFriendShip((int)item_find_new_friend_setting.getTag());
					//setMyFriendsMoreOptionFragmentTo((int)item_find_new_friend_setting.getTag());
				}
			});
		}

		private void getFriendShip(final int position){

			DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
			listFriends.get(position);
			mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(userId).child(listFriends.get(position).getFriendsId()).child(IFriendShip.seeRealTimePosition).addListenerForSingleValueEvent(new ValueEventListener() {
				@Override
				public void onDataChange(DataSnapshot dataSnapshot) {
					try {
						FriendShip friendShip = new FriendShip();
						friendShip.setId(listFriends.get(position).getFriendsId());
						friendShip.setSeePosi((Boolean) dataSnapshot.getValue());
						DialogFriendSetting dialogFriendSetting = new DialogFriendSetting();
						dialogFriendSetting.showDialog((Activity)context,friendShip,ConstantUtils.DIALOG_SETTING);
					}catch (Exception e){

					}
				}

				@Override
				public void onCancelled(DatabaseError databaseError) {

				}
			});
		}

		private void setMyFriendsMoreOptionFragmentTo(int position) {
			try {
				FriendShip friendship = new FriendShip();
				friendship.setSeePosi(listFriends.get(position).isCanSeeRealTimePosition());
				friendship.setId(listFriends.get(position).getFriendsId());
				android.app.FragmentManager fragManager = context.getFragmentManager();
				android.app.Fragment fragment = MyFriendsMoreOptionFragment.newInstance(context,friendship,userId);
				fragManager.beginTransaction().replace(R.id.friendsNet_myFriendMoreOption,fragment).commit();
				friendsNet_myFriendMoreOption.setVisibility(View.VISIBLE);
			}
			catch (Exception e)
			{
				//TODO: log
			}
		}

		private void removeRequestFriend(int postion) {

		}

		private void acceptRequestFriend(int postion) {
			String title = context.getResources().getString(R.string.allow_to_see_your_address);
			String typeRequest = "AllowFriendSeeAddress";
			showAlertDialogCofirm(title,typeRequest,postion);
		}

		private void makeNewRequestFriend(int postion) {
			try {
				String user2Id = listFriends.get(postion).getFriendsId();
				DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

				FriendShip friendShip1 = new FriendShip();// Save to list friend of user 1
				friendShip1.setId(user2Id);
				friendShip1.setCreated(DateUtils.formatCurrentDate(DateUtils.FORMAT_TO_DAY));
				friendShip1.setSeePosi(false);
				friendShip1.setStatus(ConstantUtils.FRIENDS_REQUEST); // Make Request friend;
				// Write friend to user1 listFriends;
				mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(userId).child(user2Id).setValue(friendShip1);

				FriendShip friendShip2 = new FriendShip();
				friendShip2.setId(userId);
				friendShip2.setCreated(DateUtils.formatCurrentDate(DateUtils.FORMAT_TO_DAY));
				friendShip2.setSeePosi(false);
				friendShip2.setStatus(ConstantUtils.FRIENDS_WAITING); // Receive Request friend;
				//Write request friend to user2 listFriends;
				mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(user2Id).child(userId).setValue(friendShip2);
				Toast.makeText(context,context.getResources().getString(R.string.request_friend_have_sent),Toast.LENGTH_LONG).show();
				listFriends.remove(postion);
			}catch (Exception e)
			{
				Toast.makeText(context,context.getResources().getString(R.string.request_friend_cannot_sent),Toast.LENGTH_LONG).show();
				//TODO: LOG here;
			}
		}
		private void showAlertDialogCofirm(String title, String typeRequest,int postion) {
			try{
				final int mpostion = postion;
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle(context.getString(R.string.Confirm_dialog_box));
				builder.setMessage(title);
				builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						saveFriendAccept(true,mpostion);
						dialog.dismiss();
					}
				});
				builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						saveFriendAccept(false,mpostion);
						dialog.dismiss();
					}
				});
				builder.show();
			}catch (Exception e)
			{
				//TODO: LOG here;
				System.out.print(e.getMessage());
			}

		}

		private void saveFriendAccept(boolean b,int postion) {
			DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            /*Map<String,FriendShip> dataUpdate = new HashMap<>();
            FriendShip friendShip1 = new FriendShip();// Save to list friend of user 1
            friendShip1.setCreated(DateUtils.formatCurrentDate(DateUtils.FORMAT_TO_DAY));
            friendShip1.setSeePosi(false);
            friendShip1.setCanGetAddress(b);
            friendShip1.setStatus("2"); // Make Request friend;
            dataUpdate.put(listUsers.get(postion).getFriendsId(),friendShip1);*/
			try {
				mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(userId)
						.child(listFriends.get(postion).getFriendsId())
						.child(IFriendShip.friendStatus).setValue(ConstantUtils.FRIENDS_MYFRIENDS);
				mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(userId)
						.child(listFriends.get(postion).getFriendsId())
						.child(IFriendShip.seeRealTimePosition).setValue(b);
				mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(listFriends.get(postion)
						.getFriendsId()).child(userId).child(IFriendShip.friendStatus).setValue(ConstantUtils.FRIENDS_MYFRIENDS);
				Toast.makeText(context,context.getResources().getString(R.string.congra_for_friends),Toast.LENGTH_LONG).show();
			}catch (Exception e)
			{
				//TODO: log here
			}
		}
	}

	@Override
	@NonNull
	public View getUndoClickView(@NonNull View view) {
		return view.findViewById(R.id.undo_button_notification);
	}

	@Override
	@NonNull
	public View getUndoView(final int position, final View convertView,
			@NonNull final ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			view = LayoutInflater.from(context).inflate(R.layout.list_item_undo_view,
					parent, false);
		}
		return view;
	}

	@Override
	public void onDismiss(@NonNull final ViewGroup listView,
			@NonNull final int[] reverseSortedPositions) {
		for (int position : reverseSortedPositions) {
			remove(position);
		}
	}
	public void remove(int position) {
		listFriends.remove(position);
	}
}
