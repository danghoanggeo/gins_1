package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.lang.reflect.Field;

/**
 * Created by trach on 6/12/2017.
 */

public class DisplayUtils {

    private static DisplayMetrics sDisplayMetrics;
    private static Resources mRes;

    private static final float ROUND_DIFFERENCE = 0.5f;

    public static void init(Context context) {
        sDisplayMetrics = context.getResources().getDisplayMetrics();
        mRes = context.getResources();
        checkScreen();
    }

    private static void checkScreen() {
//        ShowConfig.setIsLargeScreen(sDisplayMetrics.widthPixels > ConstantUtils.KILO
//                || sDisplayMetrics.widthPixels / sDisplayMetrics.density >= ShowConfig.LARGE_SCREEN_DIPS);
    }


    public static int getWidthPixels() {
        return sDisplayMetrics.widthPixels;
    }


    public static boolean isRightScreen(float x){
        float centerOfScreen = getWidthPixels()/2;
        if(x > centerOfScreen + 50 && x < getWidthPixels()) return true;
        return false;
    }


    public static int getHeightPixels() {
        return sDisplayMetrics.heightPixels;
    }


    public static int getStatusBarHeight() {
        final int defaultHeightInDp = 19;
        int height = DisplayUtils.dp2px(defaultHeightInDp);
        try {
            Class<?> c = Class.forName("com.android.internal.R$dimen");
            Object obj = c.newInstance();
            Field field = c.getField("status_bar_height");
            height = mRes.getDimensionPixelSize(Integer.parseInt(field.get(obj).toString()));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
//        if (EnvironmentUtils.isFlymeOs()) {
//            height = height * 2;
//        }
        return height;
    }


    public static float getDensity() {
        return sDisplayMetrics.density;
    }


    public static int dp2px(int dp) {
        return (int) (dp * sDisplayMetrics.density + ROUND_DIFFERENCE);
    }

    public static float dp2px(float dp) {
        return dp * sDisplayMetrics.density + ROUND_DIFFERENCE;
    }

    public static int px2dp(int px) {
        return (int) (px / sDisplayMetrics.density + ROUND_DIFFERENCE);
    }

    public static int getDimensionPixelSize(int dimenResId) {
        return mRes.getDimensionPixelSize(dimenResId);
    }
}
