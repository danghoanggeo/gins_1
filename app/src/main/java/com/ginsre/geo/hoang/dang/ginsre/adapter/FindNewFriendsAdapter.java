package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.fragment.MyFriendsMoreOptionFragment;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.FriendShip;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ImageUtil;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import mehdi.sakout.fancybuttons.FancyButton;


/**
 * Created by hoang on 10/6/2017.
 */

public class FindNewFriendsAdapter extends RecyclerView.Adapter<FindNewFriendsAdapter.FindNewFriendViewHolder> {
    private ArrayList<Friends> listFriends;
    private Activity context;
    private String userId;
    private String typeOfAdapter;
    private RelativeLayout friendsNet_myFriendMoreOption;
    public FindNewFriendsAdapter(Activity context, ArrayList<Friends> listUsers,String userId,String typeOfAdapter)
    {
        this.context = context;
        this.userId = userId;
        this.listFriends = listUsers;
        this.typeOfAdapter = typeOfAdapter;
    }

    @Override
    public FindNewFriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflagter = LayoutInflater.from(context);
        View view = inflagter.inflate(R.layout.item_find_new_friends,parent,false);
        friendsNet_myFriendMoreOption = (RelativeLayout)context.findViewById(R.id.friendsNet_myFriendMoreOption);
        return  new FindNewFriendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FindNewFriendViewHolder holder, int position) {
        holder.item_find_new_friend_ibtnDelete.setTag(typeOfAdapter);
        holder.item_find_new_friend_ibtnAdd.setTag(position);
        holder.item_find_new_friend_setting.setTag(position);
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return listFriends.size();
    }

    public class FindNewFriendViewHolder extends RecyclerView.ViewHolder{
        ImageView item_find_new_friend_imvAvatar;
        TextView item_find_new_friend_txvUserName;
        TextView item_find_new_friend_txvNumOfCommonFriend;
        FancyButton item_find_new_friend_ibtnDelete;
        FancyButton item_find_new_friend_ibtnAdd;
        FancyButton item_find_new_friend_setting;
        public FindNewFriendViewHolder(View itemView) {
            super(itemView);
            addControl(itemView);
            addEvent();
        }

        private void addControl(View view) {
            item_find_new_friend_imvAvatar = (ImageView) view.findViewById(R.id.item_find_new_friend_imvAvatar);
            item_find_new_friend_txvUserName = (TextView)view.findViewById(R.id.item_find_new_friend_txvUserName);
            item_find_new_friend_txvNumOfCommonFriend = (TextView)view.findViewById(R.id.item_find_new_friend_txvNumOfCommonFriend);
            item_find_new_friend_ibtnDelete = (FancyButton) view.findViewById(R.id.item_find_new_friend_ibtnDelete);
            item_find_new_friend_ibtnAdd = (FancyButton) view.findViewById(R.id.item_find_new_friend_ibtnAdd);
            item_find_new_friend_setting = (FancyButton)view.findViewById(R.id.item_find_new_friend_setting);
        }


        public void bind(int position)
        {
            item_find_new_friend_txvUserName.setText(listFriends.get(position).getFriendsName());
            ImageUtil.displayRoundImage(item_find_new_friend_imvAvatar,listFriends.get(position).getFriendsUriAvatar(),null);
            if(typeOfAdapter == null)
                typeOfAdapter = (String)item_find_new_friend_ibtnDelete.getTag();
            settingForTypeOfAdapter(typeOfAdapter);
        }
        private void settingForTypeOfAdapter(String typeOfAdapter) {

            switch (typeOfAdapter)
            {
                case ConstantUtils.FRIENDS_ADDNEW:
                {
                    if(item_find_new_friend_setting.getVisibility() == View.VISIBLE)
                        item_find_new_friend_setting.setVisibility(View.GONE);
                    item_find_new_friend_ibtnAdd.setText(context.getResources().getString(R.string.Add));
                    break;
                }
                case ConstantUtils.FRIENDS_REQUEST:
                {
                    if(item_find_new_friend_setting.getVisibility() == View.VISIBLE)
                        item_find_new_friend_setting.setVisibility(View.GONE);
                    item_find_new_friend_ibtnAdd.setText(context.getResources().getString(R.string.friend_status_2));
                    break;
                }
                case ConstantUtils.FRIENDS_WAITING:
                {
                    if(item_find_new_friend_setting.getVisibility() == View.VISIBLE)
                        item_find_new_friend_setting.setVisibility(View.GONE);
                    item_find_new_friend_ibtnAdd.setText(context.getResources().getString(R.string.request_friend_accept));
                    break;
                }
                case ConstantUtils.FRIENDS_MYFRIENDS:
                {
                    if(item_find_new_friend_setting.getVisibility() == View.GONE)
                        item_find_new_friend_setting.setVisibility(View.VISIBLE);
                    item_find_new_friend_ibtnAdd.setText(context.getResources().getString(R.string.friend_status_1)); //Friend
                    item_find_new_friend_ibtnDelete.setText(context.getResources().getString(R.string.friend_status_3)); // Unfriend
                    break;
                }
            }
        }
        private void addEvent() {
            item_find_new_friend_imvAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            item_find_new_friend_ibtnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int postion = (int) item_find_new_friend_ibtnAdd.getTag();
                    if(typeOfAdapter == null)
                        typeOfAdapter = (String)item_find_new_friend_ibtnDelete.getTag();
                    switch (typeOfAdapter)
                    {
                        case ConstantUtils.FRIENDS_ADDNEW:
                        {
                            makeNewRequestFriend(postion);
                            break;
                        }
                        case ConstantUtils.FRIENDS_WAITING:
                        {
                            acceptRequestFriend(postion);
                            break;
                        }
                        case ConstantUtils.FRIENDS_REQUEST:
                        {
                            removeRequestFriend(postion);
                            break;
                        }

                    }


                }
            });
            item_find_new_friend_ibtnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(typeOfAdapter == null)
                        typeOfAdapter = (String)item_find_new_friend_ibtnDelete.getTag();
                    if(typeOfAdapter.equalsIgnoreCase(context.getResources().getString(R.string.friend_status_3)))
                    {
                        com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB friendDB = com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB.getInstance(context);
                        friendDB.deleteFriend(listFriends.get((int)item_find_new_friend_setting.getTag()).getFriendsId());
                    }
                }
            });
            item_find_new_friend_setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setMyFriendsMoreOptionFragmentTo((int)item_find_new_friend_setting.getTag());
                }
            });
        }

        private void setMyFriendsMoreOptionFragmentTo(int position) {
            try {
                FriendShip friendship = new FriendShip();
                friendship.setSeePosi(listFriends.get(position).isCanSeeRealTimePosition());
                friendship.setId(listFriends.get(position).getFriendsId());
                android.app.FragmentManager fragManager = context.getFragmentManager();
                android.app.Fragment fragment = MyFriendsMoreOptionFragment.newInstance(context,friendship,userId);
                fragManager.beginTransaction().replace(R.id.friendsNet_myFriendMoreOption,fragment).commit();
                friendsNet_myFriendMoreOption.setVisibility(View.VISIBLE);
            }
            catch (Exception e)
            {
                //TODO: log
            }
        }

        private void removeRequestFriend(int postion) {

        }

        private void acceptRequestFriend(int postion) {
            String title = context.getResources().getString(R.string.allow_to_see_your_address);
            String typeRequest = "AllowFriendSeeAddress";
            showAlertDialogCofirm(title,typeRequest,postion);

        }

        private void makeNewRequestFriend(int postion) {
            try {
                String user2Id = listFriends.get(postion).getFriendsId();
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                FriendShip friendShip1 = new FriendShip();// Save to list friend of user 1
                //friendShip1.setId(user2Id);
                friendShip1.setCreated(DateUtils.getDateTimeForGeoHashQuery());
                friendShip1.setSeePosi(false);
                friendShip1.setStatus(ConstantUtils.FRIENDS_REQUEST); // Make Request friend;
                // Write friend to user1 listFriends;
                mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS).child(userId).child(user2Id).setValue(friendShip1);

                FriendShip friendShip2 = new FriendShip();
                //friendShip2.setId(userId);
                friendShip2.setCreated(DateUtils.getDateTimeForGeoHashQuery());
                friendShip2.setSeePosi(false);
                friendShip2.setStatus(ConstantUtils.FRIENDS_WAITING); // Receive Request friend;
                //Write request friend to user2 listFriends;
                mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS_REQUEST).child(user2Id).child(userId).setValue(friendShip2);

                com.ginsre.geo.hoang.dang.ginsre.utils.SaveFriendToDB.SaveToDB(context,listFriends.get(postion),ConstantUtils.FRIENDS_REQUEST);
                Toast.makeText(context,context.getResources().getString(R.string.request_friend_have_sent),Toast.LENGTH_LONG).show();
            }catch (Exception e)
            {
                Toast.makeText(context,context.getResources().getString(R.string.request_friend_cannot_sent),Toast.LENGTH_LONG).show();
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
            }
        }

        private void showAlertDialogCofirm(String title, String typeRequest,int postion) {
            try{
                final int mpostion = postion;
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(context.getString(R.string.Confirm_dialog_box));
                builder.setMessage(title);
                builder.setPositiveButton(context.getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        saveFriendAccept(true,mpostion);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(context.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        saveFriendAccept(false,mpostion);
                        dialog.dismiss();
                    }
                });
                builder.show();
            }catch (Exception e)
            {
                //TODO: LOG here;
                System.out.print(e.getMessage());
            }

        }

        private void saveFriendAccept(boolean b,int postion) {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

            try {
                // Save Friend to uer database;
                //Friends friends = listFriends.get(postion);
                com.ginsre.geo.hoang.dang.ginsre.utils.SaveFriendToDB.SaveToDB(context,listFriends.get(postion),ConstantUtils.FRIENDS_MYFRIENDS);
                //new SaveFriendToDB().execute(String.valueOf(postion));
               // saveFriendToUserDatabase(friends);
                //Save Friend to system database;
                FriendShip friendShip1 = new FriendShip();// Save to list friend of user 1
                friendShip1.setCreated(DateUtils.getDateTimeForGeoHashQuery());
                friendShip1.setSeePosi(b);
                friendShip1.setStatus(ConstantUtils.FRIENDS_MYFRIENDS);
                mDatabase.child(ConstantUtils.USER_REF_USER_FRIENDS_REQUEST).child(userId)
                        .child(listFriends.get(postion).getFriendsId()).setValue(friendShip1);
                Toast.makeText(context,context.getResources().getString(R.string.congra_for_friends),Toast.LENGTH_LONG).show();
            }catch (Exception e)
            {
                //TODO: log here
            }
        }
    }
}
