package com.ginsre.geo.hoang.dang.ginsre.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.CommentItemAdapter;
import com.ginsre.geo.hoang.dang.ginsre.adapter.PostViewAdapter;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoLocation;
import com.ginsre.geo.hoang.dang.ginsre.geofire.core.GeoHashQuery;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostComment;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostDetail;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hoang on 12/29/2017.
 */

public class SocialFragment  extends Fragment implements View.OnClickListener,OnDataChangeObserver {
    private ArrayList<PostDetail> listPostDetail;
    private static final String TAG = "Show Post Detail Fragment";
    private static final String ARG_COLLECTION = "collection";
    private static final String ARG_POSTID = "postid";
    private static final String ARG_LAT= "latitue";
    private static final String ARG_LON = "longtitue";
    RelativeLayout postedFragmentContainer; // from mainActivity.xml file;
    //static String  mUserToken;
    PostViewAdapter listPostAdapter;
    static Context context;
    String collection;
    GeoLocation geoLocation;
    ArrayList<String> lstpostID;
    // Comment event listener
    //ChildEventListener listener;

    //Firebase
    //private static DatabaseReference mDatabaseRefToFor;
    // Bind control of the fragment to view
    @BindView(R.id.fragment_posted_card_load_more)
    RelativeLayout fragment_posted_card_load_more;
    @BindView(R.id.fragment_posted_card_recyclerView)
    RecyclerView fragment_posted_card_recyclerView;
    @BindView(R.id.fragment_posted_card_layoutComment)
    RelativeLayout fragment_posted_card_layoutComment;
    @BindView(R.id.item_post_detail_lsvListComment)
    RecyclerView item_post_detail_lsvListComment;
    @BindView(R.id.item_post_detail_txtComment)
    EditText item_post_detail_txtComment;
    @BindView(R.id.item_post_detail_ibtnCommentEnter)
    ImageButton item_post_detail_ibtnCommentEnter;
    @BindView(R.id.fragment_posted_card_btn_load_more)
    ImageView fragment_posted_card_btn_load_more;
    @BindView(R.id.fragment_posted_card_btn_init_data)
    ImageView fragment_posted_card_btn_init_data;

    public static SocialFragment newInstance(Context mcontext, GeoLocation mgeoLocation,String collection) {
        SocialFragment socialFragment = new SocialFragment();
        Bundle b = new Bundle();
        b.putString(ARG_COLLECTION,collection);
        b.putDouble(ARG_LAT,mgeoLocation.latitude);
        b.putDouble(ARG_LON,mgeoLocation.longitude);
        socialFragment.setArguments(b);
        context = mcontext;
        return socialFragment;
    }

    public static SocialFragment newInstance(Context mcontext, ArrayList<String> mpostID,String collection) {
        SocialFragment socialFragment = new SocialFragment();
        Bundle b = new Bundle();
        b.putString(ARG_COLLECTION,collection);
        b.putStringArrayList(ARG_POSTID,mpostID);
        socialFragment.setArguments(b);
        context = mcontext;
        return socialFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if(getArguments().getStringArrayList(ARG_POSTID)==null){
                geoLocation = new GeoLocation( getArguments().getDouble(ARG_LAT), getArguments().getDouble(ARG_LON));
            }else{
                lstpostID = getArguments().getStringArrayList(ARG_POSTID);
            }
            collection =  getArguments().getString(ARG_COLLECTION);
        }catch (Exception e){

        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_posted_card_view, container, false);
        ButterKnife.bind(this, rootView);
        View mainView = inflater.inflate(R.layout.activity_main,container,false);
        postedFragmentContainer = (RelativeLayout) mainView.findViewById(R.id.postedFragmentContainer);
        return rootView;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listPostDetail = new ArrayList<>();
        if(geoLocation!=null){
            getMorePostDetail(collection);
        }else{
            getPostDetail(lstpostID);
        }
        DataChangeNotification.getInstance().addObserver(IssueKey.POST_COMMENT_VIEW, this);
        DataChangeNotification.getInstance().addObserver(IssueKey.CLOSE_POST_COMMENT, this);
    }

    private void setPostPresent(){
        LinearLayoutManager myLayoutManager = new LinearLayoutManager(getActivity());
        myLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        if (listPostDetail.size() > 0 & fragment_posted_card_recyclerView != null) {
            listPostAdapter = new PostViewAdapter(listPostDetail,context);
            fragment_posted_card_recyclerView.setAdapter(listPostAdapter);
        }
        fragment_posted_card_recyclerView.setLayoutManager(myLayoutManager);
        addEvent(myLayoutManager);
    }
    int lastScroll;
    private void addEvent(final LinearLayoutManager myLayoutManager) {

        //fragment_posted_card_back.setOnClickListener(this);
        item_post_detail_ibtnCommentEnter.setOnClickListener(this);
        lastScroll = myLayoutManager.findFirstVisibleItemPosition();
        fragment_posted_card_recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //int total = myLayoutManager.getItemCount();
                //int firstVisibleItemCount = myLayoutManager.findFirstVisibleItemPosition();
                //int first = myLayoutManager.findFirstCompletelyVisibleItemPosition();
                //int lastVisibleItemCount = myLayoutManager.findLastVisibleItemPosition();
                //int last  = myLayoutManager.findLastCompletelyVisibleItemPosition();
                //if(firstVisibleItemCount)
                try {
                    if(!(listPostDetail.size() == 1)){
                        if((myLayoutManager.findLastVisibleItemPosition()+1)==(myLayoutManager.getItemCount())){
                            if(lastScroll<myLayoutManager.findFirstVisibleItemPosition()) { // scroll down else up.
                                fragment_posted_card_load_more.setVisibility(View.VISIBLE);
                                android.view.animation.Animation animation = com.ginsre.geo.hoang.dang.ginsre.utils.AnimationUtils.buildRotateAnimation
                                        (0.0f, 360.0f,android.view.animation.Animation.RELATIVE_TO_SELF, 0.5f, android.view.animation.Animation.RELATIVE_TO_SELF,
                                                0.5f, 500);
                                animation.setRepeatCount(1);
                                fragment_posted_card_btn_load_more.setAnimation(animation);
                                animation.setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                /*String key = listPostDetail.get(listPostDetail.size()-1).getPostId();
                                String date = key.split("-")[0];
                                String databaseRefPost = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getDatabaseRefForGinsType(key.split("-")[1]);
                                if(listPostDetail.size()>1)
                                    //getMorePostDetail(databaseRefPost,date,1);
                                    fragment_posted_card_load_more.setVisibility(View.GONE);
                                else
                                    fragment_posted_card_load_more.setVisibility(View.VISIBLE);*/

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        if(listPostDetail.size()>1)
                                            fragment_posted_card_load_more.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });
                            }
                            else{
                                fragment_posted_card_load_more.setVisibility(View.GONE);
                            }
                        }/*
                else{
                    if(fragment_posted_card_load_more.getVisibility() == View.VISIBLE)
                        fragment_posted_card_load_more.setVisibility(View.GONE);
                }*/
                        lastScroll = myLayoutManager.findFirstVisibleItemPosition();
                        //Toast.makeText(context,"Total: "+total+" FirstVisibaleItemCount: "+firstVisibleItemCount+" LastVisibleItemCount: "+lastVisibleItemCount,Toast.LENGTH_LONG).show();
                    }

                }catch (Exception e){
                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                }

            }
        });
        //fragment_posted_card_load_more.setOnClickListener(this);
        fragment_posted_card_btn_load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String key = listPostDetail.get(listPostDetail.size()-1).getPostId();
                    String date = key.split("-")[0];
                    String databaseRefPost = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getDatabaseRefForGinsType(key.split("-")[1]);
                    //getMorePostDetail(databaseRefPost,date,1);
                    //android.view.animation.Animation animation = com.ginsre.geo.hoang.dang.ginsre.utils.AnimationUtils.buildRotateAnimation(0.0f,360f,1000);
                    //fragment_posted_card_btn_load_more.setAnimation(animation);
                    fragment_posted_card_btn_load_more.setClickable(false);
                }catch (Exception e){
                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int controlId = v.getId();
        if(controlId == R.id.item_post_detail_ibtnCommentEnter)
        {
            String comment = item_post_detail_txtComment.getText().toString();
            if(!(comment.equals("") || comment.length() > ConstantUtils.MAX_COMMENT_LENGTH))
            {
                //TODO: push user's comment to firebase;
                // Get zone and date preferences to database by camera location and date;
                if(item_post_detail_ibtnCommentEnter.getTag() != null) {
                    String dateTime = DateUtils.getTimeLongForPostFromRef();
                    try {
                        FirebaseAuth mAuth = FirebaseAuth.getInstance();
                        String userId = "";
                        if(mAuth!=null)
                            if(mAuth.getCurrentUser()!=null)
                                userId = mAuth.getCurrentUser().getUid();
                        String postId = (String)item_post_detail_ibtnCommentEnter.getTag();
                        String collectionRef = GinFunctions.getFireStoreDBRefForPostDetail(postId.split("-")[1]);
                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        db.collection(collection).document(postId).collection(ConstantUtils.REF_POST_COMMENT).document(dateTime +"-"+ userId).set(new PostComment(comment));
                        item_post_detail_txtComment.setText("");
                        //listPostDetail.get(postIndexSelected).setNumOfComment(listPostDetail.get(postIndexSelected).getNumOfComment()+1);
                    }catch (Exception e) {
                        com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                    }
                }
            }
            else
            {
                Toast.makeText(context,getResources().getString(R.string.user_post_comment_null),Toast.LENGTH_SHORT).show();
            }
        }
    }


    /**
     *
     * @param ginstype traffic or something else
     *  @param postdetailOrPost post detail ref or post ref
     * @return
     */

    public DatabaseReference getDatabaseRefrence(String ginstype, String postdetailOrPost){
        String databaseRefPost;
        if(postdetailOrPost.equalsIgnoreCase("detail")){
            databaseRefPost = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getDatabaseRefForPostDetail(ginstype);
        }else{
            databaseRefPost = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getDatabaseRefForGinsType(ginstype);
        }
        return FirebaseDatabase.getInstance().getReference(databaseRefPost);
    }


    int index = 0;
    public void getMorePostDetail(final String collection){
        try {
            fragment_posted_card_btn_init_data.setVisibility(View.VISIBLE);
            android.view.animation.Animation animation = com.ginsre.geo.hoang.dang.ginsre.utils.AnimationUtils.buildRotateAnimation
                    (0.0f, 360.0f,android.view.animation.Animation.RELATIVE_TO_SELF, 0.5f, android.view.animation.Animation.RELATIVE_TO_SELF,
                            0.5f, 500);
            animation.setRepeatCount(2);
            fragment_posted_card_btn_init_data.setAnimation(animation);
            com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
            //GeoStore geoFire  = new GeoStore(db.collection(ConstantUtils.GINS_TRAFFIC_LOCATION.replace("/","")));
            String valueStart = DateUtils.getDateTimeBack(10);
            String valueEnd = DateUtils.getDateTimeBack(0);
            //Set<GeoHashQuery> newQueries1 = GeoHashQuery.queriesAtLocation(geoLocation, 1*1000);
            Set<GeoHashQuery> newQueries2 = GeoHashQuery.queriesAtLocation(geoLocation, 3000);
            //Set<GeoHashQuery> newQueries3 = GeoHashQuery.queriesAtLocation(geoLocation, 5*1000);

            for (final GeoHashQuery query: newQueries2) {
                //TODO: Change query here;
                final TreeMap<String,PostDetail> map = new TreeMap<>();
                valueStart = valueStart.concat(query.getStartValue());
                valueEnd = valueEnd.concat(query.getEndValue());
                com.google.firebase.firestore.Query firebaseQuery = db.collection(collection).whereGreaterThanOrEqualTo("g",valueStart).whereLessThanOrEqualTo("g",valueEnd);
                firebaseQuery.get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                try {
                                    if (task.isSuccessful()) {
                                        int lastSize = listPostDetail.size();
                                        for (DocumentSnapshot document : task.getResult()) {
                                            PostDetail postDetail = document.toObject(PostDetail.class);
                                            postDetail.setPostId(document.getId());
                                            postDetail.setDateTime(DateUtils.getTimeFromCurrentTime((String)document.getId().split("-")[0],context));
                                            map.put(document.getId(),postDetail);
                                        }
                                        listPostDetail.addAll(map.values());
                                        if(lastSize <listPostDetail.size())
                                        {
                                            fragment_posted_card_btn_init_data.setVisibility(View.GONE);
                                            if(lastSize == 0)
                                                setPostPresent();
                                            listPostAdapter.notifyDataSetChanged();
                                        }
                                        index++;
                                    }
                                }catch (Exception e){
                                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                                }
                            }
                        });
            }

        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    /**
     *
     * @param lstpostID
     */
    public void getPostDetail(final ArrayList<String> lstpostID){
        try {
            com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
            for (int i = 0;i<lstpostID.size();i++) {
                String postID = lstpostID.get(i);
                db.collection(collection).document(postID).get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                try {
                                    if (task.isSuccessful()) {
                                        PostDetail postDetail = task.getResult().toObject(PostDetail.class);
                                        postDetail.setPostId(task.getResult().getId());
                                        postDetail.setDateTime(DateUtils.getTimeFromCurrentTime((String)task.getResult().getId().split("-")[0],context));
                                        listPostDetail.add(postDetail);
                                        if(listPostDetail.size() == 1){
                                            setPostPresent();
                                        }
                                        listPostAdapter.notifyDataSetChanged();
                                    }
                                }catch (Exception e){
                                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                                }
                            }
                        });
            }
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    @Override
    public void onDataChanged(IssueKey key, Object object) {
        if(key.equals(IssueKey.POST_COMMENT_VIEW)){
            String postID = (String)object;
            getCommentListFromFirebase(postID);
        }else if(key.equals(IssueKey.CLOSE_POST_COMMENT)){
            if(fragment_posted_card_layoutComment.getVisibility()==View.VISIBLE){
                fragment_posted_card_recyclerView.setVisibility(View.VISIBLE);
                fragment_posted_card_layoutComment.setVisibility(View.GONE);
            }else{
                DataChangeNotification.getInstance().notifyChange(IssueKey.CLOSE_POST_DETAIL);
            }
        }
    }



    private void getCommentListFromFirebase(final String postID){
        if(fragment_posted_card_layoutComment.getVisibility()==View.GONE)
        {
            fragment_posted_card_recyclerView.setVisibility(View.GONE);
            fragment_posted_card_layoutComment.setVisibility(View.VISIBLE);
        }
        com.google.firebase.firestore.FirebaseFirestore db = com.google.firebase.firestore.FirebaseFirestore.getInstance();
        db.collection(collection).document(postID).collection(ConstantUtils.REF_POST_COMMENT).limit(ConstantUtils.LIMIT_COMMENT_GET).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    try {
                        ArrayList<PostComment> lstComment = new ArrayList<>();
                        for (DocumentSnapshot document : task.getResult()) {
                            String commentId[] = document.getId().split("-");
                            PostComment postComment = document.toObject(PostComment.class);
                            postComment.setComment_userId(commentId[1]);
                            postComment.setComment_dateTime(DateUtils.getTimeFromCurrentTime(commentId[0],context));
                            lstComment.add(postComment);
                        }
                        item_post_detail_ibtnCommentEnter.setTag(postID);
                        showCommentList(lstComment);
                    }catch (Exception e){
                        com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                    }
                }
            }
        });
    }

    private void showCommentList(ArrayList<PostComment> lstComment) {
        LinearLayoutManager manager = new LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false
        );
        item_post_detail_lsvListComment.setLayoutManager(manager);
        final CommentItemAdapter commentItemAdapter = new CommentItemAdapter(lstComment);
        item_post_detail_lsvListComment.setAdapter(commentItemAdapter);
        commentItemAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                item_post_detail_lsvListComment.scrollToPosition(commentItemAdapter.getItemCount()-1);
            }
        });
    }
}