package com.ginsre.geo.hoang.dang.ginsre.activity;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.adapter.NotificationAdapter;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.common.SharedPrefenceKey;
import com.ginsre.geo.hoang.dang.ginsre.fragment.PostDetailFragment;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostDetail;
import com.ginsre.geo.hoang.dang.ginsre.model.users.Notification;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.PreferencesUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.SimpleSwipeUndoAdapter;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity implements OnDataChangeObserver {

    private DynamicListView listview_notification;
    private ImageView imv_header_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        DataChangeNotification.getInstance().addObserver(IssueKey.VIEW_POST_DETAIL, this);
        DataChangeNotification.getInstance().addObserver(IssueKey.CLOSE_POST_DETAIL, this);
        addControl();
        addEvent();
        getAllNotification();
    }


    private void addControl() {
        listview_notification = (DynamicListView)findViewById(R.id.listview_notification);
        imv_header_back = (ImageView)findViewById(R.id.imv_header_back);
    }

    private void addEvent() {
        imv_header_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     *
     */
    private void getAllNotification(){
        try {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            String userId = mAuth.getCurrentUser().getUid();
            mDatabase.child(ConstantUtils.USER_REF_USER).child(ConstantUtils.USER_REF_NOTIFICATION).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        //String num = String.valueOf(dataSnapshot.getChildrenCount());
                        //menu_txvNumOfNotifi.setText(num);
                        if(dataSnapshot.getChildrenCount()>0) {
                            ArrayList<Notification> lstNotifications = new ArrayList<>();
                            for(DataSnapshot snap:dataSnapshot.getChildren()) {
                                Notification notification = snap.getValue(Notification.class);
                                String key = snap.getKey();
                                notification.setKey(key);
                                if((key.split("-")[1]).equalsIgnoreCase(ConstantUtils.NOTIFI_FREACCEPT) && !notification.isReaded()){
                                    updateFriendStatus(notification.getUserId(),ConstantUtils.FRIENDS_MYFRIENDS);
                                }
                                lstNotifications.add(notification);
                            }
                            setDataToListView(lstNotifications);
                        }
                    }catch (Exception e){
                        com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    com.google.firebase.crash.FirebaseCrash.log(databaseError.getMessage());
                }
            });
        }catch (Exception e) {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void updateFriendStatus(String friendId, String friendsMyfriends) {
        try {
            com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB friendDB = com.ginsre.geo.hoang.dang.ginsre.database.FriendsDB.getInstance(getApplicationContext());
            friendDB.update(friendId, friendsMyfriends);
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    /**
     * To set data to listview notifications
     * @param lstNotifications
     */
    private void setDataToListView(ArrayList<Notification> lstNotifications) {
        final NotificationAdapter adapter = new NotificationAdapter(this,lstNotifications);
        SimpleSwipeUndoAdapter swipeUndoAdapter = new SimpleSwipeUndoAdapter(adapter, this, new OnDismissCallback() {
            @Override
            public void onDismiss(@NonNull ViewGroup viewGroup, @NonNull int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions) {
                    adapter.remove(position);
                }
            }
        });
        swipeUndoAdapter.setAbsListView(listview_notification);
        listview_notification.setAdapter(swipeUndoAdapter);
        listview_notification.enableSimpleSwipeUndo();
    }

    private void setPostDetailFromNotificationKey(final String key) {
        try {
            final String keyDetail[] = key.split("-");
            DatabaseReference mref = FirebaseDatabase.getInstance().getReference().child(
                    com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getDatabaseRefForGinsType(keyDetail[1])
            );
            Query query = mref.child(key);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue() != null) {
                        PostDetail postDetail = dataSnapshot.child(ConstantUtils.POST_PROPERTIES).getValue(PostDetail.class);
                        postDetail.setThank(false);
                        postDetail.setLike(false);
                        postDetail.setSpam(false);
                        String dateTime = com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils.getTimeFromCurrentTime(key.split("-")[0], getApplicationContext());
                        postDetail.setDateTime(dateTime);
                        postDetail.setPostId(key);
                        postDetail.setType(keyDetail[1]); // Type name
                        showPostDetail(postDetail);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    com.google.firebase.crash.FirebaseCrash.log(databaseError.getMessage());
                }
            });
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void showPostDetail(PostDetail postDetail)
    {
        /*ArrayList<PostDetail> lstPostDetail = new ArrayList<>();
        lstPostDetail.add(postDetail);
        String prefix = postDetail.getPostId().split("-")[1];
        prefix = prefix.substring(0,3); // TF0-
        String databaseRefFor = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getDatabaseRefForPostDetail(prefix);
        RelativeLayout postedFragmentContainer = (RelativeLayout) findViewById(R.id.notification_show_postDetail_fragment);
        FragmentManager fragmentManager1 = getSupportFragmentManager();
        android.support.v4.app.Fragment fragment = PostDetailFragment.newInstance(getApplicationContext()
                ,lstPostDetail,databaseRefFor, PreferencesUtils.getString(SharedPrefenceKey.USER_TOKEN_ID.name(),""));
        fragmentManager1.beginTransaction()
                .replace(R.id.notification_show_postDetail_fragment, fragment)
                .commit();
        postedFragmentContainer.setVisibility(View.VISIBLE);*/
    }

    @Override
    public void onDataChanged(IssueKey key, Object object) {
        if(key.equals(IssueKey.VIEW_POST_DETAIL)){
            String postKey = (String)object;
            setPostDetailFromNotificationKey(postKey);
        }else if(key.equals(IssueKey.CLOSE_POST_DETAIL)){
            RelativeLayout postedFragmentContainer = (RelativeLayout) findViewById(R.id.notification_show_postDetail_fragment);
            postedFragmentContainer.setVisibility(View.GONE);
        }
    }

}
