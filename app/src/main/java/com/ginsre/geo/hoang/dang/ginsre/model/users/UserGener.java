package com.ginsre.geo.hoang.dang.ginsre.model.users;

import com.google.android.gms.maps.model.LatLng;

import java.util.Map;

/**
 * Created by hoang on 1/1/2018.
 */

public class UserGener {
    private String uName;
    private String uEmail;
    private String uPhone;
    private String uUrl;
    private String status;
    private String birthday;
    private int grade;
    private LatLng mPosition;
    private int nNoTi;
    private int nMess;

    public UserGener() {
    }

    public UserGener(String uName, String uEmail, String uPhone, String uUrl, String birthday) {
        this.uName = uName;
        this.uEmail = uEmail;
        this.uPhone = uPhone;
        this.uUrl = uUrl;
        this.birthday = birthday;
        nNoTi = 0;
        nMess = 0;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getuUrl() {
        return uUrl;
    }

    public void setuUrl(String uUrl) {
        this.uUrl = uUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public LatLng getmPosition() {
        return mPosition;
    }

    public void setmPosition(LatLng mPosition) {
        this.mPosition = mPosition;
    }

    public int getnNoTi() {
        return nNoTi;
    }

    public void setnNoTi(int nNoTi) {
        this.nNoTi = nNoTi;
    }

    public int getnMess() {
        return nMess;
    }

    public void setnMess(int nMess) {
        this.nMess = nMess;
    }
}
