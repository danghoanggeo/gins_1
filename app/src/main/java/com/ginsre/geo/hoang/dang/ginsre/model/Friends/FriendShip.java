package com.ginsre.geo.hoang.dang.ginsre.model.Friends;


/**
 * Created by hoang on 10/6/2017.
 */

public class FriendShip {
    String id;
    String status;
    Boolean seePosi;
    String created;
    String g;

    public FriendShip(){}

    public FriendShip( String friendStatus,Boolean seeRealTimePosition,String dateCreated)
    {
        this.status = friendStatus;
        this.seePosi = seeRealTimePosition;
        this.created = dateCreated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSeePosi() {
        return seePosi;
    }

    public void setSeePosi(Boolean seePosi) {
        this.seePosi = seePosi;
    }


    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getFriendId(int from,int to)
    {
        return this.id.substring(from,to);
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }
}
