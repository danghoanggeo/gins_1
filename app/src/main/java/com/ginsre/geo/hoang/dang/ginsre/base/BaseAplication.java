package com.ginsre.geo.hoang.dang.ginsre.base;

import android.app.Application;
import android.os.Environment;

import com.ginsre.geo.hoang.dang.ginsre.utils.EnvironmentUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.LogUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.PreferencesUtils;

/**
 * Created by trach on 6/4/2017.
 */

public abstract class BaseAplication extends Application {
    /**
     * exit process
     */
    public abstract void exitProcess();

    @Override
    public void onCreate() {
        super.onCreate();
        sAplication = this;
        EnvironmentUtils.init(this);
        PreferencesUtils.init(this);
        LogUtils.setLogEnable(EnvironmentUtils.isLogEnable());
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        LogUtils.setLogFileFolder(path);
        LogUtils.setWriteToFile(EnvironmentUtils.isLogEnable());
    }

    /**
     * application singleton
     */
    public BaseAplication sAplication;

    public static final int KILL_SELF_DELAY = 200; //ms
    /**
     * Loại ứng dụng (tablet hoặc điện thoại)
     */
    public static final String APPLICAITON_TYPE = "application_type";
    /**
     * phone
     */
    public static final int PHONE_TYPE = 0;

    /**
     * table
     */
    public static final int PAD_TYPE = 1;


    public void exit(){
        exitProcess();
    }
}
