package com.ginsre.geo.hoang.dang.ginsre.model.post;

/**
 * Created by hoang on 10/17/2017.
 */

public interface IPostType {
    public static final String TB_NAME = "PostType";
    public static final String POST_ID = "postTypeId";
    public static final String POST_TYPE_NAME = "postTypeName";
    public static final String POST_TYPE_IMAGE = "postTypeDrawable";
    public static final String POST_TYPE_IS_ACTIVE = "postTypeIsActive";
    public static final String SUB_POST_TYPE_TYPE = "subPostType";
}
