package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Interpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import com.ginsre.geo.hoang.dang.ginsre.base.ActivityManager;


final public class AnimationUtils {

    private AnimationUtils() {
    }

    private static final int ABSOLUTE = Animation.ABSOLUTE;

    /**
     * TranslateAnimation
     *
     * @param fromXDelta fromXDelta
     * @param toXDelta   toXDelta
     * @param fromYDelta fromYDelta
     * @param toYDelta   toYDelta
     * @param duration   duration
     * @return TranslateAnimation
     */
    public static TranslateAnimation buildTranslateAnimation
    (float fromXDelta, float toXDelta, float fromYDelta, float toYDelta, long duration) {
        return buildTranslateAnimation(ABSOLUTE, fromXDelta, ABSOLUTE,
                toXDelta, ABSOLUTE, fromYDelta, ABSOLUTE, toYDelta, duration, false);
    }

    /**
     * TranslateAnimation
     *
     * @param fromXDelta fromXDelta
     * @param toXDelta   toXDelta
     * @param fromYDelta fromYDelta
     * @param toYDelta   toYDelta
     * @param fillAfter  fillAfter
     * @param duration   duration
     * @return TranslateAnimation
     */
    public static TranslateAnimation buildTranslateAnimation(float fromXDelta, float toXDelta
            , float fromYDelta, float toYDelta, long duration, boolean fillAfter) {
        return buildTranslateAnimation(ABSOLUTE, fromXDelta, ABSOLUTE,
                toXDelta, ABSOLUTE, fromYDelta, ABSOLUTE, toYDelta, duration, fillAfter);
    }

    /**
     * TranslateAnimation
     *
     * @param fromXType  fromXType
     * @param fromXValue fromXValue
     * @param toXType    toXType
     * @param toXValue   toXValue
     * @param fromYType  fromYType
     * @param fromYValue fromYValue
     * @param toYType    toYType
     * @param toYValue   toYValue
     * @param fillAfter  fillAfter
     * @param duration   duration
     * @return TranslateAnimation
     */
    public static TranslateAnimation buildTranslateAnimation(int fromXType, float fromXValue, int toXType, float toXValue
            , int fromYType, float fromYValue, int toYType, float toYValue, long duration, boolean fillAfter) {
        TranslateAnimation animation = new TranslateAnimation(fromXType, fromXValue, toXType, toXValue, fromYType, fromYValue, toYType, toYValue);
        animation.setDuration(duration);
        animation.setFillAfter(fillAfter);
        return animation;
    }

    /**
     * AlphaAnimation
     *
     * @param fromAlpha  fromAlpha
     * @param toAlpha    toAlpha
     * @param duration   duration
     * @param fillEnable fillEnable
     * @param fillAfter  fillAfter
     * @return AlphaAnimation
     */
    public static AlphaAnimation buildAlphaAnimation(float fromAlpha, float toAlpha, long duration, boolean fillEnable, boolean fillAfter) {
        AlphaAnimation animation = new AlphaAnimation(fromAlpha, toAlpha);
        animation.setFillEnabled(fillEnable);
        animation.setFillAfter(fillAfter);
        animation.setDuration(duration);
        return animation;
    }

    /**
     * AlphaAnimation
     *
     * @param fromAlpha fromAlpha
     * @param toAlpha   toAlpha
     * @param duration  duration
     * @return AlphaAnimation
     */
    public static AlphaAnimation buildAlphaAnimation(float fromAlpha, float toAlpha, long duration) {
        return buildAlphaAnimation(fromAlpha, toAlpha, duration, false, false);
    }

    /**
     * RotateAnimation
     *
     * @param fromDegrees fromDegrees
     * @param toDegrees   toDegrees
     * @param duration    duration
     * @return RotateAnimation
     */
    public static RotateAnimation buildRotateAnimation(float fromDegrees, float toDegrees, long duration) {
        return buildRotateAnimation(fromDegrees, toDegrees, 0.0f, 0.0f, duration);
    }

    /**
     * RotateAnimation
     *
     * @param fromDegrees fromDegrees
     * @param toDegrees   toDegrees
     * @param pivotX      pivotX
     * @param pivotY      pivotY
     * @param duration    duration
     * @return RotateAnimation
     */
    public static RotateAnimation buildRotateAnimation(float fromDegrees, float toDegrees, float pivotX, float pivotY, long duration) {
        return buildRotateAnimation(fromDegrees, toDegrees, ABSOLUTE, pivotX, ABSOLUTE, pivotY, duration);
    }


    /**
     * RotateAnimation
     *
     * @param fromDegrees fromDegrees
     * @param toDegrees   toDegrees
     * @param pivotXType  pivotXType
     * @param pivotXValue pivotXValue
     * @param pivotYType  pivotYType
     * @param pivotYValue pivotYValue
     * @param duration    duration
     * @return RotateAnimation
     */
    public static RotateAnimation buildRotateAnimation(float fromDegrees, float toDegrees, int pivotXType
            , float pivotXValue, int pivotYType, float pivotYValue, long duration) {
        return buildRotateAnimation(fromDegrees, toDegrees, pivotXType, pivotXValue, pivotYType, pivotYValue, duration, false);
    }

    /**
     * RotateAnimation
     *
     * @param fromDegrees fromDegrees
     * @param toDegrees   toDegrees
     * @param pivotXType  pivotXType
     * @param pivotXValue pivotXValue
     * @param pivotYType  pivotYType
     * @param pivotYValue pivotYValue
     * @param duration    duration
     * @param fillAfter   fillAfter
     * @return RotateAnimation
     */
    public static RotateAnimation buildRotateAnimation(float fromDegrees, float toDegrees, int pivotXType, float pivotXValue
            , int pivotYType, float pivotYValue, long duration, boolean fillAfter) {
        return buildRotateAnimation(fromDegrees, toDegrees, pivotXType, pivotXValue, pivotYType, pivotYValue
                , duration, fillAfter, new AccelerateDecelerateInterpolator());
    }

    /**
     * RotateAnimation
     *
     * @param fromDegrees  fromDegrees
     * @param toDegrees    toDegrees
     * @param pivotXType   pivotXType
     * @param pivotXValue  pivotXValue
     * @param pivotYType   pivotYType
     * @param pivotYValue  pivotYValue
     * @param duration     duration
     * @param interpolator interpolator
     * @param fillAfter    fillAfter
     * @return RotateAnimation
     */
    public static RotateAnimation buildRotateAnimation(float fromDegrees, float toDegrees, int pivotXType, float pivotXValue
            , int pivotYType, float pivotYValue, long duration, boolean fillAfter, Interpolator interpolator) {
        RotateAnimation animation = new RotateAnimation(fromDegrees, toDegrees, pivotXType, pivotXValue, pivotYType, pivotYValue);
        animation.setInterpolator(interpolator);
        animation.setFillAfter(fillAfter);
        animation.setDuration(duration);
        return animation;
    }

    /**
     * ScaleAnimation
     *
     * @param fromX    fromX
     * @param toX      toX
     * @param fromY    fromY
     * @param toY      toY
     * @param duration duration
     * @return ScaleAnimation
     */
    public static ScaleAnimation buildScaleAnimation(float fromX, float toX, float fromY, float toY, long duration) {
        return buildScaleAnimation(fromX, toX, fromY, toY, duration, 0);
    }

    /**
     * @param fromX       fromX
     * @param toX         toX
     * @param fromY       fromY
     * @param toY         toY
     * @param duration    duration
     * @param startOffset startOffset
     * @return ScaleAnimation
     */
    public static ScaleAnimation buildScaleAnimation(float fromX, float toX, float fromY, float toY, long duration, long startOffset) {
        return buildScaleAnimation(fromX, toX, fromY, toY, 0.5f, 0.5f, duration, startOffset);
    }

    /**
     * ScaleAnimation
     *
     * @param fromX       fromX
     * @param toX         toX
     * @param fromY       fromY
     * @param toY         toY
     * @param pivotX      pivotX
     * @param pivotY      pivotY
     * @param duration    duration
     * @param startOffset startOffset
     * @return ScaleAnimation
     */
    public static ScaleAnimation buildScaleAnimation(float fromX, float toX, float fromY, float toY
            , float pivotX, float pivotY, long duration, long startOffset) {
        return buildScaleAnimation(fromX, toX, fromY, toY, Animation.RELATIVE_TO_SELF, pivotX, Animation.RELATIVE_TO_SELF, pivotY, duration, startOffset);
    }

    /**
     * ScaleAnimation
     *
     * @param fromX       fromX
     * @param toX         toX
     * @param fromY       fromY
     * @param toY         toY
     * @param pivotXType  pivotXType
     * @param pivotXValue pivotXValue
     * @param pivotYType  pivotYType
     * @param pivotYValue pivotYValue
     * @param duration    duration
     * @param startOffset startOffset
     * @return ScaleAnimation
     */
    public static ScaleAnimation buildScaleAnimation(float fromX, float toX, float fromY, float toY, int pivotXType
            , float pivotXValue, int pivotYType, float pivotYValue, long duration, long startOffset) {
        ScaleAnimation animation = new ScaleAnimation(fromX, toX, fromY, toY, pivotXType, pivotXValue, pivotYType, pivotYValue);
        animation.setDuration(duration);
        animation.setStartOffset(startOffset);
        return animation;
    }

    /**
     * AnimationSet
     *
     * @param shareInterpolator shareInterpolator
     * @param animations        animations
     * @return AnimationSet
     */
    public static AnimationSet buildAnimationSet(boolean shareInterpolator, Animation... animations) {
        AnimationSet animationSet = new AnimationSet(shareInterpolator);
        for (Animation animation : animations) {
            animationSet.addAnimation(animation);
        }
        return animationSet;
    }

    /**
     * chay dich chuyen
     *
     * @param animateView
     * @param startLocation
     * @param endLocation
     * @param duration
     * @param otherAnimationSet
     */
    public static void runTrackAnimationOnTempView(final View animateView, final int[] startLocation, final int[] endLocation
            , final long duration, final AnimationSet otherAnimationSet) {
        runTrackAnimationOnTempView(animateView, startLocation, endLocation, duration, otherAnimationSet, null);
    }

    /**
     * chay dich chuyen
     *
     * @param animateView
     * @param startLocation
     * @param endLocation
     * @param duration
     * @param otherAnimationSet
     * @param listener
     */
    public static void runTrackAnimationOnTempView(final View animateView, final int[] startLocation, final int[] endLocation
            , final long duration, final AnimationSet otherAnimationSet, Animation.AnimationListener listener) {
        Activity currentActivity = ActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null) {
            final ViewGroup decorView = (ViewGroup) currentActivity.getWindow().getDecorView();
            decorView.addView(animateView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            int[] originLoc = new int[2];
            animateView.getLocationOnScreen(originLoc);

            int offset = DisplayUtils.dp2px(4);
            Animation translateAnimation = buildTranslateAnimation(startLocation[0] - originLoc[0],
                    endLocation[0] - originLoc[0] - 2 * offset, startLocation[1] - originLoc[1], endLocation[1] - originLoc[1] + 4 * offset, duration);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(translateAnimation);
            animationSet.addAnimation(otherAnimationSet);

            runAnimationOnTempView(animateView, decorView, animationSet, listener);
        }
    }

    /**
     *
     * @param animateView
     * @param animation
     */
    public static void runAnimationOnTempView(final View animateView, Animation animation) {
        runAnimationOnTempView(animateView, animation, null);
    }

    /**
     *
     * @param tempAnimateView
     * @param animation
     * @param listener
     */
    public static void runAnimationOnTempView(final View tempAnimateView, Animation animation, Animation.AnimationListener listener) {
        Activity currentActivity = ActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null) {
            final ViewGroup decorView = (ViewGroup) currentActivity.getWindow().getDecorView();
            decorView.addView(tempAnimateView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            runAnimationOnTempView(tempAnimateView, decorView, animation, listener);
        }
    }

    private static void runAnimationOnTempView(final View animateView, final ViewGroup viewGroup
            , Animation animation, final Animation.AnimationListener listener) {
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (listener != null) {
                    listener.onAnimationStart(animation);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                viewGroup.post(new Runnable() {
                    @Override
                    public void run() {
                        viewGroup.removeView(animateView);
                    }
                });

                if (listener != null) {
                    listener.onAnimationEnd(animation);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (listener != null) {
                    listener.onAnimationRepeat(animation);
                }
            }
        });
        animateView.startAnimation(animation);
    }

    /**
     * expand Input dialog
     * @param expandView expandView
     * @param button button
     * @param inputWidth inputWidth
     * @param buttonWidth buttonWidth
     * @param buttonHeight buttonHeight
     * @param duration duration
     */
    public static void expandInput(final View expandView, final View button, final int inputWidth, final int buttonWidth, final int buttonHeight, final long duration) {

        final Interpolator reverseInterpolator = new Interpolator() {
            @Override
            public float getInterpolation(float paramFloat) {
                return Math.abs(paramFloat - 1f);
            }
        };

        Animation expandButtonAnimation = expandButtonAnimation(buttonWidth, buttonHeight, duration);
        expandButtonAnimation.setInterpolator(reverseInterpolator);
        button.startAnimation(expandButtonAnimation);
        expandButtonAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                button.setVisibility(View.GONE);
                Animation dismissInputAnimation = dismissInputAnimation(inputWidth, buttonWidth, duration);
                dismissInputAnimation.setInterpolator(reverseInterpolator);
                expandView.setVisibility(View.VISIBLE);
                expandView.startAnimation(dismissInputAnimation);
                dismissInputAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) { }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Animation shrinkInputAnimation = shrinkInputAnimation(inputWidth, buttonWidth, duration);
                        shrinkInputAnimation.setInterpolator(reverseInterpolator);
                        expandView.startAnimation(shrinkInputAnimation);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) { }
                });
            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
        });
    }

    /**
     * shrink Input dialog
     * @param expandView expandView
     * @param button button
     * @param inputWidth inputWidth
     * @param buttonWidth buttonWidth
     * @param buttonHeight buttonHeight
     * @param duration duration
     */
    public static void shrinkInput(final View expandView, final View button, final int inputWidth, final int buttonWidth, final int buttonHeight, final long duration) {
        Animation shrinkInputAnimation = shrinkInputAnimation(inputWidth, buttonWidth, duration);
        expandView.startAnimation(shrinkInputAnimation);
        shrinkInputAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                Animation dismissInputAnimation = dismissInputAnimation(inputWidth, buttonWidth, duration);
                expandView.startAnimation(dismissInputAnimation);
                dismissInputAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) { }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        expandView.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) { }
                });

                Animation expandButtonAnimation = expandButtonAnimation(buttonWidth, buttonHeight, duration);
                button.setVisibility(View.VISIBLE);
                button.startAnimation(expandButtonAnimation);
            }
            @Override
            public void onAnimationRepeat(Animation animation) { }
        });
    }

    private static Animation shrinkInputAnimation(final int inputWidth, final int buttonWidth, long duration) {
        final float scale = buttonWidth / (float) inputWidth;

        final AnimationSet animationSet = new AnimationSet(true);

        ScaleAnimation scaleAnimation = new ScaleAnimation(1, scale, 1, 1);
        scaleAnimation.setDuration(duration / 2);

        TranslateAnimation translateAnimation = new TranslateAnimation(0, inputWidth - buttonWidth, 0, 0);
        translateAnimation.setDuration(duration / 2);

        AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0.5f);
        alphaAnimation.setDuration(duration / 2);

        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(alphaAnimation);

        return animationSet;
    }

    private static Animation dismissInputAnimation(final int inputWidth, final int buttonWidth, long duration) {
        AnimationSet animationSet = new AnimationSet(true);

        float scale = buttonWidth / (float) inputWidth;
        ScaleAnimation scaleAnimation = new ScaleAnimation(scale, scale, 0, 0);
        scaleAnimation.setDuration(duration / 2);

        TranslateAnimation translateAnimation = new TranslateAnimation(inputWidth - buttonWidth, inputWidth - buttonWidth, 0, 0);
        translateAnimation.setDuration(duration / 2);

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.5f, 0);
        alphaAnimation.setDuration(duration / 2);

        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(alphaAnimation);

        return animationSet;
    }

    private static Animation expandButtonAnimation(int buttonWidth, int buttonHeight, long duration) {
        AnimationSet animationSet = new AnimationSet(true);

        ScaleAnimation btnScaleAnimation = new ScaleAnimation(0, 1, 0, 1, 0.5f, 0.5f);
        btnScaleAnimation.setDuration(duration);

        TranslateAnimation btnTranslateAnimation = new TranslateAnimation(buttonWidth / 2, 0, buttonHeight / 2, 0);
        btnTranslateAnimation.setDuration(duration);

        AlphaAnimation btnAlphaAnimation = new AlphaAnimation(0.5f, 1);
        btnAlphaAnimation.setDuration(duration);

        animationSet.addAnimation(btnScaleAnimation);
        animationSet.addAnimation(btnTranslateAnimation);
        animationSet.addAnimation(btnAlphaAnimation);

        return animationSet;
    }
}
