package com.ginsre.geo.hoang.dang.ginsre.model.core;

import java.util.List;

/**
 * Created by hoang on 11/1/2017.
 */

public class geometry {
    private List<Double> coor;
    public geometry(){}
    public geometry(List<Double> coor)
    {
        this.coor = coor;
    }

    public List<Double> getCoor() {
        return coor;
    }

    public void setCoor(List<Double> coor) {
        this.coor = coor;
    }
}
