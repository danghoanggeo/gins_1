package com.ginsre.geo.hoang.dang.ginsre.model.post;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hoang on 9/5/2017.
 */
@IgnoreExtraProperties

public class PostDetail{
    private String postId;
    private String userId;
    private String content;
    private String type;
    private String dateTime;
    private String status;
    private String imagesUrl;
    private boolean isLike,isThank,isSpam;
    private int numOfComment;
    private int numOfThanks;
    private int numOfLikes;
    private int numOfShare;
    private int numOfSpam;
    private String location_name;
    private ArrayList<Map<String,String>> listComment;

    public PostDetail(){
        // Default constructor required for calls to DataSnapshot.getValue(TrafficPost.class)
    }
    public PostDetail( String userId, String content,String type,String imagesUrl) {
        this.userId = userId;
        this.content = content;
        this.type = type;
        this.imagesUrl = imagesUrl;
        this.numOfComment = 0;
        this.numOfThanks = 1;
        this.numOfLikes = 0;
        this.numOfShare = 0;
        this.numOfSpam = 0;
        this.status = "0";
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(String imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public int getNumOfLikes() {
        return numOfLikes;
    }

    public void setNumOfLikes(int numOfLikes) {
        this.numOfLikes = numOfLikes;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateTime() {
        return this.dateTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumOfComment() {
        return numOfComment;
    }

    public void setNumOfComment(int numOfComment) {
        this.numOfComment = numOfComment;
    }

    public int getNumOfThanks() {
        return numOfThanks;
    }

    public void setNumOfThanks(int numOfThanks) {
        this.numOfThanks = numOfThanks;
    }

    public int getNumOfShare() {
        return numOfShare;
    }

    public void setNumOfShare(int numOfShare) {
        this.numOfShare = numOfShare;
    }

    public int getNumOfSpam() {
        return numOfSpam;
    }

    public void setNumOfSpam(int numOfSpam) {
        this.numOfSpam = numOfSpam;
    }

    public boolean isLike() {
        return isLike;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public boolean isThank() {
        return isThank;
    }

    public void setThank(boolean thank) {
        isThank = thank;
    }

    public boolean isSpam() {
        return isSpam;
    }

    public void setSpam(boolean spam) {
        isSpam = spam;
    }

    public ArrayList<Map<String, String>> getListComment() {
        return listComment;
    }

    public void setListComment(ArrayList<Map<String, String>> listComment) {
        this.listComment = listComment;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }
}
