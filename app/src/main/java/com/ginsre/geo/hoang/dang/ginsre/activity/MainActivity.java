package com.ginsre.geo.hoang.dang.ginsre.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.base.BaseActivity;
import com.ginsre.geo.hoang.dang.ginsre.clustering.ClusterItem;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.common.SharedPrefenceKey;
import com.ginsre.geo.hoang.dang.ginsre.fragment.LocationAddNewFragment;
import com.ginsre.geo.hoang.dang.ginsre.fragment.PostAddNewFragment;
import com.ginsre.geo.hoang.dang.ginsre.fragment.PostDetailFragment;
import com.ginsre.geo.hoang.dang.ginsre.maps.DirectionsUtil;
import com.ginsre.geo.hoang.dang.ginsre.maps.SphericalUtil;
import com.ginsre.geo.hoang.dang.ginsre.model.Friends.Friends;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostDetail;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostType;
import com.ginsre.geo.hoang.dang.ginsre.model.users.UserPlaces;
import com.ginsre.geo.hoang.dang.ginsre.model.weather.Forecast;
import com.ginsre.geo.hoang.dang.ginsre.model.weather.Storm;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.utils.AnimationUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.DateUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions;
import com.ginsre.geo.hoang.dang.ginsre.utils.MapUtils;
import com.ginsre.geo.hoang.dang.ginsre.utils.MultiDrawable;
import com.ginsre.geo.hoang.dang.ginsre.utils.PreferencesUtils;
import com.ginsre.geo.hoang.dang.ginsre.view.DateTimePickerDialog;
import com.ginsre.geo.hoang.dang.ginsre.clustering.view.DefaultClusterRenderer;
import com.ginsre.geo.hoang.dang.ginsre.clustering.ui.IconGenerator;
import com.facebook.FacebookSdk;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoFire;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoLocation;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoQuery;
import com.ginsre.geo.hoang.dang.ginsre.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ginsre.geo.hoang.dang.ginsre.clustering.Cluster;
import com.ginsre.geo.hoang.dang.ginsre.clustering.ClusterManager;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

//import bolts.AppLinks;


public class MainActivity extends BaseActivity implements OnDataChangeObserver,OnMapReadyCallback
        , GeoQueryEventListener, GoogleMap.OnCameraChangeListener,
        ClusterManager.OnClusterClickListener<PostType>, ClusterManager.OnClusterInfoWindowClickListener<PostType>
        , ClusterManager.OnClusterItemClickListener<PostType>, ClusterManager.OnClusterItemInfoWindowClickListener<PostType> {
    private static final String TAG = "Main activity";

    //ProgressDialog progressDialog;
    boolean mLocationPermissionGranted = false;
    // Flag
    boolean mFlag_isMoving = false;
    boolean mFlag_isOpenFragment = false;
    //Control
    RelativeLayout addNewTrafficsPost_layout;
    RelativeLayout postedFragmentContainer;
    FragmentManager fragmentManager;
    //FancyButton main_fbtn_mylocation,main_fbtn_addNewPost,main_fbtn_direction;
    //FancyButton main_txvDateTime,main_fbtnDateTimeStart;
    //
    ImageView item_traffic_posted_imvdetail;
    // Direction;
    SeekBar main_seekbar_change_time;
    private static final GeoLocation INITIAL_CENTER = new GeoLocation(10.7714, 106.6285202);
    private GeoFire geoFire;
    private GeoQuery geoQuery;

    private Map<String,Marker> markers;
    private Map<String,PostType> postTypes;
   // private HashMap<String,PostDetail> postDetails;
    private ClusterManager<PostType> postClusterManager;

    //Heat map
    TileOverlay mOverlay;
    com.google.maps.android.heatmaps.HeatmapTileProvider mProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        extractHeaderFooterToActivity();
        addControls();
        addEvents();
        //TODO; this work do in Splash screen for the first time app running;
        PreferencesUtils.init(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        FirebaseMessaging.getInstance().subscribeToTopic("Notifications");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("Notifications");
        // FaceBook
        FacebookSdk.sdkInitialize(getApplicationContext());

        /* Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
        if (targetUrl != null) {
            Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
        }*/
        //printKeyHash(this);
    }



    @Override
    protected void onStop() {
        super.onStop();
        // remove all event listeners to stop updating in the background
        this.geoQuery.removeAllListeners();
        //postClusterManager.clearItems();
        //removeAllmarkers();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // add an event listener to start updating locations again
        //this.geoQuery.addGeoQueryEventListener(this);
        //
    }

    @Override
    public void onBackPressed() {
        if(mFlag_isOpenFragment)
        {
            postedFragmentContainer.setVisibility(View.GONE);
            mFlag_isOpenFragment = false;
        }
        else
            finish();
    }


    private void addControls() {
        try {
            // Data change notification
            DataChangeNotification.getInstance().addObserver(IssueKey.CLOSE_POST_DETAIL, this);
            DataChangeNotification.getInstance().addObserver(IssueKey.DIRECTTION_TO_ADDRESS,this);
            DataChangeNotification.getInstance().addObserver(IssueKey.CHANGE_GEO_QUERY_NOTI,this);
            DataChangeNotification.getInstance().addObserver(IssueKey.CHANGE_GEO_QUERY_DATE,this);

            // Date preference; by default;
            // Map
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

            //Control
            //main_fbtn_mylocation = (FancyButton) findViewById(R.id.main_fbtn_mylocation);
            //main_fbtn_addNewPost = (FancyButton) findViewById(R.id.main_fbtn_addNewPost);
            //main_fbtn_direction = (FancyButton)findViewById(R.id.main_fbtn_direction);
            addNewTrafficsPost_layout = (RelativeLayout)findViewById(R.id.addNewTrafficsPost_layout);
            //showPostedDetails_fragment = (LinearLayout)findViewById(R.id.showPostedDetails_fragment);
            postedFragmentContainer = (RelativeLayout)findViewById(R.id.postedFragmentContainer);
            //main_txvDateTime = (FancyButton) findViewById(R.id.main_fbtnDateTime);
            //main_fbtnDateTimeStart = (FancyButton) findViewById(R.id.main_fbtnDateTimeStart);
            main_txvDateTime.setText(DateUtils.formatCurrentDate(DateUtils.FORMAT_TO_YYYY_MM_DD));
            main_fbtnDateTimeStart.setText(DateUtils.formatCurrentDate(DateUtils.FORMAT_TO_YYYY_MM_DD));
            main_seekbar_change_time = (SeekBar)findViewById(R.id.main_seekbar_change_time);
            // ImageView In infoWindow
            item_traffic_posted_imvdetail = (ImageView)findViewById(R.id.item_traffic_posted_imvdetail);
            if(mLastKnownLocation == null)
                getLastKnowLocation();
            String lastGintype = PreferencesUtils.getString(SharedPrefenceKey.LAST_GINS_TYPE_SELECT.name(),"");
            if(lastGintype.equals(""))
                lastGintype = ConstantUtils.GINS_MAIN_TYPE[1];
            setGeoQueryToDataType(lastGintype);
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void addEvents() {
        try {
            main_fbtn_mylocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mLastKnownLocation!=null)
                        showCurrentLocation();
                    else
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.map_error_cannot_find_location),Toast.LENGTH_SHORT).show();
                }
            });
            main_fbtn_addNewPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(main_fbtn_addNewPost.getTag() == null || main_fbtn_addNewPost.getTag().equals("0")) {
                        main_fbtn_addNewPost.setTag("1");
                        main_fbtn_addNewPost.setIconResource(R.drawable.cancel_white);
                        addNewFragmentToActivity();
                    }
                    else if(main_fbtn_addNewPost.getTag().equals("1"))
                    {
                        setAddNewTrafficsPostLayoutInOut();
                    }
                }
            });
            main_fbtn_direction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
                    if(main_fbtn_direction.getTag() == null) {

                        directToDestination(null);
                    }
                    else
                    {
                        cancelDirection();
                    }
                }
            });
            main_seekbar_change_time.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    int value = seekBar.getProgress();
                    if(seekBar.getProgress()>90){
                        String dateTimeBack = DateUtils.getDateTime(0);
                        main_fbtnDateTimeStart.setText(dateTimeBack);
                        refreshData();
                    }else{
                        String dateTimeBack = DateUtils.getDateTime((100-value)/10);
                        main_fbtnDateTimeStart.setText(dateTimeBack);
                        setGeoQueryToDataType(flag_activity_choose_present);
                    }

                }
            });
            main_txvDateTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    main_txvDateTime.setTag(true);
                    main_fbtnDateTimeStart.setTag(false);
                    DateTimePickerDialog dateTimePickerDialog = new DateTimePickerDialog();
                    dateTimePickerDialog.show(getFragmentManager(),"Date piker main");
                }
            });
            main_fbtnDateTimeStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    main_txvDateTime.setTag(false);
                    main_fbtnDateTimeStart.setTag(true);
                    DateTimePickerDialog dateTimePickerDialog = new DateTimePickerDialog();
                    dateTimePickerDialog.show(getFragmentManager(),"Date piker main");
                }
            });
            main_imvViewAllPostDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*
                    if(!flag_activity_choose_present.equals(ConstantUtils.LOCATION_PREFIX)) {
                        if (postTypes.size() > 0) {
                            Map<String, PostDetail> sortedPost = new TreeMap<>(postDetails);
                            List<PostDetail> lstPostD = new ArrayList<>(sortedPost.values());
                            showListPostDetail(lstPostD);
                        } else {
                            Toast.makeText(getApplicationContext(), "No post recently", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        //TODO: Show location;
                    }*/
                    try {
                        Intent intent = new Intent(MainActivity.this,ViewPostDetailActivity.class);
                        intent.putExtra(ConstantUtils.GINS_VIEW_POSTDETAIL_TYPE,flag_activity_choose_present);
                        intent.putExtra(ConstantUtils.GINS_VIEW_POSTDETAIL_LAT,mMap.getCameraPosition().target.latitude);
                        intent.putExtra(ConstantUtils.GINS_VIEW_POSTDETAIL_LONG,mMap.getCameraPosition().target.longitude);
                        intent.putExtra(ConstantUtils.GINS_VIEW_POSTDETAIL_DATE,main_fbtnDateTimeStart.getText());
                        //intent.putExtra(ConstantUtils.POST_ID,null);
                        startActivity(intent);
                    }catch (Exception e){

                    }

                }
            });
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }

    }



    //Start Map function
    GoogleMap.OnMyLocationChangeListener listener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
        try {
            LatLng currentLocation = new LatLng(location.getLatitude(),location.getLongitude());
            //Toast.makeText(MainActivity.this,"Lat: "+location.getLatitude()+" Lon: "+location.getLongitude(),Toast.LENGTH_LONG).show();
            //update camera position.
            if(mLastKnownLocation != null){
                if(MapUtils.getDistanceBetween2Place(mLastKnownLocation,currentLocation)>0.02) // > 10m.
                {
                    if(mFlag_isMoving) {
                        Marker marker = markers.get(ConstantUtils.CURRENT_LOCATION);
                        if (marker == null) {
                            marker = mMap.addMarker(new MarkerOptions()
                                    .position(currentLocation)
                                    .title(getResources().getString(R.string.map_current_position))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.direction)));
                            markers.put(ConstantUtils.CURRENT_LOCATION, marker);
                        } else {
                            float degreee = (float) SphericalUtil.computeHeading(mLastKnownLocation, currentLocation);
                            marker.setPosition(currentLocation);
                            animateCameraToPosition(currentLocation,18,degreee,80);

                        }
                    }
                    if(MapUtils.getDistanceBetween2Place(mLastKnownLocation,currentLocation)>0.03) // Update when user is being direction.
                    {
                        //TODO: Test distances between location and current location; if distance > 50 m : current = location; and do the work below
                        // Write user location to firebase;
                        com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.writeUserLocationToFirebase(currentLocation,getApplicationContext());
                    }
                    //animateMarkerTo(markers.get(ConstantUtils.CURRENT_LOCATION),location.getLatitude(),location.getLatitude());
                }

            }
            else{
                // Write user location to firebase for the first time;
                com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.writeUserLocationToFirebase(currentLocation,getApplicationContext());

            }
            mLastKnownLocation = currentLocation;
            //writeUserLocationToFirebase(currentLocation);
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
        }
    };


    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            mMap.getUiSettings().setMapToolbarEnabled(false);
            settingMapStyle(flag_activity_choose_present);
            Intent intent = getIntent();
            String notificationPostId = "";
            if(intent != null)
            {
                notificationPostId = intent.getStringExtra(ConstantUtils.VIEW_POST_LOCATION);
            }
            //
            if(!(notificationPostId == null)){
                com.ginsre.geo.hoang.dang.ginsre.model.users.Notification notification = new com.ginsre.geo.hoang.dang.ginsre.model.users.Notification(notificationPostId,true);
                //mLastKnownLocation = notification.getLatLng();
                String ginType = (notification.getKey().split("-")[1]).substring(0,3);
                setGeoQueryToDataType(ginType);
                setFooterIbtnActive(ginType);
                this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(notification.getLatLng(), ConstantUtils.ZOOM_LEVEL+3));
            }else{
                if(mLastKnownLocation!=null) {
                    LatLng latLngCenter = new LatLng(mLastKnownLocation.latitude, mLastKnownLocation.longitude);
                    this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngCenter, ConstantUtils.ZOOM_LEVEL));
                }
                else {
                    LatLng latLngCenter = new LatLng(INITIAL_CENTER.latitude, INITIAL_CENTER.longitude);
                    this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngCenter, ConstantUtils.INITIAL_ZOOM_LEVEL));
                }
            }
            this.mMap.setOnCameraChangeListener(this);
            addMapEvents();
            //TODO move getLocationPermission to first;
            getLocationPermission();
            startClustering();
            initFireBase();
            addHeatMap();
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    //

    private void settingMapStyle(String gintype){
        com.google.android.gms.maps.model.MapStyleOptions map_style;
        if(gintype.equalsIgnoreCase(ConstantUtils.LOCATION_PREFIX))
            map_style = com.google.android.gms.maps.model.MapStyleOptions.loadRawResourceStyle(this, R.raw.standard_style);
        /*else if(gintype.equalsIgnoreCase(ConstantUtils.FRIEND_PREFIX)){
            map_style = com.google.android.gms.maps.model.MapStyleOptions.loadRawResourceStyle(this, R.raw.sliver_style);
        }*/
        else
            map_style = com.google.android.gms.maps.model.MapStyleOptions.loadRawResourceStyle(this,R.raw.sliver_style);
        mMap.setMapStyle(map_style);
        // GetNotification;
    }

    private void startClustering() {
        postClusterManager = new ClusterManager<PostType>(this, mMap);
        postClusterManager.setRenderer(new PostRenderer());
        //getMap().setOnCameraIdleListener(usersClusterManager);
        mMap.setOnMarkerClickListener(postClusterManager);
        mMap.setOnInfoWindowClickListener(postClusterManager);
        postClusterManager.setOnClusterClickListener(this);
        postClusterManager.setOnClusterInfoWindowClickListener(this);
        postClusterManager.setOnClusterItemClickListener(this);
        postClusterManager.setOnClusterItemInfoWindowClickListener(this);
    }

    private void addMapEvents() {
        /*mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                progressDialog.dismiss();
                // Show your last location here;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLastKnownLocation,ConstantUtils.INITIAL_ZOOM_LEVEL));
            }
        });*/
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                //Marker marker = mMap.addMarker(new MarkerOptions().position(latLng));
                if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[0])){

                }else if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[1])){ // traffic
                    if(markers.get(ConstantUtils.DESTINATION)==null) {
                        //destination = latLng;
                        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_black_24dp)));
                        marker.setDraggable(true);
                        marker.setTag(ConstantUtils.DESTINATION);
                        markers.put(ConstantUtils.DESTINATION, marker);
                    }
                }else if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[2])){
                    //showTheWeather(latLng);
                }else if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[3])){
                    Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_black_24dp)));
                    //showTheWeather(latLng);
                }else if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[4])){
                    if(markers.get(ConstantUtils.MEMORIES_LOCATION)==null) {
                        //destination = latLng;
                        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_person_pin_circle_black_24dp)));
                        marker.setDraggable(true);
                        marker.setTag(ConstantUtils.MEMORIES_LOCATION);
                        markers.put(ConstantUtils.MEMORIES_LOCATION, marker);
                    }
                }

            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                setAddNewTrafficsPostLayoutInOut();
                if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[3]))
                    showTheWeather(latLng);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.hideInfoWindow();
                //showPostDetailToActivity(marker);
            }
        });
    }


    private void removeAllmarkers() {
        for (Marker marker: this.markers.values()) {
            marker.remove();
        }
        this.markers.clear();
    }

    private double zoomLevelToRadius(double zoomLevel) {
        // Approximation to fit circle into view
        return 16384000/Math.pow(2, zoomLevel);
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        // Update the search criteria for this geoQuery and the circle on the map
        LatLng center = cameraPosition.target;
        cameraLocation = new LatLng(center.latitude,center.longitude);
        double radius = zoomLevelToRadius(cameraPosition.zoom);
        //this.searchCircle.setCenter(center);
        //this.searchCircle.setRadius(radius);
        //TODO: change query to new zone;
        if(cameraPosition.zoom>5) {
            this.geoQuery.setCenter(new GeoLocation(center.latitude, center.longitude));
            this.geoQuery.setRadius(radius *2/1000);
        }

    }


    // This function should move to the welcome activity ;
    private void getLocationPermission() {
    /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            updateLocationUI();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.INTERNET},
                    ConstantUtils.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults){
        switch (requestCode){
            case ConstantUtils.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationChangeListener(listener);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void showCurrentLocation() {
        if(mMap!=null)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLastKnownLocation, ConstantUtils.ZOOM_LEVEL));
    }

    private void addHeatMap(){
        int[] colors = {Color.argb(0, 0, 255, 255),// transparent
                Color.argb(255 / 3 * 2, 0, 255, 255),
                Color.rgb(102, 255, 0),
                Color.rgb(0, 0, 127),
                Color.rgb(255, 0, 0)};
        int[] colors1 = {Color.rgb(102, 225, 0), // green
                Color.rgb(255, 0, 0) };
        float[] startPoints = {0.0f,0.2f,0.5f,0.8f, 1.0f};
        Gradient gradient = new Gradient(colors,startPoints);
        List<LatLng> list = new ArrayList<LatLng>();
        list.add(new LatLng(10.801417, 106.665876));
        list.add(new LatLng(10.836503, 106.658311));
        list.add(new LatLng(10.820572, 106.694083));
        list.add(new LatLng(10.802756, 106.694984));
        list.add(new LatLng(10.773734, 106.621491));
        list.add(new LatLng(10.802997, 106.709381));
        list.add(new LatLng(10.867241, 106.787629));
        list.add(new LatLng(10.786313, 106.687650));
        list.add(new LatLng(10.801833, 106.636735));
        list.add(new LatLng(10.803824, 106.635780));
        list.add(new LatLng(10.799984, 106.660670));
        list.add(new LatLng(10.812601, 106.665215));
        list.add(new LatLng(10.756451, 106.685242));
        list.add(new LatLng(10.784541, 106.707902));
        list.add(new LatLng(10.792356, 106.751364));
        list.add(new LatLng(10.826483, 106.761081));

        final Map<String,WeightedLatLng> map = new HashMap<String,WeightedLatLng>();

        // Create the tile provider
       mProvider = new com.google.maps.android.heatmaps.HeatmapTileProvider.Builder()
                .data(list).gradient(gradient)
                .build();
       mProvider.setRadius(25);
        mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
        DatabaseReference mdatabase = FirebaseDatabase.getInstance().getReference();

        int i = 0;
        for (final LatLng latLng : list){
            map.put(String.valueOf(i),new WeightedLatLng(latLng,1));
            mdatabase.child(ConstantUtils.GINS_DATA_DYNAMIC+ConstantUtils.TRAFFIC_NOW).child(String.valueOf(i)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try{
                        int index = Integer.parseInt(dataSnapshot.getKey());
                        int weight = dataSnapshot.getValue(Integer.class);
                        map.remove(String.valueOf(index));
                        map.put(String.valueOf(index),new WeightedLatLng(latLng,weight*2));
                        changeHeatMapWeight(map);
                        //drawCycle(index,latLng,weight);
                    }catch (Exception e){

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            i ++;
        }
    }

    private void drawCycle(int index,LatLng latLng, int weight){

        int[] colors = {Color.argb(128, 102, 255, 0),// transparent
                Color.argb(255 / 3 * 2, 80, 255, 0),
                Color.argb(128, 180, 180, 0),
                Color.argb(128, 220, 80, 0),
                Color.argb(128, 255, 0, 0)};
        com.google.android.gms.maps.model.Circle circle = mMap.addCircle(new com.google.android.gms.maps.model.CircleOptions()
                .center(latLng)
                .radius(100)
                .fillColor(colors[weight-1])
                .clickable(true));
        circle.setTag(index);

    }

    private void changeHeatMapWeight(Map<String,WeightedLatLng> data){
        mProvider.setWeightedData(data.values());
        mOverlay.clearTileCache();
    }
    // End Map function

    //Start Firebase method
    private void initFireBase() {
        // setup markers
        this.markers = new HashMap<>();
        //this.postDetails =  new HashMap<>();
        this.postTypes = new HashMap<>();
    }

    //End Firebase method

    //Start Geofire override method

    @Override
    public void onKeyEntered(String key, GeoLocation location, PostDetail postDetail) {
        // Add a new marker to the map and find the marker
        if(flag_activity_choose_present.equals(ConstantUtils.LOCATION_PREFIX))
            addpostToCluster(key,location,postDetail.getLocation_name());
        else
            addpostToCluster(key,location,null);
        String dateTime = DateUtils.getTimeFromCurrentTime(key.split("-")[0],getApplicationContext());// 4 is -;
        //postDetail.setDateTime(dateTime);
        //postDetails.put(key,postDetail);
    }

    /**
     *
     * @param mkey
     * @param location
     */
    private void addpostToCluster(String mkey, final GeoLocation location,String locationName) {
        try {
            int index = Integer.parseInt((mkey.split("-")[1]).substring(3,4));/// timeRef-TypeID
            final LatLng latlon = new LatLng(location.latitude,location.longitude);
            PostType postType = new PostType();
            postType.setSubPostType(flag_activity_choose_present);
            postType.setPostTypeId(mkey);     // to call to traffic post when user click to marker
            postType.setLatLng(latlon);
            postType.setLocationName(locationName);
            String dateTime = DateUtils.getTimeFromCurrentTime(mkey.split("-")[0],getApplicationContext());// 4 is -;
            postType.setDateTime(dateTime); // from 4-15 is date time to second;
            postType.setPostTypeName(postType.getPostTypeName(index,getApplicationContext()));
            postType.setPostTypeDrawable(postType.getPostTypeDrawable(index));
            if(!postTypes.containsKey(mkey)) {
                postTypes.put(mkey, postType);
                postClusterManager.addItem(postType);
                postClusterManager.cluster();
            }
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }

    }

    @Override
    public void onKeyExited(String key) {
        // Remove any old Trafficpost with its marker
        PostType postType = this.postTypes.get(key);
        if(postType!= null)
        {
            postTypes.remove(key);
            postClusterManager.removeItem(postType);
        }
    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {
        // Move the marker
        Marker marker = this.markers.get(key);
        if (marker != null) {
            this.animateMarkerTo(marker, location.latitude, location.longitude);
        }
    }

    @Override
    public void onGeoQueryReady() {
        postClusterManager.cluster();
    }

    @Override
    public void onGeoQueryError(DatabaseError error) {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.dialog_error_title))
                .setMessage(getResources().getString(R.string.geofire_query_error) + error.getMessage())
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    //End Geofire override method

    // Animation handler for old APIs without animation support
    private void animateMarkerTo(final Marker marker, final double lat, final double lng) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long DURATION_MS = 3000;
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final LatLng startPosition = marker.getPosition();
        handler.post(new Runnable() {
            @Override
            public void run() {
                float elapsed = SystemClock.uptimeMillis() - start;
                float t = elapsed/DURATION_MS;
                float v = interpolator.getInterpolation(t);

                double currentLat = (lat - startPosition.latitude) * v + startPosition.latitude;
                double currentLng = (lng - startPosition.longitude) * v + startPosition.longitude;
                marker.setPosition(new LatLng(currentLat, currentLng));

                // if animation is not finished yet, repeat
                if (t < 1) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    // Override for cluster marker post

    @Override
    public boolean onClusterClick(Cluster<PostType> cluster) {
        //TODO: change zoom level when publish
        if(mMap.getCameraPosition().zoom<=19) {

            // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
            // inside of bounds, then animate to center of the bounds.

            // Create the builder to collect all essential cluster items for the bounds.
            LatLngBounds.Builder builder = LatLngBounds.builder();
            for (ClusterItem item : cluster.getItems()) {
                builder.include(item.getPosition());
            }
            // Get the LatLngBounds
            final LatLngBounds bounds = builder.build();
            // Animate camera to the bounds
            try {
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 19));
                CameraPosition cameraPosition1 = new CameraPosition.Builder()
                        .tilt(70)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            if(!flag_activity_choose_present.equals(ConstantUtils.LOCATION_PREFIX))
            {
                try {
                    Iterator<PostType> ite = cluster.getItems().iterator();
                    //postDetails = new HashMap<>();
                    //List<PostDetail> lstPostD = new ArrayList<>();
                    TreeMap<String,String> treeMap = new TreeMap<>();
                    while(ite.hasNext()){
                        PostType postType = ite.next();
                        //setPostDetailByKey(postType,cluster.getItems().size());
                        treeMap.put(postType.getPostTypeId(),postType.getPostTypeId());
                        //lstPostD.add(postDetails.get(postType.getPostTypeId()));
                        //setPostDetailByKeyFromFirebaseData(postType,cluster.getItems().size());
                    }

                    showListPostDetail(new ArrayList<>(treeMap.values()));
                }catch (Exception e){
                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                }
            }
        }
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<PostType> cluster) {

    }

    @Override
    public boolean onClusterItemClick(PostType item) {
        //showPostDetailToActivity(item.getTrafficTypeId());
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(PostType item) {
        //postDetails = new HashMap<>();
        //setPostDetailByKey(item,1);
        if(flag_activity_choose_present.equals(ConstantUtils.LOCATION_PREFIX))
        {
            Intent intent = new Intent(MainActivity.this,LoactionPageActivity.class);
            intent.putExtra(ConstantUtils.LOCATION_ID,item.getPostTypeId());
            startActivity(intent);
        }
        else{
            ArrayList<String>lstPostID = new ArrayList<String>();
            lstPostID.add(item.getPostTypeId());
            showListPostDetail(lstPostID);
        }

        //setPostDetailByKeyFromFirebaseData(item,1);
    }

    // Function;
    //TODO should change this function to getObjectPostByMarker
    /**
     * This function aim to get the traffic post form firebase with the locationId is also PostId too.
     */
    private void setAddNewTrafficsPostLayoutInOut() {
        if(addNewTrafficsPost_layout.getVisibility()==View.VISIBLE){
            Animation animation  = AnimationUtils.buildScaleAnimation(1,0,1,0,500);
            addNewTrafficsPost_layout.setAnimation(animation);
            addNewTrafficsPost_layout.setVisibility(View.GONE);
            main_fbtn_addNewPost.setIconResource(R.drawable.addnew);
            main_fbtn_addNewPost.setTag("0");
        }
        if(postedFragmentContainer.getVisibility() == View.VISIBLE) {
            footer_layout.setVisibility(View.VISIBLE);
            postedFragmentContainer.setVisibility(View.GONE);
        }
        mFlag_isOpenFragment = false;
    }

    private void animateCameraToPosition(LatLng currentLocation, int zoom, float degreee, int tilt) {
        CameraPosition cameraPosition1 = new CameraPosition.Builder()
                .target(currentLocation)      // Sets the center of the map to Mountain View
                .zoom(zoom)                   // Sets the zoom
                .bearing(degreee)
                .tilt(tilt)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));
    }

    /**
     *
     * @param hashMap
     */
    private void directionToDestination(HashMap<String,Object> hashMap){
        //String key = hashMap.keySet().toString();
        String key=ConstantUtils.DIRECT_TO_PLACES;
        for (String key1 : hashMap.keySet()) {
            key = key1;
        }
        Marker cMarker = mMap.addMarker(new MarkerOptions()
                .position(mLastKnownLocation)
                .title(getResources().getString(R.string.map_current_position))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.direct)));
        markers.put(ConstantUtils.CURRENT_LOCATION,cMarker);
        if(key==ConstantUtils.USER_REF_USER_PLACES_HOME){
            Marker marker = mMap.addMarker(new MarkerOptions().position((LatLng)hashMap.get(key))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.home)));
            markers.put(ConstantUtils.DESTINATION, marker);
            directToDestination((LatLng)hashMap.get(key));
        }else if(key==ConstantUtils.USER_REF_USER_PLACES_WORK){
            Marker marker = mMap.addMarker(new MarkerOptions().position((LatLng)hashMap.get(key))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.work_place)));
            markers.put(ConstantUtils.DESTINATION, marker);
            directToDestination((LatLng)hashMap.get(key));
        }
        else if(key == ConstantUtils.DIRECT_TO_FRIENDS){
            Friends friends = (Friends)hashMap.get(key);
            Marker marker = mMap.addMarker(new MarkerOptions().position(friends.getLatLng())
                    .icon(BitmapDescriptorFactory.fromBitmap(friends.getUserMarker(getApplicationContext()))));
            markers.put(ConstantUtils.DESTINATION, marker);
            directToDestination(friends.getLatLng());
        }
    }

    /**
     *
     * @param destination
     */
    private void directToDestination(LatLng destination) {
        if(destination == null) {
            if(markers.size() == 0){
                startActivity(new Intent(MainActivity.this,SearchLocationActivity.class));
            }
        }else{
            // show the weather of the destination;
            //showTheWeather(destination);
            try {
                mFlag_isMoving = true;
                DirectionsUtil directionsUtil = new DirectionsUtil(mMap);
                // Drawing polyline in the Google Map for the i-th route
                //move map camera
                directionsUtil.getDirection(mLastKnownLocation,destination);
                LatLngBounds.Builder builder = LatLngBounds.builder();
                builder.include(mLastKnownLocation);
                builder.include(destination);
                LatLng centerLatlon = new LatLng((mLastKnownLocation.latitude+destination.latitude)/2,(mLastKnownLocation.longitude+destination.longitude)/2);
                final LatLngBounds bounds = builder.build();
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 15));
                float degreee1 = (float) SphericalUtil.computeHeading(mLastKnownLocation,destination);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(centerLatlon)      // Sets the center of the map to Mountain View
                        .zoom(14)                   // Sets the zoom
                        .bearing(degreee1)
                        .tilt(80)                   // Sets the tilt of the camera to 80 degrees
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                main_fbtn_direction.setTag("isDirect");
                main_fbtn_direction.setIconResource(R.drawable.cancel_24);
            }
            catch (Exception e)
            {
                com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_network_connection),Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     *
     */
    private void cancelDirection()
    {
        mMap.clear();
        mFlag_isMoving = false;
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(mLastKnownLocation)      // Sets the center of the map to Mountain View
                .bearing(0.0f)
                .zoom(15)                   // Sets the zoom
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        markers.remove(ConstantUtils.CURRENT_LOCATION);
        markers.remove(ConstantUtils.DESTINATION);
        main_fbtn_direction.setTag(null);
        main_fbtn_direction.setIconResource(R.drawable.go);
    }



    /**
     *
     * @param listPostId
     */
    private void showListPostDetail(ArrayList<String> listPostId)
    {
        try {
            //footer_layout.setVisibility(View.GONE);
            String dbCollection = com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getFireStoreDBRefForPostDetail(flag_activity_choose_present);
            FragmentManager fragmentManager1 = getSupportFragmentManager();

            android.support.v4.app.Fragment fragment = PostDetailFragment.newInstance(getApplicationContext(),listPostId,dbCollection);
            fragmentManager1.beginTransaction()
                    .replace(R.id.postedFragmentContainer, fragment)
                    .commit();
            android.util.DisplayMetrics displayMetrics = new android.util.DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int heigh = displayMetrics.heightPixels;
            Animation animation = AnimationUtils.buildTranslateAnimation(0.0f
                    ,0.0f,0.0f
                    ,-(float)heigh,250);
            postedFragmentContainer.setAnimation(animation);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    mFlag_isOpenFragment = true;
                    postedFragmentContainer.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }

    }

    private void addNewFragmentToActivity() {
        try {
            if(mLastKnownLocation != null) {
                //Fragment add new
                fragmentManager = getSupportFragmentManager();
                mFlag_isOpenFragment = true;
                if(flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[0])) // If ibtn_traffic is active;
                {
                    fragmentManager.beginTransaction()
                            .replace(R.id.addNewTrafficsPost_layout, PostAddNewFragment
                                    .newInstance(MainActivity.this, mLastKnownLocation, PreferencesUtils.getString(SharedPrefenceKey.USER_TOKEN_ID.name(),""),ConstantUtils.GINS_MAIN_TYPE[0]))
                            .commit();
                }
                else if (flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[1])) // If ibtn_traffic is active;
                {
                    fragmentManager.beginTransaction()
                            .replace(R.id.addNewTrafficsPost_layout, PostAddNewFragment
                                    .newInstance(MainActivity.this, mLastKnownLocation, PreferencesUtils.getString(SharedPrefenceKey.USER_TOKEN_ID.name(),""),ConstantUtils.GINS_MAIN_TYPE[1]))
                            .commit();
                }
                //TODO add more
                else if (flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[2])) // Location
                {
                    fragmentManager.beginTransaction()
                            .replace(R.id.addNewTrafficsPost_layout, LocationAddNewFragment
                                    .newInstance(MainActivity.this, mLastKnownLocation,ConstantUtils.GINS_MAIN_TYPE[2]))
                            .commit();
                }
                else if (flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[3]))
                {
                    fragmentManager.beginTransaction()
                            .replace(R.id.addNewTrafficsPost_layout, PostAddNewFragment
                                    .newInstance(MainActivity.this, mLastKnownLocation, PreferencesUtils.getString(SharedPrefenceKey.USER_TOKEN_ID.name(),""),ConstantUtils.GINS_MAIN_TYPE[3]))
                            .commit();
                }
                else if (flag_activity_choose_present.equals(ConstantUtils.GINS_MAIN_TYPE[4]))
                {
                    /*fragmentManager.beginTransaction()
                            .replace(R.id.addNewTrafficsPost_layout, PostAddNewFragment
                                    .newInstance(MainActivity.this, mLastKnownLocation, PreferencesUtils.getString(SharedPrefenceKey.USER_TOKEN_ID.name(),""),ConstantUtils.GINS_MAIN_TYPE[4]))
                            .commit();*/
                }
                Animation animation  = AnimationUtils.buildScaleAnimation(0,1,0,1,300);
                addNewTrafficsPost_layout.setAnimation(animation);
                addNewTrafficsPost_layout.setVisibility(View.VISIBLE);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLastKnownLocation, ConstantUtils.ZOOM_LEVEL));
            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.map_error_cannot_find_location),Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }



    /**
     * Function: Weather
    */
    public void showTheWeather(LatLng latLng){
        com.ginsre.geo.hoang.dang.ginsre.weather.ShowCurrentWeathers weather = new com.ginsre.geo.hoang.dang.ginsre.weather.ShowCurrentWeathers(getApplicationContext(),mMap,latLng);
        weather.execute(new String[]{"weather?lat="+latLng.latitude+"&lon="+latLng.longitude+"&units=metric","showCurrentWeather"});
    }

    @Override
    public void onDataChanged(IssueKey key, Object object) {
        try {
            if(IssueKey.CLOSE_POST_DETAIL == key)
            {
                setAddNewTrafficsPostLayoutInOut();
                //postedFragmentContainer.setVisibility(View.GONE);
            }
            else if(IssueKey.DIRECTTION_TO_ADDRESS == key)
            {
                //LatLng destination = (LatLng)object;
                HashMap<String,Object> hashMap = (HashMap<String, Object>)object;
                //hashMap.put(ConstantUtils.DIRECT_TO_PLACES,destination);
                directionToDestination(hashMap);
            }
            else if(IssueKey.CHANGE_GEO_QUERY_NOTI == key)
            {
                String gintype = (String)object;
                refreshData();
                if(gintype.equalsIgnoreCase(ConstantUtils.FRIEND_PREFIX)){
                    //queryForFriendCurrentLocation();
                }else{
                    if(gintype.equals(ConstantUtils.WEATHER_PREFIX)){
                        refreshForWeather();
                    }
                    setGeoQueryToDataType(gintype);
                    //this.geoQuery.addGeoQueryEventListener(this);
                    settingMapStyle(gintype);
                }
            }else if(IssueKey.CHANGE_GEO_QUERY_DATE == key){
                refreshData();
                if((boolean)main_fbtnDateTimeStart.getTag()) {
                    //dateTimeUserSelectionStart = ((String) object).replace("/","");
                    main_fbtnDateTimeStart.setText((String)object);
                }
                if((boolean)main_txvDateTime.getTag()) {
                    //dateTimeUserSelectionEnd = ((String) object).replace("/", "");
                    main_txvDateTime.setText((String)object);
                }
                setGeoQueryToDataType(flag_activity_choose_present);
                //this.geoQuery.addGeoQueryEventListener(this);
            }
        }catch (Exception e)
        {
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    private void setGeoQueryToDataType(String gintype) {

        DatabaseReference mref = FirebaseDatabase.getInstance().getReference(
                com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getDatabaseRefForGinsType(gintype));
        this.geoFire  = new GeoFire(mref);
        // radius in 3 km
        String dateTimeUserSelectionStart;
        if(gintype.equals(ConstantUtils.LOCATION_PREFIX))
            dateTimeUserSelectionStart = "hck"; // ~ 171219
        else
            dateTimeUserSelectionStart = GinFunctions.getDateTimeCode(main_fbtnDateTimeStart.getText().toString());
        if(gintype.equals(ConstantUtils.SOCIAL_PREFIX))
            dateTimeUserSelectionStart = DateUtils.getDateTimeBack(3);

        String dateTimeUserSelectionEnd = DateUtils.getDateTimeForGeoHashQuery();
        this.geoQuery = this.geoFire.queryAtLocationWithTime(INITIAL_CENTER // center location
                , ConstantUtils.GEOFIRE_RADIUS_QUERY                       // Radius
                ,dateTimeUserSelectionStart                                    // start time
                ,dateTimeUserSelectionEnd);   // end time
        geoQuery.addGeoQueryEventListener(this);
    }

    private void queryForFriendCurrentLocation() {
        DatabaseReference mref = FirebaseDatabase.getInstance().getReference(
                com.ginsre.geo.hoang.dang.ginsre.utils.GinFunctions.getDatabaseRefForGinsType(ConstantUtils.FRIEND_PREFIX));
        this.geoFire  = new GeoFire(mref);
        String dateTimeUserSelectionEnd = DateUtils.getDateTimeForUserTempGeo(24);
        String dateTimeUserSelectionStart = DateUtils.getDateTimeForUserTempGeo(0);
        this.geoQuery = this.geoFire.queryAtLocationWithTime(INITIAL_CENTER // center location
                , ConstantUtils.GEOFIRE_RADIUS_QUERY                       // Radius
                ,dateTimeUserSelectionStart                                    // start time
                ,dateTimeUserSelectionEnd);
        final HashMap<String,GeoLocation> hashMap = new HashMap<>();
        this.geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {

            @Override
            public void onKeyEntered(String key, GeoLocation location, PostDetail postDetail) {
                hashMap.put(key,location);
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {
                hashMap.put("0",null);
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                com.google.firebase.crash.FirebaseCrash.log(error.getMessage());
            }
        });
    }


    private void refreshData() {
        this.geoQuery.removeAllListeners();
        mMap.clear();
        removeAllmarkers();
        postClusterManager.clearItems();
        postTypes = new HashMap<>();
        //postDetails = new HashMap<>();
    }

    private class PostRenderer extends DefaultClusterRenderer<PostType> {

        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;

        public PostRenderer() {
            super(getApplicationContext(), mMap, postClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_post, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.multi_post_imv);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getApplicationContext().getResources().getDimension(R.dimen.post_image_type);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getApplicationContext().getResources().getDimension(R.dimen.post_image_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(PostType postType, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
            //ad
            Drawable drawable = postType.getPostTypeMarker(getApplicationContext());
            mImageView.setImageDrawable(drawable);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                    .title(flag_activity_choose_present.equals(ConstantUtils.LOCATION_PREFIX)?postType.getLocationName():postType.getPostTypeName())
                    .snippet(postType.getDateTime());
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<PostType> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;
            Drawable drawable;
            for (PostType p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                drawable = p.getPostTypeMarker(getApplicationContext());
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);
            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }


    //
    private void refreshForWeather() {
        try {
            DatabaseReference mdatabase = FirebaseDatabase.getInstance().getReference();
            mdatabase.child(ConstantUtils.WEATHER).orderByChild("isActive").equalTo(true).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                            com.ginsre.geo.hoang.dang.ginsre.model.weather.Storm storm = snapshot.getValue(com.ginsre.geo.hoang.dang.ginsre.model.weather.Storm.class);
                            storm.setS_Name(snapshot.getKey());
                            showStormToMap(storm);
                        }
                    }catch (Exception e){
                        com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    com.google.firebase.crash.FirebaseCrash.log(databaseError.getMessage());
                }
            });
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }
    }

    void addStormCircleEvent(){
        mMap.setOnCircleClickListener(new GoogleMap.OnCircleClickListener() {
            @Override
            public void onCircleClick(Circle circle) {
                try {
                    String d = (String)circle.getTag();
                    if(d.equals("circle1")){
                        showTheWeather(circle.getCenter());
                    }else if(d.split("-")[0].equals("circle3")){
                        showAlert(String.valueOf(Double.parseDouble(d.split("-")[1])/1000).substring(0,4));
                    } else{
                        double R1 =  Double.parseDouble(d.split("-")[0]);
                        double R2 = Double.parseDouble(d.split("-")[1]);
                        LatLng latLng = new LatLng(circle.getCenter().latitude-((R1+R2/2)/1000)/111,circle.getCenter().longitude);
                        showTheWeather(latLng);
                    }
                }catch (Exception e){
                    com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
                }
            }
        });
    }

    private void showAlert(String intensity) {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.tembin_12later_title))
                .setMessage(getResources().getString(R.string.tembin_12later_intensity).replace("xx",intensity))
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }


    private void showStormToMap(com.ginsre.geo.hoang.dang.ginsre.model.weather.Storm storm ) {

        int i = 0;
        ArrayList<LatLng> latLngs = new ArrayList<>();
        for(Forecast forecast:storm.getForecast()){
            LatLng latLng = new LatLng(forecast.getL().get(0), forecast.getL().get(1));
            int R1 = forecast.getArea()*1000;
            com.google.android.gms.maps.model.Circle circle = mMap.addCircle(new com.google.android.gms.maps.model.CircleOptions()
                            .center(latLng)
                            .radius(R1)
                            .strokeWidth(i==0?4:4)
                            .strokeColor(getResources().getColor(i==0?R.color.material_red_800:R.color.material_blue_400))
                            .fillColor(i==0?Color.argb(128, 250, 0, 0):Color.argb(128, 0, 0, 255))
                            .clickable(true));
            IconGenerator iconFactory = new IconGenerator(this);
            addIcon(iconFactory, forecast.getTime()+"\n"+intensityToString(forecast.getInten()),latLng);
            circle.setTag("circle"+i);
            i++;
            latLngs.add(latLng);
        }
        mMap.addPolyline(new PolylineOptions()
                .addAll(latLngs)
                .width(3)
                .color(R.color.material_blue_400)
                .geodesic(true));
        animateCameraToPosition(latLngs.get(1),6,0,60);
        addStormCircleEvent();
    }

    private String intensityToString(int intent){
        return getResources().getString(R.string.storm_level).replace("xx",GinFunctions.getStormLevel((int)(intent*1.852)))+"\n"+String.valueOf(intent*1.852).substring(0,5)+" (Km/h)";
    }

    private void addIcon(IconGenerator iconFactory, CharSequence text, LatLng position) {
        MarkerOptions markerOptions = new MarkerOptions().
                icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(text))).
                position(position).
                anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

        mMap.addMarker(markerOptions);
    }


}
