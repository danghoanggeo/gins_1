package com.ginsre.geo.hoang.dang.ginsre.utils;

import android.content.res.Resources;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by hoang on 9/26/2017.
 */

public class FirebaseUtils {
    private static DatabaseReference mDatabase;
    private static StorageReference mStorageRef;
    private static FirebaseAuth mAuth;
    private static FirebaseUser currentUser;
    private static Resources mRes;

    public FirebaseUtils(){}
    public DatabaseReference getDatabaseRef()
    {
        return  FirebaseDatabase.getInstance().getReference();
    }
    public StorageReference getmStorageRef()
    {
        return FirebaseStorage.getInstance().getReference();
    }
    public FirebaseUser getCurrentUser()
    {
        mAuth = FirebaseAuth.getInstance();
        return mAuth.getCurrentUser();
    }
}
