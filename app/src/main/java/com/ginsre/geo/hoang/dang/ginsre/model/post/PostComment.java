package com.ginsre.geo.hoang.dang.ginsre.model.post;

/**
 * Created by hoang on 9/19/2017.
 */

public class PostComment {
    private String comment_userId;
    private String comment_dateTime;
    private String comment_content;

    public PostComment(){}

    public PostComment( String comment_content) {
        this.comment_content = comment_content;
    }

    public String getComment_userId() {
        return comment_userId;
    }

    public void setComment_userId(String comment_userId) {
        this.comment_userId = comment_userId;
    }

    public String getComment_dateTime() {
        return comment_dateTime;
    }

    public void setComment_dateTime(String comment_dateTime) {
        this.comment_dateTime = comment_dateTime;
    }

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }
}
