package com.ginsre.geo.hoang.dang.ginsre.utils;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Created by hoang on 9/18/2017.
 */

public class MapUtils {
    private static HashMap<String,String> longitudeZones;
    private static HashMap<String,String> latitudeZones;

    public static String getZonesByLocation(LatLng latLng)
    {
        String result = "";
        if(latLng != null) {
            initLatLongZones();
            if (latLng.longitude <= 180) {
                result += String.valueOf(((int) latLng.longitude / 6) + 31);
            } else {
                result += String.valueOf(((int) (latLng.longitude+180) / 6) +1);
            }
            if (latLng.latitude >= 0) {
                result += latitudeZones.get(String.valueOf((int) latLng.latitude / 8));
            } else {
                result += latitudeZones.get(String.valueOf(((int) latLng.latitude / 8) - 1));
            }
        }
        return result;
    }

    private static void initLatLongZones()
    {
        latitudeZones = new HashMap<String,String>();
        latitudeZones.put("0","N");latitudeZones.put("1","P");
        latitudeZones.put("2","Q");latitudeZones.put("3","R");
        latitudeZones.put("4","S");latitudeZones.put("5","T");
        latitudeZones.put("6","U");latitudeZones.put("7","V");
        latitudeZones.put("8","W");latitudeZones.put("9","X");
        latitudeZones.put("10","Y");latitudeZones.put("11","Z");

        latitudeZones.put("-1","M");latitudeZones.put("-2","L");
        latitudeZones.put("-3","K");latitudeZones.put("-4","J");
        latitudeZones.put("-5","H");latitudeZones.put("-6","G");
        latitudeZones.put("-7","F");latitudeZones.put("-8","E");
        latitudeZones.put("-9","D");latitudeZones.put("-10","C");
        latitudeZones.put("-11","B");latitudeZones.put("-12","Y");
    }

    // Distance between 2 point
    public static double getDistanceBetween2Place(LatLng latLng1, LatLng latLng2)
    {
        double eRadius = 6378.137;
        double latDelta = dd2rad(latLng2.latitude - latLng1.latitude);
        double lonDelta = dd2rad(latLng2.longitude - latLng1.longitude);
        double a = (Math.sin(latDelta / 2) * Math.sin(latDelta / 2)) +
                (Math.cos(dd2rad(latLng1.latitude)) * Math.cos(dd2rad(latLng2.latitude)) *
                        Math.sin(lonDelta / 2) * Math.sin(lonDelta / 2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return eRadius * c;
    }

    private static double dd2rad(double dd)
    {
        return dd*(Math.PI)/180;
    }
}
