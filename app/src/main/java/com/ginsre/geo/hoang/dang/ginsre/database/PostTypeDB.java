package com.ginsre.geo.hoang.dang.ginsre.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ginsre.geo.hoang.dang.ginsre.model.post.IPostType;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoang on 10/17/2017.
 */

public class PostTypeDB extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "GINS";
    public PostTypeDB(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "Create table "+IPostType.TB_NAME+"("+IPostType.POST_ID+" text primary key," +
                IPostType.POST_TYPE_NAME+" text,"+IPostType.POST_TYPE_IMAGE+" text,"+IPostType.POST_TYPE_IS_ACTIVE+" text,"+IPostType.SUB_POST_TYPE_TYPE+" text)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "Drop table if exists "+IPostType.TB_NAME;
        db.execSQL(sql);
        onCreate(db);
    }

    public void insert(PostType pt)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues content  = new ContentValues();
        content.put(IPostType.POST_ID,pt.getPostTypeId());
        content.put(IPostType.POST_TYPE_NAME,pt.getPostTypeName());
        content.put(IPostType.POST_TYPE_IMAGE,pt.getPostTypeDrawable());
        content.put(IPostType.POST_TYPE_IS_ACTIVE,pt.getPostTypeIsActive());
        content.put(IPostType.SUB_POST_TYPE_TYPE,pt.getSubPostType());
        db.insert(IPostType.TB_NAME,null,content);
        db.close();
    }

    public List<PostType> getAllListPostTypeBySubType(String subtype)
    {
        List<PostType> lstPostType = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        db.beginTransaction();
        Cursor cursor = db.query(IPostType.TB_NAME,null,IPostType.SUB_POST_TYPE_TYPE+"=?",new String[]{subtype+""},null,null,null);
        while (cursor.moveToNext())
        {
            PostType postType = new PostType();
            try {
                postType.setPostTypeId(cursor.getString(0));
                postType.setPostTypeName(cursor.getString(1));
                postType.setPostTypeDrawable(cursor.getString(2));
                postType.setPostTypeIsActive(cursor.getString(3));
                postType.setSubPostType(cursor.getString(4));
                lstPostType.add(postType);
            }catch (Exception e)
            {

            }
        }
        db.close();
        cursor.close();
        return lstPostType;
    }
}
