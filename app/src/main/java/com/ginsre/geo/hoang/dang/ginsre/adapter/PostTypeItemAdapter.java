package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.common.IssueKey;
import com.ginsre.geo.hoang.dang.ginsre.model.post.PostType;
import com.ginsre.geo.hoang.dang.ginsre.observer.DataChangeNotification;
import com.ginsre.geo.hoang.dang.ginsre.utils.ConstantUtils;

import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by hoang on 9/26/2017.
 */

public class PostTypeItemAdapter extends BaseAdapter {

    private final Context context;
    private final List<PostType> lstPostType;
    private final String mpostSubType;
    public PostTypeItemAdapter(Context context, String postSubType)
    {
        this.context = context;
        //TrafficTypeDB trafficTypeDB = TrafficTypeDB.getInstance(context);
        lstPostType = new PostType().initDataTrafficType(context,postSubType);
        mpostSubType = postSubType;
    }

    @Override
    public int getCount() {
        return lstPostType.size();
    }

    @Override
    public Object getItem(int position) {
        return lstPostType.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        PostType postType = lstPostType.get(position);
        if(convertView == null)
        {
            final LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_traffic_post_add_new,null);
        }
        final FancyButton fancyButton = (FancyButton)convertView.findViewById(R.id.item_traffic_post_add_newfbtn);
        fancyButton.setIconResource(postType.getPostTypeImage(context));
        fancyButton.setText(postType.getPostTypeName());
        fancyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((mpostSubType.substring(0,3)).equals(ConstantUtils.LOCATION_PREFIX))
                {
                    DataChangeNotification.getInstance().notifyChange(IssueKey.ON_ADD_NEW_LOCATION_POST_SELECT_TYPE,lstPostType.get(position));
                }else{
                    DataChangeNotification.getInstance().notifyChange(IssueKey.ON_ADD_NEW_TRAFFIC_POST_SELECT_TYPE,lstPostType.get(position));
                }

            }
        });
        return convertView;
    }

}
