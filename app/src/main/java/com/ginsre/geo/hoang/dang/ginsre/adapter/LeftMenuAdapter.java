package com.ginsre.geo.hoang.dang.ginsre.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ginsre.geo.hoang.dang.ginsre.R;

import java.util.ArrayList;

/**
 * Created by hoang on 1/2/2018.
 */

public class LeftMenuAdapter extends BaseAdapter {

    ArrayList<String> list_menu;
    private LayoutInflater mInflater;
    Context mcontext;
    public LeftMenuAdapter(Context context) {
        mcontext =  mcontext;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        list_menu = getListMenu();
    }
    @Override
    public int getCount() {
        return list_menu.size();
    }

    @Override
    public Object getItem(int i) {
        return list_menu.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(
                    R.layout.item_list_view_navigator_left, viewGroup,
                    false);
            holder = new ViewHolder();
            holder.icon = (TextView) convertView
                    .findViewById(R.id.number_of_people);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.icon.setText("Menu "+i);
        holder.title.setText("Menu "+i);

        return convertView;
    }

    private static class ViewHolder {
        public TextView icon;
        public/* Roboto */TextView title;
    }


    private ArrayList<String> getListMenu(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(mcontext.getResources().getString(R.string.menu_home_address));
        arrayList.add(mcontext.getResources().getString(R.string.menu_my_place));
        arrayList.add(mcontext.getResources().getString(R.string.menu_favorites_places));
        arrayList.add(mcontext.getResources().getString(R.string.menu_friends));
        arrayList.add(mcontext.getResources().getString(R.string.menu_groups));
        arrayList.add(mcontext.getResources().getString(R.string.menu_settings));
        return  arrayList;
    }
}
