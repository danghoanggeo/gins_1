package com.ginsre.geo.hoang.dang.ginsre.activity;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.EditText;
import android.widget.ImageView;

import com.ginsre.geo.hoang.dang.ginsre.R;
import com.ginsre.geo.hoang.dang.ginsre.font.MaterialDesignIconsTextView;
import com.ginsre.geo.hoang.dang.ginsre.fragment.FriendsFragment;
import com.ginsre.geo.hoang.dang.ginsre.observer.OnDataChangeObserver;
import com.ginsre.geo.hoang.dang.ginsre.view.PagerSlidingTabStrip;

import java.util.ArrayList;

public class FriendsActivity extends AppCompatActivity implements OnDataChangeObserver{

    FriendsActivity.MyPagerAdapter adapter;
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    public ImageView friendsNet_ibtnHome;
    public EditText friendsNet_txtSearch;
    public MaterialDesignIconsTextView friendsNet_search, friendsNet_voice_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        intiTabPage();
    }


    private void intiTabPage(){
        try {
            tabs = (PagerSlidingTabStrip) findViewById(R.id.activity_friends_tabs);
            pager = (ViewPager) findViewById(R.id.activity_friends_view_pages);
            adapter = new FriendsActivity.MyPagerAdapter(getSupportFragmentManager());
            pager.setAdapter(adapter);
            tabs.setViewPager(pager);
            final int pageMargin = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 3, getResources()
                            .getDisplayMetrics());
            pager.setPageMargin(pageMargin);

            pager.setCurrentItem(1);

            tabs.setOnTabReselectedListener(new PagerSlidingTabStrip.OnTabReselectedListener() {
                @Override
                public void onTabReselected(int position) {
                    // Toast.makeText(LoactionPageActivity.this, "Tab reselected: " + position, Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            com.google.firebase.crash.FirebaseCrash.log(e.getMessage());
        }

    }

    private void addEvent() {

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        //DataChangeNotification.getInstance().notifyChange(IssueKey.CLOSE_POST_COMMENT,"closeComment");
    }

    @Override
    public void onDataChanged(com.ginsre.geo.hoang.dang.ginsre.common.IssueKey key, Object object) {
        if(key.equals(com.ginsre.geo.hoang.dang.ginsre.common.IssueKey.CLOSE_POST_DETAIL)){
            finish();
        }
    }

    /**
     *
     */
    public class MyPagerAdapter extends FragmentPagerAdapter {
        private final ArrayList<String> tabNames = new ArrayList<String>() {
            {
                add(getResources().getString(R.string.add_new_friend));
                add(getResources().getString(R.string.my_friends));
                add(getResources().getString(R.string.friend_status_2));
            }
        };

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabNames.get(position);
        }

        @Override
        public int getCount() {
            return tabNames.size();
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            if (position == 0) {
                return FriendsFragment.newInstance(FriendsActivity.this,1);
            } else if(position == 1) {
                return FriendsFragment.newInstance(FriendsActivity.this,1);
            }else {
                return FriendsFragment.newInstance(FriendsActivity.this,2);
            }
        }
    }
}
